<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Recapitulation SHE PT. Semen Indonesia, Tbk.</title>
</head>
<body>
<style>
    .modal{
        max-height: calc(150vh - 250px);
        overflow-y: auto;
    }
</style>
<div id="result-form-konten"></div>
    <form onsubmit="return false;" id="form-konten" class='form-horizontal'>
        <table width="100%">
            <tbody>
                <tr>
                    <td class="text-center">
                        <img src="{{asset('public/assets/img')}}/logosemen.png" class="img-responsive" alt="logo" width="80" style="padding-top:20px">
                    </td>
                </tr>
                <tr>
                    <td class="text-center">
                        <h6 class="modal-title" id="title">Form Recapitulation Safety Officer</h6>
                        <h4 class="modal-title" id="title"></h4><small class="font-bold">PT. Semen Indonesia</small>
                    </td>
                </tr>
            </tbody>
        </table>
        <hr>
        <div class="modal-body">
            <div class="row go_form">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Foto temuan :</label>
                        <div class="input-group">
                            <div class="row" style="margin-top:10px">
                                <div class="col s6">
                                    <!-- <img src="$foto_bef" id="showImage" style="width:300px;height:200px;float:left;" /> -->
                                    <img src="{{asset('public/images')}}/{{'preview-icon.png'}}" id="showImage" style="width:300px;height:200px;float:left;" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Foto follow up :</label>
                        <div class="input-group">
                            <div class="row" style="margin-top:10px">
                                <div class="col s6">
                                    <!-- <img src="$foto_aft" id="showImage" style="width:300px;height:200px;float:left;" /> -->
                                    <img src="{{asset('public/images')}}/{{'preview-icon.png'}}" id="showImage" style="width:300px;height:200px;float:left;" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div style="border-width:1px; border-top:3px; border-style:solid; border-color:red; background-color:red">
                <strong style="background-color:red; margin-left:5px; color:white">Keterangan Unsafe Detail</strong>
            </div>
            <div class="modal-body" style="border-width:1px; border-style:solid; border-color:red">
                <div class="row go_form">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Commitment Date :</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fas fa-calendar"></i>
                                    </div>
                                </div>
                                <input type="date" class="form-control daterange-cus" name="comitment_date" value="{{ $data->comitment_date}}" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Status :</label>
                            <div class="input-group">
                                <select class="form-control" name="status" disabled>
                                    <option value="{{$data->status}}">{{$data->status}}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Clock Incident :</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fas fa-clock"></i>
                                    </div>
                                </div>
                                <input type="time" class="form-control daterange-cus" name="o_clock_incident" disabled value="{{ $data->o_clock_incident}}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row go_form">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>SMIG Information :</label>
                                    <div class="input-group">
                                        <select class="form-control" name="is_smig" disabled>
                                            @if($data->is_smig == 0)
                                                <option value="{{$data->is_smig}}">Non-SMIG</option>
                                            @else
                                                <option value="{{$data->is_smig}}">SMIG</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            @if($data->is_smig == 0)
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <label>Vendor Officer :</label>
                                        <select name="arruk" class="form-control" disabled>
                                        @foreach($data_vendor as $item)
                                            <option value="{{ $item->NAME1 .'|'. $item->id }}" @if(!is_null($data)) @if($item->NAME1 == $data->unit_name) selected="selected" @endif @endif>{{ $item->id}} - {{ $item->NAME1}}</option>
                                            
                                        @endforeach
                                        </select>
                                    </div>
                                </div>
                            @else
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <label>Unit Kerja Officer :</label>
                                        <select name="arruk" class="form-control" disabled>
                                        @foreach($data_uk as $item)
                                            <option value="{{ $item->muk_nama .'|'. $item->muk_kode }}" @if(!is_null($data)) @if($item->muk_nama == $data->unit_name) selected="selected" @endif @endif>{{ $item->muk_kode}} - {{ $item->muk_nama}}</option>
                                            
                                        @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row go_form">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Unsafe Type :</label>
                            <div class="input-group">
                                <select class="form-control" name="unsafe_type" disabled>
                                    <option value="{{$data->unsafe_type}}">{{$data->unsafe_type}}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label>Report Category :</label>
                            <select name="arrreport" class="form-control" disabled>
                                @foreach($data_report as $item)
                                    <option value="{{ $item->DESKRIPSI .'|'. $item->id .'|'. $item->KODE_EN }}" @if(!is_null($data)) @if($item->DESKRIPSI == $data->deskripsi) selected="selected" @endif @endif>{{ $item->KODE}} - {{ $item->DESKRIPSI}}</option>
                                
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row go_form">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Uraian aktifitas :</label>
                                    <input type="text" class="form-control daterange-cus" value="{{ $data->activity_description}}" name="activity_description" disabled>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row go_form">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Potensi terjadinya masalah :</label>
                            <input type="text" class="form-control daterange-cus" value="{{ $data->potential_hazard}}" name="potential_hazard" disabled>
                        </div>
                    </div>
                </div>
                <div class="row go_form">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Penyebab masalah :</label>
                            <input type="text" class="form-control daterange-cus" value="{{ $data->trouble_maker}}" name="trouble_maker" disabled>
                        </div>
                    </div>
                </div>
                <div class="row go_form">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Rekomendasi safety :</label>
                                    <input type="text" class="form-control daterange-cus" value="{{ $data->recomendation}}" name="recomendation" disabled>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Rencana tindak lanjut :</label>
                                    <input type="text" class="form-control daterange-cus" value="{{ $data->tindak_lanjut}}" name="tindak_lanjut" disabled>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            <!-- <button type="submit" class="btn btn-info" data-loading-text="<i class='fa fa-spinner fa-spin'></i>
                &nbsp;<font class='lowercase'></font>"> <i class="fa fa-check"></i> Save</button> -->
        </div>

    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>
</body>
</html>