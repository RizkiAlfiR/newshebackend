@extends('layouts.app')

@section('content')
<title>SHE Recapitulation Safety Officer &mdash; Safety Hygiene Environment System</title>
<section class="section">
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-12">
            <div class="card card-statistic-2">
                <div class="card-icon shadow-primary bg-primary">
                    <i class="fas fa-archive"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Total Close Report Officer</h4>
                    </div>
                    <div class="card-body">
                        {{ $jumlahReportClose }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="nav-wrapper">
                    <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link mb-sm-3 mb-md-0 active" id="tabs-icons-text-1-tab" data-toggle="tab" href="#tabs-icons-text-1" role="tab" aria-controls="tabs-icons-text-1" aria-selected="true"><i class="ni ni-cloud-upload-96 mr-2"></i>Chart Recapitulation</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-2-tab" data-toggle="tab" href="#tabs-icons-text-2" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>Data Recapitulation</a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
                        <div id="container" style="min-width: 310px; height: 400px; max-width: 800px; margin: 0 auto"></div>
                        <br><br><br><br>
                        <div id="container2" style="min-width: 310px; height: 400px; max-width: 800px; margin: 0 auto"></div>
                        <br><br><br><br>
                        <div id="container3" style="min-width: 310px; height: 400px; max-width: 800px; margin: 0 auto"></div>
                        <br><br><br><br>
                    </div>
                    <div class="tab-pane fade" id="tabs-icons-text-2" role="tabpanel" aria-labelledby="tabs-icons-text-2-tab">
                        <div class="card">
                            <div class="card-header">
                                <h4>List All Recapitulation Safety Officer</h4>
                            </div>
                            <!-- <p class="section-lead">
                                <a href="{{ url('/recapitulation/pdfview') }}">
                                    <button type="button" class="btn btn-info">
                                        <i class="fas fa-download"></i>Download PDF
                                    </button>
                                </a>
                            </p> -->
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped" id="table-detail">
                                        <thead>
                                            <tr>
                                                <th class="text-center">
                                                    #
                                                </th>
                                                <th>Unit Description</th>
                                                <th>Activity Description</th>
                                                <th>Potential Hazard</th>
                                                <th>Penyebab</th>
                                                <th>Aktifitas Follow Up</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($data as $p => $item)
                                            <tr>
                                                <td>
                                                    {{ $p+1 }}
                                                </td>
                                                @if($item->unit_code != "undefined")
                                                    <td>{{ $item->unit_code }} - {{ $item->unit_name }}</td>
                                                @else
                                                    <td>{{ $item->unit_name }}</td>
                                                @endif
                                                <td class="align-middle">
                                                    {{ $item->activity_description }} ({{ $item->unsafe_type }})
                                                </td>
                                                <td>
                                                    {{ $item->potential_hazard }}
                                                </td>
                                                <td>{{ $item->trouble_maker }}</td>
                                                <td>
                                                    {{ $item->follow_up }}  (on {{ $item->comitment_date }})
                                                </td>
                                                <td>
                                                    <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                        <button onclick="loadModal(this)" title="" target="/recapitulation/show" data="id={{$item->id}}" type="button" class="btn btn-icon icon-left btn-success">
                                                            <i class="fas fa-eye"></i>
                                                        </button>
                                                        <!-- <a href="{{ url('/recapitulation/pdfview', ['download' => 'pdf']) }}">
                                                            <button onclick="loadModal(this)" title="" target="/recapitulation/pdfview" data="id={{$item->id}}" type="button" class="btn btn-icon icon-left btn-warning">
                                                                <i class="fas fa-download"></i>
                                                            </button>
                                                        </a> -->
                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@section('scripts')
    <script>
        function deleteMaster(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("", "Do you want to remove this data?", function () {
                ajaxTransfer("/dailyreportk3/deleteMasterDetail", data, "#modal-output");
            })
        }

        function editMaster(event) {
            var button = $(event);
            var data = new FormData();
            // var data = {'id': button.data('id')};
            data.append('id', button.data('id'));

            ajaxTransfer("/dailyreportk3/addMaster", data, '#modal-output');
        }

        var table = $('#table-detail');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            // order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });
    </script>

    <script>
        $(document).ready(function() {
            $.ajax({
                type: 'GET',
                url: "{{url('recapitulation/chart')}}",
                dataType: 'json',
                success: function (data) {
                    getTransactionReport(data);
                }
            });
        });
        
        function getTransactionReport(data) {
            Highcharts.chart('container', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: [
                        'Januari',
                        'Februari',
                        'Maret',
                        'April',
                        'Mei',
                        'Juni',
                        'Juli',
                        'Agustus',
                        'September',
                        'Oktober',
                        'November',
                        'Desember'
                    ],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Jumlah Report Safety Officer'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Open Report',
                    data: data.isOpen,
                    color: 'red'

                }, {
                    name: 'Close Report',
                    data: data.isClose,
                    color: 'lightgreen'
                }]
            });
        }

        $(document).ready(function() {
            $.ajax({
                type: 'GET',
                url: "{{url('recapitulation/persentage')}}",
                dataType: 'json',
                success: function (data) {
                    getReportPersentage(data);
                }
            });
        });
        
        function getReportPersentage(data) {
            Highcharts.chart('container2', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Persentage Unsafe Type'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: data.data,
                }]
            });
        }

        $(document).ready(function() {
            $.ajax({
                type: 'GET',
                url: "{{url('recapitulation/persentagePlant')}}",
                dataType: 'json',
                success: function (data) {
                    getPlantPersentage(data);
                }
            });
        });
        
        function getPlantPersentage(data) {
            Highcharts.chart('container3', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                colors: ['#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4'],
                title: {
                    text: 'Persentage Unsafe Plant'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: data.data,
                }]
            });
        }
    </script>
@endsection
@endsection