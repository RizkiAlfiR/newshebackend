@extends('layouts.app')

@section('content')
<title>Create Accident Report &mdash; Safety Hygiene Environment System</title>

<section class="section">
    <div class="section-header">
        <h1>Form New Accident Report</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home') }}">Dashboard</a></div>
            <div class="breadcrumb-item"><a href="{{ url('accidentreport') }}">Accident Report</a></div>
            @foreach($data as $item)
            <div class="breadcrumb-item">Accident Report Detail {{$item->id}}</div>
            @endforeach
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Fill the Form</h2>

        <form action="/accidentreportstore" method="POST">
		{{ csrf_field() }}
            <div class="row">
                <div class="col-12 col-md-6 col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            <h4>Foto Kejadian</h4>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label class="form-label">Foto Kejadian</label>
                                <div class="row gutters-sm">
                                    <label class="imagecheck mb-4">
                                        <figure class="imagecheck-figure">
                                            <img src="{{asset('public/uploads/accident/'.$item->pict_before)}}" alt="}" class="imagecheck-image">
                                        </figure>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h4>Korban</h4>
                        </div>
                        <div class="card-body">
                            @foreach($data as $item)
                            <div class="form-group">
                                <label>Tanggal Kejadian</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fas fa-calendar"></i>
                                        </div>
                                    </div>
                                    <input type="date" class="form-control daterange-cus" placeholder="$item->date_acd">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Jam Kejadian</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fas fa-clock"></i>
                                        </div>
                                    </div>
                                    <input type="time" class="form-control timepicker" placeholder="$item->time_acd">
                                </div>
                            </div>
                            @endforeach
                            @foreach($data as $item)
                            <div class="section-title mt-0">Shift Kejadian</div>
                            <div class="form-group">
                                <label>Pilih Shift</label>
                                <select class="form-control">
                                    <option value="0">{{$item->shift_text}}</option>
                                    <option value="1">Shift 1</option>
                                    <option value="2">Shift 2</option>
                                    <option value="3">Shift 3</option>
                                </select>
                            </div>
                            <div class="section-title mt-0">Status Karyawan</div>
                            <div class="form-group">
                                <label>Pilih Status</label>
                                <select class="form-control">
                                    <option value="#">{{$item->state_vict_text}}</option>
                                    <option value="1">Karyawan</option>
                                    <option value="0">Non-Karyawan</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Nopeg. Korban</label>
                                <input type="text" class="form-control" name="badge_vict" placeholder="{{$item->badge_vict}}">
                            </div>
                            <div class="form-group">
                                <label>Nama Korban</label>
                                <input type="text" class="form-control" name="name_vict" placeholder="{{$item->name_vict}}">
                            </div>
                            <div class="form-group">
                                <label>Umur</label>
                                <input type="text" class="form-control" name="age" placeholder="{{$item->age}}">
                            </div>
                            <div class="form-group">
                                <label>Alamat</label>
                                <input type="text" class="form-control" name="region" placeholder="{{$item->region}}">
                            </div>
                            <div class="form-group">
                                <label>Unit Kerja / Instansi</label>
                                <input type="text" class="form-control" name="uk_vict" placeholder="{{$item->uk_vict}} - {{$item->uk_vict_text}}">
                            </div>
                            <div class="form-group">
                                <label>Jabatan</label>
                                <input type="text" class="form-control" name="position" placeholder="{{$item->position}} - {{$item->position_text}}">
                            </div>
                            <div class="form-group">
                                <label>Masuk Kerja</label>
                                <input type="text" class="form-control" name="date_in" placeholder="{{$item->date_in}}">
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            <h4>Keterangan</h4>
                        </div>
                        @foreach($data as $item)
                        <div class="card-body">
                            <div class="form-group">
                                <label>Macam Pekerjaan Waktu Terjadi Kecelakaan</label>
                                <input type="text" class="form-control" name="work_task" placeholder="{{$item->work_task}}">
                            </div>
                            <div class="form-group">
                                <label>Pengawas Kerja</label>
                                <input type="text" class="form-control" name="spv_badge" placeholder="{{$item->spv_badge}} - {{$item->spv_name}}">
                            </div>
                            <div class="form-group">
                                <label>Lokasi</label>
                                <input type="text" class="form-control" name="location_text" placeholder="{{$item->location}} - {{$item->location_text}}">
                            </div>
                            <div class="form-group">
                                <label>Penyebab Terjadinya Kecelakaan</label>
                                <input type="text" class="form-control" name="acd_cause" placeholder="{{$item->acd_cause}}">
                            </div>
                            <div class="form-group">
                                <label>Nopeg. Saksi Kecelakaan</label>
                                <input type="text" class="form-control" name="s_badge" placeholder="{{$item->s_badge}}">
                            </div>
                            <div class="form-group">
                                <label>Nama Saksi Kecelakaan</label>
                                <input type="text" class="form-control" name="s_name" placeholder="{{$item->s_name}}">
                            </div>
                            <div class="form-group">
                                <label>Jabatan Saksi Kecelakaan</label>
                                <input type="text" class="form-control" name="s_position" placeholder="{{$item->s_position}} - {{$item->s_pos_text}}">
                            </div>
                            <div class="form-group">
                                <label>Cedera yang Dialami</label>
                                <input type="text" class="form-control" name="inj_text" placeholder="{{$item->inj_text}}">
                            </div>
                            <div class="form-group">
                                <label>Keterangan Luka</label>
                                <input type="text" class="form-control" name="inj_note" placeholder="{{$item->inj_note}}">
                            </div>
                            <div class="form-group">
                                <label>Pertolongan Pertama</label>
                                <input type="text" class="form-control" name="first_aid" placeholder="{{$item->first_aid}}">
                            </div>
                            <div class="form-group">
                                <label>Pertolongan Selanjutnya</label>
                                <input type="text" class="form-control" name="next_aid" placeholder="{{$item->next_aid}}">
                            </div>
                            <div class="form-group">
                                <label>Keadaan Syarat Keselamatan Kerja</label>
                                <input type="text" class="form-control" name="safety_req" placeholder="{{$item->safety_req}}">
                            </div>
                            <div class="form-group">
                                <label>Penjelasan Mengenai Kejadian</label>
                                <input type="text" class="form-control" name="acd_rpt_text" placeholder="{{$item->acd_rpt_text}}">
                            </div>
                            <div class="form-group">
                                <label>Keterangan Unsafe Action</label>
                                <input type="text" class="form-control" name="unsafe_act" placeholder="{{$item->unsafe_act}}">
                            </div>
                            <div class="form-group">
                                <label>Keterangan Unsafe Condition</label>
                                <input type="text" class="form-control" name="unsafe_cond" placeholder="{{$item->unsafe_cond}}">
                            </div>
                            <div class="form-group">
                                <label>Saran Agar Tidak Terulang</label>
                                <input type="text" class="form-control" name="note" placeholder="{{$item->note}}">
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <hr class="my-4" />
            <button type="reset" class="btn btn-outline-success"></a>Reset</button>
            <button type="submit" class="btn btn-success">Simpan</button>
        </form>
    </div>
</section>
@endsection