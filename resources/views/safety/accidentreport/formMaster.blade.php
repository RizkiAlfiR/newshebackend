<style>
    .modal{
        max-height: calc(150vh - 250px);
        overflow-y: auto;
    }
</style>
<div id="result-form-konten"></div>
    <form onsubmit="return false;" id="form-konten" class='form-horizontal'>
        <table width="100%">
            <tbody>
                <tr>
                    <td class="text-center">
                        <img src="{{asset('public/assets/img')}}/logosemen.png" class="img-responsive" alt="logo" width="80" style="padding-top:20px">
                    </td>
                </tr>
                <tr>
                    <td class="text-center">
                        <h6 class="modal-title" id="title">Form Accident Report</h6>
                        <h4 class="modal-title" id="title"></h4><small class="font-bold">PT. Semen Indonesia</small>
                    </td>
                </tr>
            </tbody>
        </table>
        <hr>
        <div class="modal-body">
            <div class="row go_form">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Status Report :</label>
                        <select name="status_report" class="form-control" disabled>
                            <option <?php if($data->status_report =='CLOSE') {echo "selected";}?> value="CLOSE">CLOSE</option>
                            <option <?php if($data->status_report =='OPEN') {echo "selected";}?> value="OPEN">OPEN</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row go_form">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Date Accident :</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fas fa-calendar"></i>
                                </div>
                            </div>
                            <input type="date" class="form-control daterange-cus" name="date_acd"
                            @if(!is_null($data))
                                value="{{ $data->date_acd}}"
                            @else
                                value="<?php echo date('Y-m-d');?>"
                            @endif>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Time Accident :</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fas fa-clock"></i>
                                </div>
                            </div>
                            <input type="time" class="form-control daterange-cus" name="time_acd"
                            @if(!is_null($data))
                                value="{{ $data->time_acd }}"
                            @else
                                value="<?php echo date('H-i-s');?>"
                            @endif>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Shift :</label>
                        <select name="shift" class="form-control">
                            <option <?php if($data->shift =='1') {echo "selected";}?> value="1">Shift 1</option>
                            <option <?php if($data->shift =='2') {echo "selected";}?> value="2">Shift 2</option>
                            <option <?php if($data->shift =='3') {echo "selected";}?> value="3">Shift 3</option>
                        </select>
                    </div>
                </div>
            </div>
            <div style="border-width:1px; border-top:3px; border-style:solid; border-color:red; background-color:red">
                <strong style="background-color:red; margin-left:5px; color:white">Keterangan Korban Kecelakaan</strong>
            </div>
            <div class="modal-body" style="border-width:1px; border-style:solid; border-color:red">
                <div class="row go_form">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Status Karyawan :</label>
                            <select name="stat_vict" class="form-control">
                                <option <?php if($data->stat_vict =='0') {echo "selected";}?> value="0 .'|'. Non-Karyawan">Non-Karyawan</option>
                                <option <?php if($data->stat_vict =='1') {echo "selected";}?> value="1 .'|'. Karyawan">Karyawan</option>
                            </select>
                        </div>
                    </div>
                    @if($data->stat_vict == 1)
                    <div class="col-md-8">
                        <div class="form-group">
                            <label>Nama Korban :</label>
                            <select name="arrpeg" class="form-control">
                                @foreach($data_peg as $item)
                                    <option value="{{ $item->mk_nama .'|'. 
                                    $item->mk_nopeg .'|'. 
                                    $item->mk_tgl_masuk .'|'. 
                                    $item->mk_employee_emp_subgroup .'|'. 
                                    $item->mk_employee_emp_subgroup_text .'|'. 
                                    $item->mk_alamat_rumah .'|'. 
                                    $item->muk_short .'|'. 
                                    $item->muk_nama .'|'. 
                                    $item->mk_tgl_lahir }}" @if(!is_null($data)) @if($item->mk_nopeg == $data->badge_vict) selected="selected" @endif @endif>{{ $item->mk_nopeg}} - {{ $item->mk_nama}}</option>

                                @endforeach
                            </select>
                        </div>
                    </div>
                    @else
                    <div class="col-md-8">
                        <div class="form-group">
                            <label>Nama Korban :</label>
                            <input type="text" class="form-control daterange-cus" value="{{ $data->name_vict}}" name="name_vict">
                        </div>
                    </div>
                    @endif
                </div>
                @if($data->stat_vict == 0)
                <div class="row go_form">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>No. BPJS :</label>
                            <input type="text" class="form-control daterange-cus" value="{{ $data->no_bpjs}}" name="no_bpjs">
                        </div>
                    </div>
                </div>
                @endif
                <div class="row go_form">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label>Nama Supervisor Korban :</label>
                            <select name="arrspv" class="form-control">
                                @foreach($data_peg as $item)
                                    <option value="{{ $item->mk_nama .'|'. 
                                    $item->mk_nopeg .'|'. 
                                    $item->mk_employee_emp_subgroup .'|'. 
                                    $item->mk_employee_emp_subgroup_text }}" @if(!is_null($data)) @if($item->mk_nopeg == $data->spv_badge) selected="selected" @endif @endif>{{ $item->mk_nopeg}} - {{ $item->mk_nama}}</option>

                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row go_form">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Task Pekerjaan Korban :</label>
                            <input type="text" class="form-control daterange-cus" value="{{ $data->work_task}}" name="work_task">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Penyebab Kejadian :</label>
                            <input type="text" class="form-control daterange-cus" value="{{ $data->acd_cause}}" name="acd_cause">
                        </div>
                    </div>
                </div>
                <div class="row go_form">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label>Lokasi Kejadian :</label>
                            <select name="arrloc" class="form-control">
                                @foreach($data_funloc as $item)
                                    <option value="{{ $item->equpmentcode .'|'. $item->equptname }}" @if(!is_null($data)) @if($item->equpmentcode == $data->location_text) selected="selected" @endif @endif>{{ $item->equptname}} - {{ $item->equpmentcode}}</option>

                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row go_form">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label>Nama Saksi Kejadian :</label>
                            <select name="arrsaksi" class="form-control">
                                @foreach($data_peg as $item)
                                    <option value="{{ $item->mk_nama .'|'. 
                                    $item->mk_nopeg .'|'. 
                                    $item->mk_employee_emp_subgroup .'|'. 
                                    $item->mk_employee_emp_subgroup_text }}" @if(!is_null($data)) @if($item->mk_nopeg == $data->s_badge) selected="selected" @endif @endif>{{ $item->mk_nopeg}} - {{ $item->mk_nama}}</option>

                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row go_form">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Cidera yang dialami korban :</label>
                            <input type="text" class="form-control daterange-cus" value="{{ $data->inj_text}}" name="inj_text">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Keterangan cidera yang dialami korban :</label>
                            <input type="text" class="form-control daterange-cus" value="{{ $data->inj_note}}" name="inj_note">
                        </div>
                    </div>
                </div>
                <div class="row go_form">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Pertolongan pertama korban :</label>
                            <input type="text" class="form-control daterange-cus" value="{{ $data->first_aid}}" name="first_aid">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Pertolongan lanjutan korban :</label>
                            <input type="text" class="form-control daterange-cus" value="{{ $data->next_aid}}" name="next_aid">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Safety Required :</label>
                            <input type="text" class="form-control daterange-cus" value="{{ $data->safety_req}}" name="safety_req">
                        </div>
                    </div>
                </div>
                <div class="row go_form">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Penjelasan mengenai kejadian :</label>
                            <input type="text" class="form-control daterange-cus" value="{{ $data->acd_rpt_text}}" name="acd_rpt_text">
                        </div>
                    </div>
                </div>
                <div class="row go_form">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Keadaan Unsafe Action kejadian :</label>
                            <input type="text" class="form-control daterange-cus" value="{{ $data->unsafe_act}}" name="unsafe_act">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Keadaan Unsafe Condition kejadian :</label>
                            <input type="text" class="form-control daterange-cus" value="{{ $data->unsafe_cond}}" name="unsafe_cond">
                        </div>
                    </div>
                </div>
                <div class="row go_form">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Note / saran terhadap kejadian :</label>
                            <input type="text" class="form-control daterange-cus" value="{{ $data->note}}" name="note">
                        </div>
                    </div>
                </div>
                <div class="row go_form">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label>Nama PIC Kejadian :</label>
                            <select name="arrpic" class="form-control">
                                @foreach($data_peg as $item)
                                    <option value="{{ $item->mk_nama .'|'. 
                                    $item->mk_nopeg }}" @if(!is_null($data)) @if($item->mk_nopeg == $data->inv_badge) selected="selected" @endif @endif>{{ $item->mk_nopeg}} - {{ $item->mk_nama}}</option>

                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row go_form">
                    <div class="col-md-12">
                        <div class="form-group">
                                <label>Foto Accident :</label>
                                <div class="column">
                                    <div class="input-field col s6">
                                        <input type="file" id="inputImage" name="pict_before" class="validate" value="{{ $data->pict_before }}">
                                    </div>
                                    <div class="row" style="margin-top:10px">
                                    <div class="col s6">
                                        @if($data->pict_before=='')
                                            <img src="{{asset('public/images')}}/{{'preview-icon.png'}}" id="showImage" style="max-width:330px;max-height:270px;float:left;" />
                                        @else
                                            <img src="{{asset('public/uploads/accident')}}/{{$data->pict_before}}" id="showImage" style="max-width:350px;max-height:270px;float:left;" />
                                        @endif
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            <button type="submit" class="btn btn-info" data-loading-text="<i class='fa fa-spinner fa-spin'></i>
                &nbsp;<font class='lowercase'></font>"> <i class="fa fa-check"></i> Save</button>
        </div>

    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/accidentreport/saveMaster', data, '#result-form-konten');
        })
    })
</script>

<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#showImage').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#inputImage").change(function () {
        readURL(this);
    });
</script>

