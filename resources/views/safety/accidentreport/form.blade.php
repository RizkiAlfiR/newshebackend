@extends('layouts.app')

@section('content')
<title>Create Accident Report &mdash; Safety Hygiene Environment System</title>

<section class="section">
    <div class="section-header">
        <h1>Form New Accident Report</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home') }}">Dashboard</a></div>
            <div class="breadcrumb-item"><a href="{{ url('accidentreport') }}">Accident Report</a></div>
            <div class="breadcrumb-item">Create New Report</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">Fill the Form</h2>

        <form action="/accidentreportstore" method="POST">
		{{ csrf_field() }}
            <div class="row">
                <div class="col-12 col-md-6 col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            <h4>Korban</h4>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label>Tanggal Kejadian</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fas fa-calendar"></i>
                                        </div>
                                    </div>
                                    <input type="text" class="form-control daterange-cus">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Jam Kejadian</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fas fa-clock"></i>
                                        </div>
                                    </div>
                                    <input type="text" class="form-control timepicker">
                                </div>
                            </div>
                            <div class="section-title mt-0">Shift Kejadian</div>
                            <div class="form-group">
                                <label>Pilih Shift</label>
                                <select class="form-control">
                                    <option>Shift 1</option>
                                    <option>Shift 2</option>
                                    <option>Shift 3</option>
                                </select>
                            </div>
                            <div class="section-title mt-0">Status Karyawan</div>
                            <div class="form-group">
                                <label>Pilih Status</label>
                                <select class="form-control">
                                    <option>Karyawan</option>
                                    <option>Non-Karyawan</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Nopeg. Korban</label>
                                <input type="text" class="form-control" name="badge_vict">
                            </div>
                            <div class="form-group">
                                <label>Nama Korban</label>
                                <input type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Umur</label>
                                <input type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Alamat</label>
                                <input type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Unit Kerja / Instansi</label>
                                <input type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Jabatan</label>
                                <input type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Masuk Kerja</label>
                                <input type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h4>Foto Kejadian</h4>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label class="form-label">Foto Kejadian</label>
                                <div class="row gutters-sm">
                                    <label class="imagecheck mb-4">
                                        <input name="imagecheck" type="checkbox" value="1" class="imagecheck-input" />
                                        <figure class="imagecheck-figure">
                                            <img src="{{asset('assets/')}}/img/news/img01.jpg" alt="}" class="imagecheck-image">
                                        </figure>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            <h4>Keterangan</h4>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label>Macam Pekerjaan Waktu Terjadi Kecelakaan</label>
                                <input type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Pengawas Kerja</label>
                                <input type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Lokasi</label>
                                <input type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Penyebab Terjadinya Kecelakaan</label>
                                <input type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Nopeg. Saksi Kecelakaan</label>
                                <input type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Nama Saksi Kecelakaan</label>
                                <input type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Jabatan Saksi Kecelakaan</label>
                                <input type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Cedera yang Dialami</label>
                                <input type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Keterangan Luka</label>
                                <input type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Pertolongan Pertama</label>
                                <input type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Pertolongan Selanjutnya</label>
                                <input type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Keadaan Syarat Keselamatan Kerja</label>
                                <input type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Penjelasan Mengenai Kejadian</label>
                                <input type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Keterangan Unsafe Action</label>
                                <input type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Keterangan Unsafe Condition</label>
                                <input type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Penjelasan Tentang Penyebab Terjadinya Kecelakaan</label>
                                <input type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Saran Agar Tidak Terulang</label>
                                <input type="text" class="form-control">
                            </div>
                            <div class="form-group">
                                <!-- <a type="submit" class="btn btn-icon icon-left btn-success"><i class="fas fa-check"></i> Simpan</a> -->
                                <input type="submit" value="Simpan Data">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
@endsection