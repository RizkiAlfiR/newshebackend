@extends('layouts.app')

@section('content')
<title>Accident Report &mdash; Safety Hygiene Environment System</title>
<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div> -->
<section class="section">
    <div class="section-header">
        <h1>Accident Report</h1>
        <div class="section-header-breadcrumb">
            
            <div class="breadcrumb-item active"><a href="{{ route('home') }}">Dashboard</a></div>
            <div class="breadcrumb-item">Accident Report</div>
        </div>
    </div>

    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>List All Report Accident</h4>
                    </div>
                    <p class="section-lead">
                        <a href="{{ url('accidentreportcreate') }}" class="btn btn-icon icon-left btn-primary"><i
                                class="far fa-edit"></i> Create New</a>
                    </p>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped" id="table-1">
                                <thead>
                                    <tr>
                                        <th class="text-center">
                                            #
                                        </th>
                                        <th>Employee Name</th>
                                        <th>Unit Kerja</th>
                                        <th>Work Task</th>
                                        <th>Due Date</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                $data as $num => $item
                                    @foreach($accident_reports as $i => $p)
                                    <tr>
                                        <td>
                                            {{ $i+1 }}
                                        </td>
                                        <td>{{ $p->badge_vict }} - {{ $p->name_vict }}</td>
                                        <td class="align-middle">
                                            <!-- <div class="progress" data-height="4" data-toggle="tooltip" title="100%">
                                                <div class="progress-bar bg-success" data-width="100%"></div>
                                            </div> -->
                                            {{ $p->uk_vict_text }} ({{ $p->position_text }})
                                        </td>
                                        <td>
                                            <!-- <img alt="image" src="assets/img/avatar/avatar-5.png" class="rounded-circle"
                                                width="35" data-toggle="tooltip" title="Wildan Ahdian"> -->
                                            {{ $p->acd_cause }}
                                        </td>
                                        <td>{{ $p->date_acd }}</td>
                                        <td>
                                            <div class="badge badge-success">{{ $p->status_report }}</div>
                                        </td>
                                        <td>
                                            <!-- <a href="#" class="btn btn-secondary">Detail</a> -->
                                            <!-- <a href="#" class="btn btn-icon icon-left btn-info"><i
                                                    class="fas fa-info-circle"></i> Detail</a> -->
                                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                <button type="button" class="btn btn-icon icon-left btn-info">
                                                    <i class="fas fa-info-circle"></i>Detail
                                                </button>
                                                <button type="button" class="btn btn-icon icon-left btn-danger">
                                                    <i class="fas fa-times"></i>Delete
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                    <!-- <tr>
                                        <td>
                                            2
                                        </td>
                                        <td>Redesign homepage</td>
                                        <td class="align-middle">
                                            <div class="progress" data-height="4" data-toggle="tooltip" title="0%">
                                                <div class="progress-bar" data-width="0"></div>
                                            </div>
                                        </td>
                                        <td>
                                            <img alt="image" src="assets/img/avatar/avatar-1.png" class="rounded-circle"
                                                width="35" data-toggle="tooltip" title="Nur Alpiana">
                                            <img alt="image" src="assets/img/avatar/avatar-3.png" class="rounded-circle"
                                                width="35" data-toggle="tooltip" title="Hariono Yusup">
                                            <img alt="image" src="assets/img/avatar/avatar-4.png" class="rounded-circle"
                                                width="35" data-toggle="tooltip" title="Bagus Dwi Cahya">
                                        </td>
                                        <td>2018-04-10</td>
                                        <td>
                                            <div class="badge badge-info">Todo</div>
                                        </td>
                                        <td><a href="#" class="btn btn-secondary">Detail</a></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            3
                                        </td>
                                        <td>Backup database</td>
                                        <td class="align-middle">
                                            <div class="progress" data-height="4" data-toggle="tooltip" title="70%">
                                                <div class="progress-bar bg-warning" data-width="70%"></div>
                                            </div>
                                        </td>
                                        <td>
                                            <img alt="image" src="assets/img/avatar/avatar-1.png" class="rounded-circle"
                                                width="35" data-toggle="tooltip" title="Rizal Fakhri">
                                            <img alt="image" src="assets/img/avatar/avatar-2.png" class="rounded-circle"
                                                width="35" data-toggle="tooltip" title="Hasan Basri">
                                        </td>
                                        <td>2018-01-29</td>
                                        <td>
                                            <div class="badge badge-warning">In Progress</div>
                                        </td>
                                        <td><a href="#" class="btn btn-secondary">Detail</a></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            4
                                        </td>
                                        <td>Input data</td>
                                        <td class="align-middle">
                                            <div class="progress" data-height="4" data-toggle="tooltip" title="100%">
                                                <div class="progress-bar bg-success" data-width="100%"></div>
                                            </div>
                                        </td>
                                        <td>
                                            <img alt="image" src="assets/img/avatar/avatar-2.png" class="rounded-circle"
                                                width="35" data-toggle="tooltip" title="Rizal Fakhri">
                                            <img alt="image" src="assets/img/avatar/avatar-5.png" class="rounded-circle"
                                                width="35" data-toggle="tooltip" title="Isnap Kiswandi">
                                            <img alt="image" src="assets/img/avatar/avatar-4.png" class="rounded-circle"
                                                width="35" data-toggle="tooltip" title="Yudi Nawawi">
                                            <img alt="image" src="assets/img/avatar/avatar-1.png" class="rounded-circle"
                                                width="35" data-toggle="tooltip" title="Khaerul Anwar">
                                        </td>
                                        <td>2018-01-16</td>
                                        <td>
                                            <div class="badge badge-success">Completed</div>
                                        </td>
                                        <td><a href="#" class="btn btn-secondary">Detail</a></td>
                                    </tr> -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection