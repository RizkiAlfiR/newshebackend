@extends('layouts.app')

@section('content')
<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div> -->
<title>SHE Dashboard &mdash; Safety Hygiene Environment System</title>
<section class="section">
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-12">
            <div class="card card-statistic-2">
                <div class="card-icon shadow-primary bg-primary">
                    <i class="fas fa-archive"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Total Accident</h4>
                    </div>
                    <div class="card-body">
                        {{ $jumlahAccident }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="nav-wrapper">
                    <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link mb-sm-3 mb-md-0 active" id="tabs-icons-text-1-tab" data-toggle="tab" href="#tabs-icons-text-1" role="tab" aria-controls="tabs-icons-text-1" aria-selected="true"><i class="ni ni-cloud-upload-96 mr-2"></i>Chart Accident</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-2-tab" data-toggle="tab" href="#tabs-icons-text-2" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>Data Accident</a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
                        <div id="container" style="min-width: 310px; height: 400px; max-width: 800px; margin: 0 auto"></div>
                        <br><br><br><br>
                        <div id="container2" style="min-width: 310px; height: 400px; max-width: 800px; margin: 0 auto"></div>
                        <br><br><br><br>
                    </div>
                    <div class="tab-pane fade" id="tabs-icons-text-2" role="tabpanel" aria-labelledby="tabs-icons-text-2-tab">
                        <div class="card">
                            <div class="card-header">
                                <h4>List All Report Accident</h4>
                            </div>
                            <!-- <p class="section-lead">
                                <a href="{{ url('accidentreport/create') }}" class="btn btn-icon icon-left btn-primary"><i
                                        class="far fa-edit"></i> Create New</a>
                            </p> -->
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped" id="table-accident">
                                        <thead>
                                            <tr>
                                                <th class="text-center">
                                                    #
                                                </th>
                                                <th>Employee Name</th>
                                                <th>Unit Kerja</th>
                                                <th>Work Task</th>
                                                <th>Due Date</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($accident_reports as $p => $item)
                                            <tr>
                                                <td>
                                                    {{ $p+1 }}
                                                </td>
                                                <td>{{ $item->badge_vict }} - {{ $item->name_vict }}</td>
                                                <td class="align-middle">
                                                    {{ $item->uk_vict_text }} ({{ $item->position_text }})
                                                </td>
                                                <td>
                                                    {{ $item->acd_cause }}
                                                </td>
                                                <td>{{ $item->date_acd }}</td>
                                                <td>
                                                    @if($item->status_report == "OPEN")
                                                        <div class="badge badge-danger">{{ $item->status_report }}</div>
                                                    @else
                                                        <div class="badge badge-success">{{ $item->status_report }}</div>
                                                    @endif
                                                </td>
                                                <td>
                                                    <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                        <button onclick="loadModal(this)" title="" target="/accidentreport/showImage" data="id={{$item->id}}" type="button" class="btn btn-icon icon-left btn-success">
                                                            <i class="fas fa-image"></i>
                                                        </button>
                                                        <button onclick="loadModal(this)" title="" target="/accidentreport/addFollowUp" data="id={{$item->id}}" type="button" class="btn btn-icon icon-left btn-warning">
                                                            <i class="fas fa-eye"></i>
                                                        </button>
                                                        <button onclick="loadModal(this)" title="" target="/accidentreport/addMaster" data="id={{$item->id}}" type="button" class="btn btn-icon icon-left btn-info">
                                                            <i class="fas fa-edit"></i>
                                                        </button>
                                                        <button type="button" onclick="deleteMaster({{$item->id}})" class="btn btn-icon icon-left btn-danger">
                                                            <i class="fas fa-trash"></i>
                                                        </button>
                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@section('scripts')
    <script>
        function deleteMaster(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("", "Do you want to remove this data?", function () {
                ajaxTransfer("/accidentreport/deleteMaster", data, "#modal-output");
            })
        }

        function editMaster(event) {
            var button = $(event);
            var data = new FormData();
            // var data = {'id': button.data('id')};
            data.append('id', button.data('id'));

            ajaxTransfer("/accidentreport/addMaster", data, '#modal-output');
        }

        var table = $('#table-accident');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            // order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });
    </script>

    `<script>
        $(document).ready(function() {
            $.ajax({
                type: 'GET',
                url: "{{url('accidentreport/chart')}}",
                dataType: 'json',
                success: function (data) {
                    getTransactionReport(data);
                }
            });
        });
        
        function getTransactionReport(data) {
            Highcharts.chart('container', {
                chart: {
                    type: 'bar'
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: [
                        'Januari',
                        'Februari',
                        'Maret',
                        'April',
                        'Mei',
                        'Juni',
                        'Juli',
                        'Agustus',
                        'September',
                        'Oktober',
                        'November',
                        'Desember'
                    ],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Jumlah Report Accident'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Open Report',
                    data: data.isOpen,
                    color: 'red'
                }, {
                    name: 'Close Report',
                    data: data.isClose,
                    color: 'lightgreen'
                }]
            });
        }

        $(document).ready(function() {
            $.ajax({
                type: 'GET',
                url: "{{url('accidentreport/chart2')}}",
                dataType: 'json',
                success: function (data) {
                    getAccidentReport(data);
                }
            });
        });
        
        function getAccidentReport(data) {
            Highcharts.chart('container2', {
                chart: {
                    type: 'spline'
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: [
                        'Januari',
                        'Februari',
                        'Maret',
                        'April',
                        'Mei',
                        'Juni',
                        'Juli',
                        'Agustus',
                        'September',
                        'Oktober',
                        'November',
                        'Desember'
                    ],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Jumlah Accident per Month'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b> {point.y:.0f}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Accident',
                    data: data.isAccident,
                    color: 'red'
                }]
            });
        }
    </script>
@endsection
@endsection