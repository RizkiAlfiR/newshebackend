<style>
    .modal{
        max-height: calc(150vh - 250px);
        overflow-y: auto;
    }
</style>
<div id="result-form-konten"></div>
    <form onsubmit="return false;" id="form-konten" class='form-horizontal'>
        <table width="100%">
            <tbody>
                <tr>
                    <td class="text-center">
                        <img src="{{asset('public/assets/img')}}/logosemen.png" class="img-responsive" alt="logo" width="80" style="padding-top:20px">
                    </td>
                </tr>
                <tr>
                    <td class="text-center">
                        <h6 class="modal-title" id="title">Form Header Daily Report Safety Officer</h6>
                        <h4 class="modal-title" id="title"></h4><small class="font-bold">PT. Semen Indonesia</small>
                    </td>
                </tr>
            </tbody>
        </table>
        <hr>
        <div class="modal-body">
            <div class="row go_form">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Report Date :</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fas fa-calendar"></i>
                                </div>
                            </div>
                            <input type="date" class="form-control daterange-cus" name="report_date"
                            @if(!is_null($data))
                                value="{{ $data->report_date}}"
                            @else
                                value="<?php echo date('Y-m-d');?>"
                            @endif>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Shift :</label>
                        <div class="input-group">
                            <select class="form-control" name="shift">
                                <option <?php if($data->shift =='1') {echo "selected";}?> value="1">Shift 1</option>
                                <option <?php if($data->shift =='2') {echo "selected";}?> value="2">Shift 2</option>
                                <option <?php if($data->shift =='3') {echo "selected";}?> value="3">Shift 3</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div style="border-width:1px; border-top:3px; border-style:solid; border-color:red; background-color:red">
                <strong style="background-color:red; margin-left:5px; color:white">Keterangan Unsafe Header</strong>
            </div>
            <div class="modal-body" style="border-width:1px; border-style:solid; border-color:red">
                <div class="row go_form">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Area Officer :</label>
                                    <select name="arrarea" class="form-control">
                                    @foreach($data_area as $item)
                                        <option value="{{ $item->area_name .'|'. $item->id }}" @if(!is_null($data)) @if($item->area_name == $data->area_txt) selected="selected" @endif @endif>{{ $item->id}} - {{ $item->area_name}}</option>
                                        
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Sub Area Officer :</label>
                                        <input type="text" class="form-control daterange-cus" value="{{ $data->sub_area_txt}}" name="sub_area_txt">
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Planning Plant :</label>
                                    <select name="arrpplant" class="form-control">
                                    @foreach($data_pplant as $item)
                                        <option value="{{ $item->pplantdesc .'|'. $item->pplant }}" @if(!is_null($data)) @if($item->pplantdesc == $data->plant_txt) selected="selected" @endif @endif>{{ $item->pplant}} - {{ $item->pplantdesc}}</option>
                                        
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row go_form">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <label>Inspector :</label>
                                    <select name="arrpeg" class="form-control">
                                        @foreach($data_pegawai as $item)
                                                <option value="{{ $item->mk_nama .'|'. $item->mk_nopeg }}" @if(!is_null($data)) @if($item->mk_nopeg == $data->inspector_code) selected="selected" @endif @endif>{{ $item->mk_nopeg}} - {{ $item->mk_nama}}</option>
                                            
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Is - SMIG :</label>
                                    <select name="is_smig" class="form-control">
                                        <option <?php if($data->is_smig =='0') {echo "selected";}?> value="0">Non-SMIG</option>
                                        <option <?php if($data->is_smig =='1') {echo "selected";}?> value="1">SMIG</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row go_form">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Accident :</label>
                                        <input type="number" step="1" max="" name="accident" class="form-control" value={{ $data->accident }}>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Incident :</label>
                                        <input type="number" step="1" max="" name="incident" class="form-control" value={{ $data->incident }}>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Nearmiss :</label>
                                        <input type="number" step="1" max="" name="nearmiss" class="form-control" value={{ $data->nearmiss }}>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            <button type="submit" class="btn btn-info" data-loading-text="<i class='fa fa-spinner fa-spin'></i>
                &nbsp;<font class='lowercase'></font>"> <i class="fa fa-check"></i> Save</button>
        </div>

    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/dailyreportk3/saveMaster', data, '#result-form-konten');
        })
    })
</script>
