<style>
    .modal{
        max-height: calc(150vh - 250px);
        overflow-y: auto;
    }
</style>
<div id="result-form-konten"></div>
    <form onsubmit="return false;" id="form-konten" class='form-horizontal'>
        <table width="100%">
            <tbody>
                <tr>
                    <td class="text-center">
                        <img src="{{asset('public/assets/img')}}/logosemen.png" class="img-responsive" alt="logo" width="80" style="padding-top:20px">
                    </td>
                </tr>
                <tr>
                    <td class="text-center">
                        <h6 class="modal-title" id="title">Form Follow Up Safety Officer</h6>
                        <h4 class="modal-title" id="title"></h4><small class="font-bold">PT. Semen Indonesia</small>
                    </td>
                </tr>
            </tbody>
        </table>
        <hr>
        <div class="modal-body">
            <div style="border-width:1px; border-top:3px; border-style:solid; border-color:red; background-color:red">
                <strong style="background-color:red; margin-left:5px; color:white">Keterangan Follow Up</strong>
            </div>
            <div class="modal-body" style="border-width:1px; border-style:solid; border-color:red">
                <div class="row go_form">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Date Commitment :</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fas fa-calendar"></i>
                                    </div>
                                </div>
                                <input type="date" class="form-control daterange-cus" name="comitment_date"
                                @if(!is_null($data))
                                    value="{{ $data->comitment_date }}"
                                @else
                                    value="<?php echo date('Y:m:d');?>"
                                @endif>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Status Report :</label>
                            <select name="status" class="form-control">
                                <option <?php if($data->status =='CLOSE') {echo "selected";}?> value="CLOSE">CLOSE</option>
                                <option <?php if($data->status =='OPEN') {echo "selected";}?> value="OPEN">OPEN</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row go_form">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Tindak Lanjut :</label>
                            <input type="text" class="form-control daterange-cus" value="{{ $data->tindak_lanjut}}" name="tindak_lanjut">
                        </div>
                    </div>
                </div>
                <div class="row go_form">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="card">
                                <!-- <div class="card-header"> -->
                                    <label>Foto Tindak Lanjut :</label>
                                <!-- </div> -->
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input type="file" id="inputImage" name="foto_aft" class="validate" value="{{ $data->foto_aft }}">
                                    </div>
                                    </div>
                                    <div class="row" style="margin-top:10px">
                                    <div class="col s6">
                                        @if($data->foto_aft=='')
                                            <img src="{{asset('public/images')}}/{{'preview-icon.png'}}" id="showImage" style="max-width:330px;max-height:270px;float:left;" />
                                        @else
                                            <img src="{{asset('public/uploads/unsafe')}}/{{$data->foto_aft}}" id="showImage" style="max-width:350px;max-height:270px;float:left;" />
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            <button type="submit" class="btn btn-info" data-loading-text="<i class='fa fa-spinner fa-spin'></i>
                &nbsp;<font class='lowercase'></font>"> <i class="fa fa-check"></i> Save</button>
        </div>

    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/dailyreportk3/saveFollowUp', data, '#result-form-konten');
        })
    })
</script>

<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#showImage').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#inputImage").change(function () {
        readURL(this);
    });
</script>
