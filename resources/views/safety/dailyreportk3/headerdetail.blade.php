@extends('layouts.app')

@section('content')
<title>SHE Daily Report Safety Officer &mdash; Safety Hygiene Environment System</title>
<section class="section">
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>List All Detail Report Safety Officer</h4>
                    </div>
                    <!-- <p class="section-lead">
                        <a href="{{ url('dailyreportk3/create') }}" class="btn btn-icon icon-left btn-primary"><i
                                class="far fa-edit"></i> Create New</a>
                    </p> -->
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped" id="table-detail">
                                <thead>
                                    <tr>
                                        <th class="text-center">
                                            #
                                        </th>
                                        <th>Unit Description</th>
                                        <th>Activity Description</th>
                                        <th>Potential Hazard</th>
                                        <th>Follow Up Sementara</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data as $p => $item)
                                    <tr>
                                        <td>
                                            {{ $p+1 }}
                                        </td>
                                        @if($item->unit_code != "undefined")
                                            <td>{{ $item->unit_code }} - {{ $item->unit_name }}</td>
                                        @else
                                            <td>{{ $item->unit_name }}</td>
                                        @endif
                                        <td class="align-middle">
                                            {{ $item->activity_description }} ({{ $item->unsafe_type }})
                                        </td>
                                        <td>
                                            {{ $item->potential_hazard }}
                                        </td>
                                        <td>{{ $item->follow_up }}</td>
                                        <td>
                                            @if($item->status == "OPEN")
                                                <div class="badge badge-danger">{{ $item->status }}</div>
                                            @else
                                                <div class="badge badge-success">{{ $item->status }}</div>
                                            @endif
                                        </td>
                                        <td>
                                            <!-- <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                <button type="button" class="btn btn-icon icon-left btn-success">
                                                    <i class="fas fa-edit"></i>
                                                </button>
                                                <button type="button" class="btn btn-icon icon-left btn-info">
                                                    <i class="fas fa-eye"></i>
                                                </button>
                                                <button type="button" class="btn btn-icon icon-left btn-danger">
                                                    <i class="fas fa-trash"></i>
                                                </button>
                                            </div> -->
                                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                <button onclick="loadModal(this)" title="" target="/dailyreportk3/showImage" data="id={{$item->id}}" type="button" class="btn btn-icon icon-left btn-success">
                                                    <i class="fas fa-image"></i>
                                                </button>
                                                <button onclick="loadModal(this)" title="" target="/dailyreportk3/addFollowUp" data="id={{$item->id}}" type="button" class="btn btn-icon icon-left btn-warning">
                                                    <i class="fas fa-eye"></i>
                                                </button>
                                                <button onclick="loadModal(this)" title="" target="/dailyreportk3/addMasterDetail" data="id={{$item->id}}" type="button" class="btn btn-icon icon-left btn-info">
                                                    <i class="fas fa-edit"></i>
                                                </button>
                                                <button type="button" onclick="deleteMaster({{$item->id}})" class="btn btn-icon icon-left btn-danger">
                                                    <i class="fas fa-trash"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@section('scripts')
    <script>
        function deleteMaster(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("", "Do you want to remove this data?", function () {
                ajaxTransfer("/dailyreportk3/deleteMasterDetail", data, "#modal-output");
            })
        }

        function editMaster(event) {
            var button = $(event);
            var data = new FormData();
            // var data = {'id': button.data('id')};
            data.append('id', button.data('id'));

            ajaxTransfer("/dailyreportk3/addMaster", data, '#modal-output');
        }

        var table = $('#table-detail');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            // order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });
    </script>
@endsection
@endsection