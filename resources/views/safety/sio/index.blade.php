@extends('layouts.app')

@section('content')
<title>Sio Certification &mdash; Safety Hygiene Environment System</title>
<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div> -->
<section class="section">
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-12">
            <div class="card card-statistic-2">
                <div class="card-icon shadow-primary bg-primary">
                    <i class="fas fa-archive"></i>
                </div>
                <div class="card-wrap">
                    <div class="card-header">
                        <h4>Total Sio Certifications</h4>
                    </div>
                    <div class="card-body">
                        {{ $jumlahSio }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>List All Sio Certification</h4>
                    </div>
                    <div class="col-md-4">
                        <!-- <button onclick="loadModal(this)" title="" target="/sio/addMaster" class="btn btn-icon icon-left btn-primary" type="submit" id="add">
                            <i class="far fa-edit"></i> Add New Data
                        </button> -->
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped" id="table-sio">
                                <thead>
                                    <tr>
                                        <th class="text-center">
                                            #
                                        </th>
                                        <th>No Lisensi</th>
                                        <th>Pemilik</th>
                                        <th>Grup (Kelas)</th>
                                        <th>Unit Kerja</th>
                                        <th>Masa Berlaku</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($sios as $p => $item)
                                    <tr>
                                        <td>
                                            {{ $p+1 }}
                                        </td>
                                        <td>{{ $item->no_lisensi }}</td>
                                        <td class="align-middle">
                                            {{ $item->nama }} ({{ $item->badge }})
                                        </td>
                                        <td>
                                            {{ $item->grup }} ({{ $item->kelas }})
                                        </td>
                                        <td>{{ $item->unit_kerja }} - {{ $item->unit_kerja_txt }}</td>
                                        <td>
                                            <!-- {{ $item->start_date }} -->
                                            
                                            @if($item->time_certification >= 2)
                                                <div class="badge badge-success">{{ $item->time_certification }} tahun</div>
                                            @else
                                                <div class="badge badge-danger">{{ $item->time_certification }} tahun</div>
                                            @endif
                                        </td>
                                        <td>
                                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                <button onclick="loadModal(this)" title="" target="/sio/showImage" data="id={{$item->id}}" type="button" class="btn btn-icon icon-left btn-success">
                                                    <i class="fas fa-image"></i>
                                                </button>
                                                <button onclick="loadModal(this)" title="" target="/sio/addMaster" data="id={{$item->id}}" type="button" class="btn btn-icon icon-left btn-info">
                                                    <i class="fas fa-edit"></i>
                                                </button>
                                                <button type="button" onclick="deleteMaster({{$item->id}})" class="btn btn-icon icon-left btn-danger">
                                                    <i class="fas fa-trash"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@section('scripts')
    <script>
        function dateDiffInDays(endDate, now) {
            $diff = strtotime(endDate) - strtotime(now);

            return abs(round($diff / 86400));
        }

        function deleteMaster(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("", "Do you want to remove this data?", function () {
                ajaxTransfer("/sio/deleteMaster", data, "#modal-output");
            })
        }

        function editMaster(event) {
            var button = $(event);
            var data = new FormData();
            // var data = {'id': button.data('id')};
            data.append('id', button.data('id'));

            ajaxTransfer("/dailyreportk3/addMaster", data, '#modal-output');
        }

        var table = $('#table-sio');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            // order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });
    </script>
@endsection
@endsection