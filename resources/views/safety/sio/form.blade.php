@extends('layouts.app')

@section('content')
<title>Create SIO Certification &mdash; Safety Hygiene Environment System</title>

<section class="section">
    <div class="section-header">
        <h1>Form New SIO Certification</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home') }}">Dashboard</a></div>
            <div class="breadcrumb-item"><a href="{{ url('sio') }}">Sio Certification</a></div>
            <div class="breadcrumb-item">Create New Report</div>
        </div>
    </div>

    <div class="section-body">

        <form method="POST" action="{{url('/sio/store')}}">
		{{ csrf_field() }}
            <div class="row">
                <div class="col-12 col-md-6 col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            <h4>Fill the form</h4>
                        </div>
                        <div class="card-body">
                            <div class="section-title mt-0">Pemegang Surat Izin Operasi</div>
                            <div class="form-group">
                                <select class="form-control form-control-sm" name="[mk_nopeg, mk_nama]" id="exampleFormControlSelect1">
                                    <option value="0">Pilih Karyawan</option>
                                    @foreach($data_karyawan as $key =>$item)
                                        @if(is_null($item->getParent))
                                            <option value="[{{$item->mk_nopeg}}, {{$item->mk_nama}}]">{{$item->mk_nopeg}} - {{$item->mk_nama}}</option>
                                        @else
                                            <option value="[{{$item->mk_nopeg}}, {{$item->mk_nama}}]">{{$item->getParent->mk_nopeg}} - {{$item->mk_nama}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="section-title mt-0">Unit Kerja</div>
                            <div class="form-group">
                                <select class="form-control form-control-sm" name="unit_kerja" id="exampleFormControlSelect2">
                                    <option value="0">Pilih Unit Kerja</option>
                                    @foreach($data_uk as $key =>$item)
                                        @if(is_null($item->getParent))
                                            <option value="{{$item->muk_nama}}">{{$item->muk_kode}} - {{$item->muk_nama}}</option>
                                        @else
                                            <option value="{{$item->muk_nama}}">{{$item->getParent->muk_kode}}-{{$item->muk_nama}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Nomor Lisensi</label>
                                <input type="text" class="form-control" name="lisensi">
                            </div>
                            <div class="form-group">
                                <label>Pilih Kelas</label>
                                <select class="form-control" name="kelas">
                                    <option value="1">Kelas I (Satu)</option>
                                    <option value="2">Kelas II (Dua)</option>
                                    <option value="3">Kelas III (Tiga)</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <select class="form-control form-control-sm" name="group" id="exampleFormControlSelect3">
                                    <option value="0">Pilih Jenis Lisensi</option>
                                    @foreach($data_group as $key =>$item)
                                        @if(is_null($item->getParent))
                                            <option value="{{$item->NICKNAME}}">{{$item->NICKNAME}} - {{$item->NAME}}</option>
                                        @else
                                            <option value="{{$item->NICKNAME}}">{{$item->getParent->NICKNAME}}-{{$item->NAME}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Tanggal Mulai Sertifikat</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fas fa-calendar"></i>
                                        </div>
                                    </div>
                                    <input type="date" class="form-control daterange-cus" name="tanggal_mulai">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Pilih Jangka Waktu</label>
                                <select class="form-control" name="masa_berlaku">
                                    <option value="1">1 Tahun</option>
                                    <option value="2">2 Tahun</option>
                                    <option value="3">3 Tahun</option>
                                    <option value="4">4 Tahun</option>
                                    <option value="5">5 Tahun</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            <h4>Foto Sertifikat</h4>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label>Upload Here </label>
                                <input type="file" name="foto" class="form-control" id="customFileLang" lang="en">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="my-4" />
            <button type="reset" class="btn btn-outline-success"></a>Reset</button>
            <button type="submit" class="btn btn-success">Simpan</button>
        </form>
    </div>
</section>
@endsection