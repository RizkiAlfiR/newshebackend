@extends('layouts.app')

@section('content')
<title>SIO Certification &mdash; Safety Hygiene Environment System</title>

<section class="section">
    <div class="section-header">
        <h1>SIO Certification Detail</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home') }}">Dashboard</a></div>
            <div class="breadcrumb-item"><a href="{{ url('sio') }}">Sio Certification</a></div>
            @foreach($data as $item)
            <div class="breadcrumb-item">Sio Detail {{$item->id}}</div>
            @endforeach
        </div>
    </div>

    <div class="section-body">

        <form method="POST" action="{{url('/sio/store')}}">
		{{ csrf_field() }}
            <div class="row">
                <div class="col-12 col-md-6 col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            <h4>Fill the form</h4>
                        </div>
                        <div class="card-body">
                            <div class="section-title mt-0">Pemegang Surat Izin Operasi</div>
                            <div class="form-group">
                                <select class="form-control form-control-sm" name="karyawan" id="exampleFormControlSelect1">
                                    @foreach($data as $item)
                                    <option value="0">{{$item->badge}} - {{$item->nama}}</option>
                                    @endforeach
                                    @foreach($data_karyawan as $key =>$item)
                                        @if(is_null($item->getParent))
                                            <option value="{{$item->badge}}">{{$item->mk_nopeg}} - {{$item->mk_nama}}</option>
                                        @else
                                            <option value="{{$item->badge}}">{{$item->getParent->mk_nopeg}}-{{$item->mk_nama}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="section-title mt-0">Unit Kerja</div>
                            <div class="form-group">
                                <select class="form-control form-control-sm" name="uk" id="exampleFormControlSelect2">
                                    @foreach($data as $item)
                                    <option value="0">{{$item->unit_kerja}} - {{$item->unit_kerja_txt}}</option>
                                    @endforeach
                                    @foreach($data_uk as $key =>$item)
                                        @if(is_null($item->getParent))
                                            <option value="{{$item->muk_kode}}">{{$item->muk_kode}} - {{$item->muk_nama}}</option>
                                        @else
                                            <option value="{{$item->muk_kode}}">{{$item->getParent->muk_kode}}-{{$item->muk_nama}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                @foreach($data as $item)
                                <label>Nomor Lisensi</label>
                                <input type="text" class="form-control" name="lisensi" placeholder="{{$item->no_lisensi}}">
                                @endforeach
                            </div>
                            <div class="form-group">
                                <label>Pilih Kelas</label>
                                @foreach($data as $item)
                                <select class="form-control" name="kelas">
                                    <option value="0">{{$item->kelas}}</option>
                                    <option value="1">Kelas I (Satu)</option>
                                    <option value="2">Kelas II (Dua)</option>
                                    <option value="3">Kelas III (Tiga)</option>
                                </select>
                                @endforeach
                            </div>
                            <div class="form-group">
                                <select class="form-control form-control-sm" name="group" id="exampleFormControlSelect3">
                                    @foreach($data as $item)
                                    <option value="0">{{$item->grup}}</option>
                                    @foreach($data_group as $key =>$item)
                                        @if(is_null($item->getParent))
                                            <option value="{{$item->NICKNAME}}">{{$item->NICKNAME}} - {{$item->NAME}}</option>
                                        @else
                                            <option value="{{$item->NICKNAME}}">{{$item->getParent->NICKNAME}}-{{$item->NAME}}</option>
                                        @endif
                                    @endforeach
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Tanggal Mulai Sertifikat</label>
                                @foreach($data as $item)
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fas fa-calendar"></i>
                                        </div>
                                    </div>
                                    <input type="date" class="form-control daterange-cus" name="tanggal_mulai" placeholder="{{$item->start_date}}">
                                </div>
                                @endforeach
                            </div>
                            <div class="form-group">
                                <label>Pilih Jangka Waktu</label>
                                @foreach($data as $item)
                                <select class="form-control" name="masa_berlaku">
                                    <option value="0">{{$item->time_certification}}</option>
                                    <option value="1">1 Tahun</option>
                                    <option value="2">2 Tahun</option>
                                    <option value="3">3 Tahun</option>
                                    <option value="4">4 Tahun</option>
                                    <option value="5">5 Tahun</option>
                                </select>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            <h4>Foto Sertifikat</h4>
                        </div>
                        @foreach($data as $item)
                        <div class="card-body">
                            <div class="form-group">
                                <label class="form-label">Foto</label>
                                <div class="row gutters-sm">
                                    <label class="imagecheck mb-4">
                                        <figure class="imagecheck-figure">
                                            <img style="width: 600px; height: 350px" src="{{asset('public/uploads/sio/'.$item->foto)}}" alt="}" class="imagecheck-image">
                                        </figure>
                                    </label>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <hr class="my-4" />
            <button type="reset" class="btn btn-outline-success"></a>Reset</button>
            <button type="submit" class="btn btn-success">Simpan</button>
        </form>
    </div>
</section>
@endsection