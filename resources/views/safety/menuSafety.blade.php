@extends('layouts.app')

@section('content')
<title>Safety | Safety Hygiene Environment System</title>
<section class="section">
    <div class="section-body">
      <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
          <div class="card card-statistic-1">
            <div class="card-icon bg-warning">
              <i class="far fa-file"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Reports</h4>
              </div>
              <div class="card-body">
                {{ $jumlahReports }}
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
          <div class="card card-statistic-1">
            <div class="card-icon bg-danger">
              <i class="fas fa-medkit"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Total Accidents</h4>
              </div>
              <div class="card-body">
                {{ $jumlahAccident}}
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
          <div class="card card-statistic-1">
            <div class="card-icon bg-success">
              <i class="far fa-user"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Total Certifications</h4>
              </div>
              <div class="card-body">
                {{ $jumlahCertification }}
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
          <div class="card card-statistic-1">
            <div class="card-icon bg-success">
              <i class="far fa-file-alt"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Total Safety Officer</h4>
              </div>
              <div class="card-body">
                {{ $jumlahSafetyOfficer }}
              </div>
            </div>
          </div>
        </div>
      </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <center style="padding-top:50px">
                      <img src="public/assets/img/iconsafety.png" class="img-responsive" alt="logo" width="150">
                    </center>
                    <center>
                      <h5>Safety Menu Dashboard</h5>
                    </center>

                    <div class="row" style="padding-top:30px; margin-left:0px; margin-right:0px">
                      <div class="col-lg-6 col-xs-12" id="listsatu">
                        <center>
                        <a href="{{ url('/dailyreport') }}" id="firesystem" class="btn btn-icon icon-left btn-primary" style="width:100%">
                          <div class="col-12 row">
                            <div class="col-1 text-left" style="margin-top:5px"><i class="fa fa-file-alt fa-3x"></i></div>
                            <div class="col-8 text-left" style="margin-left:10px; margin-top:10px"><h3 class="font-bold"> DAILY REPORT</h3></div>
                          </div>
                        </a>
                        </center>
                        <center>
                        <a href="{{ url('/accidentreport') }}" id="firesystem" class="btn btn-icon icon-left btn-primary" style="margin-top:10px; width:100%">
                          <div class="col-12 row">
                            <div class="col-1 text-left" style="margin-top:5px"><i class="fa fa-medkit fa-3x"></i></div>
                            <div class="col-8 text-left" style="margin-left:10px; margin-top:10px"><h3 class="font-bold"> ACCIDENT REPORT</h3></div>
                          </div>
                        </a>
                        </center>
                        <center>
                        <a href="{{ url('/recapitulation') }}" id="firesystem" class="btn btn-icon icon-left btn-primary" style="margin-top:10px; width:100%">
                          <div class="col-12 row">
                            <div class="col-1 text-left" style="margin-top:5px"><i class="fa fa-book fa-3x"></i></div>
                            <div class="col-8 text-left" style="margin-left:10px; margin-top:10px"><h3 class="font-bold"> RECAPITULATION</h3></div>
                          </div>
                        </a>
                        </center>
                      </div>

                      <div class="col-lg-6 col-xs-12" id="listdua">
                        <center>
                        <a href="{{ url('/tools') }}" id="firesystem" class="btn btn-icon icon-left btn-primary" style="width:100%">
                          <div class="col-12 row">
                            <div class="col-1 text-left" style="margin-top:5px"><i class="fa fa-hammer fa-3x"></i></div>
                            <div class="col-8 text-left" style="margin-left:10px; margin-top:10px"><h3 class="font-bold"> TOOLS CERTIFICATION</h3></div>
                          </div>
                        </a>
                        </center>
                        <center>
                        <a href="{{ url('/sio') }}" id="firesystem" class="btn btn-icon icon-left btn-primary" style="margin-top:10px; width:100%">
                          <div class="col-12 row">
                            <div class="col-1 text-left" style="margin-top:5px"><i class="fa fa-user fa-3x"></i></div>
                            <div class="col-8 text-left" style="margin-left:10px; margin-top:10px"><h3 class="font-bold"> SIO CERTIFICATION</h3></div>
                          </div>
                        </a>
                        </center>
                        <center>
                        <a href="{{ url('/dailyreportk3') }}" id="firesystem" class="btn btn-icon icon-left btn-primary" style="margin-top:10px; width:100%">
                          <div class="col-12 row">
                            <div class="col-1 text-left" style="margin-top:5px"><i class="fa fa-file-alt fa-3x"></i></div>
                            <div class="col-8 text-left" style="margin-left:10px; margin-top:10px"><h3 class="font-bold"> DAILY REPORT K3</h3></div>
                          </div>
                        </a>
                        </center>
                    </div>

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped" id="table-1">

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
