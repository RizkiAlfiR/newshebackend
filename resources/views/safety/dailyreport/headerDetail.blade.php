@extends('layouts.app')

@section('content')
<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div> -->
<title>SHE Daily Report &mdash; Safety Hygiene Environment System</title>
<section class="section">
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>List All Detail Daily Report</h4>
                    </div>
                    <!-- <p class="section-lead">
                        <a href="{{ url('dailyreport/create') }}" class="btn btn-icon icon-left btn-primary"><i
                                class="far fa-edit"></i> Create New</a>
                    </p> -->
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped" id="table-unsafedetail">
                                <thead>
                                    <tr>
                                        <th class="text-center">
                                            #
                                        </th>
                                        <th>No. Dokumen</th>
                                        <th>Temuan</th>
                                        <th>Penyebab</th>
                                        <th>Rekomendasi</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data as $p => $item)
                                    <tr>
                                        <td>
                                            {{ $p+1 }}
                                        </td>
                                        <td>{{ $item->no_dokumen }}</td>
                                        <td class="align-middle">
                                            {{ $item->temuan }} ({{ $item->lokasi_kode }} - {{ $item->lokasi_text }})
                                        </td>
                                        <td>
                                            {{ $item->penyebab }}
                                        </td>
                                        <td>{{ $item->rekomendasi }}</td>
                                        <td>
                                            @if($item->status == "OPEN")
                                                <div class="badge badge-danger">{{ $item->status }}</div>
                                            @else
                                                <div class="badge badge-success">{{ $item->status }}</div>
                                            @endif
                                        </td>
                                        <td>
                                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                <button onclick="loadModal(this)" title="" target="/dailyreport/showImage" data="id={{$item->id}}" type="button" class="btn btn-icon icon-left btn-success">
                                                    <i class="fas fa-image"></i>
                                                </button>
                                                <button onclick="loadModal(this)" title="" target="/dailyreport/addFollowUp" data="id={{$item->id}}" type="button" class="btn btn-icon icon-left btn-warning">
                                                    <i class="fas fa-eye"></i>
                                                </button>
                                                <button onclick="loadModal(this)" title="" target="/dailyreport/addMaster" data="id={{$item->id}}" type="button" class="btn btn-icon icon-left btn-info">
                                                    <i class="fas fa-edit"></i>
                                                </button>
                                                <button type="button" onclick="deleteMaster({{$item->id}})" class="btn btn-icon icon-left btn-danger">
                                                    <i class="fas fa-trash"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@section('scripts')
    <script>
        function deleteMaster(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("", "Do you want to remove this data?", function () {
                ajaxTransfer("/dailyreport/deleteMaster", data, "#modal-output");
            })
        }

        function editMaster(event) {
            var button = $(event);
            var data = new FormData();
            // var data = {'id': button.data('id')};
            data.append('id', button.data('id'));

            ajaxTransfer("/dailyreport/addMaster", data, '#modal-output');
        }

        var table = $('#table-unsafedetail');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            // order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });

        var table = $('#table-inspection');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });
    </script>
@endsection
@endsection