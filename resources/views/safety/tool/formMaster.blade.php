<style>
    .modal{
        max-height: calc(150vh - 250px);
        overflow-y: auto;
    }
</style>
<div id="result-form-konten"></div>
    <form onsubmit="return false;" id="form-konten" class='form-horizontal'>
        <table width="100%">
            <tbody>
                <tr>
                    <td class="text-center">
                        <img src="{{asset('public/assets/img')}}/logosemen.png" class="img-responsive" alt="logo" width="80" style="padding-top:20px">
                    </td>
                </tr>
                <tr>
                    <td class="text-center">
                        <h6 class="modal-title" id="title">Form Tools Certification</h6>
                        <h4 class="modal-title" id="title"></h4><small class="font-bold">PT. Semen Indonesia</small>
                    </td>
                </tr>
            </tbody>
        </table>
        <hr>
        <div class="modal-body">
            <div class="row go_form">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Tanggal Mulai :</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fas fa-calendar"></i>
                                </div>
                            </div>
                            <input type="date" class="form-control daterange-cus" name="start_date"
                            @if(!is_null($data))
                                value="{{ $data->start_date}}"
                            @else
                                value="<?php echo date('Y-m-d');?>"
                            @endif>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Tanggal Berakhir :</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fas fa-calendar"></i>
                                </div>
                            </div>
                            <input type="date" class="form-control daterange-cus" name="end_date"
                            @if(!is_null($data))
                                value="{{ $data->end_date }}"
                            @else
                                value="<?php echo date('Y-m-d');?>"
                            @endif>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Waktu Sertifikasi :</label>
                        <div class="col-sm-12">
                            <select class="form-control" name="time_certification">
                                <option <?php if($data->time_certification =='1') {echo "selected";}?> value="1">1 Tahun</option>
                                <option <?php if($data->time_certification =='2') {echo "selected";}?> value="2">2 Tahun</option>
                                <option <?php if($data->time_certification =='3') {echo "selected";}?> value="3">3 Tahun</option>
                                <option <?php if($data->time_certification =='4') {echo "selected";}?> value="4">4 Tahun</option>
                                <option <?php if($data->time_certification =='5') {echo "selected";}?> value="5">5 Tahun</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div style="border-width:1px; border-top:3px; border-style:solid; border-color:red; background-color:red">
                <strong style="background-color:red; margin-left:5px; color:white">Keterangan Sertifikasi</strong>
            </div>
            <div class="modal-body" style="border-width:1px; border-style:solid; border-color:red">
                <div class="row go_form">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Nomor Buku :</label>
                                    <input type="text" class="form-control daterange-cus" value="{{ $data->no_buku}}" name="no_buku">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Nomor Pengesahan :</label>
                                    <input type="text" class="form-control daterange-cus" value="{{ $data->nomor_pengesahan}}" name="nomor_pengesahan">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row go_form">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <label>Lokasi Alat :</label>
                                    <select name="lokasi" class="form-control">
                                    @foreach($data_funloc as $item)
                                        <option value="{{ $item->equpmentcode }}" @if(!is_null($data)) @if($item->equpmentcode == $data->lokasi) selected="selected" @endif @endif>{{ $item->equptname}} - {{ $item->equpmentcode}}</option>
                                
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Data Teknis :</label>
                                    <input type="text" class="form-control daterange-cus" value="{{ $data->data_teknis}}" name="data_teknis">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row go_form">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <label>Unit Kerja Sertfikasi :</label>
                                    <select name="arruk" class="form-control">
                                    @foreach($data_uk as $item)
                                        <option value="{{ $item->muk_nama .'|'. $item->muk_kode }}" @if(!is_null($data)) @if($item->muk_nama == $data->unit_kerja_txt) selected="selected" @endif @endif>{{ $item->muk_kode}} - {{ $item->muk_nama}}</option>
                                        
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Peralatan :</label>
                                    <select name="arrequipment" class="form-control">
                                    @foreach($data_group as $item)
                                        <option value="{{ $item->NAME .'|'. $item->NICKNAME }}" @if(!is_null($data)) @if($item->NAME == $data->equipment) selected="selected" @endif @endif>{{ $item->NAME}}</option>
                                    
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row go_form">
                    <div class="col-md-12">
                        <div class="form-group">
                                <label>File Buku :</label>
                                <div class="column">
                                    <div class="input-field col s6">
                                        <input type="file" id="inputImage" name="file_buku" class="validate" value="{{ $data->file_buku }}">
                                    </div>
                                    <div class="row" style="margin-top:10px">
                                    <div class="col s6">
                                        @if($data->file_buku=='')
                                            <img src="{{asset('public/images')}}/{{'preview-icon.png'}}" id="showImage" style="max-width:330px;max-height:270px;float:left;" />
                                        @else
                                            <img src="{{asset('public/uploads/tools')}}/{{$data->file_buku}}" id="showImage" style="max-width:350px;max-height:270px;float:left;" />
                                        @endif
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="row go_form">
                    <div class="col-md-12">
                        <div class="form-group">
                                <label>File Aktual :</label>
                                <div class="column">
                                    <div class="input-field col s6">
                                        <input type="file" id="inputImage" name="file_aktual" class="validate" value="{{ $data->file_aktual }}">
                                    </div>
                                    <div class="row" style="margin-top:10px">
                                    <div class="col s6">
                                        @if($data->file_aktual=='')
                                            <img src="{{asset('public/images')}}/{{'preview-icon.png'}}" id="showImage" style="max-width:330px;max-height:270px;float:left;" />
                                        @else
                                            <img src="{{asset('public/uploads/tools')}}/{{$data->file_aktual}}" id="showImage" style="max-width:350px;max-height:270px;float:left;" />
                                        @endif
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            <button type="submit" class="btn btn-info" data-loading-text="<i class='fa fa-spinner fa-spin'></i>
                &nbsp;<font class='lowercase'></font>"> <i class="fa fa-check"></i> Save</button>
        </div>

    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/tools/saveMaster', data, '#result-form-konten');
        })
    })
</script>

<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#showImage').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#inputImage").change(function () {
        readURL(this);
    });
</script>
