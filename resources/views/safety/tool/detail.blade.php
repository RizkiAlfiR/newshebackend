@extends('layouts.app')

@section('content')
<title>Tools Certification &mdash; Safety Hygiene Environment System</title>

<section class="section">
    <div class="section-header">
        <h1>Form New Tools Certification</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home') }}">Dashboard</a></div>
            <div class="breadcrumb-item"><a href="{{ url('tools') }}">Tools Certification</a></div>
            @foreach($data as $item)
            <div class="breadcrumb-item">Tools Detail {{$item->id}}</div>
            @endforeach
        </div>
    </div>

    <div class="section-body">

        <form method="POST" action="{{url('/tools/store')}}">
		{{ csrf_field() }}
            <div class="row">
                <div class="col-12 col-md-6 col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            <h4>Fill the form</h4>
                        </div>
                        <div class="card-body">
                            @foreach($data as $item)
                            <div class="form-group">
                                <label>Nomor Buku</label>
                                <input type="text" class="form-control" name="no_buku" placeholder="{{$item->no_buku}}">
                            </div>
                            <div class="form-group">
                                <label>Pilih Peralatan</label>
                                <select class="form-control form-control-sm" name="equipment" id="exampleFormControlSelect3">
                                    <option value="0">{{$item->kode}} - {{$item->equipment}}</option>
                                    @foreach($data_group as $key =>$item)
                                        @if(is_null($item->getParent))
                                            <option value="{{$item->NICKNAME}}">{{$item->NICKNAME}} - {{$item->NAME}}</option>
                                        @else
                                            <option value="{{$item->NICKNAME}}">{{$item->getParent->NICKNAME}}-{{$item->NAME}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Tanggal Mulai Sertifikat</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fas fa-calendar"></i>
                                        </div>
                                    </div>
                                    <input type="date" class="form-control daterange-cus" name="tanggal_mulai" placeholder="{{$item->uji_awal}}">
                                </div>
                            </div>
                            @endforeach
                            @foreach($data as $item)
                            <div class="form-group">
                                <label>Pilih Jangka Waktu</label>
                                <select class="form-control" name="masa_berlaku">
                                    <option value="0">{{$item->time_certification}} Tahun</option>
                                    <option value="1">1 Tahun</option>
                                    <option value="2">2 Tahun</option>
                                    <option value="3">3 Tahun</option>
                                    <option value="4">4 Tahun</option>
                                    <option value="5">5 Tahun</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Data Teknis</label>
                                <input type="text" class="form-control" name="data_teknis" placeholder="{{$item->data_teknis}}">
                            </div>
                            <div class="form-group">
                                <label>Nomor Pengesahan</label>
                                <input type="text" class="form-control" name="no_pengesahan" placeholder="{{$item->nomor_pengesahan}}">
                            </div>
                            <div class="section-title mt-0">Unit Kerja</div>
                            <div class="form-group">
                                <select class="form-control form-control-sm" name="uk" id="exampleFormControlSelect2">
                                    <option value="0">{{$item->uk_kode}} - {{$item->uk_text}}</option>
                                    @foreach($data_uk as $key =>$item)
                                        @if(is_null($item->getParent))
                                            <option value="{{$item->muk_kode}}">{{$item->muk_kode}} - {{$item->muk_nama}}</option>
                                        @else
                                            <option value="{{$item->muk_kode}}">{{$item->getParent->muk_kode}}-{{$item->muk_nama}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="section-title mt-0">Lokasi</div>
                            <div class="form-group">
                                <select class="form-control form-control-sm" name="loc" id="exampleFormControlSelect4">
                                    <option value="0">{{$item->lokasi}}</option>
                                    @foreach($data_funcloc as $key =>$item)
                                        @if(is_null($item->getParent))
                                            <option value="{{$item->equpmentcode}}">{{$item->equpmentcode}}</option>
                                        @else
                                            <option value="{{$item->equpmentcode}}">{{$item->equpmentcode}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-6">
                    @foreach($data as $item)
                    <div class="card">
                        <div class="card-header">
                            <h4>Foto Sertifikat</h4>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label class="form-label">Foto Buku</label>
                                <div class="row gutters-sm">
                                    <label class="imagecheck mb-4">
                                        <figure class="imagecheck-figure">
                                            <img src="{{asset('public/uploads/tools/'.$item->file_buku)}}" alt="}" class="imagecheck-image">
                                        </figure>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label class="form-label">Foto Alat</label>
                                <div class="row gutters-sm">
                                    <label class="imagecheck mb-4">
                                        <figure class="imagecheck-figure">
                                            <img src="{{asset('public/uploads/tools/'.$item->file_aktual)}}" alt="}" class="imagecheck-image">
                                        </figure>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="card-body">
                            <div class="form-group">
                                <label>Upload Foto Buku Here </label>
                                <input type="file" name="foto_buku" class="form-control" id="customFileLang" lang="en">
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label>Upload Foto Alat Here </label>
                                <input type="file" name="foto_alat" class="form-control" id="customFileLang" lang="en">
                            </div>
                        </div> -->
                    </div>
                    @endforeach
                </div>
            </div>
            <hr class="my-4" />
            <button type="reset" class="btn btn-outline-success"></a>Reset</button>
            <button type="submit" class="btn btn-success">Simpan</button>
        </form>
    </div>
</section>
@endsection