<div id="result-form-konten">
    <form onsubmit="return false;" id="form-konten" class='form-horizontal'>
        <table width="100%">
            <tbody>
                <tr>
                    <td class="text-center">
                        <img src="public/assets/img/logosemen.png" class="img-responsive" alt="logo" width="80">
                    </td>
                </tr>
                <tr>
                    <td class="text-center">
                        <h6 class="modal-title" id="title">Form Master Report Category</h6>
                        <h4 class="modal-title" id="title"></h4><small class="font-bold">PT. Semen Indonesia</small>
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="modal-body" style="overflow:auto; height:400px">
            <hr>
            <!-- <form class="form-horizontal" role="form"> -->
                <div class="form-group">
                    <div class="col-sm-10">
                        <input type='hidden' name='_token' value='{{csrf_token()}}'>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="id">ID:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="fid" disabled value="{{ $data->id }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="name">Kode:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="n" name="KODE" value="{{$data->KODE}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="name">Deskripsi:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="n" name="DESKRIPSI" value="{{$data->DESKRIPSI}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="nickname">Kode EN:</label>
                    <!-- <div class="col-sm-10">
                        <input type="text" class="form-control" id="nn" name="plant" value={{$data->plant}}>
                    </div> -->
                    <div class="col-sm-10">
                        <select class="form-control" name="KODE_EN">
                            <option <?php if($data->KODE_EN =='JF') {echo "selected";}?> value="JF">JF</option>
                            <option <?php if($data->KODE_EN =='UC') {echo "selected";}?> value="UC">Unsafe Condition</option>
                            <option <?php if($data->KODE_EN =='UA') {echo "selected";}?> value="UA">Unsafe Action</option>
                        </select>
                    </div>
                </div>
            <!-- </form> -->

            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="submit" class="btn btn-info" data-loading-text="<i class='fa fa-spinner fa-spin'></i>
                    &nbsp;<font class='lowercase'></font>"> <i class="fa fa-check"></i> Save</button>
            </div>
        </div>

        <input type='hidden' name='id' value='{{ $data->id }}'>
        <input type='hidden' name='_token' value='{{ csrf_token() }}'>
    </form>
</div>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/reportcategory/saveMaster', data, '#result-form-konten');
        })
    })
</script>
