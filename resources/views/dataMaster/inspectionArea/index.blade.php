@extends('layouts.app')

@section('content')
<title>Inspection Area | Safety Hygiene Environment System</title>
<section class="section">
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>List All Inspection Area PT. Semen Indonesia, Tbk.</h4>
                    </div>
                    <!-- <p class="section-lead">
                        <a href="{{ url('accidentreportcreate') }}" class="btn btn-icon icon-left btn-primary"><i
                                class="far fa-edit"></i> Create New</a>
                    </p> -->
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped" style="width:100%" id="inspectionArea">
                                <thead>
                                    <tr>
                                        <th class="text-center">No</th>
                                        <th>Plant</th>
                                        <th>Area</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($inspectionArea as $i => $data)
                                    <tr>
                                        <td class="align-middle">{{ $i+1 }}</td>
                                        <td class="align-middle">{{ $data->plant }}</td>
                                        <td class="align-middle">{{ $data->area_name }}</td>
                                        <td class="align-middle">
                                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                <!-- <button type="button" class="btn btn-icon icon-left btn-success">
                                                    <i class="fas fa-eye"></i>
                                                </button>
                                                <button type="button" class="btn btn-icon icon-left btn-info">
                                                    <i class="fas fa-edit"></i>
                                                </button> -->
                                                <button type="button" class="btn btn-icon icon-left btn-danger"
                                                onclick="deleteInspectionArea({{$data->id}})">
                                                    <i class="fas fa-trash"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@section('scripts')
  <script type="text/javascript">
          var table = $('#inspectionArea');
          table.dataTable({
              pageLength: 10,
              responsive: true,
              dom: '<"html5buttons"B>lTfgitp',
              columnDefs: [
                  {"targets": 0, "orderable": false},
                  // {"targets": 1, "visible": false, "searchable": false},
              ],
              order: [[0, "asc"]],
              buttons: [
                  {extend: 'copy'},
                  {extend: 'csv', title: 'Tipe Fasilitas'},
                  {extend: 'excel', title: 'Tipe Fasilitas'},
                  {extend: 'pdf', title: 'Tipe Fasilitas'},
                  {
                      extend: 'print',
                      customize: function (win) {
                          $(win.document.body).addClass('white-bg');
                          $(win.document.body).css('font-size', '10px');

                          $(win.document.body).find('table')
                              .addClass('compact')
                              .css('font-size', 'inherit');
                      }
                  }
              ]
          });
  </script>
    <script>
        function deleteInspectionArea(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Confirmation", "Do you want to remove this data ?", function () {
               ajaxTransfer("/inspectionArea/delete", data, "#modal-output");
            })
        }
    </script>
@endsection

@endsection
