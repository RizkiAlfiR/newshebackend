<style>
    .modal{
        max-height: calc(150vh - 250px);
        overflow-y: auto;
    }
</style>
<div id="result-form-konten"></div>
    <form onsubmit="return false;" id="form-konten" class='form-horizontal'>
        <table width="100%">
            <tbody>
                <tr>
                    <td class="text-center">
                        <img src="public/assets/img/logosemen.png" class="img-responsive" alt="logo" width="80">
                    </td>
                </tr>
                <tr>
                    <td class="text-center">
                        <h6 class="modal-title" id="title">Form Master Planning Plant</h6>
                        <h4 class="modal-title" id="title"></h4><small class="font-bold">PT. Semen Indonesia</small>
                    </td>
                </tr>
            </tbody>
        </table>
        <hr>
        <div class="modal-body">
            <!-- <form class="form-horizontal" role="form"> -->
                <div class="form-group">
                    <label class="control-label col-sm-2" for="id">ID:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" disabled value="{{$data->id}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="nickname">Plant:</label>
                    <!-- <div class="col-sm-10">
                        <input type="text" class="form-control" id="nn" name="plant" value={{$data->plant}}>
                    </div> -->
                    <div class="col-sm-10">
                        <select class="form-control" name="plant">
                            <option <?php if($data->plant =='5001') {echo "selected";}?> value="5001">5001 - Plant Tuban</option>
                            <option <?php if($data->plant =='5002') {echo "selected";}?> value="5002">5002 - Plant Gresik</option>
                            <option <?php if($data->plant =='5003') {echo "selected";}?> value="5003">5003 - Plant Rembang</option>
                            <option <?php if($data->plant =='5004') {echo "selected";}?> value="5004">5004 - Plant Cigading</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="name">Planning Plant:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="n" name="pplant" value="{{$data->pplant}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="name">Planning Plant Desc.:</label>
                    <div class="col-sm-10">
                        <input type="text" name="pplantdesc" value="{{ $data->pplantdesc }}" class="form-control input-sm touchspin">
                    </div>
                </div>
            <!-- </form> -->

            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="submit" class="btn btn-info" data-loading-text="<i class='fa fa-spinner fa-spin'></i>
                    &nbsp;<font class='lowercase'></font>"> <i class="fa fa-check"></i> Save</button>
            </div>
        </div>

        <input type='hidden' name='id' value='{{ $data->id }}'>
        <input type='hidden' name='_token' value='{{ csrf_token() }}'>
    </form>

<script>
$(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/pplant/saveMaster', data, '#result-form-konten');
        })
    })

</script>
