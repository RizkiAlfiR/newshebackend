<div id="result-form-konten">
    <form onsubmit="return false;" id="form-konten" class='form-horizontal'>
        <table width="100%">
            <tbody>
                <tr>
                    <td class="text-center">
                        <img src="public/assets/img/logosemen.png" class="img-responsive" alt="logo" width="80">
                    </td>
                </tr>
                <tr>
                    <td class="text-center">
                        <h6 class="modal-title" id="title">Form Master Functional Location</h6>
                        <h4 class="modal-title" id="title"></h4><small class="font-bold">PT. Semen Indonesia</small>
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="modal-body" style="overflow:auto; height:400px">
            <hr>
            <!-- <form class="form-horizontal" role="form"> -->
                <div class="form-group">
                    <div class="col-sm-10">
                        <input type='hidden' name='_token' value='{{csrf_token()}}'>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="id">ID:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="fid" disabled value="{{ $data->id }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="nickname">ID Equpment:</label>
                    <div class="col-sm-10">
                        <input type="text" name="idequpment" value="{{ $data->idequpment }}" class="form-control input-sm touchspin">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="nickname">Equpment Code:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="nn" name="equptname" value="{{$data->equptname}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="nickname">Equpment Name:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="nn" name="equpmentcode" value="{{$data->equpmentcode}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="name">Planning Plant:</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="pplant">
                            <!-- <option value="0">Choose Group</option> -->
                            @foreach($data_pplant as $item)
                                    <option value="{{ $item->pplant }}" @if(!is_null($data)) @if($item->pplant == $data->pplant) selected="selected" @endif @endif>{{ $item->pplant}} - {{ $item->pplantdesc}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="nickname">Status:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="nn" name="status" value="{{$data->status}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="nickname">Functional Location:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="nn" name="funcloc" value="{{$data->funcloc}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="nickname">Master Plant:</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="mplant">
                            <option <?php if($data->mplant =='2302') {echo "selected";}?> value="2302">2302</option>
                            <option <?php if($data->mplant =='2303') {echo "selected";}?> value="2303">2303</option>
                            <option <?php if($data->mplant =='2304') {echo "selected";}?> value="2304">2304</option>
                            <option <?php if($data->mplant =='2305') {echo "selected";}?> value="2305">2305</option>
                            <option <?php if($data->mplant =='2306') {echo "selected";}?> value="2306">2306</option>
                            <option <?php if($data->mplant =='2307') {echo "selected";}?> value="2307">2307</option>
                        </select>
                    </div>
                </div>
            <!-- </form> -->

            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="submit" class="btn btn-info" data-loading-text="<i class='fa fa-spinner fa-spin'></i>
                    &nbsp;<font class='lowercase'></font>"> <i class="fa fa-check"></i> Save</button>
            </div>
        </div>

        <input type='hidden' name='id' value='{{ $data->id }}'>
        <input type='hidden' name='_token' value='{{ csrf_token() }}'>
    </form>
</div>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/funclocation/saveMaster', data, '#result-form-konten');
        })
    })
</script>
