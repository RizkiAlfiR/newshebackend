<style>
  .modal{
    max-height: calc(150vh - 250px);
    overflow-y: auto;
  }
</style>
<div id="result-form-konten"></div>
    <form onsubmit="return false;" id="form-konten" class='form-horizontal'>
        <table width="100%">
            <tbody>
                <tr>
                  <td class="text-center">
                    <img src="public/assets/img/logosemen.png" class="img-responsive" alt="logo" width="80" style="padding-top:20px">
                  </td>
                </tr>
                <tr>
                  <td class="text-center">
                    <h6 class="modal-title" id="title">Form News</h6>
                    <h4 class="modal-title" id="title"></h4><small class="font-bold">PT. Semen Indonesia</small>
                  </td>
                </tr>
            </tbody>
        </table>
        <div class="modal-body">
          <hr>
            <div class="row go_form">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" class="form-control daterange-cus" name="head" value="{{ $data->head }}">
                    </div>
                    <div class="form-group">
                        <label>Content</label>
                        <textarea rows="4" maxlength="10000" class="form-control input-sm" name="content">{{ $data->content }}</textarea>
                    </div>
                    <div class="form-group">
                        <div class="row" style="margin-top:10px">
                            <div class="input-field col s6">
                              <input type="file" id="inputImage" name="image" class="validate"/ >
                            </div>
                        </div>
                        <div class="row" style="margin-top:10px">
                            <div class="col-12">
                                @if($data->image=='')
                                  <img src="{{asset('images')}}/{{'preview-icon.png'}}" id="showImage" style="max-width:330px;max-height:270px;float:left;" />
                                @else
                                  <img src="{{asset('uploads/News')}}/{{$data->image}}" id="showImage" style="max-width:350px;max-height:270px;float:left;" />
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            <button type="submit" class="btn btn-info" data-loading-text="<i class='fa fa-spinner fa-spin'></i>
              &nbsp;<font class='lowercase'></font>"> <i class="fa fa-check"></i> Save</button>
        </div>

    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/news/save', data, '#result-form-konten');
        })
    })
</script>

<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#showImage').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#inputImage").change(function () {
        readURL(this);
    });
</script>
