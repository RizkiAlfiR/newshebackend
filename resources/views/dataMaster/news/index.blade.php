@extends('layouts.app')

@section('content')
<title>News | Safety Hygiene Environment System</title>
<section class="section">
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                  <div class="col-12" style="padding-top:20px">
                    <div>
                      <div>
                          <div class="">
                                    <h5>List All News</h5>
                                    <p class="">
                                        <a onclick="loadModal(this)" title="" target="/news/add" class="btn btn-icon icon-left btn-primary" style="color:white">
                                          <i class="far fa-edit"></i> Create New</a>
                                    </p>
                                    <div class="">
                                        <div class="table-responsive">
                                            <table class="table table-striped" style="width:100%"  id="table-news">
                                              <thead>
                                                  <tr>
                                                      <th class="align-middle text-center">No</th>
                                                      <th class="align-middle">Title</th>
                                                      <th class="align-middle">Writer</th>
                                                      <th class="align-middle">Date</th>
                                                      <th class="align-middle">Time</th>
                                                      <th class="align-middle">Image</th>
                                                      <th class="align-middle">Action</th>
                                                  </tr>
                                              </thead>
                                              <tbody>
                                                  @foreach($data as $i => $data)
                                                  <tr>
                                                      <td class="align-middle text-center">{{ $i+1 }}</td>
                                                      <td class="align-middle">{{ $data->head}}</td>
                                                      <td class="align-middle">{{ $data->writer }}</td>
                                                      <td class="align-middle">{{ $data->date}}</td>
                                                      <td class="align-middle">{{ $data->time }}</td>
                                                      <td class="align-middle">
                                                        @if(!is_null($data->image))
                                                          <a href="{{asset('uploads/News')}}/{{$data->image}}" target="_blank">
                                                              <img src="{{asset('uploads/News')}}/{{$data->image}}" class="img-responsive" alt="logo" width="80">
                                                          </a>
                                                        @else
                                                          <img src="{{asset('images')}}/{{"preview-icon.png"}}" class="img-responsive" alt="logo" width="80">
                                                        @endif
                                                      </td>
                                                      <td class="align-middle">
                                                          <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                            <button type="button" class="btn btn-icon icon-left btn-info"
                                                            onclick="loadModal(this)" title="" target="/news/add" data="id={{$data->id}}">
                                                                <i class="fas fa-edit"></i>
                                                            </button>
                                                            <button type="button" class="btn btn-icon icon-left btn-danger"
                                                            onclick="deleteNews({{$data->id}})">
                                                                <i class="fas fa-trash"></i>
                                                            </button>
                                                          </div>
                                                      </td>
                                                  </tr>
                                                  @endforeach
                                              </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                          </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@section('scripts')
    <script>
        function deleteNews(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Confirmation", "Do you want to remove this data ?", function () {
               ajaxTransfer("/news/delete", data, "#modal-output");
            })
        }

        var table = $('#table-news');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });
    </script>
@endsection

@endsection
