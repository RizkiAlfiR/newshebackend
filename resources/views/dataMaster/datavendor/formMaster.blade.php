<div id="result-form-konten">
    <form onsubmit="return false;" id="form-konten" class='form-horizontal'>
        <table width="100%">
            <tbody>
                <tr>
                    <td class="text-center">
                        <img src="public/assets/img/logosemen.png" class="img-responsive" alt="logo" width="80">
                    </td>
                </tr>
                <tr>
                    <td class="text-center">
                        <h6 class="modal-title" id="title">Form Master Vendor</h6>
                        <h4 class="modal-title" id="title"></h4><small class="font-bold">PT. Semen Indonesia</small>
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="modal-body" style="overflow:auto; height:400px">
            <hr>
            <!-- <form class="form-horizontal" role="form"> -->
                <div class="form-group">
                    <div class="col-sm-10">
                        <input type='hidden' name='_token' value='{{csrf_token()}}'>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="id">ID:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="fid" disabled value="{{ $data->id }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="nickname">LIFNR:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="nn" name="LIFNR" value="{{$data->LIFNR}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="nickname">Nama Vendor:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="nn" name="NAME1" value="{{$data->NAME1}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="nickname">Kategori Vendor:</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="KATEGORI">
                            <option <?php if($data->KATEGORI =='1') {echo "selected";}?> value="1">1</option>
                            <option <?php if($data->KATEGORI =='2') {echo "selected";}?> value="2">2</option>
                            <option <?php if($data->KATEGORI =='3') {echo "selected";}?> value="3">3</option>
                            <option <?php if($data->KATEGORI =='4') {echo "selected";}?> value="4">4</option>
                            <option <?php if($data->KATEGORI =='5') {echo "selected";}?> value="5">5</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="nickname">Alamat Vendor:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="nn" name="ALAMAT" value="{{$data->ALAMAT}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="nickname">Email Vendor:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="nn" name="EMAIL" value="{{$data->EMAIL}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="nickname">TYV Vendor:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="nn" name="TYV" value="{{$data->TYV}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="nickname">Keterangan:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="nn" name="KETERANGAN" value="{{$data->KETERANGAN}}">
                    </div>
                </div>
            <!-- </form> -->

            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="submit" class="btn btn-info" data-loading-text="<i class='fa fa-spinner fa-spin'></i>
                    &nbsp;<font class='lowercase'></font>"> <i class="fa fa-check"></i> Save</button>
            </div>
        </div>

        <input type='hidden' name='id' value='{{ $data->id }}'>
        <input type='hidden' name='_token' value='{{ csrf_token() }}'>
    </form>
</div>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/datavendor/saveMaster', data, '#result-form-konten');
        })
    })
</script>
