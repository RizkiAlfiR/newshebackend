@extends('layouts.app')

@section('content')
<title>Vendor &mdash; PT. Semen Indonesia, Tbk.</title>

<section class="section">
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>List All Vendor PT. Semen Indonesia, Tbk.</h4>
                    </div>
                    <div class="col-md-4">
                        <button onclick="loadModal(this)" title="" target="/datavendor/addMaster" class="btn btn-icon icon-left btn-primary" type="submit" id="add">
                            <i class="far fa-edit"></i> Add New Data
                        </button>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped" style="width:100%" id="datavendor">
                                <thead>
                                    <tr>
                                        <th class="text-center">
                                            #
                                        </th>
                                        <th>Nama Vendor</th>
                                        <th>Alamat Vendor</th>
                                        <th>Email Vendor</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data as $p => $item)
                                    <tr>
                                        <td>
                                            {{ $p+1 }}
                                        </td>
                                        <td>{{ $item->LIFNR }} - {{ $item->NAME1 }}</td>
                                        @if($item->ALAMAT != null)
                                            <td class="align-middle">
                                                {{ $item->ALAMAT }}
                                            </td>
                                        @else
                                            <td class="align-middle">
                                                -
                                            </td>
                                        @endif
                                        @if($item->EMAIL != null)
                                            <td>
                                                {{ $item->EMAIL }}
                                            </td>
                                        @else
                                            <td class="align-middle">
                                                -
                                            </td>
                                        @endif
                                        <td>
                                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                <button type="button" onclick="loadModal(this)" title="" target="/datavendor/addMaster" data="id={{$item->id}}" class="btn btn-icon icon-left btn-success">
                                                    <i class="fas fa-eye"></i>
                                                </button>
                                                <button onclick="loadModal(this)" title="" target="/datavendor/addMaster" data="id={{$item->id}}" class="btn btn-icon icon-left btn-info">
                                                    <i class="fas fa-edit"></i>
                                                </button>
                                                <button type="button" onclick="deleteMaster({{$item->id}})" class="btn btn-icon icon-left btn-danger">
                                                    <i class="fas fa-trash"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@section('scripts')
    <script>
        function deleteMaster(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("", "Do you want to remove this data?", function () {
                ajaxTransfer("/datavendor/deleteMaster", data, "#modal-output");
            })
        }

        function editInspection(event) {
            var button = $(event);
            var data = new FormData();
            // var data = {'id': button.data('id')};
            data.append('id', button.data('id'));

            ajaxTransfer("/inspectionHydrant/addInspectionHydrant", data, '#modal-output');
        }

        var table = $('#datavendor');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            // order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });

        var table = $('#table-inspection');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });
    </script>
@endsection
@endsection