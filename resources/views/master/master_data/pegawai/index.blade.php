@extends('layouts.app')

@section('content')
<title>Pegawai &mdash; PT. Semen Indonesia, Tbk.</title>

<section class="section">
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>List All Pegawai PT. Semen Indonesia, Tbk.</h4>
                    </div>
                    <!-- <p class="section-lead">
                        <a href="{{ url('accidentreportcreate') }}" class="btn btn-icon icon-left btn-primary"><i
                                class="far fa-edit"></i> Create New</a>
                    </p> -->
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped" id="table-1">
                                <thead>
                                    <tr>
                                        <th class="text-center">
                                            #
                                        </th>
                                        <th>Nama Pegawai</th>
                                        <th>Cost Center</th>
                                        <th>Position</th>
                                        <th>Tanggal Masuk</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($pegawai as $i => $p)
                                    <tr>
                                        <td>
                                            {{ $i+1 }}
                                        </td>
                                        <td>{{ $p->mk_nopeg }} - {{ $p->mk_nama }}</td>
                                        <td class="align-middle">
                                            {{ $p->mk_cttr }} - {{ $p->mk_cttr_text }}
                                        </td>
                                        <td>
                                            {{ $p->mk_employee_emp_subgroup }} - {{ $p->mk_employee_emp_subgroup_text }}
                                        </td>
                                        <td>{{ $p->mk_tgl_masuk }}</td>
                                        <td>
                                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                <button type="button" class="btn btn-icon icon-left btn-success">
                                                    <i class="fas fa-edit"></i>
                                                </button>
                                                <button type="button" class="btn btn-icon icon-left btn-info">
                                                    <i class="fas fa-eye"></i>
                                                </button>
                                                <button type="button" class="btn btn-icon icon-left btn-danger">
                                                    <i class="fas fa-trash"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer py-4">
                            <nav aria-label="...">
                                <ul class="pagination justify-content-end mb-0">
                                    {{ $pegawai->links() }}
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection