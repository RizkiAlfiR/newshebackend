<div id="result-form-konten">
    <form onsubmit="return false;" id="form-konten" class='form-horizontal'>
        <table width="100%">
            <tbody>
                <tr>
                    <td class="text-center">
                        <img src="public/assets/img/logosemen.png" class="img-responsive" alt="logo" width="80">
                    </td>
                </tr>
                <tr>
                    <td class="text-center">
                        <h6 class="modal-title" id="title">Form Master Unit Kerja UNDER CONSTRCUTION</h6>
                        <h4 class="modal-title" id="title"></h4><small class="font-bold">PT. Semen Indonesia</small>
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="modal-body" style="overflow:auto; height:400px">
            <hr>
            <form class="form-horizontal" role="form">
                <div class="form-group">
                    <div class="col-sm-10">
                        <input type='hidden' name='_token' value='{{csrf_token()}}'>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="id">ID:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="fid" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="nickname">Plant:</label>
                    <!-- <div class="col-sm-10">
                        <input type="text" class="form-control" id="nn" name="plant" value={{$data->plant}}>
                    </div> -->
                    <select class="form-control" name="plant">
                        <option value={{$data->plant}}>{{$data->plant}} - {{$data->pplantdesc}}</option>
                        <option value="5001">5001 - Plant Tuban</option>
                        <option value="5002">5002 - Plant Gresik</option>
                        <option value="5003">5003 - Plant Rembang</option>
                        <option value="5004">5004 - Plant Cigading</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="name">Planning Plant:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="n" name="pplant" value={{$data->pplant}}>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="name">Planning Plant Desc.:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="n" name="pplantdesc" value={{$data->pplantdesc}}>
                    </div>
                </div>
            </form>

            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="submit" class="btn btn-info" data-loading-text="<i class='fa fa-spinner fa-spin'></i>
                    &nbsp;<font class='lowercase'></font>"> <i class="fa fa-check"></i> Save</button>
            </div>
        </div>

        <input type='hidden' name='id' value='{{ $data->id }}'>
        <input type='hidden' name='_token' value='{{ csrf_token() }}'>
    </form>
</div>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/unitkerja/saveMaster', data, '#result-form-konten');
        })
    })
</script>
