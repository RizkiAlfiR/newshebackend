@extends('layouts.app')

@section('content')
<title>Inspection Area &mdash; PT. Semen Indonesia, Tbk.</title>

<section class="section">
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>List All Inspection Area PT. Semen Indonesia, Tbk.</h4>
                    </div>
                    <div class="col-md-4">
                        <button onclick="loadModal(this)" title="" target="/inspecarea/addMaster" class="btn btn-icon icon-left btn-primary" type="submit" id="add">
                            <i class="far fa-edit"></i> Add New Data
                        </button>
                    </div>
                    {{ csrf_field() }}
                    <div class="card-body">
                        <div class="table-responsive text-center">
                            <table class="table table-striped" id="table">
                                <thead>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th class="text-center">Area Name</th>
                                        <th class="text-center">Company</th>
                                        <th class="text-center">Plant</th>
                                        <th class="text-center">Actions</th>
                                    </tr>
                                </thead>
                                @foreach($data as $p => $item)
                                <tr class="item{{$item->id}}">
                                    <td>{{$p+1}}</td>
                                    <td>{{$item->AREA_NAME}}</td>
                                    <td>{{$item->COMPANY}}</td>
                                    <td>{{$item->PLANT}}</td>
                                    <td>
                                        <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                            <button type="button" onclick="loadModal(this)" title="" target="/inspecarea/addMaster" data="id={{$item->id}}" class="btn btn-icon icon-left btn-success">
                                                <i class="fas fa-eye"></i>
                                            </button>
                                            <button onclick="loadModal(this)" title="" target="/inspecarea/addMaster" data="id={{$item->id}}" class="btn btn-icon icon-left btn-info">
                                                <i class="fas fa-edit"></i>
                                            </button>
                                            <button type="button" onclick="deleteMaster({{$item->id}})" class="btn btn-icon icon-left btn-danger">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                        <div class="card-footer py-4">
                            <nav aria-label="...">
                                <ul class="pagination justify-content-end mb-0">
                                    {{ $data->links() }}
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@section('scripts')
    <script>
        function deleteMaster(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("", "Do you want to remove this data?", function () {
                ajaxTransfer("/inspecarea/deleteMaster", data, "#modal-output");
            })
        }

        function editInspection(event) {
            var button = $(event);
            var data = new FormData();
            // var data = {'id': button.data('id')};
            data.append('id', button.data('id'));

            ajaxTransfer("/inspectionHydrant/addInspectionHydrant", data, '#modal-output');
        }

        var table = $('#table-master');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });

        var table = $('#table-inspection');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });
    </script>
@endsection
@endsection