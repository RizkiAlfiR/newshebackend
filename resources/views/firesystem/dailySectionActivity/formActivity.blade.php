<style>
  .modal{
    max-height: calc(150vh - 250px);
    overflow-y: auto;
  }
</style>
<div id="result-form-konten"></div>
    <form onsubmit="return false;" id="form-konten" class='form-horizontal'>
        <table width="100%">
            <tbody>
                <tr>
                  <td class="text-center">
                    <img src="public/assets/img/logosemen.png" class="img-responsive" alt="logo" width="80" style="padding-top:20px">
                  </td>
                </tr>
                <tr>
                  <td class="text-center">
                    <h6 class="modal-title" id="title">Form Daily Section Activity</h6>
                    <h4 class="modal-title" id="title"></h4><small class="font-bold">PT. Semen Indonesia</small>
                  </td>
                </tr>
            </tbody>
        </table>
        <div class="modal-body">
          <hr>
            <div class="row go_form">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                          <div class="col-sm-7">
                              <label>Date Activity</label>
                              <div class="input-group">
                                  <input type="date" class="form-control daterange-cus" name="date_activity"
                                  @if(!is_null($data))
                                    value="{{ $data->date_activity}}"
                                  @else
                                    value="<?php echo date('Y-m-d')?>"
                                  @endif>
                              </div>
                          </div>
                          <div class="col-sm-5">
                              <label>Type Action</label>
                              <select name="type_action" class="form-control">
                                  <option <?php if($data->type_action =='SIAGA') {echo "selected";}?> value="SIAGA">SIAGA</option>
                                  <option <?php if($data->type_action =='INSPEKSI') {echo "selected";}?> value="INSPEKSI">INSPEKSI</option>
                                  <option <?php if($data->type_action =='PEMADAMAN') {echo "selected";}?> value="PEMADAMAN">PEMADAMAN</option>
                              </select>
                          </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Time Sheet</label>
                        <div class="row">
                            <div class="col-sm-6">
                                <label>Start</label>
                                <div class="input-group">
                                    <input type="time" class="form-control daterange-cus" name="timesheet_start"
                                    @if(!is_null($data))
                                      value="{{ $data->timesheet_start }}"
                                    @else
                                      value="<?php echo date('H-i-s');?>"
                                    @endif>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label>End</label>
                                <div class="input-group">
                                    <input type="time" class="form-control daterange-cus" name="timesheet_end"
                                    @if(!is_null($data))
                                      value="{{ $data->timesheet_end }}"
                                    @else
                                      value="<?php echo date('H-i-s');?>"
                                    @endif>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Activity</label>
                        <textarea rows="2" maxlength="255" class="form-control input-sm" name="daily_activity" style="resize: vertical;">{{ $data->daily_activity }}</textarea>
                    </div>
                    <div class="form-group">
                        <label>Note</label>
                        <textarea rows="2" maxlength="255" class="form-control input-sm" name="note" style="resize: vertical;">{{ $data->note }}</textarea>
                    </div>
                    <div class="form-group">
                        <div class="row" style="margin-top:10px">
                            <div class="input-field col s6">
                              <input type="file" id="inputImage" name="image" class="validate"/ >
                              <!-- <input type="file" id="inputTemuan" name="climbing_tool_picture" class="validate"/ > -->
                            </div>
                        </div>
                        <div class="row" style="margin-top:10px">
                            <div class="col-12">
                                @if($data->image=='')
                                  <img src="{{asset('public/images')}}/{{'preview-icon.png'}}" id="showImage" style="max-width:330px;max-height:270px;float:left;" />
                                @else
                                  <img src="{{asset('public/uploads/DailySectionReport')}}/{{$data->image}}" id="showImage" style="max-width:350px;max-height:270px;float:left;" />
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Shift</label>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="checkbox checkbox-primary">
                                    <input id="shift1" type="checkbox" name="shift1" value="V" <?php if($data->shift1 =='V') {echo "checked";}?>>
                                    <label for="shift1">Shift 1</label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="checkbox checkbox-primary">
                                    <input id="shift2" type="checkbox" name="shift2" value="V" <?php if($data->shift2 =='V') {echo "checked";}?>>
                                    <label for="shift2">Shift 2</label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="checkbox checkbox-primary">
                                    <input id="shift3" type="checkbox" name="shift3" value="V" <?php if($data->shift3 =='V') {echo "checked";}?>>
                                    <label for="shift3">Shift 3</label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                        </div>
                    </div>
                    <div class="form-group" style="margin-top:20px">
                        <label>PIC</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fas fa-user"></i>
                                </div>
                            </div>
                            <input type="text" class="form-control daterange-cus" name="pic" value="{{ $data->pic }}">
                        <!-- <input readonly type="text" class="form-control daterange-cus" name="location_name" value=""> -->
                        </div>
                    </div>

                    <div class="form-group">
                      <div class="nav-wrapper">
                          <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-form" role="tablist">
                              <li class="nav-item">
                                  <a class="nav-link mb-sm-3 mb-md-0 active" id="tabs-form-1-tab" data-toggle="tab" href="#tabs-form-1" role="tab" aria-controls="tabs-form-1" aria-selected="true"><i class="ni ni-cloud-upload-96 mr-2"></i>Officer</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link mb-sm-3 mb-md-0" id="tabs-form-2-tab" data-toggle="tab" href="#tabs-form-2" role="tab" aria-controls="tabs-form-2" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>Tools</a>
                              </li>
                          </ul>
                      </div>
                      <div>
                        <div>
                            <div class="tab-content" id="myTabForm">
                                <div class="tab-pane fade show active" id="tabs-form-1" role="tabpanel" aria-labelledby="tabs-form-1-tab">
                                  <div class="">
                                      <div class="row">
                                          <div class="col-7">
                                              <label>Officer</label>
                                          </div>
                                          <div class="col-3">
                                              <label>Shift</label>
                                          </div>
                                          <div class="col-2">
                                              <label>PIC</label>
                                          </div>
                                      </div>
                                      <hr>
                                      @foreach($officerFilter as $item)
                                          <div class="row">
                                              <div class="col-7">
                                                  {{ $item->officer}}
                                              </div>
                                              <div class="col-3">
                                                  Shift {{ $item->shift}}
                                              </div>
                                              <div class="col-2">
                                                  {{ $item->status_pic}}
                                              </div>
                                          </div>
                                      @endforeach
                                  </div>
                                </div>
                                <div class="tab-pane fade" id="tabs-form-2" role="tabpanel" aria-labelledby="tabs-form-2-tab">
                                  <div class="">
                                      <label>List Tools</label>
                                      <div>
                                      </div>
                                  </div>
                                </div>
                            </div>
                          </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            <button type="submit" class="btn btn-info" data-loading-text="<i class='fa fa-spinner fa-spin'></i>
              &nbsp;<font class='lowercase'></font>"> <i class="fa fa-check"></i> Save</button>
        </div>

    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/dailySectionActivity/saveActivity', data, '#result-form-konten');
        })
    })
</script>

<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#showImage').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#inputImage").change(function () {
        readURL(this);
    });
</script>
