<link rel="stylesheet" href="{{asset('assets/modules/bootstrap-toggle/css/bootstrap4-toggle.min.css')}}">
<script src="{{asset('assets/')}}/modules/bootstrap-toggle/js/bootstrap4-toggle.min.js"></script>
<div id="result-form-konten"></div>
    <form onsubmit="return false;" id="form-konten" class='form-horizontal'>
        <table width="100%">
            <tbody>
                <tr>
                  <td class="text-center">
                    <img src="public/assets/img/logosemen.png" class="img-responsive" alt="logo" width="80" style="padding-top:20px">
                  </td>
                </tr>
                <tr>
                  <td class="text-center">
                    <h6 class="modal-title" id="title">Form Officer</h6>
                    <h4 class="modal-title" id="title"></h4><small class="font-bold">PT. Semen Indonesia</small>
                  </td>
                </tr>
            </tbody>
        </table>
        <div class="modal-body">
            <hr>
            <div class="row go_form">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Tanggal</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fas fa-calendar"></i>
                                </div>
                            </div>
                            <input type="date" class="form-control daterange-cus" name="date_activity" value="{{ $data->date_activity}}" required="">
                        </div>
                    </div>
                    <!-- <div class="row go_form">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Status PIC</label>
                                <input type="checkbox" checked data-toggle="toggle" data-on="PIC" data-off="Non PIC" name="status_pic">
                            </div>
                        </div>
                    </div> -->
                    <div class="form-group">
                        <label>Status PIC</label>
                        <div class="row">
                          <div class="col-sm-6">
                            <div class="radio radio-info" style="margin:6px 0">
                              <input type="radio" name="status_pic" <?php if($data->status_pic =='V') {echo "checked";}?> value="V">
                              <label for="casing1">PIC</label>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="radio radio-info" style="margin:6px 0">
                              <input type="radio" name="status_pic" <?php if($data->status_pic =='X') {echo "checked";}?> value="X">
                              <label for="casing2">Non PIC</label>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                      <label>Shift :</label>
                      <select name="shift" class="form-control">
                          <option <?php if($data->shift =='1') {echo "selected";}?> value="1">Shift 1</option>
                          <option <?php if($data->shift =='2') {echo "selected";}?> value="2">Shift 2</option>
                          <option <?php if($data->shift =='3') {echo "selected";}?> value="3">Shift 3</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label>Officer</label>
                      <div class="input-group">
                          <div class="input-group-prepend">
                              <div class="input-group-text">
                                  <i class="fas fa-user"></i>
                              </div>
                          </div>
                          <input type="text" class="form-control daterange-cus" name="officer" value="{{$data->officer}}">
                      </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            <button type="submit" class="btn btn-info" data-loading-text="<i class='fa fa-spinner fa-spin'></i>
              &nbsp;<font class='lowercase'></font>"> <i class="fa fa-check"></i> Save</button>
        </div>

    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/dailySectionActivity/saveOfficer', data, '#result-form-konten');
        })
    })
</script>
