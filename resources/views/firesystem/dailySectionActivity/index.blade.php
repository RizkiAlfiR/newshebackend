@extends('layouts.app')
@section('content')

<style>
.btnOfficer{
  border-radius:0px;
  border-top-right-radius: 10px;
  border-bottom-right-radius: 10px;
  margin-right: 10px;
},
</style>

<title>Daily Section | Safety Hygiene Environment System</title>
<section class="section">
  <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-12">
            <div class="card card-statistic-2">
                <div class="card-stats">
                    <div class="card-stats-title">Shift 1
                    </div>
                </div>
                @foreach($officerShift1 as $i => $data)
                  @if(is_null($data))
                  <Text>gfhfgh</Text>
                  @endif
                  @if($data->status_pic == 'V')
                    <p class="">
                        <a class="btn btn-icon icon-left btn-primary btnOfficer" style="color:white"><i class="far fa-user"></i></a>
                        <Text>{{ $data->officer }}<Text>
                    </p>
                  @else
                    <p class="">
                        <a class="btn btn-icon icon-left btn-info btnOfficer" style="color:white"><i class="far fa-user"></i></a>
                        <Text>{{ $data->officer }}<Text>
                    </p>
                  @endif
                @endforeach
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12">
            <div class="card card-statistic-2">
                <div class="card-stats">
                    <div class="card-stats-title">Shift 2
                    </div>
                </div>
                @foreach($officerShift2 as $i => $data)
                  @if($data->status_pic == 'V')
                    <p class="">
                        <a class="btn btn-icon icon-left btn-primary btnOfficer" style="color:white"><i class="far fa-user"></i></a>
                        <Text>{{ $data->officer }}<Text>
                    </p>
                  @else
                    <p class="">
                        <a class="btn btn-icon icon-left btn-info btnOfficer" style="color:white"><i class="far fa-user"></i></a>
                        <Text>{{ $data->officer }}<Text>
                    </p>
                  @endif
                @endforeach
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12">
            <div class="card card-statistic-2">
                <div class="card-stats">
                    <div class="card-stats-title">Shift 3
                    </div>
                </div>
                @foreach($officerShift3 as $i => $data)
                  @if($data->status_pic == 'V')
                    <p class="">
                        <a class="btn btn-icon icon-left btn-primary btnOfficer" style="color:white"><i class="far fa-user"></i></a>
                        <Text>{{ $data->officer }}<Text>
                    </p>
                  @else
                    <p class="">
                        <a class="btn btn-icon icon-left btn-info btnOfficer" style="color:white"><i class="far fa-user"></i></a>
                        <Text>{{ $data->officer }}<Text>
                    </p>
                  @endif
                @endforeach
            </div>
        </div>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                  <div class="col-12" style="padding-top:20px">
                    <div class="nav-wrapper">
                        <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0 active" id="tabs-icons-text-1-tab" data-toggle="tab" href="#tabs-icons-text-1" role="tab" aria-controls="tabs-icons-text-1" aria-selected="true"><i class="ni ni-cloud-upload-96 mr-2"></i>Officer</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-2-tab" data-toggle="tab" href="#tabs-icons-text-2" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>Daily Section Activity</a>
                            </li>
                        </ul>
                    </div>
                    <div>
                      <div>
                          <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
                              <div class="">
                                  <h5>Officer</h5>
                                  <p class="">
                                      <a onclick="loadModal(this)" title="" target="/dailySectionActivity/addOfficer" class="btn btn-icon icon-left btn-primary" style="color:white">
                                        <i class="far fa-edit"></i> Create New</a>
                                  </p>
                              </div>

                              <div style="margin-top:20px">
                                  <div class="">
                                      <div class="table-responsive">
                                          <table class="table table-striped" style="width:100%" id="table-officer" >
                                              <thead>
                                                  <tr>
                                                      <th class="text-center">No</th>
                                                      <th>Shift</th>
                                                      <th>Date Activity</th>
                                                      <th>Name</th>
                                                      <th>PIC</th>
                                                      <th>Action</th>
                                                  </tr>
                                              </thead>
                                              <tbody>
                                                @foreach($officer as $i => $data)
                                                <tr>
                                                    <td class="text-center">{{ $i+1 }}</td>
                                                    <td>Shift {{ $data->shift }}</td>
                                                    <td>{{ $data->date_activity}}</td>
                                                    <td>{{ $data->officer }}</td>
                                                    <td>
                                                      @if($data->status_pic == "V")
                                                        <button type="button" class="btn btn-icon icon-left btn-success">
                                                            YES
                                                        </button>
                                                      @else
                                                        <button type="button" class="btn btn-icon icon-left btn-danger">
                                                            NO
                                                        </button>
                                                      @endif
                                                    </td>
                                                    <td>
                                                      <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                          <button type="button" class="btn btn-icon icon-left btn-info"
                                                          onclick="loadModal(this)" title="" target="/dailySectionActivity/addOfficer" data="id={{$data->id}}">
                                                              <i class="fas fa-edit"></i>
                                                          </button>
                                                          <button type="button" class="btn btn-icon icon-left btn-danger"
                                                          onclick="deleteOfficer({{$data->id}})">
                                                              <i class="fas fa-trash"></i>
                                                          </button>
                                                      </div>
                                                    </td>
                                                </tr>
                                                @endforeach
                                              </tbody>
                                          </table>
                                      </div>
                                  </div>
                              </div>
                            </div>

                              <div class="tab-pane fade" id="tabs-icons-text-2" role="tabpanel" aria-labelledby="tabs-icons-text-2-tab">
                                <div class="">
                                    <h5>List Daily Section Activity</h5>
                                    <p class="">
                                        <a onclick="loadModal(this)" title="" target="/dailySectionActivity/addActivity" class="btn btn-icon icon-left btn-primary" style="color:white">
                                          <i class="far fa-edit"></i> Create New</a>
                                    </p>
                                    <div class="">
                                        <div class="table-responsive">
                                            <table class="table table-striped" style="width:100%" id="table-activity">
                                              <thead>
                                                  <tr>
                                                      <th rowspan="2" class="text-center align-middle">No</th>
                                                      <th rowspan="2" class="align-middle">Date</th>
                                                      <th rowspan="2" class="align-middle">Aktivity</th>
                                                      <th rowspan="2" class="align-middle">Type</th>
                                                      <th colspan="3" class="text-center">Shift</th>
                                                      <th rowspan="2" class="align-middle">Status</th>
                                                      <th rowspan="2" class="align-middle">PIC</th>
                                                      <th colspan="2" class="text-center">Time Sheet</th>
                                                      <th rowspan="2" class="align-middle">Action</th>
                                                  </tr>
                                                  <tr>
                                                      <th class="text-center">1</th>
                                                      <th class="text-center">2</th>
                                                      <th class="text-center">3</th>
                                                      <th class="text-center">Start</th>
                                                      <th class="text-center">End</th>
                                                  </tr>
                                              </thead>
                                              <tbody>
                                                @foreach($dailySectionActivity as $i => $data)
                                                <tr>
                                                    <td class="text-center">{{ $i+1 }}</td>
                                                    <td>{{ $data->date_activity }}</td>
                                                    <td>{{ $data->daily_activity }}</td>
                                                    <td>{{ $data->type_action }}</td>
                                                    <td>
                                                    @if($data->shift1 == "V")
                                                      <button type="button" class="btn btn-icon icon-left btn-success">
                                                            V
                                                      </button>
                                                    @endif
                                                    </td>
                                                    <td>
                                                    @if($data->shift2 == "V")
                                                      <button type="button" class="btn btn-icon icon-left btn-success">
                                                            V
                                                      </button>
                                                    @endif
                                                    </td>
                                                    <td>
                                                    @if($data->shift3 == "V")
                                                      <button type="button" class="btn btn-icon icon-left btn-success">
                                                            V
                                                      </button>
                                                    @endif
                                                    </td>
                                                    <td>
                                                    @if($data->status == "OPEN")
                                                      <button type="button" class="btn btn-icon icon-left btn-success">
                                                          O
                                                      </button>
                                                    @else
                                                      <button type="button" class="btn btn-icon icon-left btn-danger">
                                                          C
                                                      </button>
                                                    @endif
                                                    </td>
                                                    <td>{{ $data->pic }}</td>
                                                    <td>{{ $data->timesheet_start }}</td>
                                                    <td>{{ $data->timesheet_end }}</td>
                                                    <td>
                                                      <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                          <button type="button" class="btn btn-icon icon-left btn-info"
                                                          onclick="loadModal(this)" title="" target="/dailySectionActivity/addActivity" data="id={{$data->id}}">
                                                              <i class="fas fa-edit"></i>
                                                          </button>
                                                          <button type="button" class="btn btn-icon icon-left btn-danger"
                                                          onclick="deleteActivity({{$data->id}})">
                                                              <i class="fas fa-trash"></i>
                                                          </button>
                                                      </div>
                                                    </td>
                                                </tr>
                                                @endforeach
                                              </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                              </div>
                          </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@section('scripts')
  <!-- <script>
    (document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/inspectionHydrant/saveInspectionHydrant', data, '#result-form-konten');
        })
    })
  </script> -->
  <script type="text/javascript">
  var table = $('#table-officer');
          table.dataTable({
              pageLength: 10,
              responsive: true,
              dom: '<"html5buttons"B>lTfgitp',
              columnDefs: [
                  {"targets": 0, "orderable": false},
                  // {"targets": 1, "visible": false, "searchable": false},
              ],
              order: [[0, "asc"]],
              buttons: [
                  {extend: 'copy'},
                  {extend: 'csv', title: 'Tipe Fasilitas'},
                  {extend: 'excel', title: 'Tipe Fasilitas'},
                  {extend: 'pdf', title: 'Tipe Fasilitas'},
                  {
                      extend: 'print',
                      customize: function (win) {
                          $(win.document.body).addClass('white-bg');
                          $(win.document.body).css('font-size', '10px');

                          $(win.document.body).find('table')
                              .addClass('compact')
                              .css('font-size', 'inherit');
                      }
                  }
              ]
          });

          var table = $('#table-activity');
                  table.dataTable({
                      pageLength: 10,
                      responsive: true,
                      dom: '<"html5buttons"B>lTfgitp',
                      columnDefs: [
                          {"targets": 0, "orderable": false},
                          // {"targets": 1, "visible": false, "searchable": false},
                      ],
                      order: [[0, "asc"]],
                      buttons: [
                          {extend: 'copy'},
                          {extend: 'csv', title: 'Tipe Fasilitas'},
                          {extend: 'excel', title: 'Tipe Fasilitas'},
                          {extend: 'pdf', title: 'Tipe Fasilitas'},
                          {
                              extend: 'print',
                              customize: function (win) {
                                  $(win.document.body).addClass('white-bg');
                                  $(win.document.body).css('font-size', '10px');

                                  $(win.document.body).find('table')
                                      .addClass('compact')
                                      .css('font-size', 'inherit');
                              }
                          }
                      ]
                  });
  </script>
    <script>
        function deleteOfficer(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Confirmation", "Do you want to remove this data?", function () {
               ajaxTransfer("/dailySectionActivity/deleteOfficer", data, "#modal-output");
            })
        }
        function deleteActivity(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Confirmation", "Do you want to remove this data?", function () {
                ajaxTransfer("/dailySectionActivity/deleteActivity", data, "#modal-output");
            })
        }
    </script>
@endsection

@endsection
