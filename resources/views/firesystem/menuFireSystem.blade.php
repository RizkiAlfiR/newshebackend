@extends('layouts.app')

@section('content')
<title>Fire System | Safety Hygiene Environment System</title>
<section class="section">
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <center style="padding-top:50px">
                      <img src="public/assets/img/iconFireSystem.png" class="img-responsive" alt="logo" width="150">
                    </center>
                    <center style="padding-top:20px">
                      <h5>Fire System</h5>
                    </center>

                    <div class="row" style="padding-top:30px; margin-left:0px; margin-right:0px">
                      <div class="col-lg-6 col-xs-12" id="listsatu">
                        <center>
                        <a href="{{ url('/menuDailySection') }}" id="firesystem" class="btn btn-icon icon-left btn-primary" style="width:100%">
                          <div class="col-12 row">
                            <div class="col-1 text-left" style="margin-top:5px"><i class="fa fa-file fa-3x"></i></div>
                            <div class="col-8 text-left" style="margin-left:10px; margin-top:10px"><h3 class="font-bold"> DAILY REPORT SECTION</h3></div>
                          </div>
                        </a>
                        </center>
                        <center>
                        <a href="{{ url('/fireReport') }}" id="firesystem" class="btn btn-icon icon-left btn-primary" style="margin-top:10px; width:100%">
                          <div class="col-12 row">
                            <div class="col-1 text-left" style="margin-top:5px"><i class="fa fa-fire fa-3x"></i></div>
                            <div class="col-8 text-left" style="margin-left:10px; margin-top:10px"><h3 class="font-bold"> FIRE REPORT</h3></div>
                          </div>
                        </a>
                        </center>
                        <center>
                        <a href="{{ url('/unitCheckDashboard') }}" id="firesystem" class="btn btn-icon icon-left btn-primary" style="margin-top:10px; width:100%">
                          <div class="col-12 row">
                            <div class="col-1 text-left" style="margin-top:5px"><i class="fa fa-desktop fa-3x"></i></div>
                            <div class="col-8 text-left" style="margin-left:10px; margin-top:10px"><h3 class="font-bold"> UNIT CHECK DASHBOARD</h3></div>
                          </div>
                        </a>
                        </center>
                      </div>

                      <div class="col-lg-6 col-xs-12" id="listdua">
                        <center>
                        <a href="{{ url('/menuInspection') }}" id="firesystem" class="btn btn-icon icon-left btn-primary" style="width:100%">
                          <div class="col-12 row">
                            <div class="col-1 text-left" style="margin-top:5px"><i class="fa fa-fire-extinguisher fa-3x"></i></div>
                            <div class="col-8 text-left" style="margin-left:10px; margin-top:10px"><h3 class="font-bold"> INSPECTION REPORT</h3></div>
                          </div>
                        </a>
                        </center>
                        <center>
                        <a href="{{ url('/unitCheck') }}" id="firesystem" class="btn btn-icon icon-left btn-primary" style="margin-top:10px; width:100%">
                          <div class="col-12 row">
                            <div class="col-1 text-left" style="margin-top:5px"><i class="fa fa-car fa-3x"></i></div>
                            <div class="col-8 text-left" style="margin-left:10px; margin-top:10px"><h3 class="font-bold"> UNIT CHECK</h3></div>
                          </div>
                        </a>
                        </center>
                        <center>
                        <a href="{{ url('/lotto') }}" id="firesystem" class="btn btn-icon icon-left btn-primary" style="margin-top:10px; width:100%">
                          <div class="col-12 row">
                            <div class="col-1 text-left" style="margin-top:5px"><i class="fa fa-lock fa-3x"></i></div>
                            <div class="col-8 text-left" style="margin-left:10px; margin-top:10px"><h3 class="font-bold"> LOTTO</h3></div>
                          </div>
                        </a>
                        </center>
                    </div>

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped" id="table-1">

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
