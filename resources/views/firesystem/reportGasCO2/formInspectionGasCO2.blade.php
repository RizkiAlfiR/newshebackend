<div id="result-form-konten"></div>
    <form onsubmit="return false;" id="form-konten" class='form-horizontal'>
        <table width="100%">
            <tbody>
                <tr>
                  <td class="text-center">
                    <img src="public/assets/img/logosemen.png" class="img-responsive" alt="logo" width="80" style="padding-top:20px">
                  </td>
                </tr>
                <tr>
                  <td class="text-center">
                    <h6 class="modal-title" id="title">Form Inspection Gas CO2</h6>
                    <h4 class="modal-title" id="title"></h4><small class="font-bold">PT. Semen Indonesia</small>
                  </td>
                </tr>
            </tbody>
        </table>
        <div class="modal-body">
          <hr>
            <div class="row go_form">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Inspection Date</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fas fa-calendar"></i>
                                </div>
                            </div>
                            <input type="date" class="form-control daterange-cus" name="inspection_date"
                            @if(!is_null($data))
                              value="{{ $data->inspection_date}}"
                            @else
                              value="<?php echo date('Y-m-d');?>"
                            @endif>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Inspection Time</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fas fa-clock"></i>
                                </div>
                            </div>
                            <input type="time" class="form-control daterange-cus" name="inspection_time"
                            @if(!is_null($data))
                              value="{{ $data->inspection_time }}"
                            @else
                              value="<?php echo date('H:i:s');?>"
                            @endif>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Shift</label>
                        <select name="shift" class="form-control">
                            <option <?php if($data->shift =='1') {echo "selected";}?> value="1">Shift 1</option>
                            <option <?php if($data->shift =='2') {echo "selected";}?> value="2">Shift 2</option>
                            <option <?php if($data->shift =='3') {echo "selected";}?> value="3">Shift 3</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                      <label>Plant Area</label>
                      <select name="pplant" class="form-control">
                          @foreach($pplant as $item)
                          <option value="{{ $item->plant.'|'.$item->pplant.'|'.$item->pplantdesc }}" @if(!is_null($data)) @if($item->pplant == $data->pplant) selected="selected" @endif @endif>{{ $item->pplantdesc}}</option>
                          @endforeach
                      </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Residual Cemetron</label>
                        <input type="number" name="total_residual_cemetron" value="{{$data->total_residual_cemetron}}" min="0" max="6" class="form-control input-sm touchspin">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Residual Samator</label>
                        <input type="number" name="total_residual_samator" value="{{$data->total_residual_samator}}" min="0" max="19" class="form-control input-sm touchspin">
                    </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                      <label>Note</label>
                      <textarea rows="2" maxlength="255" class="form-control input-sm" name="note" style="resize: vertical;">{{ $data->note }}</textarea>
                  </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            <button type="submit" class="btn btn-info" data-loading-text="<i class='fa fa-spinner fa-spin'></i>
              &nbsp;<font class='lowercase'></font>"> <i class="fa fa-check"></i> Save</button>
        </div>
    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/reportGasCO2/saveInspectionGasCO2', data, '#result-form-konten');
        })
    })
</script>
