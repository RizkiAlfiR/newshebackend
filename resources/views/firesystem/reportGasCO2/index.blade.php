@extends('layouts.app')

@section('content')
<title>Gas CO2 | Safety Hygiene Environment System</title>
<section class="section">
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                  <div class="col-12" style="padding-top:20px">
                    <div class="nav-wrapper">
                        <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0 active" id="tabs-icons-text-1-tab" data-toggle="tab" href="#tabs-icons-text-1" role="tab" aria-controls="tabs-icons-text-1" aria-selected="true"><i class="ni ni-cloud-upload-96 mr-2"></i>Master Gas CO2</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-2-tab" data-toggle="tab" href="#tabs-icons-text-2" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>Inspection Gas CO2</a>
                            </li>
                        </ul>
                    </div>
                    <div>
                      <div>
                          <div class="tab-content" id="myTabContent">
                              <div class="tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">

                                <div class="">
                                    <h5>List Master Gas CO2</h5>
                                    <p class="">
                                        <a onclick="loadModal(this)" title="" target="/reportGasCO2/addGasCO2" class="btn btn-icon icon-left btn-primary" style="color:white">
                                          <i class="far fa-edit"></i> Create New</a>
                                    </p>
                                    <div class="">
                                        <div class="table-responsive">
                                            <table class="table table-striped" style="width:100%" id="masterGas">
                                                <thead>
                                                    <tr>
                                                        <th rowspan="2" class="align-middle" class="text-center">No</th>
                                                        <th rowspan="2" class="align-middle">Plant</th>
                                                        <th rowspan="2" class="align-middle">PPlant</th>
                                                        <th rowspan="2" class="align-middle">Area PPlant</th>
                                                        <th colspan="2" class="text-center">Type Gas</th>
                                                        <th rowspan="2" class="align-middle">Action</th>
                                                    </tr>
                                                    <tr>
                                                        <th class="text-center">Cemetron</th>
                                                        <th class="text-center">Samator</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($gasCO2 as $i => $data)
                                                    <tr>
                                                        <td class="align-middle text-center">{{ $i+1 }}</td>
                                                        <td class="align-middle">PT Semen Indonesia</td>
                                                        <td class="align-middle">
                                                            @if($data->plant == "5001")
                                                            	Tuban
                                                            @elseif($data->plant == "5002")
                                                            	Gresik
                                                            @elseif($data->plant == "5003")
                                                            	Rembang
                                                            @else
                                                              Cigading
                                                            @endif
                                                        </td>
                                                        <td class="align-middle">{{ $data->pplantdesc }}</td>
                                                        <td class="align-middle text-center">
                                                            @if($data->cemetron == "Cemetron")
                                                              <button type="button" class="btn btn-icon icon-left btn-success">
                                                                  V
                                                              </button>
                                                            @endif
                                                        </td>
                                                        <td class="align-middle text-center">
                                                            @if($data->samator == "Samator")
                                                              <button type="button" class="btn btn-icon icon-left btn-success">
                                                                  V
                                                              </button>
                                                            @endif
                                                        </td>
                                                        <td class="align-middle">
                                                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                                <button type="button" class="btn btn-icon icon-left btn-danger"
                                                                onclick="deleteGasCO2({{$data->id}})">
                                                                    <i class="fas fa-trash"></i>
                                                                </button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                              </div>
                              <div class="tab-pane fade" id="tabs-icons-text-2" role="tabpanel" aria-labelledby="tabs-icons-text-2-tab">
                                <div class="">
                                    <h5>Inspection Gas CO2</h5>
                                    <p class="">
                                        <a onclick="loadModal(this)" title="" target="/reportGasCO2/addInspectionGasCO2" class="btn btn-icon icon-left btn-primary" style="color:white">
                                          <i class="far fa-edit"></i> Create New</a>
                                    </p>
                                    <div class="">
                                        <div class="table-responsive">
                                            <table class="table table-striped" style="width:100%" id="inspectionGas">
                                              <thead>
                                                  <tr>
                                                      <th class="text-center">No</th>
                                                      <th class="text-center">Inspection Date</th>
                                                      <th class="text-center">Area Plant</th>
                                                      <th class="text-center">R Cemetron</th>
                                                      <th class="text-center">R Samator</th>
                                                      <th class="text-center">PIC</th>
                                                      <th class="text-center">Action</th>
                                                  </tr>
                                              </thead>
                                              <tbody>
                                                  @foreach($inspectionGas as $i => $data)
                                                  <tr>
                                                      <td>{{ $i+1 }}</td>
                                                      <td>{{ $data->inspection_date }}</td>
                                                      <td>{{ $data->pplantdesc }}</td>
                                                      <td class="text-center">
                                                        @if($data->total_residual_cemetron <= 4)
                                                          <button type="button" class="btn btn-icon icon-left btn-danger">
                                                              {{ $data->total_residual_cemetron }}
                                                          </button>
                                                        @else
                                                          {{ $data->total_residual_cemetron }}
                                                        @endif
                                                      </td>
                                                      <td class="text-center">
                                                        @if($data->total_residual_samator <= 8)
                                                          <button type="button" class="btn btn-icon icon-left btn-danger">
                                                              {{ $data->total_residual_samator }}
                                                          </button>
                                                        @else
                                                          {{ $data->total_residual_samator }}
                                                        @endif
                                                      </td>
                                                      <td>{{ $data->pic_name }}</td>
                                                      <td class="text-center">
                                                          <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                              <!-- <button type="button" class="btn btn-icon icon-left btn-success"
                                                              onclick="loadModal(this)" title="" target="/reportGasCO2/showInspectionGasCO2?id=" data="id={{$data->id}}">
                                                                  <i class="fas fa-eye"></i>
                                                              </button> -->
                                                              <button type="button" class="btn btn-icon icon-left btn-info"
                                                              onclick="loadModal(this)" title="" target="/reportGasCO2/addInspectionGasCO2" data="id={{$data->id}}">
                                                                  <i class="fas fa-edit"></i>
                                                              </button>
                                                              <button type="button" class="btn btn-icon icon-left btn-danger"
                                                              onclick="deleteInspectionGasCO2({{$data->id}})">
                                                                  <i class="fas fa-trash"></i>
                                                              </button>
                                                          </div>
                                                      </td>
                                                  </tr>
                                                  @endforeach
                                              </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                              </div>
                          </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@section('scripts')
    <script>
        function deleteGasCO2(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Confirmation", "Do you want to remove this data ?", function () {
               ajaxTransfer("/reportGasCO2/deleteGasCO2", data, "#modal-output");
            })
        }

        function deleteInspectionGasCO2(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Confirmation", "Do you want to remove this data ?", function () {
               ajaxTransfer("/reportGasCO2/deleteInspectionGasCO2", data, "#modal-output");
            })
        }

        var table = $('#masterGas');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });

        var table = $('#inspectionGas');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });
    </script>
@endsection

@endsection
