<div id="result-form-konten"></div>
    <form onsubmit="return false;" id="form-konten" class='form-horizontal'>
        <table width="100%">
            <tbody>
                <tr>
                  <td class="text-center">
                    <img src="public/assets/img/logosemen.png" class="img-responsive" alt="logo" width="80" style="padding-top:20px">
                  </td>
                </tr>
                <tr>
                  <td class="text-center">
                    <h6 class="modal-title" id="title">Form Master Gas CO2</h6>
                    <h4 class="modal-title" id="title"></h4><small class="font-bold">PT. Semen Indonesia</small>
                  </td>
                </tr>
            </tbody>
        </table>
        <div class="modal-body">
        <hr>
            <div class="form-group">
                <label>Plant</label>
                <select name="plant" class="form-control">
                    @foreach($plant as $item)
                      <option value="{{ $item->plant }}" @if(!is_null($data)) @if($item->plant == $data->plant) selected="selected" @endif @endif>{{ $item->plant_text}}</option>
                    @endforeach
                  </select>
            </div>
            <div class="form-group">
                <label>Area Plant</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <i class="fas fa-map-marker"></i>
                        </div>
                    </div>
                    <input type="text" class="form-control daterange-cus" name="pplantdesc" value="{{$data->pplantdesc}}">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            <button type="submit" class="btn btn-info" data-loading-text="<i class='fa fa-spinner fa-spin'></i>
              &nbsp;<font class='lowercase'></font>"> <i class="fa fa-check"></i> Save</button>
        </div>
    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/reportGasCO2/saveGasCO2', data, '#result-form-konten');
        })
    })
</script>
