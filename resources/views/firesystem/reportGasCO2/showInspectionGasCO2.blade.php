<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-konten" class='form-horizontal'>
          <div class="form-group">
              @foreach($inspectionGas as $id => $data)
                <label>Type Action</label>
                <div class="input-group">
                    <td>{{ $data->type_action }}</td>
                </div>
                <label>Date Activity</label>
                <div class="input-group">
                    <td>{{ $data->date_activity }}</td>
                </div>
                <label>Time</label>
                <div class="input-group">
                    <td>{{ $data->timesheet_start }} - {{ $data->timesheet_end }}</td>
                </div>
                <label>Daily Activity</label>
                <div class="input-group">
                    <td>{{ $data->daily_activity }}</td>
                </div>
                <label>PIC</label>
                <div class="input-group">
                    <td>{{ $data->pic_badge }} - {{ $data->pic_name }}</td>
                </div>
              @endforeach
          </div>
</form>
