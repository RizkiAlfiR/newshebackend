@extends('layouts.app')

@section('content')
<title>Fire Report | Safety Hygiene Environment System</title>
<section class="section">
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                  <div class="col-12" style="padding-top:20px">
                    <div>
                      <div>
                          <div class="">
                                    <h5>List Fire Report</h5>
                                    <p class="">
                                        <a onclick="loadModal(this)" title="" target="/fireReport/add" class="btn btn-icon icon-left btn-primary" style="color:white">
                                          <i class="far fa-edit"></i> Create New</a>
                                    </p>
                                    <div class="">
                                        <div class="table-responsive">
                                            <table class="table table-striped" style="width:100%"  id="table-fireReport">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">No</th>
                                                        <th>No Report</th>
                                                        <th>Date</th>
                                                        <th>Place</th>
                                                        <th>PIC</th>
                                                        <th>Status</th>
                                                        <!-- <th>Download PDF</th> -->
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($fireReport as $i => $data)
                                                    <tr>
                                                        <td class="text-center">{{ $i+1 }}</td>
                                                        <td>{{ $data->fire_number}}</td>
                                                        <td>{{ $data->fire_date}}</td>
                                                        <td>{{ $data->incident_place}}</td>
                                                        <td>{{ $data->pic_name }}</td>
                                                        <td>
                                                          @if($data->status == "OPEN")
                                                            <button type="button" class="btn btn-icon icon-left btn-success">
                                                                OPEN
                                                            </button>
                                                          @else
                                                            <button type="button" class="btn btn-icon icon-left btn-danger">
                                                                CLOSE
                                                            </button>
                                                          @endif
                                                        </td>
                                                        <!-- <td class="text-center">
                                                          <a href="{{url('fireReport/getPDF?id=')}}{{$data->id}}" class="btn btn-info btn-xs">
                                                            <i class="fa fa-download"></i>
                                                          </a>
                                                        </td> -->
                                                        <td>
                                                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                                <button type="button" class="btn btn-icon icon-left btn-success"
                                                                onclick="loadModal(this)" title="" target="/fireReport/showImage?id=" data="id={{$data->id}}">
                                                                    <i class="fa fa-image"></i>
                                                                </button>
                                                                <button type="button" class="btn btn-icon icon-left btn-info"
                                                                onclick="loadModal(this)" title="" target="/fireReport/add" data="id={{$data->id}}">
                                                                    <i class="fas fa-edit"></i>
                                                                </button>
                                                                <button type="button" class="btn btn-icon icon-left btn-danger"
                                                                onclick="deleteFireReport({{$data->id}})">
                                                                    <i class="fas fa-trash"></i>
                                                                </button>
                                                                <button type="button" class="btn btn-icon icon-left btn-warning"
                                                                onclick="loadModal(this)" title="" target="/fireReport/uploadImage" data="id={{$data->id}}">
                                                                    <i class="fas fa-upload"></i>
                                                                </button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                          </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@section('scripts')
    <script>
        function deleteFireReport(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Confirmation", "Do you want to remove this data ?", function () {
               ajaxTransfer("/fireReport/delete", data, "#modal-output");
            })
        }

        var table = $('#table-fireReport');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });
    </script>
@endsection

@endsection
