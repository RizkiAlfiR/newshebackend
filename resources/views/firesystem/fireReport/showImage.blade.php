<div id="result-form-konten"></div>
    <form onsubmit="return false;" id="form-konten" class='form-horizontal'>
        <table width="100%">
            <tbody>
                <tr>
                  <td class="text-center">
                    <img src="public/assets/img/logosemen.png" class="img-responsive" alt="logo" width="80">
                  </td>
                </tr>
                <tr>
                  <td class="text-center">
                    <h6 class="modal-title" id="title">List Image Fire Report</h6>
                    <h4 class="modal-title" id="title"></h4><small class="font-bold">PT. Semen Indonesia</small>
                  </td>
                </tr>
            </tbody>
        </table>

        <div class="modal-body">
            <div style="border-width:1px; border-top:7px; border-bottom: 7px; border-style:solid; border-color:#dddddd; background-color:#dddddd">
              <strong style="background-color:#dddddd; margin-left:5px; color:black">List Gambar Temuan</strong>
            </div>
            <div class="modal-body" style="border-width:1px; border-style:solid; border-color:#dddddd">
                <div class="row go_form">
                    <div class="col-md-12">
                        <div class="row">
                            @foreach($data as $item)
                              @if($item->status == "UPLOAD")
                                  <div class="col-3">
                                      <a href="{{asset('public/uploads/FireReport')}}/{{$item->image}}" target="_blank">
                                          <img src="{{asset('public/uploads/FireReport')}}/{{$item->image}}" id="showTemuan" style="max-width:'100%';max-height:100px;">
                                      </a>
                                  </div>
                              @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-body">
            <div style="border-width:1px; border-top:7px; border-bottom: 7px; border-style:solid; border-color:#dddddd; background-color:#dddddd">
              <strong style="background-color:#dddddd; margin-left:5px; color:black">List Gambar Tindak Lanjut</strong>
            </div>
            <div class="modal-body" style="border-width:1px; border-style:solid; border-color:#dddddd">
                <div class="row go_form">
                    <div class="col-md-12">
                        <div class="row">
                            @foreach($data as $item)
                              @if($item->status == "FOLLOWUP")
                                  <div class="col-3">
                                      <a href="{{asset('public/uploads/FireReport')}}/{{$item->image}}" target="_blank">
                                          <img src="{{asset('public/uploads/FireReport')}}/{{$item->image}}" id="showTemuan" style="max-width:'100%';max-height:100px;">
                                      </a>
                                  </div>
                              @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            <button type="submit" class="btn btn-info" data-loading-text="<i class='fa fa-spinner fa-spin'></i>
              &nbsp;<font class='lowercase'></font>"> <i class="fa fa-check"></i> Save</button>
        </div>

</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/inspectionApar/saveMaster', data, '#result-form-konten');
        })
    })
</script>
