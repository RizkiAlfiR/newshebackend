<div id="result-form-konten"></div>
    <form onsubmit="return false;" id="form-konten" class='form-horizontal'>
        <table width="100%">
            <tbody>
                <tr>
                  <td class="text-center">
                    <img src="public/assets/img/logosemen.png" class="img-responsive" alt="logo" width="80">
                  </td>
                </tr>
                <tr>
                  <td class="text-center">
                    <h6 class="modal-title" id="title">Tindak Lanjut Fire Report</h6>
                    <h4 class="modal-title" id="title"></h4><small class="font-bold">PT. Semen Indonesia</small>
                  </td>
                </tr>
            </tbody>
        </table>
        <hr>
        <div class="modal-body">
            <div class="form-group">
                <label>File : </label>
                <div class="modal-body" style="border-width:1px; border-style:solid; border-color:#dddddd">
                    <div class="row go_form">
                        <div class="row">
                            <div class="input-field col s6">
                              <input type="file" id="inputImage" name="image" class="validate"/ >
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label>Description</label>
                <div class="input-group">
                    <input type="text" class="form-control daterange-cus" name="description" value="">
                </div>
            </div>
            <div class="form-group">
                <label>Status</label>
                <select name="status" class="form-control">
                    <option value="UPLOAD">Upload Kebakaran</option>
                    <option value="FOLLOWUP">Followup Kebakaran</option>
                </select>
            </div>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            <button type="submit" class="btn btn-info" data-loading-text="<i class='fa fa-spinner fa-spin'></i>
              &nbsp;<font class='lowercase'></font>"> <i class="fa fa-check"></i> Upload</button>
        </div>

        <input type='hidden' name='id' value='{{ $id }}'>
        <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/fireReport/saveImage', data, '#result-form-konten');
        })
    })
</script>
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#showImage').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#inputImage").change(function () {
        readURL(this);
    });
</script>
