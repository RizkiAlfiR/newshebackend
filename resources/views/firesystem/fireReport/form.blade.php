<style>
  .modal{
    max-height: calc(150vh - 250px);
    overflow-y: auto;
  }
</style>
<div id="result-form-konten"></div>
    <form onsubmit="return false;" id="form-konten" class='form-horizontal'>
        <table width="100%">
            <tbody>
                <tr>
                  <td class="text-center">
                    <img src="{{asset('assets/img')}}/logosemen.png" class="img-responsive" alt="logo" width="80" style="padding-top:20px">
                  </td>
                </tr>
                <tr>
                  <td class="text-center">
                    <h6 class="modal-title" id="title">Form Fire Report</h6>
                    <h4 class="modal-title" id="title"></h4><small class="font-bold">PT. Semen Indonesia</small>
                  </td>
                </tr>
            </tbody>
        </table>
        <hr>
        <div class="modal-body">
            <div class="row go_form">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Date incident :</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fas fa-calendar"></i>
                                </div>
                            </div>
                            <input type="date" class="form-control daterange-cus" name="fire_date"
                            @if(!is_null($data))
                              value="{{ $data->fire_date}}"
                            @else
                              value="<?php echo date('Y-m-d');?>"
                            @endif>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Time incident :</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fas fa-clock"></i>
                                </div>
                            </div>
                            <input type="time" class="form-control daterange-cus" name="fire_time"
                            @if(!is_null($data))
                              value="{{ $data->fire_time }}"
                            @else
                              value="<?php echo date('H:i:s');?>"
                            @endif>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Shift :</label>
                        <select name="shift" class="form-control">
                            <option <?php if($data->shift =='1') {echo "selected";}?> value="1">Shift 1</option>
                            <option <?php if($data->shift =='2') {echo "selected";}?> value="2">Shift 2</option>
                            <option <?php if($data->shift =='3') {echo "selected";}?> value="3">Shift 3</option>
                        </select>
                    </div>
                </div>
            </div>
            <div style="border-width:1px; border-top:3px; border-style:solid; border-color:red; background-color:red">
              <strong style="background-color:red; margin-left:5px; color:white">Keterangan Laporan Kebakaran</strong>
            </div>
            <div class="modal-body" style="border-width:1px; border-style:solid; border-color:red">
                <div class="row go_form">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Pekerjaan sewaktu terjadi kebakaran :</label>
                            <input type="text" class="form-control daterange-cus" value="{{ $data->work_on_fire}}" name="work_on_fire">
                        </div>
                    </div>
                </div>
                <div class="row go_form">
                    <div class="col-md-6">

                      <div class="row">
                        <div class="col-sm-6">
                          <label>Jam mulai</label>
                          <input type="time" class="form-control daterange-cus" name="job_walking_timestart"
                          @if(!is_null($data))
                            value="{{ $data->job_walking_timestart }}"
                          @else
                            value="<?php echo date('H-i-s');?>"
                          @endif>
                        </div>
                        <div class="col-sm-6">
                          <label>Jam selesai</label>
                          <input type="time" class="form-control daterange-cus" name="job_walking_timeend"
                          @if(!is_null($data))
                            value="{{ $data->job_walking_timeend }}"
                          @else
                            value="<?php echo date('H-i-s');?>"
                          @endif>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Sarana / alat pemadam yang di gunakan  :</label>
                                <div class="checkbox checkbox-primary">
                                    <input id="fire_extinguishers1" type="checkbox" name="fire_extinguishers[]" value="Apar">
                                    <label for="fire_extinguishers">Apar</label>
                                </div>
                                <div class="checkbox checkbox-primary">
                                    <input id="fire_extinguishers2" type="checkbox" name="fire_extinguishers[]" value="Hydrant">
                                    <label for="fire_extinguishers2">Hydrant</label>
                                </div>
                                <div class="checkbox checkbox-primary">
                                    <input id="fire_extinguishers3" type="checkbox" name="fire_extinguishers[]" value="PMK">
                                    <label for="fire_extinguishers3">PMK</label>
                                </div>
                                <div class="form-group">
                                <div class="checkbox checkbox-primary">
                                    <input id="fire_extinguishers4" type="checkbox">
                                    <label for="fire_extinguishers4">Other</label>
                                </div>
                                <!-- <div id="form-add-todo" class="form-add-todo">
                                    <div class="row form_list_extinguishers">

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input placeholder="Sarana / alat..." type="text" class="form-control input-sm new-todo-item-extinguishers" id="new-todo-item-extinguishers" name="other_extinguishers[]">
                                                    <span class="input-group-btn">
  										                                  <button type="button" class="btn btn-sm btn-info" id="add-todo-item-extinguishers"> <i class="fa fa-plus"></i></button>
  										                              </span>
                                                </div>
                                            </div>
                                            <form id="form-todo-list">
                                              <ul id="todo-list-extinguishers" class="todo-list">
                                              </ul>
                                            </form>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row go_form">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Penjelasan tentang terjadinya kebakaran :</label>
                            <textarea rows="2" maxlength="255" class="form-control input-sm" name="description_of_fire" style="resize: vertical;">{{ $data->description_of_fire }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="row go_form">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Tempat Kejadian :</label>
                            <select name="incident_place" class="form-control">
                                <option <?php if($data->incident_place =='Dalam Pabrik') {echo "selected";}?> value="Dalam Pabrik">Dalam Pabrik</option>
                                <option <?php if($data->incident_place =='Luar Pabrik') {echo "selected";}?> value="Luar Pabrik">Luar Pabrik</option>
                                <option <?php if($data->incident_place =='Proyek') {echo "selected";}?> value="Proyek">Proyek</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Jumlah Korban :</label>
                            <input type="number" name="number_victims" value="{{ $data->number_victims}}" min="0" class="form-control input-sm touchspin">
                        </div>
                    </div>
                </div>
                <div class="row go_form">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Keterangan tempat kejadian :</label>
                            <textarea rows="2" maxlength="255" class="form-control input-sm" name="note_place" style="resize: vertical;">{{ $data->note_place }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="row go_form">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Objek yang terbakar :</label>
                            <div class="input-group">
                                <input type="text" class="form-control daterange-cus" name="damage_from_fire" value="{{ $data->damage_from_fire }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Jenis bahan yang terbakar :</label>
                            <div class="input-group">
                                <input type="text" class="form-control daterange-cus" name="fire_material_type" value="{{ $data->fire_material_type }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row go_form">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Kerusakan yang ditimbulkan :</label>
                            <textarea rows="2" maxlength="255" class="form-control input-sm" name="damage_from_fire" style="resize: vertical;">{{ $data->damage_from_fire }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="row go_form">
                    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <label>Penjelasan sebab terjadinya kebakaran :</label>
                        <div class="ibox float-e-margins">
                            <div class="ibox-content" style="border: 1px solid #f0f0f0; padding:10px">
                                <label>Unsafe condition : </label><span class="small text-danger pull-right"> ( * )</span>
                                <textarea type="text" id="unsafe_condition" name="unsafe_condition" class="form-control input-sm" rows="4" maxlength="2000" style="resize: vertical;">{{ $data->unsafe_condition }}</textarea>
                                <hr style="margin-bottom: 16px;">
                                <label>Unsafe action :</label>
                                <textarea type="text" id="unsafe_action" name="unsafe_action" class="form-control input-sm" rows="4" maxlength="2000" style="resize: vertical;">{{ $data->unsafe_action }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row go_form">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Saran :</label>
                            <textarea rows="2" maxlength="255" class="form-control input-sm" name="saran" style="resize: vertical;">{{ $data->saran }}</textarea>
                        </div>
                    </div>
                </div>
                <!-- <div class="row go_form">
                    <div class="col-md-12">
                      <div class="form-group">
                          <label>Anggota PMK yang melakukan pemadaman :</label>
                              <div id="form-add-todo" class="form-add-todo">
                                  <div class="row form_list_extinguishers">

                                      <div class="col-md-12">
                                          <div class="form-group">
                                              <div class="input-group">
                                                  <input placeholder="Anggota..." type="text" class="form-control input-sm new-todo-item-extinguishers" id="new-todo-item-extinguishers" name="other_extinguishers[]">
                                                  <span class="input-group-btn">
                                                      <button type="button" class="btn btn-sm btn-info" id="add-todo-item-extinguishers"> <i class="fa fa-plus"></i></button>
                                                  </span>
                                              </div>
                                          </div>
                                          <form id="form-todo-list">
                                            <ul id="todo-list-extinguishers" class="todo-list">
                                            </ul>
                                          </form>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                    </div>
                    <div class="row go_form">
                        <div class="col-md-12">
                          <div class="form-group">
                              <label>Tembusan surat :</label>
                                  <div id="form-add-todo" class="form-add-todo">
                                      <div class="row form_list_extinguishers">

                                          <div class="col-md-12">
                                              <div class="form-group">
                                                  <div class="input-group">
                                                      <input placeholder="Yth..." type="text" class="form-control input-sm new-todo-item-extinguishers" id="new-todo-item-extinguishers" name="other_extinguishers[]">
                                                      <span class="input-group-btn">
                                                          <button type="button" class="btn btn-sm btn-info" id="add-todo-item-extinguishers"> <i class="fa fa-plus"></i></button>
                                                      </span>
                                                  </div>
                                              </div>
                                              <form id="form-todo-list">
                                                <ul id="todo-list-extinguishers" class="todo-list">
                                                </ul>
                                              </form>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            <button type="submit" class="btn btn-info" data-loading-text="<i class='fa fa-spinner fa-spin'></i>
              &nbsp;<font class='lowercase'></font>"> <i class="fa fa-check"></i> Save</button>
        </div>

    <input type='hidden' name='fire_number' value='{{ $data->fire_number }}'>
    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='pic_badge' value='{{ $data->pic_badge }}'>
    <input type='hidden' name='pic_name' value='{{ $data->pic_name }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/fireReport/save', data, '#result-form-konten');
        })
    })
</script>
