@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Chart Demo</div>

                <div class="panel-body">
                    {!! $chart->html() !!}
                </div>
                <hr>
                {!!$pie->html() !!}
            </div>
        </div>
    </div>
</div>
{!! Charts::scripts() !!}
{!! $chart->script() !!}
@endsection


<!DOCTYPE html>
<html>
<head>
<title>Fire Report | Safety Hygiene Environment System</title>

  <link rel="stylesheet" href="css\bootstrap.css">
  <link rel="stylesheet" href=".\css2\style.css">
  <link rel="stylesheet" href=".\css2\components.css">

</head>
<section class="section">
    <table width="100%" style="border-width:10px">
        <tbody style="border-width:10px">
            <tr>
                <td class="text-center">
                    <img src="{{asset('assets/img')}}/logosemen.png" class="img-responsive" alt="logo" width="150" style="padding-top:20px">
                </td>
            </tr>
            <tr>
                <td class="text-center" style="border-width:10px">
                    <h6 class="modal-title" id="title">Form Fire Report</h6>
                    <h4 class="modal-title" id="title"></h4><small class="font-bold">PT. Semen Indonesia</small>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="modal-body">
        <div class="row go_form">
            <div class="col-md-4">
                <div class="form-group">
                    <label>Date incident :</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fas fa-calendar"></i>
                            </div>
                        </div>
                        <input type="date" class="form-control daterange-cus" name="fire_date"
                        @if(!is_null($data))
                          value="{{ $data->fire_date}}"
                        @else
                          value="<?php echo date('Y-m-d');?>"
                        @endif>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Time incident :</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fas fa-clock"></i>
                            </div>
                        </div>
                        <input type="time" class="form-control daterange-cus" name="fire_time"
                        @if(!is_null($data))
                          value="{{ $data->fire_time }}"
                        @else
                          value="<?php echo date('H-i-s');?>"
                        @endif>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Shift :</label>
                    <select name="shift" class="form-control">
                        <option <?php if($data->shift =='1') {echo "selected";}?> value="1">Shift 1</option>
                        <option <?php if($data->shift =='2') {echo "selected";}?> value="2">Shift 2</option>
                        <option <?php if($data->shift =='3') {echo "selected";}?> value="3">Shift 3</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</section>
</head>
