@extends('layouts.app')

@section('content')
<title>Inspection Apar | Safety Hygiene Environment System</title>
<section class="section">
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                  <div class="col-12" style="padding-top:20px">
                    <div>
                      <div>
                          <div class="tab-content" id="myTabContent">
                              <div class="tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
                                <div class="">
                                    <h5>List Inspection Apar</h5>
                                    <div class="">
                                        <div class="table-responsive">
                                            <table width="60%">
                                            <tbody>
                                                <tr>
                                                    <td width="20%"><strong> Area </strong></td>
                                                    <td width="2%"> :</td>
                                                    <td><span id="view_area"></span>{{$lokasiApar->area}}</td>
                                                </tr>
                                                <tr>
                                                    <td width="20%"><strong> Unit Kerja </strong></td>
                                                    <td width="2%"> :</td>
                                                    <td><span id="view_unit_kerja"></span> {{$lokasiApar->unit_kerja}}</td>
                                                </tr>
                                                <tr>
                                                    <td width="20%"><strong> Plant </strong></td>
                                                    <td width="2%"> :</td>
                                                    <td><span id="view_lokasi"></span>
                                                      @if($lokasiApar->plant == "5001")
                                                        Tuban
                                                      @elseif($lokasiApar->plant == "5002")
                                                        Gresik
                                                      @elseif($lokasiApar->plant == "5003")
                                                        Rembang
                                                      @else
                                                        Cigading
                                                      @endif
                                                    </td>
                                                </tr>
                                            </tbody>
                                            </table>
                                          </div>
                                          <div class="table-responsive">
                                            <table class="table table-striped" id="table-inspection" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th rowspan="2" class="text-center align-middle">No</th>
                                                        <th rowspan="2" class="text-center align-middle">Inspector</th>
                                                        <th colspan="6" class="text-center">Inspection</th>
                                                        <th colspan="2" class="text-center">Keterangan</th>
                                                        <th rowspan="2" class="text-center align-middle">Date</th>
                                                        <th rowspan="2" class="text-center align-middle">Action</th>
                                                    </tr>
                                                    <tr>
                                                        <th class="text-center">Klem</th>
                                                        <th class="text-center">Press</th>
                                                        <th class="text-center">Hose</th>
                                                        <th class="text-center">Segel</th>
                                                        <th class="text-center">Support</th>
                                                        <th class="text-center">Weight</th>
                                                        <th class="text-center">Status</th>
                                                        <th class="text-center">Keterangan</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($inspectionApar as $i => $data)
                                                    <tr>
                                                        <td class="text-center">{{ $i+1 }}</td>
                                                        <td>{{ $data->pic_name }}</td>
                                                        <td class="text-center">{{ $data->klem }}</td>
                                                        <td class="text-center">{{ $data->press }}</td>
                                                        <td class="text-center">{{ $data->hose }}</td>
                                                        <td class="text-center">{{ $data->seal }}</td>
                                                        <td class="text-center">{{ $data->sapot }}</td>
                                                        <td class="text-center">{{ $data->weight }}</td>
                                                        <td>{{ $data->status_cond }}</td>
                                                        <td>{{ $data->note }}</td>
                                                        <td>{{ $data->inspection_date }}</td>
                                                        <td>
                                                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                                <button type="button" class="btn btn-icon icon-left btn-info"
                                                                onclick="loadModal(this)" title="" target="/inspectionApar/updateInspectionApar" data="id={{$data->id}}">
                                                                    <i class="fas fa-edit"></i>
                                                                </button>
                                                                <button type="button" class="btn btn-icon icon-left btn-danger"
                                                                onclick="deleteInspection({{$data->id}})">
                                                                    <i class="fas fa-trash"></i>
                                                                </button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                              </div>
                          </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@section('scripts')
    <script>
        function deleteInspection(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Confirmation", "Do you want to remove this data?", function () {
               ajaxTransfer("/inspectionApar/deleteHistory", data, "#modal-output");
            })
        }

        var table = $('#table-inspection');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });
    </script>
@endsection

@endsection
