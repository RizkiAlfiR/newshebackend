<div id="result-form-konten"></div>
    <form onsubmit="return false;" id="form-konten" class='form-horizontal'>
      <table width="100%">
          <tbody>
              <tr>
                <td class="text-center">
                  <img src="{{asset('assets/img')}}/logosemen.png" class="img-responsive" alt="logo" width="80" style="padding-top:20px">
                </td>
              </tr>
              <tr>
                <td class="text-center">
                  <h6 class="modal-title" id="title">Form Inspection Apar</h6>
                  <h4 class="modal-title" id="title"></h4><small class="font-bold">PT. Semen Indonesia</small>
                </td>
              </tr>
          </tbody>
      </table>

      <div class="modal-body">
        <table width="60%">
          <tbody>
              <tr>
                  <td width="20%"><strong> Area </strong></td>
                  <td width="2%"> :</td>
                  <td><span id="view_area"></span>{{$dataLokasi->area}}</td>
              </tr>
              <tr>
                  <td width="20%"><strong> Unit Kerja </strong></td>
                  <td width="2%"> :</td>
                  <td><span id="view_unit_kerja"></span> {{$dataLokasi->unit_kerja}}</td>
              </tr>
              <tr>
                  <td width="20%"><strong> Plant </strong></td>
                  <td width="2%"> :</td>
                  <td><span id="view_lokasi"></span>
                    @if($dataLokasi->plant == "5001")
                      Tuban
                    @elseif($dataLokasi->plant == "5002")
                      Gresik
                    @elseif($dataLokasi->plant == "5003")
                      Rembang
                    @else
                      Cigading
                    @endif
                  </td>
              </tr>
          </tbody>
          </table>
          <hr>
          <div class="row go_form">
              <div class="col-md-4">
                  <label>Tanggal</label>
                  <div class="input-group">
                      <div class="input-group-prepend">
                          <div class="input-group-text">
                              <i class="fas fa-calendar"></i>
                          </div>
                      </div>
                      <input type="date" class="form-control daterange-cus" name="inspection_date" value="{{$data->inspection_date}}">
                  </div>
              </div>
              <div class="col-md-4">
                  <label>Select Clamp</label>
                  <select name="klem" class="form-control">
                      <option <?php if($data->klem =='K') {echo "selected";}?> value="K">K ( Klem )</option>
                      <option <?php if($data->klem =='T') {echo "selected";}?> value="T">T ( Tanduk )</option>
                      <option <?php if($data->klem =='U') {echo "selected";}?> value="U">U ( U Klem )</option>
                      <option <?php if($data->klem =='B') {echo "selected";}?> value="B">B ( Box )</option>
                      <option <?php if($data->klem =='D') {echo "selected";}?> value="D">D ( Duduk )</option>
                  </select>
              </div>
              <div class="col-md-4">
                  <label>Status Condition</label>
                  <select name="status_cond" class="form-control">
                      <option <?php if($data->status_cond =='-') {echo "selected";}?> value="-">Belum dicek</option>
                      <option <?php if($data->status_cond =='BAIK') {echo "selected";}?> value="BAIK">Baik</option>
                      <option <?php if($data->status_cond =='HABIS') {echo "selected";}?> value="HABIS">Habis</option>
                      <option <?php if($data->status_cond =='GANTI') {echo "selected";}?> value="GANTI">Ganti</option>
                      <option <?php if($data->status_cond =='REFILL') {echo "selected";}?> value="REFILL">Refill</option>
                  </select>
              </div>
          </div>
          <div style="margin-top:20px"></div>
          <div class="row go_form">
              <div class="col-md-2">
                  <label>Hose</label>
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="radio radio-info" style="margin:6px 0">
                        <input type="radio" name="hose" <?php if($data->hose =='X') {echo "checked";}?> value="X">
                        <label for="hose1">X</label>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="radio radio-info" style="margin:6px 0">
                        <input type="radio" name="hose" <?php if($data->hose =='V') {echo "checked";}?> value="V">
                        <label for="hose2">V</label>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="col-md-2">
                  <label>Segel</label>
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="radio radio-info" style="margin:6px 0" <>
                        <input type="radio" name="seal" <?php if($data->seal =='X') {echo "checked";}?> value="X">
                        <label for="seal1">X</label>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="radio radio-info" style="margin:6px 0">
                        <input type="radio" name="seal" <?php if($data->seal =='V') {echo "checked";}?> value="V">
                        <label for="seal2">V</label>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="col-md-3">
                  <label>Sapot</label>
                  <div class="row">
                    <div class="col-sm-4">
                      <div class="radio radio-info" style="margin:6px 0">
                        <input type="radio" name="sapot" <?php if($data->sapot =='X') {echo "checked";}?> value="X">
                        <label for="sapot1">X</label>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="radio radio-info" style="margin:6px 0">
                        <input type="radio" name="sapot" <?php if($data->sapot =='V') {echo "checked";}?> value="V">
                        <label for="sapot2">V</label>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="col-md-2">
                  <label>Press</label>
                  <input type="number" name="press" value="{{$data->press}}" min="0" class="form-control input-sm touchspin">
              </div>
              <div class="col-md-2">
                  <label>Weight</label>
                  <input type="number" name="weight" value="{{$data->weight}}" min="0" class="form-control input-sm touchspin">
              </div>
          </div>
          <div class="row go_form">
              <div class="col-md-6">
                  <div class="form-group">
                      <label class="control-label" for="note">Keterangan</label>
                      <textarea rows="2" maxlength="255" class="form-control input-sm" name="note" style="resize: vertical;">{{$data->note}}</textarea>
                  </div>
              </div>
          </div>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
          <button type="submit" class="btn btn-info" data-loading-text="<i class='fa fa-spinner fa-spin'></i>
            &nbsp;<font class='lowercase'></font>"> <i class="fa fa-check"></i> Save</button>
      </div>
    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='id_apar' value='{{ $data->id_apar }}'>
    <input type='hidden' name='pic_badge' value='{{ $data->pic_badge }}'>
    <input type='hidden' name='pic_name' value='{{ $data->pic_name }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/inspectionApar/saveUpdateInspectionApar', data, '#result-form-konten');
        })
    })
</script>
