@extends('layouts.app')

@section('content')
<title>Inspection | Safety Hygiene Environment System</title>
<section class="section">
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <center style="padding-top:50px">
                      <img src="public/assets/img/firesystem.png" class="img-responsive" alt="logo" width="150">
                    </center>
                    <center>
                      <h5>Inspection Report</h5>
                    </center>

                    <div class="row" style="padding-top:30px; margin-left:0px; margin-right:0px">
                      <div class="col-lg-6 col-xs-12" id="listsatu">
                        <center>
                        <a href="{{ url('/inspectionApar') }}" id="firesystem" class="btn btn-icon icon-left btn-primary" style="width:100%">
                          <div class="col-12 row">
                            <div class="col-1 text-left" style="margin-top:5px"><i class="fa fa-fire-extinguisher fa-3x"></i></div>
                            <div class="col-8 text-left" style="margin-left:10px; margin-top:10px"><h3 class="font-bold"> INSPECTION APAR</h3></div>
                          </div>
                        </a>
                        </center>
                        <center>
                        <a href="{{ url('/inspectionAlarm') }}" id="firesystem" class="btn btn-icon icon-left btn-primary" style="width:100%; margin-top:10px">  
                          <div class="col-12 row">
                            <div class="col-1 text-left" style="margin-top:5px"><i class="fa fa-bell fa-3x"></i></div>
                            <div class="col-8 text-left" style="margin-left:10px; margin-top:10px"><h3 class="font-bold"> INSPECTION ALARM</h3></div>
                          </div>
                        </a>
                        </center>
                      </div>

                      <div class="col-lg-6 col-xs-12" id="listdua">
                        <center>
                        <a href="{{ url('/inspectionHydrant') }}" id="firesystem" class="btn btn-icon icon-left btn-primary" style="width:100%">
                          <div class="col-12 row">
                            <div class="col-1 text-left" style="margin-top:5px"><i class="fa fa-archive fa-3x"></i></div>
                            <div class="col-8 text-left" style="margin-left:10px; margin-top:10px"><h3 class="font-bold"> INSPECTION HYDRANT</h3></div>
                          </div>
                        </a>
                        </center>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped" id="table-1">

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
