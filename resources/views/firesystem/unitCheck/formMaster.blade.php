<style>
  .modal{
    max-height: calc(150vh - 250px);
    overflow-y: auto;
  }
</style>
<div id="result-form-konten"></div>
    <form onsubmit="return false;" id="form-konten" class='form-horizontal'>
        <table width="100%">
            <tbody>
                <tr>
                  <td class="text-center">
                    <img src="public/assets/img/logosemen.png" class="img-responsive" alt="logo" width="80" style="padding-top:20px">
                  </td>
                </tr>
                <tr>
                  <td class="text-center">
                    <h6 class="modal-title" id="title">Form Master Vehicle</h6>
                    <h4 class="modal-title" id="title"></h4><small class="font-bold">PT. Semen Indonesia</small>
                  </td>
                </tr>
            </tbody>
        </table>
        <hr>
        <div class="modal-body">
            <div class="row go_form">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Select Vehicle</label>
                        <select name="type" class="form-control">
                            <option <?php if($data->type =='PMK') {echo "selected";}?> value="PMK">PMK</option>
                            <option <?php if($data->type =='PICKUP') {echo "selected";}?> value="PICKUP">PICKUP</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Vehicle Number / Code</label>
                        <input type="text" name="vehicle_code" value="{{ $data->vehicle_code }}" class="form-control input-sm touchspin">
                    </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                      <label>No Rangka</label>
                      <input type="text" name="nomor_rangka" value="{{ $data->nomor_rangka }}" class="form-control input-sm touchspin">
                  </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Vehicle Merk</label>
                        <input type="text" name="vehicle_merk" value="{{ $data->vehicle_merk }}" class="form-control input-sm touchspin">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Vehicle Type</label>
                        <input type="text" name="vehicle_type" value="{{ $data->vehicle_type }}" class="form-control input-sm touchspin">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                    <label>Foto</label>
                        <div class="row">
                            <div class="input-field col s6">
                              <input type="file" id="inputImage" name="image" class="validate"/ >
                              <!-- <input type="file" id="inputTemuan" name="climbing_tool_picture" class="validate"/ > -->
                            </div>
                        </div>
                        <div class="row" style="margin-top:10px">
                            <div class="col s6">
                                @if($data->image=='')
                                  <img src="{{asset('public/images')}}/{{'preview-icon.png'}}" id="showImage" style="max-width:330px;max-height:270px;float:left;" />
                                @else
                                  <img src="{{asset('public/uploads/MasterVehicle')}}/{{$data->image}}" id="showImage" style="max-width:350px;max-height:270px;float:left;" />
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Tahun</label>
                        <input type="text" name="tahun" value="{{ $data->tahun }}" class="form-control input-sm touchspin">
                    </div>
                </div>

            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            <button type="submit" class="btn btn-info" data-loading-text="<i class='fa fa-spinner fa-spin'></i>
              &nbsp;<font class='lowercase'></font>"> <i class="fa fa-check"></i> Save</button>
        </div>
    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='last_inspection' value='{{ $data->last_inspection }}'>
    <input type='hidden' name='exp_inspection' value='{{ $data->exp_inspection }}'>
    <input type='hidden' name='sum_negative' value='{{ $data->sum_negative }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/unitCheck/saveMaster', data, '#result-form-konten');
        })
    })
</script>

<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#showImage').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#inputImage").change(function () {
        readURL(this);
    });
</script>
