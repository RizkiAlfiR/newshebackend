@extends('layouts.app')

@section('content')
<title>Unit Check | Safety Hygiene Environment System</title>
<section class="section">
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                  <div class="col-12" style="padding-top:20px">
                    <div class="nav-wrapper">
                        <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0 active" id="tabs-icons-text-1-tab" data-toggle="tab" href="#tabs-icons-text-1" role="tab" aria-controls="tabs-icons-text-1" aria-selected="true"><i class="ni ni-cloud-upload-96 mr-2"></i>Master Vehicle</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-2-tab" data-toggle="tab" href="#tabs-icons-text-2" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>Inspection Pickup</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-3-tab" data-toggle="tab" href="#tabs-icons-text-3" role="tab" aria-controls="tabs-icons-text-3" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>Inspection PMK</a>
                            </li>
                        </ul>
                    </div>
                    <div>
                      <div>
                          <div class="tab-content" id="myTabContent">
                              <div class="tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
                                <div class="">
                                    <h5>List Master Vehicle</h5>
                                    <p class="">
                                        <a onclick="loadModal(this)" title="" target="/unitCheck/addMaster" class="btn btn-icon icon-left btn-primary" title="Create New" style="color:white">
                                          <i class="far fa-edit"></i>Create New</a>
                                    </p>
                                    <div class="">
                                        <div class="table-responsive">
                                            <table class="table table-striped" style="width:100%" id="table-master">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">No</th>
                                                        <th>Vehicle</th>
                                                        <th>Vehicle Code</th>
                                                        <th>Merk</th>
                                                        <th>Type</th>
                                                        <th>Tahun</th>
                                                        <th>No Rangka</th>
                                                        <th>Image</th>
                                                        <th class="text-center">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                  @foreach($listVehicle as $i => $data)
                                                  <tr>
                                                      <td class="align-middle text-center">{{ $i+1 }}</td>
                                                      <td class="align-middle">{{ $data->type }}</td>
                                                      <td class="align-middle">{{ $data->vehicle_code }}</td>
                                                      <td class="align-middle">{{ $data->vehicle_merk }}</td>
                                                      <td class="align-middle">{{ $data->vehicle_type }}</td>
                                                      <td class="align-middle">{{ $data->tahun }}</td>
                                                      <td class="align-middle">{{ $data->nomor_rangka }}</td>
                                                      <td class="align-middle">
                                                        @if(!is_null($data->image))
                                                          <a href="{{asset('public/uploads/MasterVehicle')}}/{{$data->image}}" target="_blank">
                                                              <img src="{{asset('public/uploads/MasterVehicle')}}/{{$data->image}}" class="img-responsive" alt="logo" width="80">
                                                          </a>
                                                        @else
                                                          <img src="{{asset('public/images')}}/{{"preview-icon.png"}}" class="img-responsive" alt="logo" width="80">
                                                        @endif
                                                      </td>
                                                      <td class="align-middle text-center">
                                                          <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                              <button type="button" class="btn btn-icon icon-left btn-info" title=""
                                                              onclick="loadModal(this)" target="/unitCheck/addMaster" data="id={{$data->id}}">
                                                                  <i class="fas fa-edit"></i>
                                                              </button>
                                                              <button type="button" class="btn btn-icon icon-left btn-danger"
                                                              onclick="deleteMaster({{$data->id}})">
                                                                  <i class="fas fa-trash"></i>
                                                              </button>
                                                          </div>
                                                      </td>
                                                  </tr>
                                                  @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                              </div>
                              <div class="tab-pane fade" id="tabs-icons-text-2" role="tabpanel" aria-labelledby="tabs-icons-text-2-tab">
                                <div class="">
                                    <h5>Inspection Pickup</h5>
                                    <div class="">
                                        <div class="table-responsive">
                                            <table class="table table-striped" style="width:100%" id="table-inspectionPickup">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">No</th>
                                                        <th>Vehicle Code</th>
                                                        <th>Vehicle Merk</th>
                                                        <th class="text-center">Negative List</th>
                                                        <th class="text-center">Status</th>
                                                        <th>Last Inspection</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($listVehiclePickup as $i => $data)
                                                    <tr>
                                                        <td class="align-middle text-center">{{ $i+1 }}</td>
                                                        <td class="align-middle">{{ $data->vehicle_code}}</td>
                                                        <td class="align-middle">{{ $data->vehicle_merk}}</td>
                                                        <td class="align-middle text-center">
                                                          <?php
                                                            $exp = $data->exp_inspection;
                                                            $now = date('Y-m-d');
                                                          ?>
                                                          @if($data->sum_negative==0 || $data->sum_negative=='' || $data->sum_negative=='null' || $now > $exp)
                                                            <button type="button" class="btn btn-icon icon-left btn-success">
                                                                0
                                                            </button>
                                                          @else
                                                            <button type="button" class="btn btn-icon icon-left btn-danger">
                                                                {{ $data->sum_negative }}
                                                            </button>
                                                          @endif
                                                        </td>
                                                        <td class="align-middle text-center">
                                                          <?php
                                                            $exp = $data->exp_inspection;
                                                            $now = date('Y-m-d');
                                                            if($now < $exp){
                                                              ?>
                                                              <button type="button" class="btn btn-icon icon-left btn-success">
                                                                  Checked
                                                              </button>
                                                              <?php
                                                            } else {
                                                              ?>
                                                              <button type="button" class="btn btn-icon icon-left btn-danger">
                                                                  Not Checked
                                                              </button>
                                                              <?php

                                                            }
                                                          ?>
                                                        </td>
                                                        <td class="align-middle">{{ $data->last_inspection}}</td>
                                                        <td class="align-middle">
                                                              <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                                  <button type="button" class="btn btn-icon icon-left btn-info"
                                                                  onclick="loadModal(this)" target="/unitCheck/addInspectionPickup" data="id={{$data->id}}">
                                                                      <i class="fas fa-edit"></i>
                                                                  </button>
                                                                  <a href="{{url('unitCheck/historyInspectionPickup?id=')}}{{$data->id}}" class="btn btn-warning btn-xs">
                                                                    <i class="fa fa-history"></i>
                                                                  </a>
                                                              </div>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                              </div>
                              <div class="tab-pane fade" id="tabs-icons-text-3" role="tabpanel" aria-labelledby="tabs-icons-text-3-tab">
                                <div class="">
                                    <h5>Inspection PMK</h5>
                                    <div class="">
                                        <div class="table-responsive">
                                            <table class="table table-striped" style="width:100%" id="table-inspectionPMK">
                                              <thead>
                                                  <tr>
                                                      <th class="text-center">No</th>
                                                      <th>Vehicle Code</th>
                                                      <th>Vehicle Merk</th>
                                                      <th class="text-center">Negative List</th>
                                                      <th class="text-center">Status</th>
                                                      <th>Last Inspection</th>
                                                      <th>Action</th>
                                                  </tr>
                                              </thead>
                                              <tbody>
                                                  @foreach($listVehiclePMK as $i => $data)
                                                  <tr>
                                                      <td class="align-middle text-center">{{ $i+1 }}</td>
                                                      <td class="align-middle">{{ $data->vehicle_code}}</td>
                                                      <td class="align-middle">{{ $data->vehicle_merk}}</td>
                                                      <td class="align-middle text-center">
                                                        <?php
                                                          $exp = $data->exp_inspection;
                                                          $now = date('Y-m-d');
                                                        ?>
                                                        @if($data->sum_negative==0 || $data->sum_negative=='' || $data->sum_negative=='null' || $now > $exp)
                                                          <button type="button" class="btn btn-icon icon-left btn-success">
                                                              0
                                                          </button>
                                                        @else
                                                          <button type="button" class="btn btn-icon icon-left btn-danger">
                                                              {{ $data->sum_negative }}
                                                          </button>
                                                        @endif
                                                      </td>
                                                      <td class="align-middle text-center">
                                                        <?php
                                                          $exp = $data->exp_inspection;
                                                          $now = date('Y-m-d');
                                                          if($now < $exp){
                                                            ?>
                                                            <button type="button" class="btn btn-icon icon-left btn-success">
                                                                Checked
                                                            </button>
                                                            <?php
                                                          } else {
                                                            ?>
                                                            <button type="button" class="btn btn-icon icon-left btn-danger">
                                                                Not Checked
                                                            </button>
                                                            <?php

                                                          }
                                                        ?>
                                                      </td>
                                                      <td class="align-middle">{{ $data->last_inspection}}</td>
                                                      <td class="align-middle">
                                                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                                <button type="button" class="btn btn-icon icon-left btn-info"
                                                                onclick="loadModal(this)" target="/unitCheck/addInspectionPMK" data="id={{$data->id}}">
                                                                    <i class="fas fa-edit"></i>
                                                                </button>
                                                                <a href="{{url('unitCheck/historyInspectionPMK?id=')}}{{$data->id}}" class="btn btn-warning btn-xs">
                                                                  <i class="fa fa-history"></i>
                                                                </a>
                                                            </div>
                                                      </td>
                                                  </tr>
                                                  @endforeach
                                              </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                              </div>
                          </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@section('scripts')
    <script>
        function deleteMaster(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Confirmation", "Do you want to remove this data ?", function () {
               ajaxTransfer("/unitCheck/deleteMaster", data, "#modal-output");
            })
        }

        function deleteInspectionPickup(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Confirmation", "Do you want to remove this data ?", function () {
               ajaxTransfer("/unitCheck/deleteInspectionPickup", data, "#modal-output");
            })
        }

        function deleteInspectionPMK(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Confirmation", "Do you want to remove this data ?", function () {
               ajaxTransfer("/unitCheck/deleteInspectionPMK", data, "#modal-output");
            })
        }

        var table = $('#table-master');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });

        var table = $('#table-inspectionPickup');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });

        var table = $('#table-inspectionPMK');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });
    </script>
@endsection

@endsection
