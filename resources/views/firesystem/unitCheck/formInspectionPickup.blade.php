<style>
  .modal{
    max-height: calc(150vh - 250px);
    overflow-y: auto;
  }
</style>
<div id="result-form-konten"></div>
    <form onsubmit="return false;" id="form-konten" class='form-horizontal'>
        <table width="100%">
            <tbody>
                <tr>
                  <td class="text-center">
                    <img src="public/assets/img/logosemen.png" class="img-responsive" alt="logo" width="80" style="padding-top:20px">
                  </td>
                </tr>
                <tr>
                  <td class="text-center">
                    <h6 class="modal-title" id="title">Form Inspection Pickup</h6>
                    <h4 class="modal-title" id="title"></h4><small class="font-bold">PT. Semen Indonesia</small>
                  </td>
                </tr>
            </tbody>
        </table>
        <hr>
        <div class="modal-body">
            <div class="row go_form">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Vehicle Code :</label>
                        <input type="text" readonly class="form-control daterange-cus" name="vehicle_code" value="{{ $data->vehicle_code }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Report Date</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fas fa-calendar"></i>
                                </div>
                            </div>
                            <input type="date" class="form-control daterange-cus" name="report_date" value="<?php echo date('Y-m-d');?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Shift :</label>
                        <select name="shift" class="form-control">
                            <option <?php if($data->shift =='1') {echo "selected";}?> value="1">Shift 1</option>
                            <option <?php if($data->shift =='2') {echo "selected";}?> value="2">Shift 2</option>
                            <option <?php if($data->shift =='3') {echo "selected";}?> value="3">Shift 3</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Select Clock</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fas fa-clock"></i>
                                </div>
                            </div>
                            <input type="time" class="form-control daterange-cus" name="report_time" value="<?php echo date('H-i-s');?>">
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th width="1%" class="text-center">No.</th>
                        <th width="40%" > Checked Item</th>
                        <th width="79%" class="text-left">Inspection <span style="color:red; float: right;">(*)</span></th>
                        <!-- <th width="35%" class="text-left">Note <small style="color:gray;float: right;"><i>(Optional)</i></small></th>
                        <th width="15%" class="text-left">Image <small style="color:gray;float: right;"><i>(Optional)</i></small></th> -->
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th class="text-center">1</th>
                        <th>Pemanasan (Menit)</th>
                        <th>
                            <input type="number" min="0" class="form-control daterange-cus" name="pemanasan" value="{{ $data->pemanasan }}" placeholder="Ex : 0">
                        </th>
                    </tr>
                    <tr>
                        <th class="text-center">2</th>
                        <th>Spedometer (Km)</th>
                        <th>
                            <input type="number" min="0" class="form-control daterange-cus" name="spedometer" value="{{ $data->spedometer }}" placeholder="Ex : 0">
                        </th>
                    </tr>
                    <tr>
                        <th class="text-center">3</th>
                        <th>Level BBM</th>
                        <th>
                            <select name="level_bbm" class="form-control">
                                <option <?php if($data->level_bbm =='1') {echo "selected";}?> value="1">R</option>
                                <option <?php if($data->level_bbm =='1/4') {echo "selected";}?> value="1/4">1/4</option>
                                <option <?php if($data->level_bbm =='1/2') {echo "selected";}?> value="1/2">1/2</option>
                                <option <?php if($data->level_bbm =='3/4') {echo "selected";}?> value="3/4">3/4</option>
                                <option <?php if($data->level_bbm =='F') {echo "selected";}?> value="F">F</option>
                            </select>
                        </th>
                    </tr>
                    <tr>
                        <th colspan="3" class="btn-primary" style="height:10px">Kondisi Oli</th>
                    </tr>
                    <tr>
                        <th class="text-center">4</th>
                        <th>Oli Mesin</th>
                        <th>
                            <select name="oli_mesin" class="form-control">
                                <option <?php if($data->oli_mesin =='L') {echo "selected";}?> value="L">L</option>
                                <option <?php if($data->oli_mesin =='M') {echo "selected";}?> value="M">M</option>
                                <option <?php if($data->oli_mesin =='H') {echo "selected";}?> value="H">H</option>
                            </select>
                        </th>
                    </tr>
                    <tr>
                        <th class="text-center">5</th>
                        <th>Oli Rem</th>
                        <th>
                            <select name="oli_rem" class="form-control">
                                <option <?php if($data->oli_rem =='L') {echo "selected";}?> value="L">L</option>
                                <option <?php if($data->oli_rem =='M') {echo "selected";}?> value="M">M</option>
                                <option <?php if($data->oli_rem =='H') {echo "selected";}?> value="H">H</option>
                            </select>
                        </th>
                    </tr>
                    <tr>
                        <th class="text-center">6</th>
                        <th>Oli Power Steering</th>
                        <th>
                            <select name="oli_power" class="form-control">
                                <option <?php if($data->oli_power =='L') {echo "selected";}?> value="L">L</option>
                                <option <?php if($data->oli_power =='M') {echo "selected";}?> value="M">M</option>
                                <option <?php if($data->oli_power =='H') {echo "selected";}?> value="H">H</option>
                            </select>
                        </th>
                    </tr>
                    <tr>
                        <th colspan="3" class="btn-primary" style="height:10px">Kondisi Air</th>
                    </tr>
                    <tr>
                        <th class="text-center">7</th>
                        <th>Air Radiator</th>
                        <th>
                            <select name="air_radiator" class="form-control">
                                <option <?php if($data->air_radiator =='1') {echo "selected";}?> value="1">R</option>
                                <option <?php if($data->air_radiator =='1/4') {echo "selected";}?> value="1/4">1/4</option>
                                <option <?php if($data->air_radiator =='1/2') {echo "selected";}?> value="1/2">1/2</option>
                                <option <?php if($data->air_radiator =='3/4') {echo "selected";}?> value="3/4">3/4</option>
                                <option <?php if($data->air_radiator =='F') {echo "selected";}?> value="F">F</option>
                            </select>
                        </th>
                    </tr>
                    <tr>
                        <th class="text-center">8</th>
                        <th>Air Cadangan Radiator</th>
                        <th>
                            <select name="air_cad_radiator" class="form-control">
                                <option <?php if($data->air_cad_radiator =='1') {echo "selected";}?> value="1">R</option>
                                <option <?php if($data->air_cad_radiator =='1/4') {echo "selected";}?> value="1/4">1/4</option>
                                <option <?php if($data->air_cad_radiator =='1/2') {echo "selected";}?> value="1/2">1/2</option>
                                <option <?php if($data->air_cad_radiator =='3/4') {echo "selected";}?> value="3/4">3/4</option>
                                <option <?php if($data->air_cad_radiator =='F') {echo "selected";}?> value="F">F</option>
                            </select>
                        </th>
                    </tr>
                    <tr>
                        <th class="text-center">9</th>
                        <th>Air Wiper</th>
                        <th>
                            <select name="air_wiper" class="form-control">
                                <option <?php if($data->air_wiper =='1') {echo "selected";}?> value="1">R</option>
                                <option <?php if($data->air_wiper =='1/4') {echo "selected";}?> value="1/4">1/4</option>
                                <option <?php if($data->air_wiper =='1/2') {echo "selected";}?> value="1/2">1/2</option>
                                <option <?php if($data->air_wiper =='3/4') {echo "selected";}?> value="3/4">3/4</option>
                                <option <?php if($data->air_wiper =='F') {echo "selected";}?> value="F">F</option>
                            </select>
                        </th>
                    </tr>
                    <tr>
                        <th colspan="3" class="btn-primary" style="height:10px">Kondisi Tekanan Ban (Bar)</th>
                    </tr>
                    <tr>
                        <th class="text-center">10</th>
                        <th>Ban Depan Kiri</th>
                        <th>
                            <input type="number" min="0" class="form-control daterange-cus" name="ban_depan_kiri" value="{{ $data->ban_depan_kiri }}" placeholder="Ex : 0">
                        </th>
                    </tr>
                    <tr>
                        <th class="text-center">11</th>
                        <th>Ban Depan Kanan</th>
                        <th>
                            <input type="number" min="0" class="form-control daterange-cus" name="ban_depan_kanan" value="{{ $data->ban_depan_kanan }}" placeholder="Ex : 0">
                        </th>
                    </tr>
                    <tr>
                        <th class="text-center">12</th>
                        <th>Ban Belakang Kiri Dalam</th>
                        <th>
                            <input type="number" min="0" class="form-control daterange-cus" name="ban_belakang_kiri_dalam" value="{{ $data->ban_belakang_kiri_dalam }}" placeholder="Ex : 0">
                        </th>
                    </tr>
                    <tr>
                        <th class="text-center">13</th>
                        <th>Ban Belakang Kiri Luar</th>
                        <th>
                            <input type="number" min="0" class="form-control daterange-cus" name="ban_belakang_kiri_luar" value="{{ $data->ban_belakang_kiri_luar }}" placeholder="Ex : 0">
                        </th>
                    </tr>
                    <tr>
                        <th class="text-center">14</th>
                        <th>Ban Belakang Kanan Dalam</th>
                        <th>
                            <input type="number" min="0" class="form-control daterange-cus" name="ban_belakang_kanan_dalam" value="{{ $data->ban_belakang_kanan_dalam }}" placeholder="Ex : 0">
                        </th>
                    </tr>
                    <tr>
                        <th class="text-center">15</th>
                        <th>Ban Belakang Kanan Luar</th>
                        <th>
                            <input type="number" min="0" class="form-control daterange-cus" name="ban_belakang_kanan_luar" value="{{ $data->ban_belakang_kanan_luar }}" placeholder="Ex : 0">
                        </th>
                    </tr>
                    <tr>
                        <th colspan="3" class="btn-primary" style="height:10px">Kondisi Lampu Lampu</th>
                    </tr>
                    <tr>
                        <th class="text-center">16</th>
                        <th>Lampu Kabin</th>
                        <th>
                          <div class="row">
                            <div class="col-sm-6">
                              <div class="radio radio-info" style="margin:6px 0">
                                <input type="radio" name="lampu_cabin" <?php if($data->lampu_cabin =='BAIK') {echo "checked";}?> value="BAIK" checked>
                                <label for="casing1">BAIK</label>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="radio radio-info" style="margin:6px 0">
                                <input type="radio" name="lampu_cabin" <?php if($data->lampu_cabin =='MATI') {echo "checked";}?> value="MATI">
                                <label for="casing2">MATI</label>
                              </div>
                            </div>
                          </div>
                        </th>
                    </tr>
                    <tr>
                        <th class="text-center">17</th>
                        <th>Lampu Kota</th>
                        <th>
                          <div class="row">
                            <div class="col-sm-6">
                              <div class="radio radio-info" style="margin:6px 0">
                                <input type="radio" name="lampu_kota" <?php if($data->lampu_kota =='BAIK') {echo "checked";}?> value="BAIK" checked>
                                <label for="casing1">BAIK</label>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="radio radio-info" style="margin:6px 0">
                                <input type="radio" name="lampu_kota" <?php if($data->lampu_kota =='MATI') {echo "checked";}?> value="MATI">
                                <label for="casing2">MATI</label>
                              </div>
                            </div>
                          </div>
                        </th>
                    </tr>
                    <tr>
                        <th class="text-center">18</th>
                        <th>Lampu Jauh</th>
                        <th>
                          <div class="row">
                            <div class="col-sm-6">
                              <div class="radio radio-info" style="margin:6px 0">
                                <input type="radio" name="lampu_jauh" <?php if($data->lampu_jauh =='BAIK') {echo "checked";}?> value="BAIK" checked>
                                <label for="casing1">BAIK</label>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="radio radio-info" style="margin:6px 0">
                                <input type="radio" name="lampu_jauh" <?php if($data->lampu_jauh =='MATI') {echo "checked";}?> value="MATI">
                                <label for="casing2">MATI</label>
                              </div>
                            </div>
                          </div>
                        </th>
                    </tr>
                    <tr>
                        <th class="text-center">19</th>
                        <th>Lampu Sign Kiri</th>
                        <th>
                          <div class="row">
                            <div class="col-sm-6">
                              <div class="radio radio-info" style="margin:6px 0">
                                <input type="radio" name="lampu_sign_kiri" <?php if($data->lampu_sign_kiri =='BAIK') {echo "checked";}?> value="BAIK" checked>
                                <label for="casing1">BAIK</label>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="radio radio-info" style="margin:6px 0">
                                <input type="radio" name="lampu_sign_kiri" <?php if($data->lampu_sign_kiri =='MATI') {echo "checked";}?> value="MATI">
                                <label for="casing2">MATI</label>
                              </div>
                            </div>
                          </div>
                        </th>
                    </tr>
                    <tr>
                        <th class="text-center">20</th>
                        <th>Lampu Sign Kanan</th>
                        <th>
                          <div class="row">
                            <div class="col-sm-6">
                              <div class="radio radio-info" style="margin:6px 0">
                                <input type="radio" name="lampu_sign_kanan" <?php if($data->lampu_sign_kanan =='BAIK') {echo "checked";}?> value="BAIK" checked>
                                <label for="casing1">BAIK</label>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="radio radio-info" style="margin:6px 0">
                                <input type="radio" name="lampu_sign_kanan" <?php if($data->lampu_sign_kanan =='MATI') {echo "checked";}?> value="MATI">
                                <label for="casing2">MATI</label>
                              </div>
                            </div>
                          </div>
                        </th>
                    </tr>
                    <tr>
                        <th class="text-center">21</th>
                        <th>Lampu Rem</th>
                        <th>
                          <div class="row">
                            <div class="col-sm-6">
                              <div class="radio radio-info" style="margin:6px 0">
                                <input type="radio" name="lampu_rem" <?php if($data->lampu_rem =='BAIK') {echo "checked";}?> value="BAIK" checked>
                                <label for="casing1">BAIK</label>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="radio radio-info" style="margin:6px 0">
                                <input type="radio" name="lampu_rem" <?php if($data->lampu_rem =='MATI') {echo "checked";}?> value="MATI">
                                <label for="casing2">MATI</label>
                              </div>
                            </div>
                          </div>
                        </th>
                    </tr>
                    <tr>
                        <th class="text-center">22</th>
                        <th>Lampu Atret</th>
                        <th>
                          <div class="row">
                            <div class="col-sm-6">
                              <div class="radio radio-info" style="margin:6px 0">
                                <input type="radio" name="lampu_atret" <?php if($data->lampu_atret =='BAIK') {echo "checked";}?> value="BAIK" checked>
                                <label for="casing1">BAIK</label>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="radio radio-info" style="margin:6px 0">
                                <input type="radio" name="lampu_atret" <?php if($data->lampu_atret =='MATI') {echo "checked";}?> value="MATI">
                                <label for="casing2">MATI</label>
                              </div>
                            </div>
                          </div>
                        </th>
                    </tr>
                    <tr>
                        <th class="text-center">23</th>
                        <th>Lampu Sorot</th>
                        <th>
                          <div class="row">
                            <div class="col-sm-6">
                              <div class="radio radio-info" style="margin:6px 0">
                                <input type="radio" name="lampu_sorot" <?php if($data->lampu_sorot =='BAIK') {echo "checked";}?> value="BAIK" checked>
                                <label for="casing1">BAIK</label>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="radio radio-info" style="margin:6px 0">
                                <input type="radio" name="lampu_sorot" <?php if($data->lampu_sorot =='MATI') {echo "checked";}?> value="MATI">
                                <label for="casing2">MATI</label>
                              </div>
                            </div>
                          </div>
                        </th>
                    </tr>
                    <tr>
                        <th class="text-center">24</th>
                        <th>Panel Dashboard</th>
                        <th>
                          <div class="row">
                            <div class="col-sm-6">
                              <div class="radio radio-info" style="margin:6px 0">
                                <input type="radio" name="panel_dashboard" <?php if($data->panel_dashboard =='BAIK') {echo "checked";}?> value="BAIK" checked>
                                <label>BAIK</label>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="radio radio-info" style="margin:6px 0">
                                <input type="radio" name="panel_dashboard" <?php if($data->panel_dashboard =='MATI') {echo "checked";}?> value="MATI">
                                <label>MATI</label>
                              </div>
                            </div>
                          </div>
                        </th>
                    </tr>
                    <tr>
                        <th colspan="3" class="btn-primary" style="height:10px">Kondisi Kaca Spion</th>
                    </tr>
                    <tr>
                        <th class="text-center">25</th>
                        <th>Kaca Spion Kiri</th>
                        <th>
                          <div class="row">
                            <div class="col-sm-6">
                              <div class="radio radio-info" style="margin:6px 0">
                                <input type="radio" name="kaca_spion_kiri" <?php if($data->kaca_spion_kiri =='BAIK') {echo "checked";}?> value="BAIK" checked>
                                <label for="casing1">BAIK</label>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="radio radio-info" style="margin:6px 0">
                                <input type="radio" name="kaca_spion_kiri" <?php if($data->kaca_spion_kiri =='PECAH') {echo "checked";}?> value="PECAH">
                                <label for="casing2">PECAH</label>
                              </div>
                            </div>
                          </div>
                        </th>
                    </tr>
                    <tr>
                        <th class="text-center">26</th>
                        <th>Kaca Spion Kanan</th>
                        <th>
                          <div class="row">
                            <div class="col-sm-6">
                              <div class="radio radio-info" style="margin:6px 0">
                                <input type="radio" name="kaca_spion_kanan" <?php if($data->kaca_spion_kanan =='BAIK') {echo "checked";}?> value="BAIK" checked>
                                <label for="casing1">BAIK</label>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="radio radio-info" style="margin:6px 0">
                                <input type="radio" name="kaca_spion_kanan" <?php if($data->kaca_spion_kanan =='PECAH') {echo "checked";}?> value="PECAH">
                                <label for="casing2">PECAH</label>
                              </div>
                            </div>
                          </div>
                        </th>
                    </tr>
                </tbody>
            </table>
            <div class="table-responsive">
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            <button type="submit" class="btn btn-primary" data-loading-text="<i class='fa fa-spinner fa-spin'></i>
              &nbsp;<font class='lowercase'></font>"> <i class="fa fa-check"></i> Save</button>
        </div>
    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/unitCheck/saveInspectionPickup', data, '#result-form-konten');
        })
    })
</script>
