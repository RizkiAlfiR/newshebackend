@extends('layouts.app')

@section('content')
<title>Inspection Pickup | Safety Hygiene Environment System</title>
<section class="section">
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                  <div class="col-12" style="padding-top:20px">
                    <div>
                      <div>
                          <div class="tab-content" id="myTabContent">
                              <div class="tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
                                <div class="">
                                    <h5>List Inspection Pickup</h5>
                                    <div class="">
                                        <div class="table-responsive">
                                            <table width="60%">
                                            <tbody>
                                                <tr>
                                                    <td width="20%"><strong> Vehicle Code </strong></td>
                                                    <td width="2%"> :</td>
                                                    <td><span id="view_vehicle_code"></span>{{$listVehicle->vehicle_code}}</td>
                                                </tr>
                                                <tr>
                                                    <td width="20%"><strong> Vehicle Merk </strong></td>
                                                    <td width="2%"> :</td>
                                                    <td><span id="view_vehicle_merk"></span> {{$listVehicle->vehicle_merk}}</td>
                                                </tr>
                                                <tr>
                                                    <td width="20%"><strong> Nomor Rangka </strong></td>
                                                    <td width="2%"> :</td>
                                                    <td><span id="view_nomor_rangka"></span> {{$listVehicle->nomor_rangka}}</td>
                                                </tr>
                                            </tbody>
                                            </table>
                                          </div>
                                          <div class="table-responsive">
                                            <table class="table table-striped" id="table-inspection" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center align-middle">No</th>
                                                        <th class="text-center align-middle">Inspector</th>
                                                        <th class="text-center align-middle">Inspection Date</th>
                                                        <th class="text-center align-middle">Inspection Time</th>
                                                        <th class="text-center align-middle">Negative List</th>
                                                        <th class="text-center align-middle">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($inspectionPickup as $i => $data)
                                                    <tr>
                                                        <td class="text-center">{{ $i+1 }}</td>
                                                        <td>{{ $data->pic_name }}</td>
                                                        <td class="text-center">{{ $data->report_date }}</td>
                                                        <td class="text-center">{{ $data->report_time }}</td>
                                                        <td class="align-middle text-center">
                                                          @if($data->sum_negative==0 || $data->sum_negative=='' || $data->sum_negative=='null')
                                                            <button type="button" class="btn btn-icon icon-left btn-success">
                                                                0
                                                            </button>
                                                          @else
                                                            <button type="button" class="btn btn-icon icon-left btn-danger">
                                                                {{ $data->sum_negative }}
                                                            </button>
                                                          @endif
                                                        </td>
                                                        <td>
                                                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                                <!-- <button type="button" class="btn btn-icon icon-left btn-info"
                                                                onclick="loadModal(this)" title="" target="/unitCheck/updateInspectionPickup" data="id={{$data->id}}">
                                                                    <i class="fas fa-edit"></i>
                                                                </button> -->
                                                                <button type="button" class="btn btn-icon icon-left btn-danger"
                                                                onclick="deleteInspection({{$data->id}})">
                                                                    <i class="fas fa-trash"></i>
                                                                </button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                              </div>
                          </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@section('scripts')
    <script>
        function deleteInspection(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Confirmation", "Do you want to remove this data?", function () {
               ajaxTransfer("/unitCheck/deleteInspectionPickup", data, "#modal-output");
            })
        }

        var table = $('#table-inspection');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });
    </script>
@endsection

@endsection
