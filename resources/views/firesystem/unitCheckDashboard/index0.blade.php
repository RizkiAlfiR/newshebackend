@extends('layouts.app')

@section('content')
<title>Unit Check Dashboard | Safety Hygiene Environment System</title>
<section class="section">
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                  <div class="col-12" style="padding-top:20px">
                    <div class="nav-wrapper">
                        <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0 active" id="tabs-icons-text-1-tab" data-toggle="tab" href="#tabs-icons-text-1" role="tab" aria-controls="tabs-icons-text-1" aria-selected="true"><i class="ni ni-cloud-upload-96 mr-2"></i>Inspection Pickup</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-2-tab" data-toggle="tab" href="#tabs-icons-text-2" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>Inspection PMK</a>
                            </li>
                        </ul>
                    </div>
                    <div>
                      <div>
                          <div class="tab-content" id="myTabContent">
                              <div class="tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
                                <div id="result-form-konten"></div>
                                    <form onsubmit="return false;" id="form-konten" class='form-horizontal'>
                                        <input type="text" class="form-control daterange-cus" id="bulan" name="bulan">
                                        <button type="submit" class="btn btn-info" data-loading-text="<i class='fa fa-spinner fa-spin'></i>
                                          &nbsp;<font class='lowercase'></font>"> <i class="fa fa-check"></i> Save</button>
                                    </form>
                                  <div class="" style="margin-top:50px">
                                      <div class="table-responsive">
                                          <div id="vehiclePickup" style="min-width: 310px; height: 500px; max-width: 1000px; margin: 0 auto"></div>
                                      </div>
                                  </div>
                              </div>
                              <div class="tab-pane fade" id="tabs-icons-text-2" role="tabpanel" aria-labelledby="tabs-icons-text-2-tab">
                                  <div class="" style="margin-top:50px">
                                      <div class="table-responsive">
                                          <div id="vehiclePMK" style="min-width: 310px; height: 800px; max-width: 1000px; margin: 0 auto"></div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@section('scripts')
  <script>
      // $(document).ready(function(){
      //     $("#btn").click(function(){
      //         var bulan = $("#bulan").val();
      //
      //         ajaxTransfer("/unitCheckDashboard/getDate", bulan);
      //     })
      // });

      $(document).ready(function () {
          $('#form-konten').submit(function () {
              var data = getFormData('form-konten');
              ajaxTransfer('/unitCheckDashboard/getDate', data, '#result-form-konten');
          })
      })
  </script>
    <script>
        function deleteMaster(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Confirmation", "Do you want to remove this data ?", function () {
               ajaxTransfer("/unitCheck/deleteMaster", data, "#modal-output");
            })
        }

        function deleteInspectionPickup(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Confirmation", "Do you want to remove this data ?", function () {
               ajaxTransfer("/unitCheck/deleteInspectionPickup", data, "#modal-output");
            })
        }

        function deleteInspectionPMK(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Confirmation", "Do you want to remove this data ?", function () {
               ajaxTransfer("/unitCheck/deleteInspectionPMK", data, "#modal-output");
            })
        }

        var table = $('#table-master');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });

        var table = $('#table-inspectionPickup');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });

        var table = $('#table-inspectionPMK');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });
    </script>

    <script>
        $(document).ready(function() {
            $.ajax({
                type: 'GET',
                url: "{{url('unitCheckDashboard/masterVehicle')}}",
                dataType: 'json',
                success: function (data) {
                    getVehiclePickup(data);
                    getVehiclePMK(data);
                }
            });
        });

        // function getVehiclePickup(data) {
        //     Highcharts.chart('container', {
        //         chart: {
        //             type: 'bar'
        //         },
        //         title: {
        //             text: ''
        //         },
        //         subtitle: {
        //             text: ''
        //         },
        //         xAxis: {
        //           categories: [
        //               'Januari',
        //               'Februari',
        //               'Maret',
        //               'April',
        //               'Mei',
        //               'Juni',
        //               'Juli',
        //               'Agustus',
        //               'September',
        //               'Oktober',
        //               'November',
        //               'Desember'
        //           ],
        //           crosshair: true
        //         },
        //         yAxis: {
        //             min: 0,
        //             title: {
        //                 text: 'Jumlah Report Accident'
        //             }
        //         },
        //         tooltip: {
        //             headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        //             pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
        //             '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
        //             footerFormat: '</table>',
        //             shared: true,
        //             useHTML: true
        //         },
        //         plotOptions: {
        //             column: {
        //                 pointPadding: 0.2,
        //                 borderWidth: 0
        //             }
        //         },
        //         series: [{
        //             name: 'Open Report',
        //             data: ['0','0','1','0','3','4','5','6','2','3','1','12'],
        //             color: 'red'
        //         }, {
        //             name: 'Close Report',
        //             data: ['0','0','1','0','3','4','5','6','2','3','1','12'],
        //             color: 'lightgreen'
        //         }]
        //     });
        // }

    function getVehiclePickup(data) {
            Highcharts.chart('vehiclePickup', {
                chart: {
                    type: 'bar'
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: data.isPickup,
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Negative List Inspection Pickup Periode ' + data.title
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b> {point.y:.0f}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: data.title,
                    data: data.isInspectionPickup,
                    color: 'red'
                }]
            });
        }

        function getVehiclePMK(data) {
                Highcharts.chart('vehiclePMK', {
                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        categories: data.isPMK,
                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Negative List Inspection PMK Periode ' + data.title
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b> {point.y:.0f}</b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    series: [{
                        name: data.title,
                        data: data.isInspectionPMK,
                        color: 'red'
                    }]
                });
            }
    </script>
@endsection

@endsection
