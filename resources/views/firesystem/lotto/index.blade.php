@extends('layouts.app')

@section('content')
<title>Lotto | Safety Hygiene Environment System</title>
<section class="section">
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                  <div class="col-12" style="padding-top:20px">
                    <div>
                      <div>
                          <div class="">
                                    <h5>List Lotto</h5>
                                    <p class="">
                                        <a onclick="loadModal(this)" title="" target="/lotto/add" class="btn btn-icon icon-left btn-primary" style="color:white">
                                          <i class="far fa-edit"></i> Create New</a>
                                    </p>
                                    <div class="">
                                        <div class="table-responsive">
                                            <table class="table table-striped" style="width:100%"  id="table-lotto">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">No</th>
                                                        <th>Date</th>
                                                        <th>Equipment Number</th>
                                                        <th>Location</th>
                                                        <th>Activity</th>
                                                        <th>Status</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($lotto as $i => $data)
                                                    <tr>
                                                        <td class="align-middle text-center">{{ $i+1 }}</td>
                                                        <td class="align-middle">{{ $data->date}}</td>
                                                        <td class="align-middle">{{ $data->equipment_number}}</td>
                                                        <td class="align-middle">{{ $data->location}}</td>
                                                        <td class="align-middle">{{ $data->activity_description }}</td>
                                                        <td class="align-middle text-center">
                                                          @if($data->status == "OPEN")
                                                            <button type="button" class="btn btn-icon icon-left btn-success">
                                                                OPEN
                                                            </button>
                                                          @else
                                                            <button type="button" class="btn btn-icon icon-left btn-danger">
                                                                CLOSE
                                                            </button>
                                                          @endif
                                                        </td>
                                                        <td class="align-middle text-center">
                                                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                                <button type="button" class="btn btn-icon icon-left btn-info"
                                                                onclick="loadModal(this)" title="" target="/lotto/add" data="id={{$data->id}}">
                                                                    <i class="fas fa-edit"></i>
                                                                </button>
                                                                <button type="button" class="btn btn-icon icon-left btn-danger"
                                                                onclick="deleteLotto({{$data->id}})">
                                                                    <i class="fas fa-trash"></i>
                                                                </button>
                                                                <button type="button" class="btn btn-icon icon-left btn-success"
                                                                onclick="loadModal(this)" title="" target="/lotto/draw?id=" data="id={{$data->id}}">
                                                                    <i class="fa fa-lock"></i>
                                                                </button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                          </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@section('scripts')
    <script>
        function deleteLotto(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Confirmation", "Do you want to remove this data ?", function () {
               ajaxTransfer("/lotto/delete", data, "#modal-output");
            })
        }

        var table = $('#table-lotto');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });
    </script>
@endsection

@endsection
