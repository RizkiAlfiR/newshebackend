<style>
  .modal{
    max-height: calc(150vh - 250px);
    overflow-y: auto;
  }
</style>
<div id="result-form-konten"></div>
    <form onsubmit="return false;" id="form-konten" class='form-horizontal'>
        <table width="100%">
            <tbody>
                <tr>
                  <td class="text-center">
                    <img src="public/assets/img/logosemen.png" class="img-responsive" alt="logo" width="80" style="padding-top:20px">
                  </td>
                </tr>
                <tr>
                  <td class="text-center">
                    <h6 class="modal-title" id="title">Form Draw IN/Draw OUT</h6>
                    <h4 class="modal-title" id="title"></h4><small class="font-bold">PT. Semen Indonesia</small>
                  </td>
                </tr>
            </tbody>
        </table>
        <div class="modal-body">
          <hr>
          <div class="col-md-12">
              <div class="form-group">
                <div class="nav-wrapper">
                    <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-form" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link mb-sm-3 mb-md-0 active" id="tabs-form-1-tab" data-toggle="tab" href="#tabs-form-1" role="tab" aria-controls="tabs-form-1" aria-selected="true"><i class="ni ni-cloud-upload-96 mr-2"></i>DRAW IN</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link mb-sm-3 mb-md-0" id="tabs-form-2-tab" data-toggle="tab" href="#tabs-form-2" role="tab" aria-controls="tabs-form-2" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>DRAW OUT</a>
                        </li>
                    </ul>
                </div>
                <div>
                  <div>
                      <div class="tab-content" id="myTabForm">
                          <div class="tab-pane fade show active" id="tabs-form-1" role="tabpanel" aria-labelledby="tabs-form-1-tab">
                            <div class="row">
                            <div class="col-md-6" style="background-color:#F6F7FE">
                              <div class="form-group">
                                  @if($data->k3_drawin_name=='')
                                    <div class="btn-danger text-center">
                                  @else
                                    <div class="btn-primary text-center">
                                  @endif
                                    <Text>K3</Text>
                                  </div>
                                  <label>PIC</label>
                                  <select name="k3_drawin_name" class="form-control">
                                      <option value="{{null.'|'.null}}">Chose Employee</option>
                                      @foreach($employe as $item)
                                      <option value="{{ $item->mk_nopeg.'|'.$item->mk_nama }}" @if(!is_null($data)) @if($item->mk_nama == $data->k3_drawin_name) selected="selected" @endif @endif>{{ $item->mk_nama}}</option>
                                      @endforeach
                                  </select>
                                  <label>Keterangan</label>
                                  <select name="k3_drawin_ket" class="form-control">
                                      @foreach($keterangan as $item)
                                      <option value="{{ $item->sim }}" @if(!is_null($data)) @if($item->sim == $data->k3_drawin_ket) selected="selected" @endif @endif>{{ $item->ket}}</option>
                                      @endforeach
                                  </select>
                              </div>
                            </div>
                            <div class="col-md-6" style="background-color:#F6F7FE">
                              <div class="form-group">
                                  @if($data->pmll_drawin_name=='')
                                    <div class="btn-danger text-center">
                                  @else
                                    <div class="btn-primary text-center">
                                  @endif
                                    <Text>PML LISTRIK</Text>
                                  </div>
                                  <label>PIC</label>
                                  <select name="pmll_drawin_name" class="form-control">
                                      <option value="{{null.'|'.null}}">Chose Employee</option>
                                      @foreach($employe as $item)
                                      <option value="{{ $item->mk_nopeg.'|'.$item->mk_nama }}" @if(!is_null($data)) @if($item->mk_nama == $data->pmll_drawin_name) selected="selected" @endif @endif>{{ $item->mk_nama}}</option>
                                      @endforeach
                                  </select>
                                  <label>Keterangan</label>
                                  <select name="pmll_drawin_ket" class="form-control">
                                      @foreach($keterangan as $item)
                                      <option value="{{ $item->sim }}" @if(!is_null($data)) @if($item->sim == $data->pmll_drawin_ket) selected="selected" @endif @endif>{{ $item->ket}}</option>
                                      @endforeach
                                  </select>
                              </div>
                            </div>
                            <div class="col-md-6" style="padding-top:20px; background-color:#F6F7FE">
                              <div class="form-group">
                                  @if($data->operator_drawin_name=='')
                                    <div class="btn-danger text-center">
                                  @else
                                    <div class="btn-primary text-center">
                                  @endif
                                    <Text>OPERATOR</Text>
                                  </div>
                                  <label>PIC</label>
                                  <select name="operator_drawin_name" class="form-control">
                                      <option value="{{null.'|'.null}}">Chose Employee</option>
                                      @foreach($employe as $item)
                                      <option value="{{ $item->mk_nopeg.'|'.$item->mk_nama }}" @if(!is_null($data)) @if($item->mk_nama == $data->operator_drawin_name) selected="selected" @endif @endif>{{ $item->mk_nama}}</option>
                                      @endforeach
                                  </select>
                                  <label>Keterangan</label>
                                  <select name="operator_drawin_ket" class="form-control">
                                      @foreach($keterangan as $item)
                                      <option value="{{ $item->sim }}" @if(!is_null($data)) @if($item->sim == $data->operator_drawin_ket) selected="selected" @endif @endif>{{ $item->ket}}</option>
                                      @endforeach
                                  </select>
                              </div>
                            </div>
                            <div class="col-md-6" style="padding-top:20px; background-color:#F6F7FE">
                              <div class="form-group">
                                  @if($data->pmlt_drawin_name=='')
                                    <div class="btn-danger text-center">
                                  @else
                                    <div class="btn-primary text-center">
                                  @endif
                                    <Text>PML TERKAIT</Text>
                                  </div>
                                  <label>PIC</label>
                                  <select name="pmlt_drawin_name" class="form-control">
                                      <option value="{{null.'|'.null}}">Chose Employee</option>
                                      @foreach($employe as $item)
                                      <option value="{{ $item->mk_nopeg.'|'.$item->mk_nama }}" @if(!is_null($data)) @if($item->mk_nama == $data->pmlt_drawin_name) selected="selected" @endif @endif>{{ $item->mk_nama}}</option>
                                      @endforeach
                                  </select>
                                  <label>Keterangan</label>
                                  <select name="pmlt_drawin_ket" class="form-control">
                                      @foreach($keterangan as $item)
                                      <option value="{{ $item->sim }}" @if(!is_null($data)) @if($item->sim == $data->pmlt_drawin_ket) selected="selected" @endif @endif>{{ $item->ket}}</option>
                                      @endforeach
                                  </select>
                                  <!-- <div class="row">
                                    <div class="col-lg-12">
                                      <form class="form-group">
                                        <label>Select Employee</label>
                                        <select name="employe" class="form-control employe">

                                        </select>
                                      </form>
                                    </div>
                                  </div> -->
                              </div>
                            </div>
                          </div>
                          </div>
                          <div class="tab-pane fade" id="tabs-form-2" role="tabpanel" aria-labelledby="tabs-form-2-tab">
                            <div class="row">
                            <div class="col-md-6" style="background-color:#F6F7FE">
                              <div class="form-group">
                                  @if($data->k3_drawout_name=='')
                                    <div class="btn-danger text-center">
                                  @else
                                    <div class="btn-primary text-center">
                                  @endif
                                    <Text>K3</Text>
                                  </div>
                                  <label>PIC</label>
                                  <select name="k3_drawout_name" class="form-control">
                                      <option value="{{null.'|'.null}}">Chose Employee</option>
                                      @foreach($employe as $item)
                                      <option value="{{ $item->mk_nopeg.'|'.$item->mk_nama }}" @if(!is_null($data)) @if($item->mk_nama == $data->k3_drawout_name) selected="selected" @endif @endif>{{ $item->mk_nama}}</option>
                                      @endforeach
                                  </select>
                                  <label>Keterangan</label>
                                  <select name="k3_drawout_ket" class="form-control">
                                      @foreach($keterangan as $item)
                                      <option value="{{ $item->sim }}" @if(!is_null($data)) @if($item->sim == $data->k3_drawout_ket) selected="selected" @endif @endif>{{ $item->ket}}</option>
                                      @endforeach
                                  </select>
                              </div>
                            </div>
                            <div class="col-md-6" style="background-color:#F6F7FE">
                              <div class="form-group">
                                  @if($data->pmll_drawout_name=='')
                                    <div class="btn-danger text-center">
                                  @else
                                    <div class="btn-primary text-center">
                                  @endif
                                    <Text>PML LISTRIK</Text>
                                  </div>
                                  <label>PIC</label>
                                  <select name="pmll_drawout_name" class="form-control">
                                      <option value="{{null.'|'.null}}">Chose Employee</option>
                                      @foreach($employe as $item)
                                      <option value="{{ $item->mk_nopeg.'|'.$item->mk_nama }}" @if(!is_null($data)) @if($item->mk_nama == $data->pmll_drawout_name) selected="selected" @endif @endif>{{ $item->mk_nama}}</option>
                                      @endforeach
                                  </select>
                                  <label>Keterangan</label>
                                  <select name="pmll_drawout_ket" class="form-control">
                                      @foreach($keterangan as $item)
                                      <option value="{{ $item->sim }}" @if(!is_null($data)) @if($item->sim == $data->pmll_drawout_ket) selected="selected" @endif @endif>{{ $item->ket}}</option>
                                      @endforeach
                                  </select>
                              </div>
                            </div>
                            <div class="col-md-6" style="padding-top:20px; background-color:#F6F7FE">
                              <div class="form-group">
                                  @if($data->operator_drawout_name=='')
                                    <div class="btn-danger text-center">
                                  @else
                                    <div class="btn-primary text-center">
                                  @endif
                                    <Text>OPERATOR</Text>
                                  </div>
                                  <label>PIC</label>
                                  <select name="operator_drawout_name" class="form-control">
                                      <option value="{{null.'|'.null}}">Chose Employee</option>
                                      @foreach($employe as $item)
                                      <option value="{{ $item->mk_nopeg.'|'.$item->mk_nama }}" @if(!is_null($data)) @if($item->mk_nama == $data->operator_drawout_name) selected="selected" @endif @endif>{{ $item->mk_nama}}</option>
                                      @endforeach
                                  </select>
                                  <label>Keterangan</label>
                                  <select name="operator_drawout_ket" class="form-control">
                                      @foreach($keterangan as $item)
                                      <option value="{{ $item->sim }}" @if(!is_null($data)) @if($item->sim == $data->operator_drawout_ket) selected="selected" @endif @endif>{{ $item->ket}}</option>
                                      @endforeach
                                  </select>
                              </div>
                            </div>
                            <div class="col-md-6" style="padding-top:20px; background-color:#F6F7FE">
                              <div class="form-group">
                                  @if($data->pmlt_drawout_name=='')
                                    <div class="btn-danger text-center">
                                  @else
                                    <div class="btn-primary text-center">
                                  @endif
                                    <Text>PML TERKAIT</Text>
                                  </div>
                                  <label>PIC</label>
                                  <select name="pmlt_drawout_name" class="form-control">
                                      <option value="{{null.'|'.null}}">Chose Employee</option>
                                      @foreach($employe as $item)
                                      <option value="{{ $item->mk_nopeg.'|'.$item->mk_nama }}" @if(!is_null($data)) @if($item->mk_nama == $data->pmlt_drawout_name) selected="selected" @endif @endif>{{ $item->mk_nama}}</option>
                                      @endforeach
                                  </select>
                                  <label>Keterangan</label>
                                  <select name="pmlt_drawout_ket" class="form-control">
                                      @foreach($keterangan as $item)
                                      <option value="{{ $item->sim }}" @if(!is_null($data)) @if($item->sim == $data->pmlt_drawout_ket) selected="selected" @endif @endif>{{ $item->ket}}</option>
                                      @endforeach
                                  </select>
                              </div>
                            </div>
                          </div>
                      </div>
                    </div>
                </div>
              </div>
          </div>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            <button type="submit" class="btn btn-info" data-loading-text="<i class='fa fa-spinner fa-spin'></i>
              &nbsp;<font class='lowercase'></font>"><i class="fa fa-check"></i> Save</button>
        </div>
        <input type='hidden' name='id' value='{{ $data->id }}'>
        <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

@section('js')
    <script type="application/javascript">
        $(document).ready(function () {
            $('#course_category').select2({
                ajax:{
                    type:"GET",
                    placeholder:"Choose your category in here",
                    url:"{{url('/employee/getEmployee')}}",
                    dataType:"json",
                    data :function(params){
                        return{
                            key:params.term
                        };
                    },
                    processResults:function(data){
                        return{
                            results:data
                        };
                    },
                    cache:true
                },
            }).on('select2:select',function (evt) {
                var data = $('#course_category option:selected').val();
                $('#course_category_hid').val(data);
            });
        })
    </script>
@endsection





<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/lotto/saveDraw', data, '#result-form-konten');
        })
    })
</script>
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#showImage').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#inputImage").change(function () {
        readURL(this);
    });
</script>
