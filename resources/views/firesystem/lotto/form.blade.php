<style>
  .modal{
    max-height: calc(150vh - 250px);
    overflow-y: auto;
  }
</style>
<div id="result-form-konten"></div>
    <form onsubmit="return false;" id="form-konten" class='form-horizontal'>
        <table width="100%">
            <tbody>
                <tr>
                  <td class="text-center">
                    <img src="public/assets/img/logosemen.png" class="img-responsive" alt="logo" width="80" style="padding-top:20px">
                  </td>
                </tr>
                <tr>
                  <td class="text-center">
                    <h6 class="modal-title" id="title">Form Lotto</h6>
                    <h4 class="modal-title" id="title"></h4><small class="font-bold">PT. Semen Indonesia</small>
                  </td>
                </tr>
            </tbody>
        </table>
        <div class="modal-body">
          <hr>
            <div class="row go_form">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                          <div class="col-sm-7">
                              <label>Date Activity</label>
                              <div class="input-group">
                                  <input type="date" class="form-control daterange-cus" name="date"
                                  @if(!is_null($data))
                                    value="{{ $data->date}}"
                                  @else
                                    value="<?php echo date('Y-m-d')?>"
                                  @endif>
                              </div>
                          </div>
                          <div class="col-sm-5">
                              <label>Time Activity</label>
                              <div class="input-group">
                                  <input type="time" class="form-control daterange-cus" name="time"
                                  @if(!is_null($data))
                                    value="{{ $data->time }}"
                                  @else
                                    value="<?php echo date('H-i-s');?>"
                                  @endif>
                              </div>
                          </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Activity</label>
                        <textarea rows="2" maxlength="255" class="form-control input-sm" name="activity_description" style="resize: vertical;">{{ $data->activity_description }}</textarea>
                    </div>
                    <div class="form-group">
                        <label>Location</label>
                        <input type="text" class="form-control daterange-cus" name="location" value="{{ $data->location }}">
                    </div>
                    <div class="form-group">
                        <label>Note</label>
                        <textarea rows="2" maxlength="255" class="form-control input-sm" name="note" style="resize: vertical;">{{ $data->note }}</textarea>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Equipment Number</label>
                        <input type="text" class="form-control daterange-cus" name="equipment_number" value="{{ $data->equipment_number }}">
                    </div>
                    <div class="form-group" style="margin-top:20px">
                        <label>No Er</label>
                        <input type="text" class="form-control daterange-cus" name="no_er" value="{{ $data->no_er }}">
                    </div>

                    <div class="form-group">
                      <div class="nav-wrapper">
                          <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-form" role="tablist">
                              <li class="nav-item">
                                  <a class="nav-link mb-sm-3 mb-md-0 active" id="tabs-form-1-tab" data-toggle="tab" href="#tabs-form-1" role="tab" aria-controls="tabs-form-1" aria-selected="true"><i class="ni ni-cloud-upload-96 mr-2"></i>Safety Key</a>
                              </li>
                              <li class="nav-item">
                                  <a class="nav-link mb-sm-3 mb-md-0" id="tabs-form-2-tab" data-toggle="tab" href="#tabs-form-2" role="tab" aria-controls="tabs-form-2" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>Safety Tools</a>
                              </li>
                          </ul>
                      </div>
                      <div>
                        <div>
                            <div class="tab-content" id="myTabForm">
                                <div class="tab-pane fade show active" id="tabs-form-1" role="tabpanel" aria-labelledby="tabs-form-1-tab">
                                  <div class="">
                                      <label>Safety Key</label>
                                      <input type="text" class="form-control daterange-cus" name="safety_key" value="{{ $data->safety_key }}">
                                  </div>
                                </div>
                                <div class="tab-pane fade" id="tabs-form-2" role="tabpanel" aria-labelledby="tabs-form-2-tab">
                                  <div class="">
                                      <label>List Tools</label>
                                      <div>
                                      </div>
                                  </div>
                                </div>
                            </div>
                          </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            <button type="submit" class="btn btn-info" data-loading-text="<i class='fa fa-spinner fa-spin'></i>
              &nbsp;<font class='lowercase'></font>"> <i class="fa fa-check"></i> Save</button>
        </div>

    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/lotto/save', data, '#result-form-konten');
        })
    })
</script>
