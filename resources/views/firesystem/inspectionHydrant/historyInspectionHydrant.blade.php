@extends('layouts.app')

@section('content')
<title>Inspection Hydrant | Safety Hygiene Environment System</title>
<section class="section">
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                  <div class="col-12" style="padding-top:20px">
                    <div>
                      <div>
                          <div class="tab-content" id="myTabContent">
                              <div class="tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
                                <div class="">
                                    <h5>List Inspection Hydrant</h5>
                                    <div class="">
                                        <div class="table-responsive">
                                            <table width="60%">
                                            <tbody>
                                                <tr>
                                                    <td width="20%"><strong> Area </strong></td>
                                                    <td width="2%"> :</td>
                                                    <td><span id="view_area"></span>{{$lokasiHydrant->area}}</td>
                                                </tr>
                                                <tr>
                                                    <td width="20%"><strong> Unit Kerja </strong></td>
                                                    <td width="2%"> :</td>
                                                    <td><span id="view_unit_kerja"></span> {{$lokasiHydrant->unit_kerja}}</td>
                                                </tr>
                                                <tr>
                                                    <td width="20%"><strong> Plant </strong></td>
                                                    <td width="2%"> :</td>
                                                    <td>
                                                      @if($lokasiHydrant->plant == "5001")
                                                        Tuban
                                                      @elseif($lokasiHydrant->plant == "5002")
                                                        Gresik
                                                      @elseif($lokasiHydrant->plant == "5003")
                                                        Rembang
                                                      @else
                                                        Cigading
                                                      @endif
                                                    </td>
                                                </tr>
                                            </tbody>
                                            </table>
                                            <table class="table table-striped w-auto" id="table-inspection">
                                                <thead>
                                                    <tr>
                                                        <th rowspan="2" class="text-center align-middle">No</th>
                                                        <th colspan="6" class="text-center">Hasil Pengecekan</th>
                                                        <th rowspan="2" class="text-center align-middle">Date</th>
                                                        <th rowspan="2" class="text-center align-middle">Action</th>
                                                    </tr>
                                                    <tr>
                                                        <th class="text-center">Pilar</th>
                                                        <th class="text-center">Valve Copling</th>
                                                        <th class="text-center">Copling</th>
                                                        <th class="text-center">Tutup Copling</th>
                                                        <th class="text-center">Box</th>
                                                        <th class="text-center">Press</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($inspectionHydrant as $i => $data)
                                                    <tr>
                                                        <td class="text-center">{{ $i+1 }}</td>
                                                        <!-- <td>{{ $data->pic_name }}</td> -->
                                                        <td>
                                                          <ul>
                                                            <li>Body : {{ $data->pilar_body }}</li>
                                                            <li>Keterangan : {{ $data->pilar_note }}</li>
                                                          </ul>
                                                        </td>
                                                        <td>
                                                          <ul>
                                                            <li>Kanan : {{ $data->valve_kanan }}</li>
                                                            <li>Atas : {{ $data->valve_atas }}</li>
                                                            <li>Kiri : {{ $data->valve_kiri }}</li>
                                                            <li>Keterangan : {{ $data->pilar_note }}</li>
                                                          </ul>
                                                        </td>
                                                        <td>
                                                          <ul>
                                                            <li>Kanan : {{ $data->copling_kanan }}</li>
                                                            <li>Kiri : {{ $data->copling_kiri }}</li>
                                                            <li>Keterangan : {{ $data->copling_note }}</li>
                                                          </ul>
                                                        </td>
                                                        <td>
                                                          <ul>
                                                            <li>Kanan : {{ $data->t_copling_kanan }}</li>
                                                            <li>Kiri : {{ $data->t_copling_kiri }}</li>
                                                            <li>Keterangan : {{ $data->t_copling_note }}</li>
                                                          </ul>
                                                        </td>
                                                        <td>
                                                          <ul>
                                                            <li>Casing : {{ $data->box_casing }}</li>
                                                            <li>Hose : {{ $data->box_hose }}</li>
                                                            <li>Kunci : {{ $data->box_kunci }}</li>
                                                            <li>Nozle : {{ $data->box_nozle }}</li>
                                                            <li>Keterangan : {{ $data->box_note }}</li>
                                                          </ul>
                                                        </td>
                                                        <td>{{ $data->press }}</td>
                                                        <td>{{ $data->inspection_date }}</td>
                                                        <td>
                                                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                                <a href="{{url('inspectionHydrant/updateInspectionHydrant?id=')}}{{$data->id}}" class="btn btn-info btn-xs">
                                                                  <i class="fa fa-edit"></i>
                                                                </a>
                                                                <button type="button" class="btn btn-icon icon-left btn-danger"
                                                                onclick="deleteInspection({{$data->id}})">
                                                                    <i class="fas fa-trash"></i>
                                                                </button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                              </div>
                          </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@section('scripts')
    <script>
        function deleteInspection(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Confirmation", "Do you want to remove this data ?", function () {
               ajaxTransfer("/inspectionHydrant/deleteHistory", data, "#modal-output");
            })
        }

        var table = $('#table-inspection');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });
    </script>
@endsection

@endsection
