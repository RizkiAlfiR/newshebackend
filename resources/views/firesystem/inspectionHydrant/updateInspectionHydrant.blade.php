@extends('layouts.app')

@section('content')
<title>Inspection Hydrant | Safety Hygiene Environment System</title>

<section class="section">
    <div class="section-header">
        <h1>Form Inspection Report Hydrant</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home') }}">Dashboard</a></div>
            <div class="breadcrumb-item"><a href="{{ url('inspectionHydrant') }}">Inspection Hydrant</a></div>
            <div class="breadcrumb-item">Form Inspection Report Hydrant</div>
        </div>
    </div>

    <div class="section-body">
        <h2 class="section-title">{{ $dataLokasi->area}}</h2>
        <p class="section-lead">
          @if($dataLokasi->plant == "5001")
            {{ $dataLokasi->unit_kerja }} : Tuban
          @elseif($dataLokasi->plant == "5002")
            {{ $dataLokasi->unit_kerja }} : Gresik
          @elseif($dataLokasi->plant == "5003")
            {{ $dataLokasi->unit_kerja }} : Rembang
          @else
            {{ $dataLokasi->unit_kerja }} : Cigading
          @endif
        </p>
        <div id="result-form-konten"></div>
            <form onsubmit="return false;" id="form-konten" class='form-horizontal'>
		{{ csrf_field() }}
            <div class="row">
                <div class="col-12 col-md-4 col-lg-4">
                    <div class="card">
                        <div class="card-header">
                            <h4>Inspection Date</h4>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label>Inspection Date</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fas fa-calendar"></i>
                                        </div>
                                    </div>
                                    <input type="date" class="form-control daterange-cus" value="{{$data->inspection_date}}" name="inspection_date">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h4>Inspection Box</h4>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label>Casing</label>
                                <div class="row">
                                  <div class="col-sm-6">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="box_casing" <?php if($data->box_casing =='V') {echo "checked";}?> value="V">
                                      <label for="casing1">BAIK</label>
                                    </div>
                                  </div>
                                  <div class="col-sm-6">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="box_casing" <?php if($data->box_casing =='K') {echo "checked";}?> value="K">
                                      <label for="casing2">KOTOR</label>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-sm-6">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="box_casing" <?php if($data->box_casing =='-') {echo "checked";}?> value="-">
                                      <label for="casing3">TIDAK ADA</label>
                                    </div>
                                  </div>
                                  <div class="col-sm-6">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="box_casing" <?php if($data->box_casing =='X') {echo "checked";}?> value="X">
                                      <label for="casing4">RUSAK</label>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-sm-12">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="box_casing" <?php if($data->box_casing =='I') {echo "checked";}?> value="I">
                                      <label for="casing5">SERET / MACET / MEREMBES</label>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label>Hose</label>
                                <div class="row">
                                  <div class="col-sm-6">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="box_hose" <?php if($data->box_hose =='V') {echo "checked";}?> value="V">
                                      <label for="hose1">BAIK</label>
                                    </div>
                                  </div>
                                  <div class="col-sm-6">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="box_hose" <?php if($data->box_hose =='K') {echo "checked";}?> value="K">
                                      <label for="hose2">KOTOR</label>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-sm-6">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="box_hose" <?php if($data->box_hose =='-') {echo "checked";}?> value="-">
                                      <label for="hose3">TIDAK ADA</label>
                                    </div>
                                  </div>
                                  <div class="col-sm-6">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="box_hose" <?php if($data->box_hose =='X') {echo "checked";}?> value="X">
                                      <label for="hose4">RUSAK</label>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-sm-12">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="box_hose" <?php if($data->box_hose =='I') {echo "checked";}?> value="I">
                                      <label for="hose5">SERET / MACET / MEREMBES</label>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label>Nozle</label>
                                <div class="row">
                                  <div class="col-sm-6">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="box_nozle" <?php if($data->box_nozle =='V') {echo "checked";}?> value="V">
                                      <label for="nozle1">BAIK</label>
                                    </div>
                                  </div>
                                  <div class="col-sm-6">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="box_nozle" <?php if($data->box_nozle =='K') {echo "checked";}?> value="K">
                                      <label for="nozle2">KOTOR</label>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-sm-6">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="box_nozle" <?php if($data->box_nozle =='-') {echo "checked";}?> value="-">
                                      <label for="nozle3">TIDAK ADA</label>
                                    </div>
                                  </div>
                                  <div class="col-sm-6">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="box_nozle" <?php if($data->box_nozle =='X') {echo "checked";}?> value="X">
                                      <label for="nozle4">RUSAK</label>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-sm-12">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="box_nozle" <?php if($data->box_nozle =='I') {echo "checked";}?> value="I">
                                      <label for="nozle5">SERET / MACET / MEREMBES</label>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label>Kunci</label>
                                <div class="row">
                                  <div class="col-sm-6">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="box_kunci" <?php if($data->box_kunci =='V') {echo "checked";}?> value="V">
                                      <label for="kunci1">BAIK</label>
                                    </div>
                                  </div>
                                  <div class="col-sm-6">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="box_kunci" <?php if($data->box_kunci =='K') {echo "checked";}?> value="K">
                                      <label for="kunci2">KOTOR</label>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-sm-6">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="box_kunci" <?php if($data->box_kunci =='-') {echo "checked";}?> value="-">
                                      <label for="kunci3">TIDAK ADA</label>
                                    </div>
                                  </div>
                                  <div class="col-sm-6">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="box_kunci" <?php if($data->box_kunci =='X') {echo "checked";}?> value="X">
                                      <label for="kunci4">RUSAK</label>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-sm-12">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="box_kunci" <?php if($data->box_kunci =='I') {echo "checked";}?> value="I">
                                      <label for="kunci5">SERET / MACET / MEREMBES</label>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label>Note</label>
                                <textarea rows="2" maxlength="255" class="form-control input-sm" name="box_note" style="resize: vertical;">{{$data->box_note}}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h4>Temuan</h4>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label>Press</label>
                                <input type="number" name="press" value="{{$data->press}}" min="0" class="form-control input-sm touchspin">
                            </div>
                            <div class="form-group">
                                <label>Temuan</label>
                                <textarea rows="2" maxlength="255" class="form-control input-sm" name="temuan" style="resize: vertical;">{{$data->temuan}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4 col-lg-4">
                    <div class="card">
                        <div class="card-header">
                            <h4>Inspection Valve Copling</h4>
                        </div>
                        <div class="card-body">
                          <div class="form-group">
                              <label>Kiri</label>
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="radio radio-info" style="margin:6px 0">
                                    <input type="radio" name="valve_kiri" <?php if($data->valve_kiri =='V') {echo "checked";}?> value="V">
                                    <label for="hose1">BAIK</label>
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="radio radio-info" style="margin:6px 0">
                                    <input type="radio" name="valve_kiri" <?php if($data->valve_kiri =='K') {echo "checked";}?> value="K">
                                    <label for="hose2">KOTOR</label>
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="radio radio-info" style="margin:6px 0">
                                    <input type="radio" name="valve_kiri" <?php if($data->valve_kiri =='-') {echo "checked";}?> value="-">
                                    <label for="hose1">TIDAK ADA</label>
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="radio radio-info" style="margin:6px 0">
                                    <input type="radio" name="valve_kiri" <?php if($data->valve_kiri =='X') {echo "checked";}?> value="X">
                                    <label for="hose2">RUSAK</label>
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-sm-12">
                                  <div class="radio radio-info" style="margin:6px 0">
                                    <input type="radio" name="valve_kiri" <?php if($data->valve_kiri =='I') {echo "checked";}?> value="I">
                                    <label for="hose1">SERET / MACET / MEREMBES</label>
                                  </div>
                                </div>
                              </div>
                          </div>
                          <hr>
                          <div class="form-group">
                              <label>Atas</label>
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="radio radio-info" style="margin:6px 0">
                                    <input type="radio" name="valve_atas" <?php if($data->valve_atas =='V') {echo "checked";}?> value="V">
                                    <label for="valve_atas1">BAIK</label>
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="radio radio-info" style="margin:6px 0">
                                    <input type="radio" name="valve_atas" <?php if($data->valve_atas =='K') {echo "checked";}?> value="K">
                                    <label for="valve_atas2">KOTOR</label>
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="radio radio-info" style="margin:6px 0">
                                    <input type="radio" name="valve_atas" <?php if($data->valve_atas =='-') {echo "checked";}?> value="-">
                                    <label for="valve_atas3">TIDAK ADA</label>
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="radio radio-info" style="margin:6px 0">
                                    <input type="radio" name="valve_atas" <?php if($data->valve_atas =='X') {echo "checked";}?> value="X">
                                    <label for="valve_atas4">RUSAK</label>
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-sm-12">
                                  <div class="radio radio-info" style="margin:6px 0">
                                    <input type="radio" name="valve_atas" <?php if($data->valve_atas =='I') {echo "checked";}?> value="I">
                                    <label for="valve_atas5">SERET / MACET / MEREMBES</label>
                                  </div>
                                </div>
                              </div>
                          </div>
                          <hr>
                          <div class="form-group">
                              <label>Kanan</label>
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="radio radio-info" style="margin:6px 0">
                                    <input type="radio" name="valve_kanan" <?php if($data->valve_kanan =='V') {echo "checked";}?> value="V">
                                    <label for="valve_kanan1">BAIK</label>
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="radio radio-info" style="margin:6px 0">
                                    <input type="radio" name="valve_kanan" <?php if($data->valve_kanan =='K') {echo "checked";}?> value="K">
                                    <label for="valve_kanan2">KOTOR</label>
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-sm-6">
                                  <div class="radio radio-info" style="margin:6px 0">
                                    <input type="radio" name="valve_kanan" <?php if($data->valve_kanan =='-') {echo "checked";}?> value="-">
                                    <label for="valve_kanan3">TIDAK ADA</label>
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <div class="radio radio-info" style="margin:6px 0">
                                    <input type="radio" name="valve_kanan" <?php if($data->valve_kanan =='X') {echo "checked";}?> value="X">
                                    <label for="valve_kanan4">RUSAK</label>
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-sm-12">
                                  <div class="radio radio-info" style="margin:6px 0">
                                    <input type="radio" name="valve_kanan" <?php if($data->valve_kanan =='I') {echo "checked";}?> value="I">
                                    <label for="valve_kanan5">SERET / MACET / MEREMBES</label>
                                  </div>
                                </div>
                              </div>
                          </div>
                          <hr>
                          <div class="form-group">
                              <label>Note</label>
                              <textarea rows="2" maxlength="255" class="form-control input-sm" name="valve_note" style="resize: vertical;">{{$data->valve_note}}</textarea>
                          </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h4>Inspection Tutup Copling</h4>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label>Kiri</label>
                                <div class="row">
                                  <div class="col-sm-6">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="t_copling_kiri" <?php if($data->t_copling_kiri =='V') {echo "checked";}?> value="V">
                                      <label for="tkoplingkiri1">BAIK</label>
                                    </div>
                                  </div>
                                  <div class="col-sm-6">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="t_copling_kiri" <?php if($data->t_copling_kiri =='K') {echo "checked";}?> value="K">
                                      <label for="tkoplingkiri2">KOTOR</label>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-sm-6">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="t_copling_kiri" <?php if($data->t_copling_kiri =='-') {echo "checked";}?> value="-">
                                      <label for="tkoplingkiri3">TIDAK ADA</label>
                                    </div>
                                  </div>
                                  <div class="col-sm-6">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="t_copling_kiri" <?php if($data->t_copling_kiri =='X') {echo "checked";}?> value="X">
                                      <label for="tkoplingkiri4">RUSAK</label>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-sm-12">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="t_copling_kiri" <?php if($data->t_copling_kiri =='I') {echo "checked";}?> value="I">
                                      <label for="tkoplingkiri5">SERET / MACET / MEREMBES</label>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Kanan</label>
                                <div class="row">
                                  <div class="col-sm-6">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="t_copling_kanan" <?php if($data->t_copling_kanan =='V') {echo "checked";}?> value="V">
                                      <label for="tkoplingkanan1">BAIK</label>
                                    </div>
                                  </div>
                                  <div class="col-sm-6">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="t_copling_kanan" <?php if($data->t_copling_kanan =='K') {echo "checked";}?> value="K">
                                      <label for="tkoplingkanan2">KOTOR</label>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-sm-6">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="t_copling_kanan" <?php if($data->t_copling_kanan =='-') {echo "checked";}?> value="-">
                                      <label for="tkoplingkanan3">TIDAK ADA</label>
                                    </div>
                                  </div>
                                  <div class="col-sm-6">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="t_copling_kanan" <?php if($data->t_copling_kanan =='X') {echo "checked";}?> value="X">
                                      <label for="tkoplingkanan4">RUSAK</label>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-sm-12">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="t_copling_kanan" <?php if($data->t_copling_kanan =='I') {echo "checked";}?> value="I">
                                      <label for="tkoplingkanan5">SERET / MACET / MEREMBES</label>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Note</label>
                                <textarea rows="2" maxlength="255" class="form-control input-sm" name="t_copling_note" style="resize: vertical;">{{$data->t_copling_note}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4 col-lg-4">
                    <div class="card">
                        <div class="card-header mt-0">
                            <h4>Inspection Pilar</h4>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label>Kanan</label>
                                <div class="row">
                                  <div class="col-sm-6">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="pilar_body" <?php if($data->pilar_body =='V') {echo "checked";}?> value="V">
                                      <label for="pilarbody1">BAIK</label>
                                    </div>
                                  </div>
                                  <div class="col-sm-6">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="pilar_body" <?php if($data->pilar_body =='K') {echo "checked";}?> value="K">
                                      <label for="pilarbody2">KOTOR</label>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-sm-6">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="pilar_body" <?php if($data->pilar_body =='-') {echo "checked";}?> value="-">
                                      <label for="pilarbody3">TIDAK ADA</label>
                                    </div>
                                  </div>
                                  <div class="col-sm-6">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="pilar_body" <?php if($data->pilar_body =='X') {echo "checked";}?> value="X">
                                      <label for="pilarbody4">RUSAK</label>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-sm-12">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="pilar_body" <?php if($data->pilar_body =='I') {echo "checked";}?> value="I">
                                      <label for="pilarbody5">SERET / MACET / MEREMBES</label>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Note</label>
                                <textarea rows="2" maxlength="255" class="form-control input-sm" name="pilar_note" style="resize: vertical;">{{$data->pilar_note}}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h4>Inspection Copling</h4>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label>Kiri</label>
                                <div class="row">
                                  <div class="col-sm-6">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="copling_kiri" <?php if($data->copling_kiri =='V') {echo "checked";}?> value="V">
                                      <label for="coplingkiri1">BAIK</label>
                                    </div>
                                  </div>
                                  <div class="col-sm-6">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="copling_kiri" <?php if($data->copling_kiri =='K') {echo "checked";}?> value="K">
                                      <label for="coplingkiri2">KOTOR</label>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-sm-6">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="copling_kiri" <?php if($data->copling_kiri =='-') {echo "checked";}?> value="-">
                                      <label for="coplingkiri3">TIDAK ADA</label>
                                    </div>
                                  </div>
                                  <div class="col-sm-6">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="copling_kiri" <?php if($data->copling_kiri =='X') {echo "checked";}?> value="X">
                                      <label for="coplingkiri4">RUSAK</label>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-sm-12">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="copling_kiri" <?php if($data->copling_kiri =='I') {echo "checked";}?> value="I">
                                      <label for="coplingkiri5">SERET / MACET / MEREMBES</label>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Kanan</label>
                                <div class="row">
                                  <div class="col-sm-6">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="copling_kanan" <?php if($data->copling_kanan =='V') {echo "checked";}?> value="V">
                                      <label for="coplingkanan1">BAIK</label>
                                    </div>
                                  </div>
                                  <div class="col-sm-6">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="copling_kanan" <?php if($data->copling_kanan =='K') {echo "checked";}?> value="K">
                                      <label for="coplingkanan2">KOTOR</label>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-sm-6">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="copling_kanan" <?php if($data->copling_kanan =='-') {echo "checked";}?> value="-">
                                      <label for="coplingkanan3">TIDAK ADA</label>
                                    </div>
                                  </div>
                                  <div class="col-sm-6">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="copling_kanan" <?php if($data->copling_kanan =='X') {echo "checked";}?> value="X">
                                      <label for="coplingkanan4">RUSAK</label>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-sm-12">
                                    <div class="radio radio-info" style="margin:6px 0">
                                      <input type="radio" name="copling_kanan" <?php if($data->copling_kanan =='I') {echo "checked";}?> value="I">
                                      <label for="coplingkanan5">SERET / MACET / MEREMBES</label>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Note</label>
                                <textarea rows="2" maxlength="255" class="form-control input-sm" name="copling_note" style="resize: vertical;">{{$data->copling_note}}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h4>Foto Temuan</h4>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col s6">
                                        @if($data->foto_temuan=='')
                                          <img src="{{asset('public/images')}}/{{'preview-icon.png'}}" id="showTemuan" style="max-width:270px;max-height:270px;float:left;" />
                                        @else
                                          <img src="{{asset('public/uploads/InspectionHydrant')}}/{{$data->foto_temuan}}" id="showTemuan" style="max-width:270px;max-height:270px;float:left;" />
                                        @endif
                                    </div>
                                </div>
                                <div class="row" style="margin-top:10px">
                                    <div class="input-field col s6">
                                      <input type="file" id="inputTemuan" name="foto_temuan" class="validate"/ >
                                      <!-- <input type="file" id="inputTemuan" name="climbing_tool_picture" class="validate"/ > -->
                                    </div>
                                </div>
                            </div>
                            <div class="form-group text-right">
                              <a href="{{url('inspectionHydrant')}}" class="btn btn-style btn-xs" style="background-color:#dddddd; color:black">
                                <i class="fa fa-times"></i> Close
                              </a>
                              <button type="submit" class="btn btn-info" data-loading-text="<i class='fa fa-spinner fa-spin'></i>
                                &nbsp;<font class='lowercase'></font>"> <i class="fa fa-check"></i> Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type='hidden' name='id' value='{{ $data->id }}'>
            <input type='hidden' name='id_hydrant' value='{{ $data->id_hydrant }}'>
            <input type='hidden' name='pic_badge' value='{{ $data->pic_badge }}'>
            <input type='hidden' name='pic_name' value='{{ $data->pic_name }}'>
            <input type='hidden' name='_token' value='{{ csrf_token() }}'>
        </form>
        <script>
            $(document).ready(function () {
                $('#form-konten').submit(function () {
                    var data = getFormData('form-konten');
                    ajaxTransfer('/inspectionHydrant/saveUpdateInspectionHydrant', data, '#result-form-konten');
                })
            })
        </script>
        <script type="text/javascript">
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#showTemuan').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#inputTemuan").change(function () {
                readURL(this);
            });
        </script>
    </div>
</section>
@endsection
