<div id="result-form-konten"></div>
    <form onsubmit="return false;" id="form-konten" class='form-horizontal'>
        <table width="100%">
            <tbody>
                <tr>
                  <td class="text-center">
                    <img src="public/assets/img/logosemen.png" class="img-responsive" alt="logo" width="80">
                  </td>
                </tr>
                <tr>
                  <td class="text-center">
                    <h6 class="modal-title" id="title">Create Parameter Alarm</h6>
                    <h4 class="modal-title" id="title"></h4><small class="font-bold">PT. Semen Indonesia</small>
                  </td>
                </tr>
            </tbody>
        </table>
        <div class="modal-body">
            <hr>
            <div class="row go_form">
                <div class="col-md-12">
                    <label>Parameter</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fas fa-calendar"></i>
                            </div>
                        </div>
                        <input type="text" class="form-control daterange-cus" name="parameter" value="{{$data->parameter}}">
                    </div>
                </div>
            </div>
            <hr>
            <div class="row go_form">
                <div class="col-md-12">
                  <label>Type Parameter</label>
                  <select name="type" class="form-control">
                      @foreach($type as $item)
                      <option value="{{ $item }}" @if(!is_null($data)) @if($item == $data->type) selected="selected" @endif @endif>{{ $item}}</option>
                      @endforeach
                  </select>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            <button type="submit" class="btn btn-info" data-loading-text="<i class='fa fa-spinner fa-spin'></i>
              &nbsp;<font class='lowercase'></font>"> <i class="fa fa-check"></i> Save</button>
        </div>

    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/inspectionAlarm/saveParameter', data, '#result-form-konten');
        })
    })
</script>
