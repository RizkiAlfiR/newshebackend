@extends('layouts.app')

@section('content')
<title>Inspection Apar | Safety Hygiene Environment System</title>
<section class="section">
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                  <div class="col-12" style="padding-top:20px">
                    <div>
                      <div>
                          <div class="tab-content" id="myTabContent">
                              <div class="tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
                                <div class="">
                                    <h5>List Inspection Alarm</h5>
                                    <div class="">
                                        <div class="table-responsive">
                                            <table width="60%">
                                            <tbody>
                                                <tr>
                                                    <td width="20%"><strong> Area </strong></td>
                                                    <td width="2%"> :</td>
                                                    <td><span id="view_area"></span>{{$lokasiAlarm->area}}</td>
                                                </tr>
                                                <tr>
                                                    <td width="20%"><strong> Unit Kerja </strong></td>
                                                    <td width="2%"> :</td>
                                                    <td><span id="view_unit_kerja"></span> {{$lokasiAlarm->unit_kerja}}</td>
                                                </tr>
                                                <tr>
                                                    <td width="20%"><strong> Plant </strong></td>
                                                    <td width="2%"> :</td>
                                                    <td><span id="view_lokasi"></span>
                                                      @if($lokasiAlarm->plant == "5001")
                                                        Tuban
                                                      @elseif($lokasiAlarm->plant == "5002")
                                                        Gresik
                                                      @elseif($lokasiAlarm->plant == "5003")
                                                        Rembang
                                                      @else
                                                        Cigading
                                                      @endif
                                                    </td>
                                                </tr>
                                            </tbody>
                                            </table>
                                          </div>
                                          <div class="table-responsive">
                                            <table class="table table-striped" id="table-inspection" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center align-middle">No</th>
                                                        <th class="align-middle">Inspector</th>
                                                        <th class="text-center">Inspection Date</th>
                                                        <th class="text-center">Type Periode</th>
                                                        <th class="text-center">Image</th>
                                                        <th class="text-center align-middle">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($inspectionAlarm as $i => $data)
                                                    <tr>
                                                        <td class="text-center align-middle">{{ $i+1 }}</td>
                                                        <td class="text-center align-middle">{{ $data->pic_name }}</td>
                                                        <td class="text-center align-middle">{{ $data->inspection_date }}</td>
                                                        <td class="text-center align-middle">{{ $data->type_periode }}</td>
                                                        <td class="text-center align-middle">

                                                          {{ $data->detail }}
                                                        </td>
                                                        <td class="text-center align-middle">
                                                          @if(!is_null($data->image))
                                                            <a href="{{asset('public/uploads/InspectionAlarm')}}/{{$data->image}}" target="_blank">
                                                                <img src="{{asset('public/uploads/InspectionAlarm')}}/{{$data->image}}" class="img-responsive" alt="logo" height="80">
                                                            </a>
                                                          @else
                                                            <img src="{{asset('images')}}/{{"preview-icon.png"}}" class="img-responsive" alt="logo" width="80">
                                                          @endif
                                                        </td>
                                                        <td class="text-center align-middle">
                                                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                                <!-- <button type="button" class="btn btn-icon icon-left btn-info"
                                                                onclick="loadModal(this)" title="" target="/inspectionApar/updateInspectionApar" data="id={{$data->id}}">
                                                                    <i class="fas fa-edit"></i>
                                                                </button> -->
                                                                <button type="button" class="btn btn-icon icon-left btn-danger"
                                                                onclick="deleteInspection({{$data->id}})">
                                                                    <i class="fas fa-trash"></i>
                                                                </button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                              </div>
                          </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@section('scripts')
    <script>
        function deleteInspection(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Confirmation", "Do you want to remove this data?", function () {
               ajaxTransfer("/inspectionAlarm/deleteHistoryInspectionAlarm", data, "#modal-output");
            })
        }

        var table = $('#table-inspection');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });
    </script>
@endsection

@endsection
