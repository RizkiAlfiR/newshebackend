<div id="result-form-konten"></div>
    <form onsubmit="return false;" id="form-konten" class='form-horizontal'>
        <table width="100%">
            <tbody>
                <tr>
                  <td class="text-center">
                    <img src="public/assets/img/logosemen.png" class="img-responsive" alt="logo" width="80" style="padding-top:20px">
                  </td>
                </tr>
                <tr>
                  <td class="text-center">
                    <h6 class="modal-title" id="title">Form Master Alarm</h6>
                    <h4 class="modal-title" id="title"></h4><small class="font-bold">PT. Semen Indonesia</small>
                  </td>
                </tr>
            </tbody>
        </table>
        <div class="modal-body">
            <hr>
            <div class="row go_form">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Unit Kerja</label>
                        <select name="unit_kerja" class="form-control">
                            @foreach($unitKerja as $item)
                            <option value="{{ $item->muk_nama }}" @if(!is_null($data)) @if($item->muk_nama == $data->unit_kerja) selected="selected" @endif @endif>{{ $item->muk_nama}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Area</label>
                        <select name="area" class="form-control">
                            @foreach($inspectionArea as $item)
                            <option value="{{ $item->area_name }}" @if(!is_null($data)) @if($item->area_name == $data->area) selected="selected" @endif @endif>{{ $item->area_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row go_form">
              <div class="col-md-6">
                  <div class="form-group">
                      <label>Plant</label>
                      <select name="plant" class="form-control">
                          @foreach($plant as $item)
                          <option value="{{ $item->plant }}" @if(!is_null($data)) @if($item->plant == $data->plant) selected="selected" @endif @endif>{{ $item->plant_text}}</option>
                          @endforeach
                      </select>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                      <label>Company</label>
                      <select name="company" class="form-control">
                          <option value="5000">PT Semen Indonesia</option>
                      </select>
                  </div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            <button type="submit" class="btn btn-info" data-loading-text="<i class='fa fa-spinner fa-spin'></i>
              &nbsp;<font class='lowercase'></font>"> <i class="fa fa-check"></i> Save</button>
        </div>

    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/inspectionAlarm/saveMaster', data, '#result-form-konten');
        })
    })
</script>
