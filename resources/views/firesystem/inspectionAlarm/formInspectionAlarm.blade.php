<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-konten" class='form-horizontal'>
    <div class="card">
        <div class="card-body">
            <div class="form-group">
              <label>Lokasi</label>
              <select name="location_name" class="form-control">
                  @foreach($lokasiAlarm as $item)
                  <option value="{{ $item->area }}" @if(!is_null($data)) @if($item->area == $data->location_name) selected="selected" @endif @endif>{{ $item->area}}</option>
                  @endforeach
              </select>
            </div>
            <div class="form-group">
              <label>Type Parameter</label>
              <select name="type" class="form-control">
                  @foreach($type as $item)
                  <option value="{{ $item }}" @if(!is_null($data)) @if($item == $data->type) selected="selected" @endif @endif>{{ $item}}</option>
                  @endforeach
              </select>
            </div>
            <div class="form-group">
              <div class="input-group">
                  <div class="input-group-prepend">
                      <div class="input-group-text">
                          <i class="fas fa-calendar"></i>
                      </div>
                  </div>
                  <input type="text" class="form-control daterange-cus" name="location_name" value="{{$data->location_name}}">
              </div>
            </div>

            <div class="form-group">
                <!-- <a type="submit" class="btn btn-icon icon-left btn-success"><i class="fas fa-check"></i> Simpan</a> -->
                <input type="submit" value="Simpan Data">
            </div>
        </div>
    </div>

    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/inspectionAlarm/saveParameter', data, '#result-form-konten');
        })
    })
</script>
