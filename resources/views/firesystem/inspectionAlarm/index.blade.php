@extends('layouts.app')

@section('content')
<title>Inspection Alarm | Safety Hygiene Environment System</title>
<section class="section">
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                  <div class="col-12" style="padding-top:20px">
                    <div class="nav-wrapper">
                        <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0 active" id="tabs-icons-text-1-tab" data-toggle="tab" href="#tabs-icons-text-1" role="tab" aria-controls="tabs-icons-text-1" aria-selected="true"><i class="ni ni-cloud-upload-96 mr-2"></i>Master Alarm</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-2-tab" data-toggle="tab" href="#tabs-icons-text-2" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>Parameter Pengecekan</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-3-tab" data-toggle="tab" href="#tabs-icons-text-3" role="tab" aria-controls="tabs-icons-text-3" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>Inspection Report Alarm</a>
                            </li>
                        </ul>
                    </div>
                    <div>
                      <div>
                          <div class="tab-content" id="myTabContent">
                              <div class="tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
                                <div class="">
                                    <h5>List Master Alarm</h5>
                                    <p class="">
                                        <a onclick="loadModal(this)" title="" target="/inspectionAlarm/addMaster" class="btn btn-icon icon-left btn-primary" title="Create New" style="color:white">
                                          <i class="far fa-edit"></i>Create New</a>
                                    </p>
                                    <div class="">
                                        <div class="table-responsive">
                                            <table class="table table-striped" id="table-master" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">No</th>
                                                        <th>Company</th>
                                                        <th>Plant</th>
                                                        <th>Unit Kerja</th>
                                                        <th>Area</th>
                                                        <th>QR-Code</th>
                                                        <th class="text-center">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($lokasiAlarm as $i => $data)
                                                    <tr>
                                                        <td class="align-middle text-center">{{ $i+1 }}</td>
                                                        <td class="align-middle">
                                                          @if($data->company == "5000")
                                                            PT Semen Indonesia
                                                          @else
                                                            {{ $data->company }}
                                                          @endif
                                                        </td>
                                                        <td class="align-middle">
                                                          @if($data->plant == "5001")
                                                            Tuban
                                                          @elseif($data->plant == "5002")
                                                            Gresik
                                                          @elseif($data->plant == "5003")
                                                            Rembang
                                                          @else
                                                            Cigading
                                                          @endif
                                                        </td>
                                                        <td class="align-middle">{{ $data->unit_kerja }}</td>
                                                        <td class="align-middle">{{ $data->area }}</td>
                                                        <td class="align-middle">
                                                            {!! QrCode::size(100)->generate('ALARM~'.$data->id); !!}
                                                        </td>
                                                        <td class="align-middle text-center">
                                                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                                <button type="button" class="btn btn-icon icon-left btn-info"
                                                                onclick="loadModal(this)" title="" target="/inspectionAlarm/addMaster" data="id={{$data->id}}">
                                                                    <i class="fas fa-edit"></i>
                                                                </button>
                                                                <button type="button" class="btn btn-icon icon-left btn-danger"
                                                                onclick="deleteMaster({{$data->id}})">
                                                                    <i class="fas fa-trash"></i>
                                                                </button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                              </div>
                              <div class="tab-pane fade" id="tabs-icons-text-2" role="tabpanel" aria-labelledby="tabs-icons-text-2-tab">
                                <div class="">
                                    <h5>List Master Parameter Pengecekan</h5>
                                    <p class="">
                                        <a onclick="loadModal(this)" title="" target="/inspectionAlarm/addParameter" class="btn btn-icon icon-left btn-primary" style="color:white">
                                          <i class="far fa-edit"></i> Create New</a>
                                    </p>
                                    <div class="">
                                        <div class="table-responsive">
                                            <table class="table table-striped" id="table-parameter" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">No</th>
                                                        <th>Parameter</th>
                                                        <th>Type</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($parameterPengecekanAlarm as $i => $data)
                                                    <tr>
                                                        <td class="align-middle text-center">{{ $i+1 }}</td>
                                                        <td class="align-middle">{{ $data->parameter }}</td>
                                                        <td class="align-middle">{{ $data->type }}</td>
                                                        <td class="align-middle">
                                                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                              <button type="button" class="btn btn-icon icon-left btn-info"
                                                              onclick="loadModal(this)" title="" target="/inspectionAlarm/addParameter" data="id={{$data->id}}">
                                                                  <i class="fas fa-edit"></i>
                                                              </button>
                                                              <button type="button" class="btn btn-icon icon-left btn-danger"
                                                              onclick="deleteParameter({{$data->id}})">
                                                                  <i class="fas fa-trash"></i>
                                                              </button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                              </div>
                              <div class="tab-pane fade" id="tabs-icons-text-3" role="tabpanel" aria-labelledby="tabs-icons-text-3-tab">
                                <div class="">
                                    <h5>Inspection Report Apar</h5>
                                    <div class="">
                                        <div class="table-responsive">
                                            <table class="table table-striped" id="table-inspection" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">No</th>
                                                        <th>Unit</th>
                                                        <th>Area</th>
                                                        <th>Plant</th>
                                                        <th>Status</th>
                                                        <th>Last Inspection</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                  @foreach($lokasiAlarm as $i => $data)
                                                  <tr>
                                                      <td class="text-center">{{ $i+1 }}</td>
                                                      <td class="align-middle">{{ $data->unit_kerja }}</td>
                                                      <td class="align-middle">{{ $data->area }}</td>
                                                      <td class="align-middle">
                                                        @if($data->plant == "5001")
                                                          Tuban
                                                        @elseif($data->plant == "5002")
                                                          Gresik
                                                        @elseif($data->plant == "5003")
                                                          Rembang
                                                        @else
                                                          Cigading
                                                        @endif
                                                      </td>
                                                      <td class="align-middle">
                                                        <?php
                                                          $exp = $data->exp_inspection;
                                                          $now = date('Y-m-d');
                                                          if($now < $exp){
                                                            ?>
                                                            <button type="button" class="btn btn-icon icon-left btn-success">
                                                                Checked
                                                            </button>
                                                            <?php
                                                          } else {
                                                            ?>
                                                            <button type="button" class="btn btn-icon icon-left btn-danger">
                                                                Not Checked
                                                            </button>
                                                            <?php

                                                          }
                                                        ?>
                                                      </td>
                                                      <td class="align-middle">{{ $data->last_inspection}}</td>
                                                      <td class="align-middle">
                                                        <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                            <!-- <button type="button" class="btn btn-icon icon-left btn-info"
                                                            onclick="loadModal(this)" target="/inspectionApar/addInspectionApar" data="id={{$data->id}}">
                                                                <i class="fas fa-edit"></i>
                                                            </button> -->
                                                            <a href="{{url('inspectionAlarm/historyInspectionAlarm?id=')}}{{$data->id}}" class="btn btn-warning btn-xs">
                                                              <i class="fa fa-history"></i>
                                                            </a>
                                                        </div>
                                                      </td>
                                                  </tr>
                                                  @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                              </div>
                          </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@section('scripts')
    <script>
        function deleteMaster(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Confirmation", "Do you want to remove this data ?", function () {
               ajaxTransfer("/inspectionAlarm/deleteMaster", data, "#modal-output");
            })
        }

        function deleteParameter(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Confirmation", "Do you want to remove this data ?", function () {
               ajaxTransfer("/inspectionAlarm/deleteParameter", data, "#modal-output");
            })
        }

        function deleteInspectionAlarm(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("Confirmation", "Do you want to remove this data ?", function () {
               ajaxTransfer("/inspectionAlarm/deleteInspectionAlarm", data, "#modal-output");
            })
        }

        var table = $('#table-master');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });
        var table = $('#table-parameter');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });
        var table = $('#table-inspection');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });
    </script>
@endsection

@endsection
