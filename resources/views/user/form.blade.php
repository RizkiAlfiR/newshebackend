<link rel="stylesheet" href="{{asset('assets/modules/bootstrap-toggle/css/bootstrap4-toggle.min.css')}}">
<script src="{{asset('assets/')}}/modules/bootstrap-toggle/js/bootstrap4-toggle.min.js"></script>
<div id="result-form-konten"></div>
    <form onsubmit="return false;" id="form-konten" class='form-horizontal'>
        <table width="100%">
            <tbody>
                <tr>
                  <td class="text-center">
                    <img src="public/assets/img/logosemen.png" class="img-responsive" alt="logo" width="80" style="padding-top:20px">
                  </td>
                </tr>
                <tr>
                  <td class="text-center">
                    <h6 class="modal-title" id="title">Form Users</h6>
                    <h4 class="modal-title" id="title"></h4><small class="font-bold">PT. Semen Indonesia</small>
                  </td>
                </tr>
            </tbody>
        </table>
        <div class="modal-body">
            <hr>
            <!-- <div class="row go_form">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Plant</label>
                        <input type="checkbox" checked data-toggle="toggle" data-on="Hello<br>World" data-off="Goodbye<br>World" name="saya">
                    </div>
                </div>
            </div> -->

            <!-- <div class="row">
                                                   <div class="col-lg-12">
                                                       <div class="form-group {{$errors->has('employee') ? 'has-error' : ''}}">
                                                           <label class="form-control-label" for="employee">Employee</label>
                                                           <input type="hidden" name="employee_hid" id="employee_hid">
                                                           <select id="employee" name="employee" class="form-control"></select>
                                                           @if($errors->has('employee'))
                                                               <span class="text-danger">{{ $errors->first('employee') }}</span>
                                                           @endif
                                                       </div>
                                                   </div>
            </div> -->

            <div class="row go_form">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Employee</label>
                        <select class="name form-control" name="name" id="name">
                          @foreach($employee as $item)
                          <option value="{{ $item->id }}" @if(!is_null($data)) @if($item->mk_nama == $data->name) selected="selected" @endif @endif>{{ $item->mk_nopeg}} - {{ $item->mk_nama}}</option>
                          @endforeach
                        </select>

                        <!-- <select name="name" class="form-control">
                            @foreach($employee as $item)
                            <option value="{{ $item->id }}" @if(!is_null($data)) @if($item->mk_nama == $data->name) selected="selected" @endif @endif>{{ $item->mk_nopeg}} - {{ $item->mk_nama}}</option>
                            @endforeach
                        </select> -->
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Role</label>
                        <select name="role_id" class="form-control">
                            <option <?php if($data->role_id =='1') {echo "selected";}?> value="1">Administrator</option>
                            <option <?php if($data->role_id =='2') {echo "selected";}?> value="2">Manager K3</option>
                            <option <?php if($data->role_id =='3') {echo "selected";}?> value="3">Admin K3</option>
                            <option <?php if($data->role_id =='4') {echo "selected";}?> value="4">User</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row go_form">
              <div class="col-md-6">
                  <div class="form-group">
                      <label>Plant</label>
                      <select name="plant" class="form-control">
                          <option <?php if($data->plant =='5001') {echo "selected";}?> value="5001">Tuban</option>
                          <option <?php if($data->plant =='5002') {echo "selected";}?> value="5002">Gresik</option>
                          <option <?php if($data->plant =='5003') {echo "selected";}?> value="5003">Rembang</option>
                          <option <?php if($data->plant =='5004') {echo "selected";}?> value="5004">Cigading</option>
                      </select>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                      <label>Company</label>
                      <select name="company" class="form-control">
                          <option value="PT Semen Indonesia">PT Semen Indonesia</option>
                      </select>
                  </div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            <button type="submit" class="btn btn-info" data-loading-text="<i class='fa fa-spinner fa-spin'></i>
              &nbsp;<font class='lowercase'></font>"> <i class="fa fa-check"></i> Save</button>
        </div>
    <input type="hidden" id="post_to_fb" name="post_to_fb" value="">
    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/users/save', data, '#result-form-konten');
        })
    })
</script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

@section('js')
    <script type="application/javascript">
        $(document).ready(function () {
            $('#employee').select2({
                dropdownParent: $('#result-form-konten'),
                ajax:{
                    type:"GET",
                    placeholder:"Choose your category in here",
                    url:"{{url('/employee/loadEmployee')}}",
                    dataType:"json",
                    data :function(params){
                        return{
                            key:params.term
                        };
                    },
                    processResults:function(data){
                        return{
                            results:data
                        };
                    },
                    cache:true
                },
            }).on('select2:select',function (evt) {
                var data = $('#employee option:selected').val();
                $('#employee_hid').val(data);
            });
        })
    </script>
@endsection


<script type="text/javascript">
  $('.cari').select2({

  dropdownParent: $('#result-form-konten'),

    placeholder: 'Search Plant...',
    minimumInputLength:3,
    ajax: {
      type:"GET",
      url:"{{url('/plant/loadPlant')}}",
      delay: 50,
      processResults: function (data) {
        return {
          results:  $.map(data, function (item) {
            return {
              text: item.plant_text,
              id: item.id
            }
          })
        };
      },
      cache: true
    }
  });

  $('.name').select2({
    placeholder: 'Search Employee...',
    minimumInputLength:2,
    ajax: {
      type:"GET",
      url:"{{url('/employee/loadEmployee')}}",
      delay: 50,
      processResults: function (data) {
        return {
          results:  $.map(data, function (item) {
            return {
              text: item.mk_nama,
              id: item.id
            }
          })
        };
      },
      cache: true
    }
  });

</script>
