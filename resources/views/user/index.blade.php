@extends('layouts.app')

@section('content')
<title>Users | Safety Hygiene Environment System</title>
<section class="section">
    <div class="section-body">
      <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
          <div class="card card-statistic-1">
            <div class="card-icon bg-warning">
              <i class="fas fa-user"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Administrator</h4>
              </div>
              <div class="card-body">
                {{ $jumlahAdministrator }}
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
          <div class="card card-statistic-1">
            <div class="card-icon bg-danger">
              <i class="fas fa-user"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Total Manager K3</h4>
              </div>
              <div class="card-body">
                {{ $jumlahManager }}
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
          <div class="card card-statistic-1">
            <div class="card-icon bg-success">
              <i class="fas fa-user"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Total Admin K3</h4>
              </div>
              <div class="card-body">
                {{ $jumlahAdminK3 }}
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
          <div class="card card-statistic-1">
            <div class="card-icon bg-info">
              <i class="fas fa-user"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4>Total User</h4>
              </div>
              <div class="card-body">
                  {{ $jumlahUser }}
              </div>
            </div>
          </div>
        </div>
      </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h5>List All Users</h5>
                    </div>
                    <p class="">
                        <a onclick="loadModal(this)" title="" target="/users/add" class="btn btn-icon icon-left btn-primary" style="color:white">
                          <i class="far fa-edit"></i> Create New</a>
                    </p>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped" style="width:100%"  id="table-users">
                                <thead>
                                    <tr>
                                        <th class="text-center">No</th>
                                        <th>Nama User</th>
                                        <th>Position</th>
                                        <th>Unit Kerja</th>
                                        <th>Email</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($users as $i => $data)
                                    <tr>
                                        <td class="align-middle">
                                            {{ $i+1 }}
                                        </td>
                                        <td class="align-middle">{{ $data->name }}</td>
                                        <td class="align-middle">{{ $data->pos_text }}</td>
                                        <td class="align-middle">
                                            {{ $data->unit_kerja }}
                                        </td>
                                        <td class="align-middle">
                                            {{ $data->email }}
                                        </td>
                                        <td class="align-middle">
                                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                <button type="button" class="btn btn-icon icon-left btn-info"
                                                    onclick="loadModal(this)" title="" target="/users/add" data="id={{$data->id}}">
                                                    <i class="fas fa-edit"></i>
                                                </button>
                                                <button type="button" class="btn btn-icon icon-left btn-danger"
                                                    onclick="deleteUsers({{$data->id}})">
                                                    <i class="fas fa-trash"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @section('scripts')
        <script>
            function deleteUsers(id) {
                var data = new FormData();
                data.append('id', id);

                modalConfirm("Confirmation", "Do you want to remove this data ?", function () {
                   ajaxTransfer("/users/delete", data, "#modal-output");
                })
            }

            var table = $('#table-users');
            table.dataTable({
                pageLength: 10,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                columnDefs: [
                    {"targets": 0, "orderable": false},
                    // {"targets": 1, "visible": false, "searchable": false},
                ],
                order: [[1, "asc"]],
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv', title: 'Tipe Fasilitas'},
                    {extend: 'excel', title: 'Tipe Fasilitas'},
                    {extend: 'pdf', title: 'Tipe Fasilitas'},
                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]
            });
        </script>
@endsection

@endsection
