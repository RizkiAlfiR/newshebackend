@extends('layouts.app')

@section('content')
<title>Users &mdash; Safety Hygiene Environment System</title>
<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div> -->
<section class="section">
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>List All Users</h4>
                    </div>
                    <p class="section-lead">
                        <a href="{{ url('accidentreportcreate') }}" class="btn btn-icon icon-left btn-primary"><i
                                class="far fa-edit"></i> Create New</a>
                    </p>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped" id="table-1">
                                <thead>
                                    <tr>
                                        <th class="text-center">
                                            #
                                        </th>
                                        <th>Nama User</th>
                                        <th>Unit Kerja</th>
                                        <th>Cost Center</th>
                                        <th>Position</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($users as $i => $p)
                                    <tr>
                                        <td>
                                            {{ $i+1 }}
                                        </td>
                                        <td>{{ $p->name }}</td>
                                        <td class="align-middle">
                                            <!-- <div class="progress" data-height="4" data-toggle="tooltip" title="100%">
                                                <div class="progress-bar bg-success" data-width="100%"></div>
                                            </div> -->
                                            {{ $p->uk_kode }} - {{ $p->unit_kerja }}
                                        </td>
                                        <td>
                                            <!-- <img alt="image" src="assets/img/avatar/avatar-5.png" class="rounded-circle"
                                                width="35" data-toggle="tooltip" title="Wildan Ahdian"> -->
                                            {{ $p->cost_center }} - {{ $p->cc_text }}
                                        </td>
                                        <td>{{ $p->pos_text }}</td>
                                        <td>
                                            <!-- <a href="#" class="btn btn-secondary">Detail</a> -->
                                            <!-- <a href="#" class="btn btn-icon icon-left btn-info"><i
                                                    class="fas fa-info-circle"></i> Detail</a> -->
                                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                <button type="button" class="btn btn-icon icon-left btn-success">
                                                    <i class="fas fa-edit"></i>
                                                </button>
                                                <button type="button" class="btn btn-icon icon-left btn-info">
                                                    <i class="fas fa-eye"></i>
                                                </button>
                                                <button type="button" class="btn btn-icon icon-left btn-danger">
                                                    <i class="fas fa-trash"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection