@extends('layouts.app')

@section('content')
<title>SHE Dashboard | Safety Hygiene Environment System</title>
<section class="section">
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <center style="padding-top:50px">
                      <img src="public/assets/img/shelogo.png" class="img-responsive" alt="logo" width="150">
                    </center>
                    <center>
                      <h5>SHE Dashboard</h5>
                    </center>

                    <div class="row" style="padding-top:30px; margin-left:0px; margin-right:0px">
                      <div class="col-lg-6 col-xs-12" id="listsatu">
                        <center>
                        <a href="{{ url('/menuAPD') }}" id="firesystem" class="btn btn-icon icon-left btn-primary" style="width:100%">
                          <div class="col-12 row">
                            <div class="col-1 text-left" style="margin-top:5px"><i class="fa fa-eye fa-3x"></i></div>
                            <div class="col-8 text-left" style="margin-left:10px; margin-top:10px"><h3 class="font-bold"> APD MENU</h3></div>
                          </div>
                        </a>
                        </center>
                        <center>
                        <a href="{{ url('/menuSafety') }}" id="firesystem" class="btn btn-icon icon-left btn-primary" style="margin-top:10px; width:100%">
                          <div class="col-12 row">
                            <div class="col-1 text-left" style="margin-top:5px"><i class="fa fa-leaf fa-3x"></i></div>
                            <div class="col-8 text-left" style="margin-left:10px; margin-top:10px"><h3 class="font-bold"> SAFETY MENU</h3></div>
                          </div>
                        </a>
                        </center>
                      </div>

                      <div class="col-lg-6 col-xs-12" id="listdua">
                        <center>
                        <a href="{{ url('/menuFireSystem') }}" id="firesystem" class="btn btn-icon icon-left btn-primary" style="width:100%">
                          <div class="col-12 row">
                            <div class="col-1 text-left" style="margin-top:5px"><i class="fa fa-fire-extinguisher fa-3x"></i></div>
                            <div class="col-8 text-left" style="margin-left:10px; margin-top:10px"><h3 class="font-bold"> FIRE SYSTEM MENU</h3></div>
                          </div>
                        </a>
                        </center>
                    </div>

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped" id="table-1">

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
