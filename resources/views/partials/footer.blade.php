<!-- General JS Scripts -->
<!-- <script src="{{asset('public/assets/')}}/modules/jquery.min.js"></script>
<script src="{{asset('public/assets/')}}/modules/popper.js"></script>
<script src="{{asset('public/assets/')}}/modules/tooltip.js"></script>
<script src="{{asset('public/assets/')}}/modules/bootstrap/js/bootstrap.min.js"></script>
<script src="{{asset('public/assets/')}}/modules/nicescroll/jquery.nicescroll.min.js"></script>
<script src="{{asset('public/assets/')}}/modules/moment.min.js"></script>
<script src="{{asset('public/assets/')}}/js/stisla.js"></script> -->

<!-- JS Libraies -->
<script src="{{asset('public/assets/')}}/modules/jquery.sparkline.min.js"></script>
<script src="{{asset('public/assets/')}}/modules/chart.min.js"></script>
<script src="{{asset('public/assets/')}}/modules/owlcarousel2/dist/owl.carousel.min.js"></script>
<script src="{{asset('public/assets/')}}/modules/summernote/summernote-bs4.js"></script>
<script src="{{asset('public/assets/')}}/modules/chocolat/dist/js/jquery.chocolat.min.js"></script>

<script src="{{ asset('public/assets/modules/datatables/datatables.min.js') }}"></script>

<!-- Page Specific JS File -->
<script src="{{asset('public/assets/')}}/js/page/index.js"></script>
<!-- <script src="{{asset('public/assets/')}}/js/page/components-statistic.js"></script> -->

<!-- Template JS File -->
<script src="{{asset('public/assets/')}}/js/scripts.js"></script>
<script src="{{asset('public/assets/')}}/js/custom.js"></script>
<!-- <script src="{{asset('public/assets/')}}/js/ajax.js"></script> -->

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
@yield('scripts')
