<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">

    <!-- General CSS Files -->
    <link rel="stylesheet" href="{{asset('public/assets/')}}/modules/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('public/assets/')}}/modules/fontawesome/css/all.min.css">

    <!-- CSS Libraries -->
    <link rel="stylesheet" href="{{asset('public/assets/')}}/modules/jqvmap/dist/jqvmap.min.css">
    <link rel="stylesheet" href="{{asset('public/assets/')}}/modules/summernote/summernote-bs4.css">
    <link rel="stylesheet" href="{{asset('public/assets/')}}/modules/owlcarousel2/dist/public/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="{{asset('public/assets/')}}/modules/owlcarousel2/dist/public/assets/owl.theme.default.min.css">
    <link href="{{asset('public/assets/corelib/ajax.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('public/assets/modules/datatables/datatables.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('public/assets/')}}/modules/select2/dist/css/select2.min.css">

    <!-- Template CSS -->
    <link rel="stylesheet" href="{{asset('public/assets/')}}/css/style.css">
    <link rel="stylesheet" href="{{asset('public/assets/')}}/css/components.css">

    <!-- <link href="{{asset('public/assets/corelib/ajax.css')}}" rel="stylesheet" type="text/css">
    <script src="{{asset('public/assets/corelib/core.js')}}"></script> -->

    <!-- Start GA -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>

    <script src="{{asset('public/assets/')}}/modules/jquery.min.js"></script>
    <script src="{{asset('public/assets/')}}/modules/popper.js"></script>
    <script src="{{asset('public/assets/')}}/modules/tooltip.js"></script>
    <script src="{{asset('public/assets/')}}/modules/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{asset('public/assets/')}}/modules/nicescroll/jquery.nicescroll.min.js"></script>
    <script src="{{asset('public/assets/')}}/modules/moment.min.js"></script>
    <script src="{{asset('public/assets/')}}/js/stisla.js"></script>
    <script src="{{asset('public/assets/')}}/modules/select2/dist/js/select2.min.js"></script>

    <!-- Ajax -->
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
    <script src="{{asset('public/assets/corelib/core.js')}}"></script>

    @yield('scripts')

    <script>
        baseURL = '{{url("/")}}';
    </script>
    @yield('style')

    <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-94034622-3');
    </script>
    <!-- /END GA -->
</head>
