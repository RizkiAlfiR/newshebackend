<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="{{ route('home') }}"><img src="{{asset('public/assets/img/shelogo.png')}}" class="img-responsive" alt="logo" width="40"> SEMEN INDONESIA</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <!-- <a href="{{ route('home') }}">SHE</a> -->
            <a href="{{ route('home') }}"><img src="{{asset('public/assets/img/shelogo.png')}}" class="img-responsive" alt="logo" width="40"></a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>
            <li class="{{ request()->is('home') ? 'active' : '' }}">
                <a href="{{ route('home') }}" class="nav-link"><i class="fas fa-th-large"></i><span>Dashboard</span></a>
            </li>

            <li class="menu-header">Master Data</li>
            <li class="dropdown">
                <a href="" class="nav-link has-dropdown"><i class="fas fa-inbox"></i> <span>Master Data</span></a>
                <ul class="dropdown-menu">
                    <li class="{{ request()->is('users') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ url('/users') }}">Users</a>
                    </li>
                    <li class="{{ request()->is('employee') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ url('/employee') }}">Employee</a>
                    </li>
                    <li class="{{ request()->is('unitKerja') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ url('/unitKerja') }}">Unit Kerja</a>
                    </li>
                    <li class="{{ request()->is('plant') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ url('/plant') }}">Plant</a>
                    </li>
                    <li class="{{ request()->is('pplant') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ url('/pplant') }}">Planning Plant</a>
                    </li>
                    <li class="{{ request()->is('funclocation') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ url('/funclocation') }}">Func. Location</a>
                    </li>
                    <li class="{{ request()->is('inspectionArea') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ url('/inspectionArea') }}">Inspection Area</a>
                    </li>
                    <li class="{{ request()->is('reportcategory') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ url('/reportcategory') }}">Report Category</a>
                    </li>
                    <li class="{{ request()->is('siogroup') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ url('/siogroup') }}">SIO Group</a>
                    </li>
                    <li class="{{ request()->is('datavendor') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ url('/datavendor') }}">Vendor</a>
                    </li>
                </ul>
            </li>

            <li class="menu-header">News</li>
            <li class="{{ request()->is('news') ? 'active' : '' }}">
                <a href="{{ url('/news') }}" ><i class="fas fa-file-alt"></i> <span>News</span></a>
            </li>

            <li class="menu-header">Menu SHE</li>
            <li class="dropdown">
                <li class="{{ request()->is('menuAPD') ? 'active' : '' }}">
                    <a href="{{ url('/menuAPD') }}" class="nav-link"><i class="fas fa-medkit"></i><span>APD</span></a>
                </li>
                <li class="{{ request()->is('menuSafety') ? 'active' : '' }}">
                    <a href="{{ url('/menuSafety') }}" class="nav-link"><i class="fas fa-umbrella"></i><span>Safety</span></a>
                </li>
                <li class="{{ request()->is('menuFireSystem') ? 'active' : '' }}">
                    <a href="{{ url('/menuFireSystem') }}" class="nav-link"><i class="fas fa-fire"></i><span>Fire System</span></a>
                </li>
            </li>
        </ul>
    </aside>
</div>
