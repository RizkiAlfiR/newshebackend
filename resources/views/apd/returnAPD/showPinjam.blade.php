<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-konten" class='form-horizontal'>
    <table width="100%">
            <tbody>
                <tr>
                  <td class="text-center">
                    <img src="public/assets/img/logosemen.png" class="img-responsive" alt="logo" width="80">
                  </td>
                </tr>
                <tr>
                  <td class="text-center">
                    <h6 class="modal-title" id="title">List Detail Return APD</h6>
                    <h4 class="modal-title" id="title"></h4><small class="font-bold">PT. Semen Indonesia</small>
                  </td>
                </tr>
            </tbody>
        </table>

        <div style="height:400px">
          <div class="form-group">
              @foreach($pinjamAPD as $id => $data)
              <div class="row go_form">
                <div class="col-md-2" style="margin-top: 10px">
                    <label>Tanggal request</label>
                </div>
                <div class="col-md-10" style="margin-top: 10px">
                    <label>: {{ date('d-m-Y',strtotime($data->create_at)) }}</label>
                </div>
              </div>
              <div class="row go_form">
                <div class="col-md-2" style="margin-top: 10px">
                    <label>Kode Pinjam</label>
                </div>
                <div class="col-md-10" style="margin-top: 10px">
                    <label>: {{ $data->kode_pinjam }}</label>
                </div>
              </div>
              <div class="row go_form">
                <div class="col-md-2" style="margin-top: 10px">
                    <label>No. Badge</label>
                </div>
                <div class="col-md-10" style="margin-top: 10px">
                    <label>: {{ $data->nopeg }}</label>
                </div>
              </div>
              <div class="row go_form">
                <div class="col-md-2" style="margin-top: 10px">
                    <label>Nama</label>
                </div>
                <div class="col-md-10" style="margin-top: 10px">
                    <label>: {{ $data->name }}</label>
                </div>
              </div>
              <div class="row go_form">
                <div class="col-md-2" style="margin-top: 10px">
                    <label>Unit Kerja</label>
                </div>
                <div class="col-md-10" style="margin-top: 10px">
                    <label>: {{ $data->uk_text }}</label>
                </div>
              </div>
              <div class="row go_form">
                <div class="col-md-2" style="margin-top: 10px">
                    <label>Tanggal Pinjam</label>
                </div>
                <div class="col-md-10" style="margin-top: 10px">
                    <label>: {{ $data->tanggal_pinjam }}</label>
                </div>
              </div>
              <div class="row go_form">
                <div class="col-md-2" style="margin-top: 10px">
                    <label>Tanggal Kembali</label>
                </div>
                <div class="col-md-10" style="margin-top: 10px">
                    <label>: {{ $data->retur_date}}</label>
                </div>
              </div>

              <div class="">
                    <div class="table-responsive">
                        <table class="table table-striped" id="table-1">
                            <thead>
                                <tr>
                                    <th class="text-center">No</th>
                                    <th>APD Name</th>
                                    <th>Merk</th>
                                    <th>Quantity</th>
                                    <th>Note</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($detailPinjam as $i => $dataPinjam)
                                <tr>
                                    <td>{{ $i+1 }}</td>
                                    <td>{{ $dataPinjam->nama_apd }}</td>
                                    <td>{{ $dataPinjam->merk}}</td>
                                    <td>{{ $dataPinjam->jumlah}}</td>
                                    <td>{{ $dataPinjam->keterangan}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
              @endforeach
          </div>
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            <button type="submit" class="btn btn-info" data-loading-text="<i class='fa fa-spinner fa-spin'></i>
              &nbsp;<font class='lowercase'></font>"> <i class="fa fa-check"></i> Return</button>
        </div>


    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/returnAPD/return', data, '#result-form-konten');
        })
    })
</script>