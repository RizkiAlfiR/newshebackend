@extends('layouts.app')

@section('content')
<title>Return APD - Safety Hygiene Environment System</title>
<section class="section">
    <div class="section-body">
    <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="col-12" style="padding-top:20px">
                        <div class="nav-wrapper">
                            <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link mb-sm-3 mb-md-0 active" id="tabs-icons-text-1-tab" data-toggle="tab" href="#tabs-icons-text-1" role="tab" aria-controls="tabs-icons-text-1" aria-selected="true"><i class="ni ni-cloud-upload-96 mr-2"></i>Pengembalian  APD</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-2-tab" data-toggle="tab" href="#tabs-icons-text-2" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>Pengembalian Terlambat</a>
                                </li>
                            </ul>
                        </div>
                        <div>
                            <div>
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
                                        <div class="">
                                            <h5>List Peminjaman APD</h5>
                                            <div class="">
                                                <div class="table-responsive">
                                                    <table class="table table-striped" id="table-return" style="width:100%">
                                                        <thead>
                                                            <tr>
                                                                <th class="text-center">No</th>
                                                                <th>Tangal Order</th>
                                                                <th>Kode Pinjam</th>
                                                                <th>Nama</th>
                                                                <th>Unit Kerja</th>
                                                                <th>Tanggal Pinjam</th>
                                                                <th>Note Pinjam</th>
                                                                <th>Tanggal Kembali</th>
                                                                <th>Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($returnAPD as $i => $data)
                                                            <tr>
                                                                <td class="text-center">{{ $i+1 }}</td>
                                                                <td>{{ date('Y-m-d',strtotime($data->create_at)) }}</td>
                                                                <td>{{ $data->kode_pinjam}}</td>
                                                                <td>[{{ $data->nopeg}}] {{ $data->name}}</td>
                                                                <td>{{ $data->uk_text}}</td>
                                                                <td>{{date('Y-m-d',strtotime($data->tanggal_pinjam))}}</td>
                                                                <td>{{$data->note}}</td>
                                                                <td>{{date('Y-m-d',strtotime($data->retur_date))}}</td>
                                                                <td>
                                                                    <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                                        <button type="button" class="btn btn-icon icon-left btn-success"
                                                                        onclick="loadModal(this)" target="/returnAPD/showPinjam?id=" data="id={{$data->id}}">
                                                                            <i class="fas fa-eye"></i>
                                                                        </button>
                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade show" id="tabs-icons-text-2" role="tabpanel" aria-labelledby="tabs-icons-text-2-tab">
                                        <div class="">
                                            <h5>List Peminjaman APD Terlambat</h5>
                                            <div class="">
                                                <div class="table-responsive">
                                                    <table class="table table-striped" id="table-telat" style="width:100%">
                                                        <thead>
                                                            <tr>
                                                                <th class="text-center">No</th>
                                                                <th>Tangal Order</th>
                                                                <th>Kode Pinjam</th>
                                                                <th>Nama</th>
                                                                <th>Unit Kerja</th>
                                                                <th>Tanggal Pinjam</th>
                                                                <th>Note Pinjam</th>
                                                                <th>Tanggal Kembali</th>
                                                                <th>Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($telatAPD as $i => $data)
                                                            <tr>
                                                                <td class="text-center">{{ $i+1 }}</td>
                                                                <td>{{ date('Y-m-d',strtotime($data->create_at)) }}</td>
                                                                <td>{{ $data->kode_pinjam}}</td>
                                                                <td>[{{ $data->nopeg}}] {{ $data->name}}</td>
                                                                <td>{{ $data->uk_text}}</td>
                                                                <td>{{date('Y-m-d',strtotime($data->tanggal_pinjam))}}</td>
                                                                <td>{{$data->note}}</td>
                                                                <td>{{date('Y-m-d',strtotime($data->retur_date))}}</td>
                                                                <td>
                                                                    <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                                        <button type="button" class="btn btn-icon icon-left btn-success"
                                                                        onclick="loadModal(this)" target="/returnAPD/showPinjam?id=" data="id={{$data->id}}">
                                                                            <i class="fas fa-eye"></i>
                                                                        </button>
                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <div>
    </div>
</section>
@section('scripts')
    <script>
        function deleteMaster(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("", "Do you want to remove this data ?", function () {
               ajaxTransfer("/stockAPD/deleteMaster", data, "#modal-output");
            })
        }


        var table = $('#table-return');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            // order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });

        var table = $('#table-telat');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            // order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });
    </script>
@endsection

@endsection
