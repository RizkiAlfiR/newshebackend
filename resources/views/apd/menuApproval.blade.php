@extends('layouts.app')

@section('content')
<title>APD - Safety Hygiene Environment System</title>
<section class="section">
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <center style="padding-top:50px">
                      <img src="public/assets/img/apd.png" class="img-responsive" alt="logo" width="150">
                    </center>
                    <center>
                      <h5>Approval Alat Pelindung Diri</h5>
                    </center>

                    <div class="row" style="padding-top:30px; margin-left:0px; margin-right:0px">
                      <div class="col-lg-6 col-xs-12" id="listsatu">
                        <center>
                        <a href="{{ url('/menuDailySection') }}" id="apd" class="btn btn-icon icon-left btn-primary" style="width:100%">
                          <div class="col-lg-12">
                            <div class="widget style1">
                              <div class="row vertical-align">
                                <div class="col-xs-3" style="padding-right: 15px">
                                  <i class="fa fa-file fa-3x"></i>
                                </div>
                                <div class="col-xs-9 text-right" style="padding-left: 0px;">
                                  <h3 class="font-bold">PERMINTAAN PERSONAL</h3>
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                        </center>
                        <center>
                        <a href="{{ url('/fireReport') }}" id="apd" class="btn btn-icon icon-left btn-primary" style="margin-top:10px; width:100%">
                          <div class="col-lg-12">
                            <div class="widget style1">
                              <div class="row vertical-align">
                                <div class="col-xs-3" style="padding-right: 15px">
                                  <i class="fa fa-fire fa-3x"></i>
                                </div>
                                <div class="col-xs-9 text-right" style="padding-left: 0px;">
                                  <h3 class="font-bold">PERMINTAAN UNIT KERJA</h3>
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                        </center>
                        <center>
                        <a href="{{ url('/menuUnitCheckDashboard') }}" id="apd" class="btn btn-icon icon-left btn-primary" style="margin-top:10px; width:100%">
                          <div class="col-lg-12">
                            <div class="widget style1">
                              <div class="row vertical-align">
                                <div class="col-xs-3" style="padding-right: 15px">
                                  <i class="fa fa-cogs fa-3x"></i>
                                </div>
                                <div class="col-xs-9 text-right" style="padding-left: 0px;">
                                  <h3 class="font-bold">PERMINTAAN PEMINJAMAN</h3>
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                        </center>
                      </div>

                      <div class="col-lg-6 col-xs-12" id="listdua">
                        <center>
                        <a href="{{ url('/menuInspection') }}" id="apd" class="btn btn-icon icon-left btn-primary" style="width:100%">
                          <div class="col-lg-12">
                            <div class="widget style1">
                              <div class="row vertical-align">
                                <div class="col-xs-3" style="padding-right: 15px">
                                  <i class="fa fa-cogs fa-3x"></i>
                                </div>
                                <div class="col-xs-9 text-right" style="padding-left: 0px;">
                                  <h3 class="font-bold">APD RUSAK</h3>
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                        </center>
                    </div>

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped" id="table-1">

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
