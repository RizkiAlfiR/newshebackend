<div id="result-form-konten">
    <form onsubmit="return false;" id="form-konten" class='form-horizontal'>
        <table width="100%">
            <tbody>
                <tr>
                  <td class="text-center">
                    <img src="public/assets/img/logosemen.png" class="img-responsive" alt="logo" width="80">
                  </td>
                </tr>
                <tr>
                  <td class="text-center">
                    <h6 class="modal-title" id="title">Create Master APD</h6>
                    <h4 class="modal-title" id="title"></h4><small class="font-bold">PT. Semen Indonesia</small>
                  </td>
                </tr>
            </tbody>
        </table>
        <div class="modal-body" style="overflow: auto; height: 400px">
            <hr>
            <div class="row go_form">
                <div class="col-md-6">
                    <label>Company</label>
                    <select name="code_comp" class="form-control">
                        <option value="5000">PT Semen Indonesia</option>
                    </select>
                </div>
                <div class="col-md-6" >
                    <label>Plant</label>
                    <select name="code_plant" class="form-control">
                        @foreach($plant as $item)
                        <option value="{{ $item->plant }}" @if(!is_null($data)) @if($item->plant == $data->code_plant) selected="selected" @endif @endif>{{ $item->plant_text}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row go_form">
                <div class="col-md-12" style="margin-top: 10px">
                    <label>APD Type</label>
                    <select name="group_code" class="form-control">
                         @foreach($mastercategory as $item)
                        <option value="{{ $item->id}}" @if(!is_null($data)) @if($item->id == $data->group_code) selected="selected" @endif @endif>{{ $item->id."-".$item->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row go_form">
                <div class="col-md-12" style="margin-top: 10px">
                    <label>APD Name</label>
                    <input type="text" class="form-control daterange-cus" name="nama" value="{{$data->nama}}">
                </div>
            </div>
            <div class="row go_form">
                <div class="col-md-12" style="margin-top: 10px">
                    <label>APD Merk</label>
                    <input type="text" class="form-control daterange-cus" name="merk" value="{{$data->merk}}">
                </div>
            </div>
            
            <div class="row go_form">
                <div class="col-md-4" style="margin-top: 10px">
                    <label>APD Size</label>
                    <input type="text" class="form-control daterange-cus" name="size" value="{{$data->size}}">
                </div>
                <div class="col-md-4" style="margin-top: 10px">
                    <label>Stock</label>
                    <input type="text" class="form-control daterange-cus" name="stok" value="{{$data->stok}}">
                </div>
                <div class="col-md-4" style="margin-top: 10px">
                    <label>Expired (In Month)</label>
                    <input type="text" class="form-control daterange-cus" name="masa_text" value="{{$data->masa_text}}">
                </div>
            </div>
            <div class="row go_form">
                <div class="col-md-12" style="margin-top: 10px">
                    <label>APD Image</label>
                    <div class="controls">
                    <input type="file" name="foto_bef" id="foto_bef">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            <button type="submit" class="btn btn-info" data-loading-text="<i class='fa fa-spinner fa-spin'></i>
              &nbsp;<font class='lowercase'></font>"> <i class="fa fa-check"></i> Save</button>
        </div>

    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='kode' value='{{ $data->kode }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>
</div>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/stockAPD/saveMaster', data, '#result-form-konten');
        })
    })
</script>
