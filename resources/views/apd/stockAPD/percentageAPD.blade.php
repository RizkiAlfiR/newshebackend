@extends('layouts.app')
@section('content')
<title>Return APD - Safety Hygiene Environment System</title>
<section class="section">
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="col-12" style="padding-top:20px">
                        <div>
                            <div>
                                <div class="">
                                    <h5>Stock APD</h5>
                                    <div id="container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                                
                                    <div class="">
                                        <div class="table-responsive">
                                            <table class="table table-striped" id="table-return" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">No</th>
                                                        <th>Kategori APD</th>
                                                        <th>Total Stok</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($data as $i => $dataTabel)
                                                    <tr>
                                                        <td class="text-center">{{ $i+1 }}</td>
                                                        <td>{{ $dataTabel['name']}}</td>
                                                        <td>{{ $dataTabel['y']}}</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <div>
    </div>
</section>
</title>
        
@endsection
@section('scripts')
<script>
        $(document).ready(function() {
            $.ajax({
                type: 'GET',
                url: "{{url('/stockAPD/percentage')}}",
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    getStockAPD(data);
                }
            });
 
        });
       
        function getStockAPD(data) {
            Highcharts.chart('container', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Stock Percentage APD'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: data.data,
                }]
            });
        }

        var table = $('#table-return');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            // order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });
        
    </script>
@endsection