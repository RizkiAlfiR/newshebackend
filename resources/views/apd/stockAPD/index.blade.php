@extends('layouts.app')

@section('content')
<title>Stock APD - Safety Hygiene Environment System</title>
<section class="section">
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <p class="" style="margin: 10px;">
                    <a onclick="loadModal(this)" target="/stockAPD/addMaster" class="btn btn-icon icon-left btn-secondary " title="Create New">
                        <i class="fa fa-plus"></i> Create New APD</a>
                    </p>
                  <div class="col-12" style="padding-top:20px">
                    <div class="nav-wrapper">
                        <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0 active" id="tabs-icons-text-1-tab" data-toggle="tab" href="#tabs-icons-text-1" role="tab" aria-controls="tabs-icons-text-1" aria-selected="true"><i class="ni ni-cloud-upload-96 mr-2"></i>Pelindung Kepala</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-2-tab" data-toggle="tab" href="#tabs-icons-text-2" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>Pelindung Mata</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-3-tab" data-toggle="tab" href="#tabs-icons-text-3" role="tab" aria-controls="tabs-icons-text-3" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>Pelindung Telinga</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-4-tab" data-toggle="tab" href="#tabs-icons-text-4" role="tab" aria-controls="tabs-icons-text-4" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>Pelindung Hidung</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-5-tab" data-toggle="tab" href="#tabs-icons-text-5" role="tab" aria-controls="tabs-icons-text-5" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>Pelindung Tangan</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-6-tab" data-toggle="tab" href="#tabs-icons-text-6" role="tab" aria-controls="tabs-icons-text-6" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>Pelindung Badan</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-7-tab" data-toggle="tab" href="#tabs-icons-text-7" role="tab" aria-controls="tabs-icons-text-7" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>Pelindung Kaki</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-8-tab" data-toggle="tab" href="#tabs-icons-text-8" role="tab" aria-controls="tabs-icons-text-8" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>Full Body Harness</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-9-tab" data-toggle="tab" href="#tabs-icons-text-9" role="tab" aria-controls="tabs-icons-text-9" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>Paket Obat PPPK</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-10-tab" data-toggle="tab" href="#tabs-icons-text-10" role="tab" aria-controls="tabs-icons-text-10" aria-selected="false"><i class="ni ni-bell-55 mr-2"></i>Lain-Lain</a>
                            </li>
                        </ul>
                    </div>
                    <div>
                      <div>
                          <div class="tab-content" id="myTabContent">
                          <!-- Pelindung Kepala -->
                              <div class="tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
                                <div class="">
                                    <h5>Data Stock : Pelindung Kepala</h5>
                                    <div class="">
                                        <div class="table-responsive">
                                            <table class="table table-striped" id="table-1" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">No</th>
                                                        <th>Code</th>
                                                        <th>Name</th>
                                                        <th>Merk</th>
                                                        <th>Size</th>
                                                        <th>Stock</th>
                                                        <th>Available</th>
                                                        <th>Image</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($pelindungKepala as $i => $data)
                                                    <tr>
                                                        <td class="text-center">{{ $i+1 }}</td>
                                                        <td>{{ $data->kode}}</td>
                                                        <td>{{ $data->nama}}</td>
                                                        <td>{{ $data->merk}}</td>
                                                        <td>{{ $data->size}}</td>
                                                        <td>{{ $data->stok}}</td>
                                                        <td>{{ $data->a_stok}}/{{ $data->stok}}</td>
                                                        <td class="align-middle">
                                                            @if(!is_null($data->foto_bef))
                                                            <a href="{{asset('public/uploads/APD/')}}/{{$data->foto_bef}}" target="_blank">
                                                                <img src="{{asset('public/uploads/APD/')}}/{{$data->foto_bef}}" class="img-responsive" alt="logo" width="80">
                                                            </a>
                                                            @else
                                                            <img src="{{asset('public/images')}}/{{'preview-icon.png'}}" class="img-responsive" alt="logo" width="80">
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                                <button type="button" class="btn btn-icon icon-left btn-info"
                                                                onclick="loadModal(this)" target="/stockAPD/editMaster?id=" data="id={{$data->id}}">
                                                                    <i class="fas fa-edit"></i>
                                                                </button>
                                                                <button type="button" class="btn btn-icon icon-left btn-danger"
                                                              onclick="deleteMaster({{$data->id}})">
                                                                  <i class="fas fa-trash"></i>
                                                              </button>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                              </div>

                              <!-- Pelindung Mata -->
                              <div class="tab-pane fade show" id="tabs-icons-text-2" role="tabpanel" aria-labelledby="tabs-icons-text-2-tab">
                                <div class="">
                                    <h5>Data Stock : Pelindung Mata</h5>
                                    <div class="">
                                        <div class="table-responsive">
                                            <table class="table table-striped" id="table-2" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">No</th>
                                                        <th>Code</th>
                                                        <th>Name</th>
                                                        <th>Merk</th>
                                                        <th>Size</th>
                                                        <th>Stock</th>
                                                        <th>Available</th>
                                                        <th>Image</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($pelindungMata as $i => $data)
                                                    <tr>
                                                        <td class="text-center">{{ $i+1 }}</td>
                                                        <td>{{ $data->kode}}</td>
                                                        <td>{{ $data->nama}}</td>
                                                        <td>{{ $data->merk}}</td>
                                                        <td>{{ $data->size}}</td>
                                                        <td>{{ $data->stok}}</td>
                                                        <td>{{ $data->a_stok}}/{{ $data->stok}}</td>
                                                        <td class="align-middle">
                                                            @if(!is_null($data->foto_bef))
                                                            <a href="{{asset('public/uploads/APD/')}}/{{$data->foto_bef}}" target="_blank">
                                                                <img src="{{asset('public/uploads/APD/')}}/{{$data->foto_bef}}" class="img-responsive" alt="logo" width="80">
                                                            </a>
                                                            @else
                                                            <img src="{{asset('public/images')}}/{{'preview-icon.png'}}" class="img-responsive" alt="logo" width="80">
                                                            @endif
                                                        </td>   
                                                        <td>
                                                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                                <button type="button" class="btn btn-icon icon-left btn-info"
                                                                    onclick="loadModal(this)" target="/stockAPD/editMaster?id=" data="id={{$data->id}}">
                                                                        <i class="fas fa-edit"></i>
                                                                    </button>
                                                                    <button type="button" class="btn btn-icon icon-left btn-danger"
                                                                onclick="deleteMaster({{$data->id}})">
                                                                    <i class="fas fa-trash"></i>
                                                                </button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                              </div>

                              <!-- Pelindung Telinga -->
                              <div class="tab-pane fade show" id="tabs-icons-text-3" role="tabpanel" aria-labelledby="tabs-icons-text-3-tab">
                                <div class="">
                                    <h5>Data Stock : Pelindung Telinga</h5>
                                    <div class="">
                                        <div class="table-responsive">
                                            <table class="table table-striped" id="table-3" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">No</th>
                                                        <th>Code</th>
                                                        <th>Name</th>
                                                        <th>Merk</th>
                                                        <th>Size</th>
                                                        <th>Stock</th>
                                                        <th>Available</th>
                                                        <th>Image</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($pelindungTelinga as $i => $data)
                                                    <tr>
                                                        <td class="text-center">{{ $i+1 }}</td>
                                                        <td>{{ $data->kode}}</td>
                                                        <td>{{ $data->nama}}</td>
                                                        <td>{{ $data->merk}}</td>
                                                        <td>{{ $data->size}}</td>
                                                        <td>{{ $data->stok}}</td>
                                                        <td>{{ $data->a_stok}}/{{ $data->stok}}</td>
                                                        <td class="align-middle">
                                                            @if(!is_null($data->foto_bef))
                                                            <a href="{{asset('public/uploads/APD/')}}/{{$data->foto_bef}}" target="_blank">
                                                                <img src="{{asset('public/uploads/APD/')}}/{{$data->foto_bef}}" class="img-responsive" alt="logo" width="80">
                                                            </a>
                                                            @else
                                                            <img src="{{asset('public/images')}}/{{'preview-icon.png'}}" class="img-responsive" alt="logo" width="80">
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                            <button type="button" class="btn btn-icon icon-left btn-info"
                                                                onclick="loadModal(this)" target="/stockAPD/editMaster?id=" data="id={{$data->id}}">
                                                                    <i class="fas fa-edit"></i>
                                                                </button>
                                                                <button type="button" class="btn btn-icon icon-left btn-danger"
                                                              onclick="deleteMaster({{$data->id}})">
                                                                  <i class="fas fa-trash"></i>
                                                              </button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                              </div>

                              <!-- Pelindung Hidung -->
                              <div class="tab-pane fade show" id="tabs-icons-text-4" role="tabpanel" aria-labelledby="tabs-icons-text-4-tab">
                                <div class="">
                                    <h5>Data Stock : Pelindung Hidung</h5>
                                    <div class="">
                                        <div class="table-responsive">
                                            <table class="table table-striped" id="table-4" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">No</th>
                                                        <th>Code</th>
                                                        <th>Name</th>
                                                        <th>Merk</th>
                                                        <th>Size</th>
                                                        <th>Stock</th>
                                                        <th>Available</th>
                                                        <th>Image</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($pelindungHidung as $i => $data)
                                                    <tr>
                                                        <td class="text-center">{{ $i+1 }}</td>
                                                        <td>{{ $data->kode}}</td>
                                                        <td>{{ $data->nama}}</td>
                                                        <td>{{ $data->merk}}</td>
                                                        <td>{{ $data->size}}</td>
                                                        <td>{{ $data->stok}}</td>
                                                        <td>{{ $data->a_stok}}/{{ $data->stok}}</td>
                                                        <td class="align-middle">
                                                            @if(!is_null($data->foto_bef))
                                                            <a href="{{asset('public/uploads/APD/')}}/{{$data->foto_bef}}" target="_blank">
                                                                <img src="{{asset('public/uploads/APD/')}}/{{$data->foto_bef}}" class="img-responsive" alt="logo" width="80">
                                                            </a>
                                                            @else
                                                            <img src="{{asset('public/images')}}/{{'preview-icon.png'}}" class="img-responsive" alt="logo" width="80">
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                            <button type="button" class="btn btn-icon icon-left btn-info"
                                                                onclick="loadModal(this)" target="/stockAPD/editMaster?id=" data="id={{$data->id}}">
                                                                    <i class="fas fa-edit"></i>
                                                                </button>
                                                                <button type="button" class="btn btn-icon icon-left btn-danger"
                                                              onclick="deleteMaster({{$data->id}})">
                                                                  <i class="fas fa-trash"></i>
                                                              </button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                              </div>

                              <!-- Pelindung Tangan-->
                              <div class="tab-pane fade show" id="tabs-icons-text-5" role="tabpanel" aria-labelledby="tabs-icons-text-5-tab">
                                <div class="">
                                    <h5>Data Stock : Pelindung Tangan</h5>
                                    <div class="">
                                        <div class="table-responsive">
                                            <table class="table table-striped" id="table-5" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">No</th>
                                                        <th>Code</th>
                                                        <th>Name</th>
                                                        <th>Merk</th>
                                                        <th>Size</th>
                                                        <th>Stock</th>
                                                        <th>Available</th>
                                                        <th>Image</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($pelindungTangan as $i => $data)
                                                    <tr>
                                                        <td class="text-center">{{ $i+1 }}</td>
                                                        <td>{{ $data->kode}}</td>
                                                        <td>{{ $data->nama}}</td>
                                                        <td>{{ $data->merk}}</td>
                                                        <td>{{ $data->size}}</td>
                                                        <td>{{ $data->stok}}</td>
                                                        <td>{{ $data->a_stok}}/{{ $data->stok}}</td>
                                                        <td class="align-middle">
                                                            @if(!is_null($data->foto_bef))
                                                            <a href="{{asset('public/uploads/APD/')}}/{{$data->foto_bef}}" target="_blank">
                                                                <img src="{{asset('public/uploads/APD/')}}/{{$data->foto_bef}}" class="img-responsive" alt="logo" width="80">
                                                            </a>
                                                            @else
                                                            <img src="{{asset('public/images')}}/{{'preview-icon.png'}}" class="img-responsive" alt="logo" width="80">
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                            <button type="button" class="btn btn-icon icon-left btn-info"
                                                                onclick="loadModal(this)" target="/stockAPD/editMaster?id=" data="id={{$data->id}}">
                                                                    <i class="fas fa-edit"></i>
                                                                </button>
                                                                <button type="button" class="btn btn-icon icon-left btn-danger"
                                                              onclick="deleteMaster({{$data->id}})">
                                                                  <i class="fas fa-trash"></i>
                                                              </button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                              </div>

                              <!-- Pelindung Badan -->
                              <div class="tab-pane fade show" id="tabs-icons-text-6" role="tabpanel" aria-labelledby="tabs-icons-text-6-tab">
                                <div class="">
                                    <h5>Data Stock : Pelindung Badan</h5>
                                    <div class="">
                                        <div class="table-responsive">
                                            <table class="table table-striped" id="table-6" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">No</th>
                                                        <th>Code</th>
                                                        <th>Name</th>
                                                        <th>Merk</th>
                                                        <th>Size</th>
                                                        <th>Stock</th>
                                                        <th>Available</th>
                                                        <th>Image</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($pelindungBadan as $i => $data)
                                                    <tr>
                                                        <td class="text-center">{{ $i+1 }}</td>
                                                        <td>{{ $data->kode}}</td>
                                                        <td>{{ $data->nama}}</td>
                                                        <td>{{ $data->merk}}</td>
                                                        <td>{{ $data->size}}</td>
                                                        <td>{{ $data->stok}}</td>
                                                        <td>{{ $data->a_stok}}/{{ $data->stok}}</td>
                                                        <td class="align-middle">
                                                            @if(!is_null($data->foto_bef))
                                                            <a href="{{asset('public/uploads/APD/')}}/{{$data->foto_bef}}" target="_blank">
                                                                <img src="{{asset('public/uploads/APD/')}}/{{$data->foto_bef}}" class="img-responsive" alt="logo" width="80">
                                                            </a>
                                                            @else
                                                            <img src="{{asset('public/images')}}/{{'preview-icon.png'}}" class="img-responsive" alt="logo" width="80">
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                            <button type="button" class="btn btn-icon icon-left btn-info"
                                                                onclick="loadModal(this)" target="/stockAPD/editMaster?id=" data="id={{$data->id}}">
                                                                    <i class="fas fa-edit"></i>
                                                                </button>
                                                                <button type="button" class="btn btn-icon icon-left btn-danger"
                                                              onclick="deleteMaster({{$data->id}})">
                                                                  <i class="fas fa-trash"></i>
                                                              </button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                              </div>

                              <!-- Pelindung Kaki -->
                              <div class="tab-pane fade show" id="tabs-icons-text-7" role="tabpanel" aria-labelledby="tabs-icons-text-7-tab">
                                <div class="">
                                    <h5>Data Stock : Pelindung Kaki</h5>
                                    <div class="">
                                        <div class="table-responsive">
                                            <table class="table table-striped" id="table-7" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">No</th>
                                                        <th>Code</th>
                                                        <th>Name</th>
                                                        <th>Merk</th>
                                                        <th>Size</th>
                                                        <th>Stock</th>
                                                        <th>Available</th>
                                                        <th>Image</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($pelindungKaki as $i => $data)
                                                    <tr>
                                                        <td class="text-center">{{ $i+1 }}</td>
                                                        <td>{{ $data->kode}}</td>
                                                        <td>{{ $data->nama}}</td>
                                                        <td>{{ $data->merk}}</td>
                                                        <td>{{ $data->size}}</td>
                                                        <td>{{ $data->stok}}</td>
                                                        <td>{{ $data->a_stok}}/{{ $data->stok}}</td>
                                                        <td class="align-middle">
                                                            @if(!is_null($data->foto_bef))
                                                            <a href="{{asset('public/uploads/APD/')}}/{{$data->foto_bef}}" target="_blank">
                                                                <img src="{{asset('public/uploads/APD/')}}/{{$data->foto_bef}}" class="img-responsive" alt="logo" width="80">
                                                            </a>
                                                            @else
                                                            <img src="{{asset('public/images')}}/{{'preview-icon.png'}}" class="img-responsive" alt="logo" width="80">
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                            <button type="button" class="btn btn-icon icon-left btn-info"
                                                                onclick="loadModal(this)" target="/stockAPD/editMaster?id=" data="id={{$data->id}}">
                                                                    <i class="fas fa-edit"></i>
                                                                </button>
                                                                <button type="button" class="btn btn-icon icon-left btn-danger"
                                                              onclick="deleteMaster({{$data->id}})">
                                                                  <i class="fas fa-trash"></i>
                                                              </button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                              </div>

                              <!-- Full Body Harness -->
                              <div class="tab-pane fade show" id="tabs-icons-text-8" role="tabpanel" aria-labelledby="tabs-icons-text-8-tab">
                                <div class="">
                                    <h5>Data Stock : Full Body Harness</h5>
                                    <div class="">
                                        <div class="table-responsive">
                                            <table class="table table-striped" id="table-8" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">No</th>
                                                        <th>Code</th>
                                                        <th>Name</th>
                                                        <th>Merk</th>
                                                        <th>Size</th>
                                                        <th>Stock</th>
                                                        <th>Available</th>
                                                        <th>Image</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($fullBodyHarness as $i => $data)
                                                    <tr>
                                                        <td class="text-center">{{ $i+1 }}</td>
                                                        <td>{{ $data->kode}}</td>
                                                        <td>{{ $data->nama}}</td>
                                                        <td>{{ $data->merk}}</td>
                                                        <td>{{ $data->size}}</td>
                                                        <td>{{ $data->stok}}</td>
                                                        <td>{{ $data->a_stok}}/{{ $data->stok}}</td>
                                                        <td class="align-middle">
                                                            @if(!is_null($data->foto_bef))
                                                            <a href="{{asset('public/uploads/APD/')}}/{{$data->foto_bef}}" target="_blank">
                                                                <img src="{{asset('public/uploads/APD/')}}/{{$data->foto_bef}}" class="img-responsive" alt="logo" width="80">
                                                            </a>
                                                            @else
                                                            <img src="{{asset('public/images')}}/{{'preview-icon.png'}}" class="img-responsive" alt="logo" width="80">
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                            <button type="button" class="btn btn-icon icon-left btn-info"
                                                                onclick="loadModal(this)" target="/stockAPD/editMaster?id=" data="id={{$data->id}}">
                                                                    <i class="fas fa-edit"></i>
                                                                </button>
                                                                <button type="button" class="btn btn-icon icon-left btn-danger"
                                                              onclick="deleteMaster({{$data->id}})">
                                                                  <i class="fas fa-trash"></i>
                                                              </button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                              </div>

                              <!-- Paket Obat PPPK -->
                              <div class="tab-pane fade show" id="tabs-icons-text-9" role="tabpanel" aria-labelledby="tabs-icons-text-9-tab">
                                <div class="">
                                    <h5>Data Stock : Paket Obat PPPK</h5>
                                    <div class="">
                                        <div class="table-responsive">
                                            <table class="table table-striped" id="table-9" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">No</th>
                                                        <th>Code</th>
                                                        <th>Name</th>
                                                        <th>Merk</th>
                                                        <th>Size</th>
                                                        <th>Stock</th>
                                                        <th>Available</th>
                                                        <th>Image</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($paketObatP3K as $i => $data)
                                                    <tr>
                                                        <td class="text-center">{{ $i+1 }}</td>
                                                        <td>{{ $data->kode}}</td>
                                                        <td>{{ $data->nama}}</td>
                                                        <td>{{ $data->merk}}</td>
                                                        <td>{{ $data->size}}</td>
                                                        <td>{{ $data->stok}}</td>
                                                        <td>{{ $data->a_stok}}/{{ $data->stok}}</td>
                                                        <td class="align-middle">
                                                            @if(!is_null($data->foto_bef))
                                                            <a href="{{asset('public/uploads/APD/')}}/{{$data->foto_bef}}" target="_blank">
                                                                <img src="{{asset('public/uploads/APD/')}}/{{$data->foto_bef}}" class="img-responsive" alt="logo" width="80">
                                                            </a>
                                                            @else
                                                            <img src="{{asset('public/images')}}/{{'preview-icon.png'}}" class="img-responsive" alt="logo" width="80">
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                            <button type="button" class="btn btn-icon icon-left btn-info"
                                                                onclick="loadModal(this)" target="/stockAPD/editMaster?id=" data="id={{$data->id}}">
                                                                    <i class="fas fa-edit"></i>
                                                                </button>
                                                                <button type="button" class="btn btn-icon icon-left btn-danger"
                                                              onclick="deleteMaster({{$data->id}})">
                                                                  <i class="fas fa-trash"></i>
                                                              </button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                              </div>

                              <div class="tab-pane fade show" id="tabs-icons-text-10" role="tabpanel" aria-labelledby="tabs-icons-text-10-tab">
                                <div class="">
                                    <h5>Data Stock : Lain-Lain</h5>
                                    <div class="">
                                        <div class="table-responsive">
                                            <table class="table table-striped" id="table-10" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">No</th>
                                                        <th>Code</th>
                                                        <th>Name</th>
                                                        <th>Merk</th>
                                                        <th>Size</th>
                                                        <th>Stock</th>
                                                        <th>Available</th>
                                                        <th>Image</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($lainlain as $i => $data)
                                                    <tr>
                                                        <td class="text-center">{{ $i+1 }}</td>
                                                        <td>{{ $data->kode}}</td>
                                                        <td>{{ $data->nama}}</td>
                                                        <td>{{ $data->merk}}</td>
                                                        <td>{{ $data->size}}</td>
                                                        <td>{{ $data->stok}}</td>
                                                        <td>{{ $data->a_stok}}/{{ $data->stok}}</td>
                                                        <td class="align-middle">
                                                            @if(!is_null($data->foto_bef))
                                                            <a href="{{asset('public/uploads/APD/')}}/{{$data->foto_bef}}" target="_blank">
                                                                <img src="{{asset('public/uploads/APD/')}}/{{$data->foto_bef}}" class="img-responsive" alt="logo" width="80">
                                                            </a>
                                                            @else
                                                            <img src="{{asset('public/images')}}/{{'preview-icon.png'}}" class="img-responsive" alt="logo" width="80">
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <div class="btn-group mb-2" role="group" aria-label="Basic example">
                                                            <button type="button" class="btn btn-icon icon-left btn-info"
                                                                onclick="loadModal(this)" target="/stockAPD/editMaster?id=" data="id={{$data->id}}">
                                                                    <i class="fas fa-edit"></i>
                                                                </button>
                                                                <button type="button" class="btn btn-icon icon-left btn-danger"
                                                              onclick="deleteMaster({{$data->id}})">
                                                                  <i class="fas fa-trash"></i>
                                                              </button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                              </div>
                          </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@section('scripts')
    <script>
        function deleteMaster(id) {
            var data = new FormData();
            data.append('id', id);

            modalConfirm("", "Do you want to remove this data ?", function () {
               ajaxTransfer("/stockAPD/deleteMaster", data, "#modal-output");
            })
        }


        var table = $('#table-1');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            // order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });

        var table = $('#table-2');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            // order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });

        var table = $('#table-3');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            // order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });

        var table = $('#table-4');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            // order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });

        var table = $('#table-5');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            // order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });

        var table = $('#table-6');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            // order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });

        var table = $('#table-7');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            // order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });

        var table = $('#table-8');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            // order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });

        var table = $('#table-9');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            // order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });

        var table = $('#table-10');
        table.dataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            columnDefs: [
                {"targets": 0, "orderable": false},
                // {"targets": 1, "visible": false, "searchable": false},
            ],
            // order: [[1, "asc"]],
            buttons: [
                {extend: 'copy'},
                {extend: 'csv', title: 'Tipe Fasilitas'},
                {extend: 'excel', title: 'Tipe Fasilitas'},
                {extend: 'pdf', title: 'Tipe Fasilitas'},
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]
        });
    </script>
@endsection

@endsection
