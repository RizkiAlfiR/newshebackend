<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-konten" class='form-horizontal'>
    <table width="100%">
            <tbody>
                <tr>
                  <td class="text-center">
                    <img src="public/assets/img/logosemen.png" class="img-responsive" alt="logo" width="80">
                  </td>
                </tr>
                <tr>
                  <td class="text-center">
                    <h6 class="modal-title" id="title">List Detail Request APD</h6>
                    <h4 class="modal-title" id="title"></h4><small class="font-bold">PT. Semen Indonesia</small>
                  </td>
                </tr>
            </tbody>
        </table>
        <hr>

          <div class="form-group">
              @foreach($orderAPD as $id => $data)
              <div class="row go_form">
                <div class="col-md-2" style="margin-top: 10px">
                    <label>Tanggal request</label>
                </div>
                <div class="col-md-10" style="margin-top: 10px">
                    <label>: {{ date('d-m-Y',strtotime($data->create_at)) }}</label>
                </div>
              </div>
              <div class="row go_form">
                <div class="col-md-2" style="margin-top: 10px">
                    <label>Kode Order</label>
                </div>
                <div class="col-md-10" style="margin-top: 10px">
                    <label>: {{ $data->kode_order }}</label>
                </div>
              </div>
              <div class="row go_form">
                <div class="col-md-2" style="margin-top: 10px">
                    <label>No. Badge</label>
                </div>
                <div class="col-md-10" style="margin-top: 10px">
                    <label>: {{ $data->no_badge }}</label>
                </div>
              </div>
              <div class="row go_form">
                <div class="col-md-2" style="margin-top: 10px">
                    <label>Nama</label>
                </div>
                <div class="col-md-10" style="margin-top: 10px">
                    <label>: {{ $data->name }}</label>
                </div>
              </div>
              <div class="row go_form">
                <div class="col-md-2" style="margin-top: 10px">
                    <label>Unit Kerja</label>
                </div>
                <div class="col-md-10" style="margin-top: 10px">
                    <label>: {{ $data->uk_text }}</label>
                </div>
              </div>

              <div class="">
                    <div class="table-responsive">
                        <table class="table table-striped" id="table-1">
                            <thead>
                                <tr>
                                    <th class="text-center">No</th>
                                    <th>APD Name</th>
                                    <th>Merk</th>
                                    <th>Quantity</th>
                                    <th>Note</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($detailOrder as $i => $dataDetail)
                                <tr>
                                    <td>{{ $i+1 }}</td>
                                    <td>{{ $dataDetail->nama_apd }}</td>
                                    <td>{{ $dataDetail->merk}}</td>
                                    <td>{{ $dataDetail->jumlah}}</td>
                                    <td>{{ $dataDetail->keterangan}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
              @endforeach
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            <button type="submit" class="btn btn-info" data-loading-text="<i class='fa fa-spinner fa-spin'></i>
              &nbsp;<font class='lowercase'></font>"> <i class="fa fa-check"></i> Release</button>
        </div>

    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/releaseAPD/release', data, '#result-form-konten');
        })
    })
</script>