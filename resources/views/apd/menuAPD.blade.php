@extends('layouts.app')

@section('content')
<title>APD - Safety Hygiene Environment System</title>
<section class="section">
    <div class="section-body">
        <div class="row">
          <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
              <div class="card-icon bg-warning">
                <i class="far fa-file"></i>
              </div>
              <div class="card-wrap">
                <div class="card-header">
                  <h4>Release APD</h4>
                </div>
                <div class="card-body">
                  {{$jumlahRelease}}
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
              <div class="card-icon bg-danger">
                <i class="fas fa-reply"></i>
              </div>
              <div class="card-wrap">
                <div class="card-header">
                  <h4>Pengembalian APD</h4>
                </div>
                <div class="card-body">
                  {{$jumlahReturn}}
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
              <div class="card-icon bg-success">
                <i class="far fa-calendar"></i>
              </div>
              <div class="card-wrap">
                <div class="card-header">
                  <h4>Pengembalian APD Terlambat</h4>
                </div>
                <div class="card-body">
                  {{$jumlahReturnTelat}}
                </div>
              </div>
            </div>
          </div>
        </div>
          <div clas="row">
            <div class="col-12">
                <div class="card">
                    <center style="padding-top:50px">
                      <img src="public/assets/img/iconAPD.png" class="img-responsive" alt="logo" width="150">
                    </center>
                    <center>
                      <h5 style="padding-top: 10px">Alat Pelindung Diri</h5>
                    </center>

                    <div class="row" style="padding-top:30px; margin-left:0px; margin-right:0px">
                      <div class="col-lg-6 col-xs-12" id="listsatu">
                        <center>
                        <a href="{{ url('/stockAPD') }}" id="apd" class="btn btn-icon icon-left btn-primary" style="width:100%">
                          <div class="col-lg-12">
                            <div class="widget style1">
                              <div class="row vertical-align">
                                <div class="col-xs-3" style="padding-right: 15px">
                                  <i class="fa fa-database fa-3x"></i>
                                </div>
                                <div class="col-xs-9 text-right" style="padding-left: 0px;">
                                  <h3 class="font-bold">STOCK APD</h3>
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                        </center>
                        <center>
                        <a href="{{ url('/releaseAPD') }}" id="apd" class="btn btn-icon icon-left btn-primary" style="margin-top:10px; width:100%">
                          <div class="col-lg-12">
                            <div class="widget style1">
                              <div class="row vertical-align">
                                <div class="col-xs-3" style="padding-right: 15px">
                                  <i class="fa fa-paper-plane fa-3x"></i>
                                </div>
                                <div class="col-xs-9 text-right" style="padding-left: 0px;">
                                  <h3 class="font-bold">RELEASE APD</h3>
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                        </center>
                        <center>
                        <a href="{{ url('/orderAPD/jumlahOrderView') }}" id="apd" class="btn btn-icon icon-left btn-primary" style="margin-top:10px; width:100%">
                          <div class="col-lg-12">
                            <div class="widget style1">
                              <div class="row vertical-align">
                                <div class="col-xs-3" style="padding-right: 15px">
                                  <i class="fa fa-file fa-3x"></i>
                                </div>
                                <div class="col-xs-9 text-right" style="padding-left: 0px;">
                                  <h3 class="font-bold">REPORT ORDER APD</h3>
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                        </center>
                      </div>

                      <div class="col-lg-6 col-xs-12" id="listdua">
                        <center>
                        <a href="{{ url('/returnAPD') }}" id="apd" class="btn btn-icon icon-left btn-primary" style="width:100%">
                          <div class="col-lg-12">
                            <div class="widget style1">
                              <div class="row vertical-align">
                                <div class="col-xs-3" style="padding-right: 15px">
                                  <i class="fa fa-reply fa-3x"></i>
                                </div>
                                <div class="col-xs-9 text-right" style="padding-left: 0px;">
                                  <h3 class="font-bold">RETURN APD</h3>
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                        </center>
                        <center>
                        <a href="{{ url('/pengaduanAPD') }}" id="apd" class="btn btn-icon icon-left btn-primary" style="margin-top:10px; width:100%">
                          <div class="col-lg-12">
                            <div class="widget style1">
                              <div class="row vertical-align">
                                <div class="col-xs-3" style="padding-right: 15px">
                                  <i class="fa fa-exclamation-circle fa-3x"></i>
                                </div>
                                <div class="col-xs-9 text-right" style="padding-left: 0px;">
                                  <h3 class="font-bold">PENGADUAN APD</h3>
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                        </center>
                        <center>
                        <a href="{{ url('/stockAPD/percentageView') }}" id="apd" class="btn btn-icon icon-left btn-primary" style="margin-top:10px; width:100%">
                          <div class="col-lg-12">
                            <div class="widget style1">
                              <div class="row vertical-align">
                                <div class="col-xs-3" style="padding-right: 15px">
                                  <i class="fa fa-chart-pie fa-3x"></i>
                                </div>
                                <div class="col-xs-9 text-right" style="padding-left: 0px;">
                                  <h3 class="font-bold">REPORT STOCK APD</h3>
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                        </center>
                    </div>

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped" id="table-1">

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
