<div id="result-form-konten"></div>
<form onsubmit="return false;" id="form-konten" class='form-horizontal'>
    <div class="card">
        <div class="card-body">
            <!-- <div class="form-group">
                <label>Tanggal 2</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <i class="fas fa-calendar"></i>
                        </div>
                    </div>
                    <input type="date" class="form-control daterange-cus" name="date_activity" value="{{$data->date_activity}}">
                </div>
            </div> -->
            <div class="form-group">
              <label>APD Name</label>
              <select name="shift" class="form-control">
                  @foreach($kode_apd as $item)
                  <option value="{{ $item }}" @if(!is_null($data)) @if($item == $data->kode_apd) selected="selected" @endif @endif>Shift {{ $item}}</option>
                  @endforeach
              </select>
            </div>
            <div class="form-group">
              <label>Status PIC</label>
              <form>
                <br>
                <input type="radio" name="status_pic" value="V" checked>PIC
                <br>
                <input type="radio" name="status_pic" value="X">Non PIC
              </form>
            </div>
            <div class="form-group">
              <label>Officer</label>
              <div class="control-group">
                <label class="control-label">Activity Image</label>
                <div class="controls">
                  <input type="file" name="file_documentation" id="file_documentation">
                </div>
              </div>
            </div>
            <div class="form-group">
                <input type="submit" value="Simpan Data">
            </div>
        </div>
    </div>

    <input type='hidden' name='id' value='{{ $data->id }}'>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'>
</form>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/dailySectionActivity/saveActivity', data, '#result-form-konten');
        })
    })
</script>
