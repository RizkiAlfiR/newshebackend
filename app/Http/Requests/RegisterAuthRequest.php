<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterAuthRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'role_id' => 'required|integer',
            'email' => 'required|email|unique:users',
            'username' => 'required|string|unique:users',
            'password' => 'required|string|min:6|max:30',
            'name' => 'required|string',
            'company' => 'required|string',
            'plant' => 'required|string',
            'ad' => 'required|string',
            'unit_kerja' => 'required|string',
            'uk_kode' => 'required|string',
            'no_badge' => 'required|string',
            'cost_center' => 'required|string',
            'cc_text' => 'required|string',
            'position' => 'required|string',
            'pos_text' => 'required|string',
            'is_k3' => 'required|integer',
        ];
    }
}
