<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Auth;
use PDF;
use App\Models\Safety\DailyReportK3;
use App\Models\Safety\DailyReportK3Detail;
use App\Models\GlobalList\InspectionArea;
use App\Models\GlobalList\PPlant;
use App\Models\GlobalList\Employee;
use App\Models\GlobalList\UnitKerja;
use App\Models\GlobalList\Vendor;
use App\Models\GlobalList\ReportCategory;


class RecapitulationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $jumlahReport = DailyReportK3Detail::count();
        $jumlahReportClose = DailyReportK3Detail::where(['status' => 'CLOSE'])->count();
        $data = DailyReportK3Detail::where(['status' => 'CLOSE'])->get();

        $params = [
            'jumlahReport' => $jumlahReport,
            'jumlahReportClose' => $jumlahReportClose,
            'data' => $data
        ];

        return view('safety.recapitulation.index', $params);
    }

    public function show(Request $request)
    {
        $id = intval($request->input('id',0));
        $data_uk = UnitKerja::all();
        $data_vendor = Vendor::all();
        $data_report = ReportCategory::all();

        if($id) {
            $data = DailyReportK3Detail::find($id);
        } else
        {
            $data = new DailyReportK3Detail();
        }
        $params =[
            'title' => 'Manajemen Daily Report K3 Details',
            'data_uk' => $data_uk,
            'data_vendor' => $data_vendor,
            'data_report' => $data_report,
            'data' => $data,
        ];
        return view('safety.recapitulation.show', $params);
    }

    // public function pdfview(Request $request)
    // {
    //     $id = intval($request->input('id',0));
    //     $data_uk = UnitKerja::all();
    //     $data_vendor = Vendor::all();
    //     $data_report = ReportCategory::all();

    //     $data = DailyReportK3Detail::find($id);
        
    //     $params =[
    //         'title' => 'Manajemen Daily Report K3 Details',
    //         'data_uk' => $data_uk,
    //         'data_vendor' => $data_vendor,
    //         'data_report' => $data_report,
    //         // 'foto_bef' => public_path(). '/uploads/public/' .$data->foto_bef,
    //         // 'foto_aft' => public_path(). '/uploads/public/' .$data->foto_aft,
    //         'data' => $data,
    //     ];

    //     view()->share('data',$data);


    //     if($request->has('download')){
    //         $pdf = PDF::loadView('pdfview');
    //         return $pdf->download('pdfview.pdf');
    //     }

    //     return view('safety.recapitulation.pdfview', $params);
    // }
    public function pdfview()
    {
        // $id = intval($request->input('id',0));
        $data_uk = UnitKerja::all();
        $data_vendor = Vendor::all();
        $data_report = ReportCategory::all();
        $data = DailyReportK3Detail::all();

        // if($id) {
        //     $data = DailyReportK3Detail::find($id);
        // } else
        // {
        //     $data = new DailyReportK3Detail();
        // }
        $params =[
            'title' => 'Manajemen Daily Report K3 Details',
            'foto_bef' => public_path(). '/uploads/public/' .$data->foto_bef,
            'foto_aft' => public_path(). '/uploads/public/' .$data->foto_aft,
            'data_uk' => $data_uk,
            'data_vendor' => $data_vendor,
            'data_report' => $data_report,
            'data' => $data,
        ];

        $pdf = PDF::loadview('safety.recapitulation.show', $params);
        return $pdf->download('recapitulation-pdf');
    }

    public function chart()
    {
        $monthlyTransaction = [];
        for ($i = 1; $i <= 12; $i++){
            $monthIsOpen  = DailyReportK3Detail::whereMonth('created_at', $this->generateMonth($i))
                ->where('status', 'OPEN')
                // ->where('user_id',$userId)
                ->count();
            $monthIsClose  = DailyReportK3Detail::whereMonth('created_at', $this->generateMonth($i))
                ->where('status', 'CLOSE')
                // ->where('user_id',$userId)
                ->count();

            $monthlyTransaction[] = [
                'valueIsOpen' =>$monthIsOpen,
                'valueIsClose'=>$monthIsClose
            ];
        }
        $isOpen = [];
        $isClose = [];
        foreach ($monthlyTransaction as $item){
            $isOpen[] = $item['valueIsOpen'];
            $isClose[] = $item['valueIsClose'];
        }

        $params = [
            'isOpen' => $isOpen,
            'isClose' => $isClose,
            'data' => $monthlyTransaction
        ];

        return response()->json($params);
    }

    private function generateMonth($idx)
    {
        if(strlen($idx) < 2)
        {
            $idx = '0'.$idx;
        }
        return $idx;
    }

    public function persentage()
    {
        $masterUnsafe = DailyReportK3Detail::select('unsafe_type')->groupby('unsafe_type')->get();
        $currentData = [];
        foreach($masterUnsafe as $item){
            $sumType = DailyReportK3Detail::where('unsafe_type', $item->unsafe_type)->count();
            $currentData[] = [
                'name' => $item->unsafe_type,
                'y' => (int)$sumType
            ];
        }

        $params =[
            'data' => $currentData
        ];

        return response()->json($params);
    }

    public function persentagePlant()
    {
        $masterPlant = DailyReportK3::select('plant_txt')->groupby('plant_txt')->get();
        $currentData = [];
        foreach($masterPlant as $item){
            $sumPlant = DailyReportK3::where('plant_txt', $item->plant_txt)->count();
            $currentData[] = [
                'name' => $item->plant_txt,
                'y' => (int)$sumPlant
            ];
        }

        $params =[
            'data' => $currentData
        ];

        return response()->json($params);
    }
}
