<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\GlobalList\Employee;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

     public function index()
     {
        $roleAdmin=1; $roleManager=2; $roleAdminK3=3; $roleuUser=4;
        $jumlahAdministrator = User::where('role_id', 'LIKE', '%' .$roleAdmin. '%')->count();
        $jumlahManager = User::where('role_id', 'LIKE', '%' .$roleManager. '%')->count();
        $jumlahAdminK3 = User::where('role_id', 'LIKE', '%' .$roleAdminK3. '%')->count();
        $jumlahUser = User::where('role_id', 'LIKE', '%' .$roleuUser. '%')->count();
        $users = User::all();

        $params=[
            'users'=>$users,
            'jumlahAdministrator'=>$jumlahAdministrator,
            'jumlahManager'=>$jumlahManager,
            'jumlahAdminK3'=>$jumlahAdminK3,
            'jumlahUser'=>$jumlahUser,
        ];

        return view('user/index', $params);
    }

    public function form(Request $request)
    {
        $employee = Employee::all();
        $id = $request->input('id');
        if($id){
            $data = User::find($id);
        }else{
            $data = new User();
        }
        $params =[
            'title' =>'Manajemen User',
            'data' =>$data,
            'employee' =>$employee,
        ];
        return view('user/form', $params);
    }

    public function save(Request $request)
    {
        $id = intval($request->input('id',0));
        if($id){
            $data = User::find($id);
        }else{
            $data = new User();
        }

        $noPeg = $request->name;
        $data->role_id = $request->role_id;
        $data->plant = $request->plant;
        $data->company = $request->company;
        $data->ad = '1';
        $data->is_k3 = '1';
        $data->password = bcrypt('semenindonesia');

        $dataEmployee = Employee::find($noPeg);

        $data->email = $dataEmployee->mk_email;
        $data->username = $dataEmployee->mk_nama;
        $data->name = $dataEmployee->mk_nama;

        $data->position = $dataEmployee->mk_employee_emp_subgroup;
        $data->pos_text = $dataEmployee->mk_employee_emp_subgroup_text;
        $data->unit_kerja = $dataEmployee->muk_nama;
        $data->uk_kode = $dataEmployee->muk_short;
        $data->no_badge = $dataEmployee->mk_nopeg;
        $data->cost_center = $dataEmployee->mk_py_area;
        $data->cc_text = $dataEmployee->mk_py_area_text;

        try{
            $data->save();
            return "
            <div class='alert alert-success'>User Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            dd($ex);
            return "<div class='alert alert-danger'>User Failed! User not saved!</div>";
        }
    }

    public function delete(Request $request){

        $id = intval($request->input('id',0));
        try{
            User::find($id)->delete();
            return "
            <div class='alert alert-success'>User Remove Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        }catch(\Exception $ex){
            return "<div class='alert alert-danger'>Remove Failed! User not removed!</div>";
        }

    }


}
