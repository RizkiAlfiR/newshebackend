<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use App\Models\Safety\Sio;
use App\Models\GlobalList\SioGroup;
use App\Models\GlobalList\Employee;
use App\Models\GlobalList\UnitKerja;

class SioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $jumlahSio = Sio::count();
        // $jumlahAccidentOpen = AccidentReport::where(['status_report' => 'OPEN'])->count();

        $params = [
            'jumlahSio' => $jumlahSio,
            // 'jumlahAccidentOpen' => $jumlahAccidentOpen,
        ];

        // mengambil data dari table accident report
        $sios = DB::table('sios')->get();

    	// mengirim data accident ke view index
        return view('safety.sio.index', $params, ['sios' => $sios]);
    }

    public function indexgroup()
    {
        $siogroup=SioGroup::paginate(10);

    	// mengirim data accident ke view index
        return view('siogroup', ['siogroup' => $siogroup]);
    }

    public  function show($id) {
        $data = Sio::where('id', $id)->get();
        $data_karyawan = Employee::all();
        $data_uk = UnitKerja::all();
        $data_group = SioGroup::all();
        $params=[
            'data'=> $data,
            'title'=>'Manajemen Transaksi',
            'data_karyawan' => $data_karyawan,
            'data_uk' => $data_uk,
            'data_group' => $data_group
        ];
        // dd($params);
        return view('safety.sio.detail', $params);
    }

    public function addMaster(Request $request)
    {
        $id = $request->input('id');
        $data_karyawan = Employee::all();
        $data_uk = UnitKerja::all();
        $data_group = SioGroup::all();

        if($id) {
            $data = Sio::find($id);
            //dd($data);
        } else
        {
            $data = new Sio();
        }
        $params =[
            'title' => 'Manajemen Sio Certification',
            'data_karyawan' => $data_karyawan,
            'data_uk' => $data_uk,
            'data_group' => $data_group,
            'data' => $data,
        ];
        return view('safety.sio.formMaster', $params);
    }

    public function saveMaster(Request $request)
    {
        $id = intval($request->input('id', 0));

        if($id) {
            $data = Sio::find($id);
        } else
        {
            $data = new Sio();
            $checkData = Sio::where(['no_lisensi' => $request->no_lisensi])->first();
            if($checkData){
                return "<div class='alert alert-danger'>Data Nomor Lisensi sudah tersedia!</div>";
            }
        }

        $temppeg = explode('|', $request->arrpeg);
        $data->nama = $temppeg[0];
        $data->badge = $temppeg[1];
        $tempuk = explode('|', $request->arruk);
        $data->unit_kerja_txt = $tempuk[0];
        $data->unit_kerja = $tempuk[1];
        $data->no_lisensi = $request->no_lisensi;
        $data->kelas = $request->kelas;
        $data->time_certification = $request->time_certification;
        $data->masa_awal = $request->start_date;
        $data->start_date = $request->start_date;

        if($request->time_certification == 1)
            $end_date = date('Y-m-d', strtotime('+1 Years', strtotime($request->start_date)));
        else if($request->time_certification == 2)
            $end_date = date('Y-m-d', strtotime('+2 Years', strtotime($request->start_date)));
        else if($request->time_certification == 3)
            $end_date = date('Y-m-d', strtotime('+3 Years', strtotime($request->start_date)));
        else if($request->time_certification == 4)
            $end_date = date('Y-m-d', strtotime('+4 Years', strtotime($request->start_date)));
        else
            $end_date = date('Y-m-d', strtotime('+5 Years', strtotime($request->start_date)));
        
        $data->masa_berlaku = $end_date;
        $data->end_date = $end_date;

        if(!is_null($data->foto)){
            if(is_null($request->file('foto')) && $request->file('foto')==''){
                $fileImageFoto = $data->foto;
            }else{
                $rules = [
                    'foto' => 'mimes:jpeg,bmp,png,gif,jpg,pdf',
                ];
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails())
                {
                    return "<div class='alert alert-danger'>Terjadi kesalahan!Format tidak valid!</div>";
                } else
                {
                    $fotoImageFoto = $request->file('foto');
                    $destinationPath = 'public/uploads/sio/'.$request->image_foto_id;
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $fileImageFoto = date('YmdHis') . '_' . $fotoImageFoto->getClientOriginalName();
                    $fotoImageFoto->move($destinationPath, $fileImageFoto);
                }
            }
        }

        if(is_null($data->foto)){
            if(is_null($request->file('foto')) && $request->file('foto')==''){
                $fileImageFoto = $data->foto;
            }else{
                $rules = [
                    'foto' => 'mimes:jpeg,bmp,png,gif,jpg,pdf',
                ];
                $validator=Validator::make($request->all(), $rules);
                if($validator->fails())
                {
                    return "<div class='alert alert-danger'>Terjadi kesalahan!Format tidak valid!</div>";
                } else
                {
                    $fotoImageFoto = $request->file('foto');
                    $destinationPath = 'public/uploads/sio/'.$request->image_foto_id;
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $fileImageFoto = date('YmdHis') . '_' . $fotoImageFoto->getClientOriginalName();
                    $fotoImageFoto->move($destinationPath, $fileImageFoto);
                }
            }
        }
        $data->foto = $fileImageFoto;
        $data->user_id = Auth::id();
        $data->user_name = 'Administrator';
        $data->grup = $request->grup;
        $data->company = 2000;
        $data->plant = 5002;
        
        try {
            $data->save();
            return "
            <div class='alert alert-success'> Add Sio Certification Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Add Sio Certification Failed! Sio Certification not saved!</div>";
        }
    }

    public function showImage(Request $request){
        $id = intval($request->input('id',0));

        if($id) {
            // $data = Sio::where('foto', 'LIKE', '%' .$id. '%')->get();
            $data = Sio::find($id);
            // dd($data);
        } else
        {
            $data = new Sio();
        }
        $params =[
            'title' => 'Manajemen Sio Certification',
            'data' => $data,
        ];
        return view('safety.sio.showImage', $params);
    }

    public function deleteMaster(Request $request){

        $id = intval($request->input('id', 0));
        $data = Sio::find($id);

        try {
            $data->delete();
            return "
            <div class='alert alert-success'>Sio Certification Remove Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch(\Exception $ex){
            return "<div class='alert alert-danger'>Remove Failed! Sio Certification not removed!</div>";
        }
    }

    public function create()
    {
        $data_karyawan = Employee::all();
        $data_uk = UnitKerja::all();
        $data_group = SioGroup::all();
        $params = [
            'title' => 'Manajemen Transaksi',
            'data_karyawan' => $data_karyawan,
            'data_uk' => $data_uk,
            'data_group' => $data_group
        ];
        return view('master.sio.form', $params);
    }

    public function store(Request $request)
    {
        $sio = new Sio();
        // $sio->badge = $request->mk_nopeg;
        // $sio->unit_kerja = $request->mk_nopeg;
        // $sio->no_lisensi = $request->lisensi;
        // $sio->kelas = $request->kelas;
        // $sio->masa_berlaku = date("Y-m-d");

        $filenamefoto = null;
        $foto = $request->file('foto');
        if($foto)
        {
            $destinationPath = 'uploads/sio/';
            $filenamefoto = $foto->getClientOriginalName();
            $foto->move($destinationPath, $filenamefoto);
            $sio->foto = $filenamefoto;
        } else
        {
            $filenamefoto = null;
        }

        // $sio->user_id = Session::get('activeUser')->id;
        // $sio->user_name = "Administrator";
        // $sio->unit_kerja_txt = $request->muk_nama;
        // $sio->grup = $request->group;
        // $sio->company = 5000;
        // $sio->plant = 5002;
        // $sio->nama = $request->mk_nama;
        // $sio->time_certification = $request->masa_berlaku;
        // $sio->masa_awal = $request->tanggal_mulai;
        // $sio->start_date = $request->tanggal_mulai;
        // $sio->end_date = date("Y-m-d");

        $params = [
            'badge' => $request->mk_nopeg,
            'unit_kerja' => $request->unit_kerja,
            'no_lisensi' => $request->lisensi,
            'kelas' => $request->kelas,
            'masa_berlaku' => date("Y-m-d"),
            'foto' => $request->foto,
            'user_id' => Auth::id(),
            'user_name' => "Administrator",
            'unit_kerja_txt' => $request->unit_kerja,
            'grup' => $request->group,
            'company' => 5000,
            'plant' => 5002,
            'nama' => $request->mk_nama,
            'time_certification' => $request->masa_berlaku,
            'masa_awal' => $request->tanggal_mulai,
            'start_date' => $request->tanggal_mulai,
            'end_date' => date("Y-m-d"),
        ];

        try {
            Sio::insert($params);
            // $sio->save();
            return redirect('/sio')->with('success', 'Transaksi has been added');
        } catch (\Exception $ex){
            dd($ex);
            return redirect('/sio')->with('failed', 'Transaksi has not been added');
        }
    }
}
