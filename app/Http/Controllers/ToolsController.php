<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use App\Models\Safety\Tools;
use App\Models\GlobalList\SioGroup;
use App\Models\GlobalList\UnitKerja;
use App\Models\GlobalList\FuncLocation;

class ToolsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $jumlahTools = Tools::count();
        // $jumlahAccidentOpen = AccidentReport::where(['status_report' => 'OPEN'])->count();

        $params = [
            'jumlahTools' => $jumlahTools,
            // 'jumlahAccidentOpen' => $jumlahAccidentOpen,
        ];

        // mengambil data dari table accident report
        $tools = DB::table('tools')->get();

    	// mengirim data accident ke view index
        return view('safety.tool.index', $params, ['tools' => $tools]);
    }

    public  function show($id) {
        $data = Tools::where('id', $id)->get();
        $data_uk = UnitKerja::all();
        $data_group = SioGroup::all();
        $data_funcloc = FuncLocation::all();
        $params=[
            'data'=> $data,
            'title'=>'Manajemen Transaksi',
            'data_uk' => $data_uk,
            'data_group' => $data_group,
            'data_funcloc' => $data_funcloc
        ];
        // dd($params);
        return view('safety.tool.detail', $params);
    }

    public function addMaster(Request $request)
    {
        $id = $request->input('id');
        $data_uk = UnitKerja::all();
        $data_group = SioGroup::all();
        $data_funloc = FuncLocation::all();

        if($id) {
            $data = Tools::find($id);
            //dd($data);
        } else
        {
            $data = new Tools();
        }
        $params =[
            'title' => 'Manajemen Tools Certification',
            'data_uk' => $data_uk,
            'data_group' => $data_group,
            'data_funloc' => $data_funloc,
            'data' => $data,
        ];
        return view('safety.tool.formMaster', $params);
    }

    public function saveMaster(Request $request)
    {
        $id = intval($request->input('id', 0));

        if($id) {
            $data = Tools::find($id);
        } else
        {
            $data = new Tools();
            $checkData = Tools::where(['no_buku' => $request->no_buku])->first();
            if($checkData){
                return "<div class='alert alert-danger'>Data Buku Lisensi sudah tersedia!</div>";
            }
        }

        $data->no_buku = $request->no_buku;
        $tempequpiment = explode('|', $request->arrequipment);
        $data->equipment = $tempequpiment[0];
        $data->kode = $tempequpiment[1];
        $data->data_teknis = $request->data_teknis;
        $data->nomor_pengesahan = $request->nomor_pengesahan;
        $data->lokasi = $request->lokasi;
        $data->user_id = Auth::id();
        $data->user_name = 'Administrator';
        $data->note = $request->note;
        $tempuk = explode('|', $request->arruk);
        $data->uk_kode = $tempuk[1];
        $data->uk_text = $tempuk[0];
        $data->company = 2000;
        $data->plant = 5002;
        $data->file_buku = $request->file_buku;
        $data->file_aktual = $request->file_aktual;
        $data->uji_awal = $request->start_date;
        $data->time_certification = $request->time_certification;
        $data->start_date = $request->start_date;

        if(!is_null($data->file_buku)){
            if(is_null($request->file('file_buku')) && $request->file('file_buku')==''){
                $fileImageBuku = $data->file_buku;
            }else{
                $rules = [
                    'file_buku' => 'mimes:jpeg,bmp,png,gif,jpg,pdf',
                ];
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails())
                {
                    return "<div class='alert alert-danger'>Terjadi kesalahan!Format tidak valid!</div>";
                } else
                {
                    $fotoImageBuku = $request->file('file_buku');
                    $destinationPath = 'public/uploads/tools/'.$request->image_buku_id;
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $fileImageBuku = date('YmdHis') . '_' . $fotoImageBuku->getClientOriginalName();
                    $fotoImageBuku->move($destinationPath, $fileImageBuku);
                }
            }
        }

        if(is_null($data->file_buku)){
            if(is_null($request->file('file_buku')) && $request->file('file_buku')==''){
                $fileImageBuku = $data->file_buku;
            }else{
                $rules = [
                    'file_buku' => 'mimes:jpeg,bmp,png,gif,jpg,pdf',
                ];
                $validator=Validator::make($request->all(), $rules);
                if($validator->fails())
                {
                    return "<div class='alert alert-danger'>Terjadi kesalahan!Format tidak valid!</div>";
                } else
                {
                    $fotoImageBuku = $request->file('file_buku');
                    $destinationPath = 'public/uploads/tools/'.$request->image_buku_id;
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $fileImageBuku = date('YmdHis') . '_' . $fotoImageBuku->getClientOriginalName();
                    $fotoImageBuku->move($destinationPath, $fileImageBuku);
                }
            }
        }

        if(!is_null($data->file_aktual)){
            if(is_null($request->file('file_aktual')) && $request->file('file_aktual')==''){
                $fileImageAktual = $data->file_aktual;
            }else{
                $rules = [
                    'file_aktual' => 'mimes:jpeg,bmp,png,gif,jpg,pdf',
                ];
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails())
                {
                    return "<div class='alert alert-danger'>Terjadi kesalahan!Format tidak valid!</div>";
                } else
                {
                    $fotoImageAktual = $request->file('file_aktual');
                    $destinationPath = 'public/uploads/tools/'.$request->image_aktual_id;
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $fileImageAktual = date('YmdHis') . '_' . $fotoImageAktual->getClientOriginalName();
                    $fotoImageAktual->move($destinationPath, $fileImageAktual);
                }
            }
        }

        if(is_null($data->file_aktual)){
            if(is_null($request->file('file_aktual')) && $request->file('file_aktual')==''){
                $fileImageAktual = $data->file_aktual;
            }else{
                $rules = [
                    'file_aktual' => 'mimes:jpeg,bmp,png,gif,jpg,pdf',
                ];
                $validator=Validator::make($request->all(), $rules);
                if($validator->fails())
                {
                    return "<div class='alert alert-danger'>Terjadi kesalahan!Format tidak valid!</div>";
                } else
                {
                    $fotoImageAktual = $request->file('file_aktual');
                    $destinationPath = 'public/uploads/tools/'.$request->image_aktual_id;
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $fileImageAktual = date('YmdHis') . '_' . $fotoImageAktual->getClientOriginalName();
                    $fotoImageAktual->move($destinationPath, $fileImageAktual);
                }
            }
        }
        
        if($request->time_certification == 1)
            $end_date = date('Y-m-d', strtotime('+1 Years', strtotime($request->start_date)));
        else if($request->time_certification == 2)
            $end_date = date('Y-m-d', strtotime('+2 Years', strtotime($request->start_date)));
        else if($request->time_certification == 3)
            $end_date = date('Y-m-d', strtotime('+3 Years', strtotime($request->start_date)));
        else if($request->time_certification == 4)
            $end_date = date('Y-m-d', strtotime('+4 Years', strtotime($request->start_date)));
        else
            $end_date = date('Y-m-d', strtotime('+5 Years', strtotime($request->start_date)));
        
        $data->file_buku = $fileImageBuku;
        $data->file_aktual = $fileImageAktual;
        $data->uji_ulang = $end_date;
        $data->end_date = $end_date;
            
        try {
            $data->save();
            return "
            <div class='alert alert-success'> Add Tools Certification Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            dd($ex);
            return "<div class='alert alert-danger'>Add Tools Certification Failed! Tools Certification not saved!</div>";
        }
    }

    public function showImage(Request $request){
        $id = intval($request->input('id',0));

        if($id) {
            // $data = Sio::where('foto', 'LIKE', '%' .$id. '%')->get();
            $data = Tools::find($id);
            // dd($data);
        } else
        {
            $data = new Tools();
        }
        $params =[
            'title' => 'Manajemen Tools Certification',
            'data' => $data,
        ];
        return view('safety.tool.showImage', $params);
    }

    public function deleteMaster(Request $request){

        $id = intval($request->input('id', 0));
        $data = Tools::find($id);

        try {
            $data->delete();
            return "
            <div class='alert alert-success'>Tools Certification Remove Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch(\Exception $ex){
            return "<div class='alert alert-danger'>Remove Failed! Tools Certification not removed!</div>";
        }
    }
}
