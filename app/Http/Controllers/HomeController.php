<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Safety\AccidentReport;
use App\User;
use App\Models\Safety\Tools;
use App\Models\Safety\Sio;
use App\Models\Safety\DailyReportK3Detail;
use App\Models\APD\PinjamAPD;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $this->cekTelat();
        $jumlahUser = User::count();

        $params = [
            'jumlahUser' => $jumlahUser,
        ];

        return view('home', $params);
    }

    // public function accident()
    // {
    //     // mengambil data dari table accident report
    //     $accidentreport = DB::table('accident_reports')->get();
        
    // 	// mengirim data accident ke view index
    //     // return view('accidentreport', ['accident_reports' => $accidentreport]);
    //     return view ('accidentreport');
    // }

    private function cekTelat()
    {
        $data=PinjamAPD::where('cek_status',1)->get();
        foreach($data as $key => $item)
        {
            if(date('Y-m-d H:i:s')>$item->retur_date){
                $check=PinjamAPD::where('id',$item->id)->first();
                $check->cek_status=2;
                $check->save();
            }
        }
        
    }
}
