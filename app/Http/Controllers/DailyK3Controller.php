<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Auth;
use App\Models\Safety\DailyReportK3;
use App\Models\Safety\DailyReportK3Detail;
use App\Models\Safety\ListFileUpload;
use App\Models\GlobalList\InspectionArea;
use App\Models\GlobalList\PPlant;
use App\Models\GlobalList\Employee;
use App\Models\GlobalList\UnitKerja;
use App\Models\GlobalList\Vendor;
use App\Models\GlobalList\ReportCategory;

class DailyK3Controller extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $jumlahReport = DailyReportK3Detail::count();
        $jumlahReportOpen = DailyReportK3Detail::where(['status' => 'OPEN'])->count();
        $jumlahReportClose = DailyReportK3Detail::where(['status' => 'CLOSE'])->count();

        $params = [
            'jumlahReport' => $jumlahReport,
            'jumlahReportOpen' => $jumlahReportOpen,
            'jumlahReportClose' => $jumlahReportClose
        ];

        // mengambil data dari table accident report
        $dailyk3 = DB::table('daily_report_k3_s')->get();

    	// mengirim data accident ke view index
        return view('safety.dailyreportk3.index', $params, ['dailyk3' => $dailyk3]);
    }

    public  function show($id_unsafe) {
        $data = DailyReportK3Detail::where('id_unsafe_report', $id_unsafe)->get();
        $params=[
            'data'=> $data,
            'title'=>'Manajemen Transaksi',
        ];
        // dd($params);
        return view('safety.dailyreportk3.headerdetail', $params);
    }

    public function addMaster(Request $request)
    {
        $id = $request->input('id');
        $data_area = InspectionArea::all();
        $data_pplant = PPlant::all();
        $data_pegawai = Employee::all();

        if($id) {
            $data = DailyReportK3::find($id);
        } else
        {
            $data = new DailyReportK3();
        }
        $params =[
            'title' => 'Manajemen Header Daily Report K3',
            'data_area' => $data_area,
            'data_pplant' => $data_pplant,
            'data_pegawai' => $data_pegawai,
            'data' => $data,
        ];
        return view('safety.dailyreportk3.formMasterHeader', $params);
    }

    public function saveMaster(Request $request)
    {
        $id = intval($request->input('id', 0));

        if($id) {
            $data = DailyReportK3::find($id);
        } else
        {
            $data = new DailyReportK3();
            $checkData = DailyReportK3::where(['area_txt' => $request->area_txt])->first();
            if($checkData){
                return "<div class='alert alert-danger'>Data Area sudah tersedia!</div>";
            }
        }

        $data->report_date = $request->report_date;
        $data->shift = $request->shift;
        $temparea = explode('|', $request->arrarea);
        $data->area_code = $temparea[1];
        $data->area_txt = $temparea[0];
        $data->sub_area_code = null;
        $data->sub_area_txt = $request->sub_area_txt;
        $temppplant = explode('|', $request->arrpplant);
        $data->plant_code = $temppplant[1];
        $data->plant_txt = $temppplant[0];
        $temppeg = explode('|', $request->arrpeg);
        $data->inspector_code = $temppeg[1];
        $data->inspector_name = $temppeg[0];
        $data->user_id = Auth::id();
        $data->is_smig = $request->is_smig;
        $data->accident = $request->accident;
        $data->incident = $request->incident;
        $data->nearmiss = $request->nearmiss;
        $data->close_date = null;
        $data->close_by = null;
        
        try {
            $data->save();
            return "
            <div class='alert alert-success'> Add Header Daily Report K3 Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Add Header Daily Report K3 Failed! Header Daily Report K3 not saved!</div>";
        }
    }

    public function deleteMaster(Request $request){

        $id = intval($request->input('id', 0));
        $data = DailyReportK3::find($id);

        try {
            $data->delete();
            return "
            <div class='alert alert-success'>Header Daily Report K3 Remove Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch(\Exception $ex){
            return "<div class='alert alert-danger'>Remove Failed! Header Daily Report K3 not removed!</div>";
        }
    }

    public function showImage(Request $request){
        $id = intval($request->input('id',0));

        if($id) {
            $data = DailyReportK3Detail::find($id);
        } else
        {
            $data = new DailyReportK3Detail();
        }
        $params =[
            'title' => 'Manajemen Daily Report K3 Details',
            'data' => $data,
        ];
        return view('safety.dailyreportk3.showImage', $params);
    }

    public function addFollowUp(Request $request)
    {
        $id = $request->input('id');

        if($id) {
            $data = DailyReportK3Detail::find($id);
        } else
        {
            $data = new DailyReportK3Detail();
        }
        $params =[
            'title' => 'Manajemen Daily Report K3 Detail',
            'data' => $data,
        ];
        return view('safety.dailyreportk3.formFollowUp', $params);
    }

    public function saveFollowUp(Request $request)
    {
        $id = intval($request->input('id', 0));

        if($id) {
            $data = DailyReportK3Detail::find($id);
        } else
        {
            $data = new DailyReportK3Detail();
        }

        $data->status = $request->status;
        $data->comitment_date = $request->comitment_date;
        $data->tindak_lanjut = $request->tindak_lanjut;

        if(!is_null($data->foto_aft)){
            if(is_null($request->file('foto_aft')) && $request->file('foto_aft')==''){
                $fileImageAft = $data->foto_aft;
            }else{
                $rules = [
                    'foto_aft' => 'mimes:jpeg,bmp,png,gif,jpg,pdf',
                ];
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails())
                {
                    return "<div class='alert alert-danger'>Terjadi kesalahan!Format tidak valid!</div>";
                } else
                {
                    $fotoImageAft = $request->file('foto_aft');
                    $destinationPath = 'public/uploads/unsafe/'.$request->image_aft_id;
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $fileImageAft = date('YmdHis') . '_' . $fotoImageAft->getClientOriginalName();
                    $fotoImageAft->move($destinationPath, $fileImageAft);
                }
            }
        }

        if(is_null($data->foto_aft)){
            if(is_null($request->file('foto_aft')) && $request->file('foto_aft')==''){
                $fileImageAft = $data->foto_aft;
            }else{
                $rules = [
                    'foto_aft' => 'mimes:jpeg,bmp,png,gif,jpg,pdf',
                ];
                $validator=Validator::make($request->all(), $rules);
                if($validator->fails())
                {
                    return "<div class='alert alert-danger'>Terjadi kesalahan!Format tidak valid!</div>";
                } else
                {
                    $fotoImageAft = $request->file('foto_aft');
                    $destinationPath = 'public/uploads/unsafe/'.$request->image_aft_id;
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $fileImageAft = date('YmdHis') . '_' . $fotoImageAft->getClientOriginalName();
                    $fotoImageAft->move($destinationPath, $fileImageAft);
                }
            }
        }
        $data->foto_aft = $fileImageAft;
        
        try {
            $data->update();
            return "
            <div class='alert alert-success'> Add Follow Up Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            // return "<div class='alert alert-danger'>Add Follow Up Failed! Follow Up not saved!</div>";
            dd($ex);
        }
    }

    public function addMasterDetail(Request $request)
    {
        $id = $request->input('id');
        $data_uk = UnitKerja::all();
        $data_vendor = Vendor::all();
        $data_report = ReportCategory::all();

        if($id) {
            $data = DailyReportK3Detail::find($id);
        } else
        {
            $data = new DailyReportK3Detail();
        }
        $params =[
            'title' => 'Manajemen Detail Daily Report K3',
            'data_uk' => $data_uk,
            'data_vendor' => $data_vendor,
            'data_report' => $data_report,
            'data' => $data,
        ];
        return view('safety.dailyreportk3.formMasterDetail', $params);
    }

    public function saveMasterDetail(Request $request)
    {
        $id = intval($request->input('id', 0));

        if($id) {
            $data = DailyReportK3Detail::find($id);
        } else
        {
            $data = new DailyReportK3Detail();
            $checkData = DailyReportK3Detail::where(['area_txt' => $request->area_txt])->first();
            if($checkData){
                return "<div class='alert alert-danger'>Data Area sudah tersedia!</div>";
            }
        }

        $data->comitment_date = $request->comitment_date;
        $data->status = $request->status;
        $data->unsafe_type = $request->unsafe_type;
        $data->o_clock_incident = $request->o_clock_incident;
        $data->is_smig = $request->is_smig;
        $tempuk = explode('|', $request->arruk);
        $data->unit_code = $tempuk[1];
        $data->unit_name = $tempuk[0];
        $tempreport = explode('|', $request->arrreport);
        $data->id_category = $tempreport[1];
        $data->deskripsi = $tempreport[0];
        $data->kode_en = $tempreport[2];
        $data->activity_description = $request->activity_description;
        $data->potential_hazard = $request->potential_hazard;
        $data->trouble_maker = $request->trouble_maker;
        $data->recomendation = $request->recomendation;
        $data->follow_up = $request->follow_up;

        if(!is_null($data->foto_bef)){
            if(is_null($request->file('foto_bef')) && $request->file('foto_bef')==''){
                $fileImageFotoBef = $data->foto_bef;
            }else{
                $rules = [
                    'foto_bef' => 'mimes:jpeg,bmp,png,gif,jpg,pdf',
                ];
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails())
                {
                    return "<div class='alert alert-danger'>Terjadi kesalahan!Format tidak valid!</div>";
                } else
                {
                    $fotoImageFotoBef = $request->file('foto_bef');
                    $destinationPath = 'public/uploads/unsafe/'.$request->image_fotobef_id;
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $fileImageFotoBef = date('YmdHis') . '_' . $fotoImageFotoBef->getClientOriginalName();
                    $fotoImageFotoBef->move($destinationPath, $fileImageFotoBef);
                }
            }
        }

        if(is_null($data->foto_bef)){
            if(is_null($request->file('foto_bef')) && $request->file('foto_bef')==''){
                $fileImageFotoBef = $data->foto_bef;
            }else{
                $rules = [
                    'foto_bef' => 'mimes:jpeg,bmp,png,gif,jpg,pdf',
                ];
                $validator=Validator::make($request->all(), $rules);
                if($validator->fails())
                {
                    return "<div class='alert alert-danger'>Terjadi kesalahan!Format tidak valid!</div>";
                } else
                {
                    $fotoImageFotoBef = $request->file('foto_bef');
                    $destinationPath = 'public/uploads/unsafe/'.$request->image_fotobef_id;
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $fileImageFotoBef = date('YmdHis') . '_' . $fotoImageFotoBef->getClientOriginalName();
                    $fotoImageFotoBef->move($destinationPath, $fileImageFotoBef);
                }
            }
        }
        $data->foto_bef = $fileImageFotoBef;
        
        try {
            $data->save();
            return "
            <div class='alert alert-success'> Add Detail Daily Report K3 Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Add Detail Daily Report K3 Failed! Detail Daily Report K3 not saved!</div>";
        }
    }

    public function deleteMasterDetail(Request $request){

        $id = intval($request->input('id', 0));
        $data = DailyReportK3Detail::find($id);

        try {
            $data->delete();
            return "
            <div class='alert alert-success'>Detail Daily Report K3 Remove Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch(\Exception $ex){
            return "<div class='alert alert-danger'>Remove Failed! Detail Daily Report K3 not removed!</div>";
        }
    }
}
