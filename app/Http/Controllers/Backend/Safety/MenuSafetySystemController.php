<?php

namespace App\Http\Controllers\Backend\Safety;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Safety\AccidentReport;
use App\User;
use App\Models\Safety\Tools;
use App\Models\Safety\Sio;
use App\Models\Safety\DailyReportK3Detail;

class MenuSafetySystemController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $jumlahUser = User::count();
        $jumlahAccident = AccidentReport::count();
        $jumlahTools = Tools::count();
        $jumlahSio = Sio::count();
        $jumlahSafetyOfficer = DailyReportK3Detail::count();
        $jumlahCertification = $jumlahTools + $jumlahSio;
        $jumlahReports = $jumlahAccident + $jumlahCertification + $jumlahSafetyOfficer;

        $params = [
            'jumlahAccident' => $jumlahAccident,
            'jumlahUser' => $jumlahUser,
            'jumlahCertification' => $jumlahCertification,
            'jumlahSafetyOfficer' => $jumlahSafetyOfficer,
            'jumlahReports' => $jumlahReports
        ];

        return view('safety.menuSafety', $params);
    }
}
