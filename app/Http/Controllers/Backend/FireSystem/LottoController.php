<?php

namespace App\Http\Controllers\Backend\FireSystem;

use Illuminate\Http\Request;
use App\Models\FireSystem\Lotto\Lotto;
use App\Models\GlobalList\Employee;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;

class LottoController  extends Controller
{
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $lotto=Lotto::all();
        $params=[
             'lotto'=>$lotto,
             'title'=>'List Lotto'
        ];

        return view('firesystem/lotto/index', $params);
    }

    public function form(Request $request)
    {
        $id = $request->input('id');
        if($id){
            $data = Lotto::find($id);
        }else{
            $data = new Lotto();
        }
        $params =[
            'title' =>'Manajemen Lotto',
            'data' =>$data,
        ];
        return view('firesystem/lotto/form', $params);
    }

    public function save(Request $request)
    {
        $id = intval($request->input('id',0));
        if($id){
            $data = Lotto::find($id);
        }else{
            $data = new Lotto();
        }

        $data->equipment_number = $request->equipment_number;
        $data->location = $request->location;
        $data->no_er = $request->no_er;
        $data->activity_description = $request->activity_description;
        $data->date = $request->date;
        $data->time = $request->time;
        $data->note = $request->note;
        $data->safety_key = $request->safety_key;
        $data->safety_tools = "['APD']";
        $data->status = 'OPEN';

        if($id){
          $data->k3_drawin_badge=$data->k3_drawin_badge;
          $data->k3_drawin_name=$data->k3_drawin_name;
          $data->k3_drawin_ket=$data->k3_drawin_ket;
          $data->k3_drawout_badge=$data->k3_drawout_badge;
          $data->k3_drawout_name=$data->k3_drawout_name;
          $data->k3_drawout_ket=$data->k3_drawout_ket;

          $data->pmll_drawin_badge=$data->pmll_drawin_badge;
          $data->pmll_drawin_name=$data->pmll_drawin_name;
          $data->pmll_drawin_ket=$data->pmll_drawin_ket;
          $data->pmll_drawout_badge=$data->pmll_drawout_badge;
          $data->pmll_drawout_name=$data->pmll_drawout_name;
          $data->pmll_drawout_ket=$data->pmll_drawout_ket;

          $data->operator_drawin_badge=$data->operator_drawin_badge;
          $data->operator_drawin_name=$data->operator_drawin_name;
          $data->operator_drawin_ket=$data->operator_drawin_ket;
          $data->operator_drawout_badge=$data->operator_drawout_badge;
          $data->operator_drawout_name=$data->operator_drawout_name;
          $data->operator_drawout_ket=$data->operator_drawout_ket;

          $data->pmlt_drawin_badge=$data->pmlt_drawin_badge;
          $data->pmlt_drawin_name=$data->pmlt_drawin_name;
          $data->pmlt_drawin_ket=$data->pmlt_drawin_ket;
          $data->pmlt_drawout_badge=$data->pmlt_drawout_badge;
          $data->pmlt_drawout_name=$data->pmlt_drawout_name;
          $data->pmlt_drawout_ket=$data->pmlt_drawout_ket;
        }

        try{
            $data->save();
            return "
            <div class='alert alert-success'>Lotto Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Report Failed! Lotto not saved!</div>";
        }
    }

    public function delete(Request $request){

        $id = intval($request->input('id',0));
        try{
            Lotto::find($id)->delete();
            return "
            <div class='alert alert-success'>Lotto Remove Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        }catch(\Exception $ex){
            return "<div class='alert alert-danger'>Remove Failed! Lotto not removed!</div>";
        }
    }

    public function draw(Request $request){
        $id = intval($request->input('id',0));
        $employe = Employee::all();
        $data = Lotto::find($id);
        $keterangan = json_decode('[{"ket":"Tidak Datang & Tidak Mengamankan","sim":"X"},{"ket":"Datang & Tidak Mengamankan","sim":"Y"},{"ket":"Datang & Mengamankan","sim":"V"}]');
        $params =[
            'title' =>'Manajemen Lotto',
            'data' =>$data,
            'employe' =>$employe,
            'keterangan' =>$keterangan,
        ];
        return view('firesystem/lotto/draw', $params);
    }

    // public function employe(Request $request){
    //
    //   if($request->has('q')){
    //     $search = $request->q;
    //     $employe = Employee::where('mk_nama','LIKE','%$search%')->get();
    //     return response()->json($employe);
    //   }
    //
    // }

    public function saveDraw(Request $request)
    {
        $id = intval($request->input('id',0));
        if($id){
            $data = Lotto::find($id);
        }else{
            $data = new Lotto();
        }

        if($id){
          $data->equipment_number = $data->equipment_number;
          $data->location = $data->location;
          $data->no_er = $data->no_er;
          $data->activity_description = $data->activity_description;
          $data->date = $data->date;
          $data->time = $data->time;
          $data->note = $data->note;
          $data->safety_key = $data->safety_key;
          $data->safety_tools = $data->safety_tools;
          $data->status = $data->status;
        }

        $k3_drawin_name=explode('|', $request->k3_drawin_name);
        $data->k3_drawin_badge=$k3_drawin_name[0];
        $data->k3_drawin_name=$k3_drawin_name[1];
        $data->k3_drawin_ket=$request->k3_drawin_ket;

        $k3_drawout_name=explode('|', $request->k3_drawout_name);
        $data->k3_drawout_badge=$k3_drawout_name[0];
        $data->k3_drawout_name=$k3_drawout_name[1];
        $data->k3_drawout_ket=$request->k3_drawout_ket;

        $pmll_drawin_name=explode('|', $request->pmll_drawin_name);
        $data->pmll_drawin_badge=$pmll_drawin_name[0];
        $data->pmll_drawin_name=$pmll_drawin_name[1];
        $data->pmll_drawin_ket=$request->pmll_drawin_ket;

        $pmll_drawout_name=explode('|', $request->pmll_drawout_name);
        $data->pmll_drawout_badge=$pmll_drawout_name[0];
        $data->pmll_drawout_name=$pmll_drawout_name[1];
        $data->pmll_drawout_ket=$request->pmll_drawout_ket;

        $operator_drawin_name=explode('|', $request->operator_drawin_name);
        $data->operator_drawin_badge=$operator_drawin_name[0];
        $data->operator_drawin_name=$operator_drawin_name[1];
        $data->operator_drawin_ket=$request->operator_drawin_ket;

        $operator_drawout_name=explode('|', $request->operator_drawout_name);
        $data->operator_drawout_badge=$operator_drawout_name[0];
        $data->operator_drawout_name=$operator_drawout_name[1];
        $data->operator_drawout_ket=$request->operator_drawout_ket;

        $pmlt_drawin_name=explode('|', $request->pmlt_drawin_name);
        $data->pmlt_drawin_badge=$pmlt_drawin_name[0];
        $data->pmlt_drawin_name=$pmlt_drawin_name[1];
        $data->pmlt_drawin_ket=$request->pmlt_drawin_ket;

        $pmlt_drawout_name=explode('|', $request->pmlt_drawout_name);
        $data->pmlt_drawout_badge=$pmlt_drawout_name[0];
        $data->pmlt_drawout_name=$pmlt_drawout_name[1];
        $data->pmlt_drawout_ket=$request->pmlt_drawout_ket;

        try{
            $data->save();
            return "
            <div class='alert alert-success'>Lotto Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Report Failed! Lotto not saved!</div>";
        }
    }
}
