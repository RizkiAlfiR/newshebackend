<?php

namespace App\Http\Controllers\Backend\FireSystem;

use Illuminate\Http\Request;
use App\Models\FireSystem\InspectionHydrant\LokasiHydrant;
use App\Models\FireSystem\InspectionHydrant\InspectionHydrant;
use App\Models\FireSystem\GlobalList\InspectionArea;
use App\Models\FireSystem\GlobalList\UnitKerja;
use App\Models\GlobalList\Plant;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;

class InspectionHydrantController extends Controller
{
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $lokasiHydrant=LokasiHydrant::all();
        $inspectionHydrant=InspectionHydrant::all();
        $params=[
             'lokasiHydrant'=>$lokasiHydrant,
             'inspectionHydrant'=>$inspectionHydrant,
             'title'=>'Hydrant'
        ];

        return view('firesystem/inspectionHydrant/index', $params);
    }

    public function formMaster(Request $request)
    {
        $id = $request->input('id');
        $inspectionArea = InspectionArea::all();
        $plant = Plant::all();
        $unitKerja = UnitKerja::all();
        if($id){
            $data = LokasiHydrant::find($id);
        }else{
            $data = new LokasiHydrant();
        }
        $params =[
            'title' => 'Manajemen Lokasi Hydrant',
            'data' => $data,
            'inspectionArea' => $inspectionArea,
            'plant' => $plant,
            'unitKerja' => $unitKerja,
        ];
        return view('firesystem/inspectionHydrant/formMaster', $params);
    }

    public function saveMaster(Request $request)
    {
        $id = intval($request->input('id',0));

        if($id){
            $data = LokasiHydrant::find($id);
        }else{
            $data = new LokasiHydrant();
            $checkData=LokasiHydrant::where(['area'=>$request->area])->first();
            if($checkData){
                return "<div class='alert alert-danger'>Data sudah tersedia!</div>";
            }
        }
        $data->unit_kerja = $request->unit_kerja;
        $data->area = $request->area;
        $data->company = $request->company;
        $data->plant = $request->plant;
        $data->qr_code = $data->area;
        $data->last_inspection = $data->last_inspection;
        $data->exp_inspection = $data->exp_inspection;
        //Image::make('public')->put($data->area.".png", base64_decode(DNS2D::getBarcodePNG($data->area, "QRCODE")));
        //echo '<img src="data:image/png;base64,' . $data->qr_code . '" alt="barcode"   />';
        try{
            $data->save();
            return "
            <div class='alert alert-success'>Location Hydrant Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Location Hydrant Failed! Location Hydrant not saved!</div>";
        }
    }

    public function deleteMaster(Request $request){

        $id = intval($request->input('id',0));
        try{
            LokasiHydrant::find($id)->delete();
            return "
            <div class='alert alert-success'>Location Hydrant Remove Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        }catch(\Exception $ex){
            return "<div class='alert alert-danger'>Remove Failed! Location Hydrant not removed!</div>";
        }
    }

    public function formInspectionHydrant(Request $request)
    {
        $id = $request->input('id');
        $inspectionArea = InspectionArea::all();
        $plant = Plant::all();
        if($id){
            $data = LokasiHydrant::find($id);
        }else{
            $data = new LokasiHydrant();
        }
        $params =[
            'title' => 'Manajemen Lokasi Hydrant',
            'data' => $data,
            'inspectionArea' => $inspectionArea,
            'plant' => $plant,
        ];
        return view('firesystem/inspectionHydrant/formInspectionHydrant', $params);
    }

    public function saveInspectionHydrant(Request $request)
    {
        $id = intval($request->input('id',0));

        $data = new InspectionHydrant();

        if(!is_null($data->foto_temuan)){
            if(is_null($request->file('foto_temuan')) && $request->file('foto_temuan')==''){
                $fileFotoTemuan=$data->foto_temuan;
            }else{
                $rules=[
                    'foto_temuan' => 'mimes:jpeg,bmp,png,gif,jpg,pdf',
                ];
                $validator=Validator::make($request->all(),$rules);
                if($validator->fails()){
                    return "<div class='alert alert-danger'>Terjadi kesalahan!Format tidak valid!</div>";
                }else{
                    $fotoTemuan=$request->file('foto_temuan');
                    $destinationPath = 'public/uploads/InspectionHydrant/'.$request->foto_temuan_id;
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $fileFotoTemuan = date('YmdHis') . '_' . $fotoTemuan->getClientOriginalName();
                    $fotoTemuan->move($destinationPath, $fileFotoTemuan);
                }
            }
        }

        if(is_null($data->foto_temuan)){
            if(is_null($request->file('foto_temuan')) && $request->file('foto_temuan')==''){
                $fileFotoTemuan=$data->foto_temuan;
            }else{
                $rules=[
                    'foto_temuan' => 'mimes:jpeg,bmp,png,gif,jpg,pdf',
                ];
                $validator=Validator::make($request->all(),$rules);
                if($validator->fails()){
                    return "<div class='alert alert-danger'>Terjadi kesalahan!Format tidak valid!</div>";
                }else{
                    $fotoTemuan=$request->file('foto_temuan');
                    $destinationPath = 'public/uploads/InspectionHydrant/'.$request->foto_temuan_id;
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $fileFotoTemuan = date('YmdHis') . '_' . $fotoTemuan->getClientOriginalName();
                    $fotoTemuan->move($destinationPath, $fileFotoTemuan);
                }
            }
        }

        $data->id_hydrant = $id;
        $data->inspection_date = $request->inspection_date;
        $data->pilar_body = $request->pilar_body;
        $data->pilar_note = $request->pilar_note;
        $data->valve_kiri = $request->valve_kiri;
        $data->valve_atas = $request->valve_atas;
        $data->valve_kanan = $request->valve_kanan;
        $data->valve_note = $request->valve_note;
        $data->copling_kiri = $request->copling_kiri;
        $data->copling_kanan = $request->copling_kanan;
        $data->copling_note = $request->copling_note;
        $data->t_copling_kiri = $request->t_copling_kiri;
        $data->t_copling_kanan = $request->t_copling_kanan;
        $data->t_copling_note = $request->t_copling_note;
        $data->box_casing = $request->box_casing;
        $data->box_hose = $request->box_hose;
        $data->box_nozle = $request->box_nozle;
        $data->box_kunci = $request->box_kunci;
        $data->box_note = $request->box_note;
        $data->press = $request->press;
        $data->temuan = $request->temuan;
        $data->foto_temuan = $fileFotoTemuan;
        $data->pic_badge = '-';
        $data->pic_name = 'Administrator';

        $dataLokasi = LokasiHydrant::find($id);
        $dataLokasi->unit_kerja = $dataLokasi->unit_kerja;
        $dataLokasi->area = $dataLokasi->area;
        $dataLokasi->company = $dataLokasi->company;
        $dataLokasi->plant = $dataLokasi->plant;
        $dataLokasi->qr_code = $dataLokasi->area;
        $dataLokasi->last_inspection = $request->inspection_date;
        $exp = date('Y-m-d', strtotime('+7 days', strtotime($request->inspection_date)));
        $dataLokasi->exp_inspection = $exp;
        try{
            $data->save();
            $dataLokasi->save();
            return "
            <div class='alert alert-success'>Inspection Hydrant Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Location Hydrant Failed! Inspection Hydrant not saved!</div>";
        }
    }


    public function historyInspectionHydrant(Request $request)
    {
        $id = $request->input('id');

        $lokasiHydrant = LokasiHydrant::find($id);
        $inspectionHydrant = InspectionHydrant::where('id_hydrant', 'LIKE', '%' .$id. '%')->get();

        $params =[
            'title' => 'Manajemen Inspection Hydrant',
            'lokasiHydrant' => $lokasiHydrant,
            'inspectionHydrant' => $inspectionHydrant,
        ];
        return view('firesystem/inspectionHydrant/historyInspectionHydrant', $params);
    }

    public function deleteHistory(Request $request){

        $id = intval($request->input('id',0));
        try{
            InspectionHydrant::find($id)->delete();
            return "
            <div class='alert alert-success'>Inspection Hydrant Remove Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        }catch(\Exception $ex){
            return "<div class='alert alert-danger'>Remove Failed! Inspection Hydrant not removed!</div>";
        }
    }

    public function updateInspectionHydrant(Request $request)
    {
        $id = $request->input('id');
        if($id){
            $data = InspectionHydrant::find($id);
            $dataLokasi = LokasiHydrant::find($data->id_hydrant);
        }else{
            $data = new InspectionHydrant();
        }

        $params =[
            'title' => 'Manajemen Inspection Hydrant',
            'data' => $data,
            'dataLokasi' => $dataLokasi,
        ];
        return view('firesystem/inspectionHydrant/updateInspectionHydrant', $params);
    }

    public function saveUpdateInspectionHydrant(Request $request)
    {
        $id = intval($request->input('id',0));

        if($id){
            $data = InspectionHydrant::find($id);
        }else{
            $data = new InspectionHydrant();
        }

        if(!is_null($data->foto_temuan)){
            if(is_null($request->file('foto_temuan')) && $request->file('foto_temuan')==''){
                $fileFotoTemuan=$data->foto_temuan;
            }else{
                $rules=[
                    'foto_temuan' => 'mimes:jpeg,bmp,png,gif,jpg,pdf',
                ];
                $validator=Validator::make($request->all(),$rules);
                if($validator->fails()){
                    return "<div class='alert alert-danger'>Terjadi kesalahan!Format tidak valid!</div>";
                }else{
                    $fotoTemuan=$request->file('foto_temuan');
                    $destinationPath = 'public/uploads/InspectionHydrant/'.$request->foto_temuan_id;
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $fileFotoTemuan = date('YmdHis') . '_' . $fotoTemuan->getClientOriginalName();
                    $fotoTemuan->move($destinationPath, $fileFotoTemuan);
                }
            }
        }

        if(is_null($data->foto_temuan)){
            if(is_null($request->file('foto_temuan')) && $request->file('foto_temuan')==''){
                $fileFotoTemuan=$data->foto_temuan;
            }else{
                $rules=[
                    'foto_temuan' => 'mimes:jpeg,bmp,png,gif,jpg,pdf',
                ];
                $validator=Validator::make($request->all(),$rules);
                if($validator->fails()){
                    return "<div class='alert alert-danger'>Terjadi kesalahan!Format tidak valid!</div>";
                }else{
                    $fotoTemuan=$request->file('foto_temuan');
                    $destinationPath = 'public/uploads/InspectionHydrant/'.$request->foto_temuan_id;
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $fileFotoTemuan = date('YmdHis') . '_' . $fotoTemuan->getClientOriginalName();
                    $fotoTemuan->move($destinationPath, $fileFotoTemuan);
                }
            }
        }

        $data->id_hydrant = $request->id_hydrant;
        $data->inspection_date = $request->inspection_date;
        $data->pilar_body = $request->pilar_body;
        $data->pilar_note = $request->pilar_note;
        $data->valve_kiri = $request->valve_kiri;
        $data->valve_atas = $request->valve_atas;
        $data->valve_kanan = $request->valve_kanan;
        $data->valve_note = $request->valve_note;
        $data->copling_kiri = $request->copling_kiri;
        $data->copling_kanan = $request->copling_kanan;
        $data->copling_note = $request->copling_note;
        $data->t_copling_kiri = $request->t_copling_kiri;
        $data->t_copling_kanan = $request->t_copling_kanan;
        $data->t_copling_note = $request->t_copling_note;
        $data->box_casing = $request->box_casing;
        $data->box_hose = $request->box_hose;
        $data->box_nozle = $request->box_nozle;
        $data->box_kunci = $request->box_kunci;
        $data->box_note = $request->box_note;
        $data->press = $request->press;
        $data->temuan = $request->temuan;
        $data->foto_temuan=$fileFotoTemuan;
        if($id){
          $data->pic_badge = $request->pic_badge;
          $data->pic_name = $request->pic_name;
        }else{
          $data->pic_badge = '-';
          $data->pic_name = 'Administrator';
        }

        try{
            $data->save();
            return "
            <div class='alert alert-success'>Inspection Hydrant Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Inspection Hydrant Failed! Inspection Hydrant not saved!</div>";
        }
    }
}
