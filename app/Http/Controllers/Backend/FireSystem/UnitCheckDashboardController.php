<?php

namespace App\Http\Controllers\Backend\FireSystem;

use Illuminate\Http\Request;
use App\Models\FireSystem\UnitCheck\ListVehicle;
use App\Models\FireSystem\UnitCheck\InspectionPickup;
use App\Models\FireSystem\UnitCheck\InspectionPMK;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

class UnitCheckDashboardController extends Controller
{
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $listVehicle = ListVehicle::all();
        $listVehiclePickup = ListVehicle::where('type', 'LIKE', '%' .'PICKUP'. '%')->get();
        $listVehiclePMK = ListVehicle::where('type', 'LIKE', '%' .'PMK'. '%')->get();

        $params=[
             'listVehicle'=>$listVehicle,
             'listVehiclePickup'=>$listVehiclePickup,
             'listVehiclePMK'=>$listVehiclePMK,
             'title'=>'List Vehicle'
        ];

        return view('firesystem/unitCheckDashboard/index', $params);
    }

    public function getDate(Request $request)
    {
        $bulan = $request->bulan;

        // $month = intval($request->input('date',0));
        test();
    }
    public function test()
    {
      dd('test');

    }

    public function masterVehicle()
    {
        $date = date('Y-m-d');
        $title3 = date('F');
        $title2 = date('F', strtotime('-1 month', strtotime($date)));
        $title1 = date('F', strtotime('-2 month', strtotime($date)));
        $month3 = date('Y-m');
        $month2 = date('Y-m', strtotime('-1 month', strtotime($month3)));
        $month1 = date('Y-m', strtotime('-2 month', strtotime($month3)));

        $listVehiclePickup = ListVehicle::where('type', 'LIKE', '%' .'PICKUP'. '%')->select('vehicle_code')->get();
        $listVehiclePMK = ListVehicle::where('type', 'LIKE', '%' .'PMK'. '%')->select('vehicle_code')->get();

        $countPickup=$listVehiclePickup->count();
        $countPMK=$listVehiclePMK->count();

        $pickup=[];
        $inspectionPickup1=[];
        $inspectionPickup2=[];
        $inspectionPickup3=[];
        for ($i = 0; $i < $countPickup; $i++){
          $pickup[] = [
              'valuePickup' => $listVehiclePickup[$i]->vehicle_code,
          ];
          $inspectionPickup1[] = [
              'valueInspectionPickup' => InspectionPickup::where('report_date', 'LIKE', '%' .$month1. '%')->
              where('vehicle_code', 'LIKE', '%' .$listVehiclePickup[$i]->vehicle_code. '%')->sum('sum_negative'),
          ];
          $inspectionPickup2[] = [
              'valueInspectionPickup' => InspectionPickup::where('report_date', 'LIKE', '%' .$month2. '%')->
              where('vehicle_code', 'LIKE', '%' .$listVehiclePickup[$i]->vehicle_code. '%')->sum('sum_negative'),
          ];
          $inspectionPickup3[] = [
              'valueInspectionPickup' => InspectionPickup::where('report_date', 'LIKE', '%' .$month3. '%')->
              where('vehicle_code', 'LIKE', '%' .$listVehiclePickup[$i]->vehicle_code. '%')->sum('sum_negative'),
          ];
        }

        $pmk=[];
        $inspectionPMK1=[];
        $inspectionPMK2=[];
        $inspectionPMK3=[];
        for ($i = 0; $i < $countPMK; $i++){
          $pmk[] = [
              'valuePMK' => $listVehiclePMK[$i]->vehicle_code,
          ];
          $inspectionPMK1[] = [
              'valueInspectionPMK' => InspectionPMK::where('report_date', 'LIKE', '%' .$month1. '%')->
              where('vehicle_code', 'LIKE', '%' .$listVehiclePMK[$i]->vehicle_code. '%')->sum('sum_negative'),
          ];
          $inspectionPMK2[] = [
              'valueInspectionPMK' => InspectionPMK::where('report_date', 'LIKE', '%' .$month2. '%')->
              where('vehicle_code', 'LIKE', '%' .$listVehiclePMK[$i]->vehicle_code. '%')->sum('sum_negative'),
          ];
          $inspectionPMK3[] = [
              'valueInspectionPMK' => InspectionPMK::where('report_date', 'LIKE', '%' .$month3. '%')->
              where('vehicle_code', 'LIKE', '%' .$listVehiclePMK[$i]->vehicle_code. '%')->sum('sum_negative'),
          ];
        }

        $isPickup = [];
        foreach ($pickup as $item){
            $isPickup[] = $item['valuePickup'];
        }

        $isPMK = [];
        foreach ($pmk as $item){
            $isPMK[] = $item['valuePMK'];
        }

        $isInspectionPickup1 = [];
        $isInspectionPickup2 = [];
        $isInspectionPickup3 = [];
        foreach ($inspectionPickup1 as $item){
            $isInspectionPickup1[] = (int)$item['valueInspectionPickup'];
        }
        foreach ($inspectionPickup2 as $item){
            $isInspectionPickup2[] = (int)$item['valueInspectionPickup'];
        }
        foreach ($inspectionPickup3 as $item){
            $isInspectionPickup3[] = (int)$item['valueInspectionPickup'];
        }

        $isInspectionPMK1 = [];
        $isInspectionPMK2 = [];
        $isInspectionPMK3 = [];
        foreach ($inspectionPMK1 as $item){
            $isInspectionPMK1[] = (int)$item['valueInspectionPMK'];
        }
        foreach ($inspectionPMK2 as $item){
            $isInspectionPMK2[] = (int)$item['valueInspectionPMK'];
        }
        foreach ($inspectionPMK3 as $item){
            $isInspectionPMK3[] = (int)$item['valueInspectionPMK'];
        }

        $params = [
            'isPickup' => $isPickup,
            'isPMK' => $isPMK,
            'isInspectionPickup1' => $isInspectionPickup1,
            'isInspectionPickup2' => $isInspectionPickup2,
            'isInspectionPickup3' => $isInspectionPickup3,
            'isInspectionPMK1' => $isInspectionPMK1,
            'isInspectionPMK2' => $isInspectionPMK2,
            'isInspectionPMK3' => $isInspectionPMK3,
            'title1' => $title1,
            'title2' => $title2,
            'title3' => $title3,
        ];

        return response()->json($params);
    }

    public function performaVehicle()
    {
        $monthlyInspectionPickup = [];
        $monthlyInspectionPMK = [];
        for ($i = 1; $i <= 12; $i++){
            $monthIsInspectionPickup  = InspectionPickup::whereMonth('report_date', $this->generateMonth($i))->sum('sum_negative');
            $monthIsInspectionPMK  = InspectionPMK::whereMonth('report_date', $this->generateMonth($i))->sum('sum_negative');

            $monthlyInspectionPickup[] = [
                'valueIsInspectionPickup' =>$monthIsInspectionPickup,
            ];
            $monthlyInspectionPMK[] = [
                'valueIsInspectionPMK' =>$monthIsInspectionPMK,
            ];
        }
        $isInspectionPickup = [];
        $isInspectionPMK = [];
        foreach ($monthlyInspectionPickup as $item){
            $isInspectionPickup[] = (int)$item['valueIsInspectionPickup'];
        }
        foreach ($monthlyInspectionPMK as $item){
            $isInspectionPMK[] = (int)$item['valueIsInspectionPMK'];
        }

        $params = [
            'isInspectionPickup' => $isInspectionPickup,
            'isInspectionPMK' => $isInspectionPMK,
        ];

        return response()->json($params);
    }

    private function generateMonth($idx)
    {
        if(strlen($idx) < 2)
        {
            $idx = '0'.$idx;
        }
        return $idx;
    }
}
