<?php

namespace App\Http\Controllers\Backend\FireSystem;

use Illuminate\Http\Request;

class menuDailySectionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('firesystem.menuDailySection');
    }


}
