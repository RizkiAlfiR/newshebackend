<?php

namespace App\Http\Controllers\Backend\FireSystem;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\Models\FireSystem\InspectionAlarm\LokasiAlarm;
use App\Models\FireSystem\InspectionAlarm\ParameterPengecekanAlarm;
use App\Models\FireSystem\InspectionAlarm\InspectionAlarm;
use App\Models\FireSystem\GlobalList\InspectionArea;

use App\Models\GlobalList\Plant;
use Milon\Barcode\DNS2D as DNS2D;
use Milon\Barcode\DNS1D as DNS1D;
use Illuminate\Support\Facades\Storage;
use Image;

class LokasiAlarmController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

     public function index() {

         $lokasiAlarm=LokasiAlarm::all();
         $parameterPengecekanAlarm=ParameterPengecekanAlarm::all();
         $inspectionAlarm=InspectionAlarm::all();
         $plant = Plant::all();

         $params = [
             'title' => 'Manajemen Diagnosa Penyakit',
             'lokasiAlarm' => $lokasiAlarm,
             'parameterPengecekanAlarm' => $parameterPengecekanAlarm,
             'inspectionAlarm' => $inspectionAlarm,
             'plant' => $plant
         ];

         return view('firesystem/inspectionAlarm/index', $params);
     }

    public function form() {
      $textdt="01 May 2019";
      $dt= strtotime( $textdt);
      $currdt=$dt;
      $nextmonth=strtotime($textdt."+1 month");
      $i=0;
      do
      {
          $weekday= date("w",$currdt);
          $nextday=7-$weekday;
          $endday=abs($weekday-6);
          $startarr[$i]=$currdt;
          $endarr[$i]=strtotime(date("Y-m-d",$currdt)."+$endday day");
          $currdt=strtotime(date("Y-m-d",$endarr[$i])."+1 day");
          echo "Week ".($i+1). "-> start date = ". date("Y-m-d",$startarr[$i])." end date = ". date("Y-m-d",$endarr[$i])."<br>";
           $i++;

      }while($endarr[$i-1]<$nextmonth);
        // $data = new LokasiAlarm();
        // $inspectionArea = InspectionArea::all();
        // $plant = Plant::all();
        //
        // $params = [
        //     'title' => 'Manajemen Diagnosa Penyakit',
        //     'data' => $data,
        //     'inspectionArea' => $inspectionArea,
        //     'plant' => $plant
        // ];
        //
        // return view('firesystem/inspectionAlarm/form', $params);
    }

    public function save(Request $request)
    {
        $data = new LokasiAlarm();
        $data->area = $request->area;
        $data->company = $request->company;
        $data->plant = $request->plant;
        $data->qr_code = $data->area;

        //Image::make('public')->put($data->area.".png", base64_decode(DNS2D::getBarcodePNG($data->area, "QRCODE")));
        //echo '<img src="data:image/png;base64,' . $data->qr_code . '" alt="barcode"   />';
        try{
            $data->save();
            return "
            <div class='alert alert-success'>Penyakit berhasil disimpan!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Terjadi kesalahan! Penyakit gfgfg gagal disimpan!</div>";
        }
    }
}
