<?php

namespace App\Http\Controllers\Backend\FireSystem;

use Illuminate\Http\Request;
use App\Models\FireSystem\DailySectionActivity\Officer;
use App\Models\FireSystem\DailySectionActivity\DailySectionActivity;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;

class DailySectionActivityController extends Controller
{
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $date_activity=date('Y-m-d');
        $shift1=1;
        $shift2=2;
        $shift3=3;
        $officerShift1 = Officer::where('date_activity', 'LIKE', '%' .$date_activity. '%')->where('shift', 'LIKE', '%' .$shift1. '%')->get();
        $officerShift2 = Officer::where('date_activity', 'LIKE', '%' .$date_activity. '%')->where('shift', 'LIKE', '%' .$shift2. '%')->get();
        $officerShift3 = Officer::where('date_activity', 'LIKE', '%' .$date_activity. '%')->where('shift', 'LIKE', '%' .$shift3. '%')->get();
        $officer=Officer::orderBy('date_activity', 'DESC')->get();

        $dailySectionActivity=DailySectionActivity::orderBy('date_activity', 'DESC')->get();
        $params=[
             'officerShift1'=>$officerShift1,
             'officerShift2'=>$officerShift2,
             'officerShift3'=>$officerShift3,
             'officer'=>$officer,
             'dailySectionActivity'=>$dailySectionActivity,
             'title'=>'Daily Section Activity'
        ];

        return view('firesystem/dailySectionActivity/index', $params);
    }

    public function formOfficer(Request $request)
    {
        $id = $request->input('id');
        if($id){
            $data = Officer::find($id);
        }else{
            $data = new Officer();
        }
        $params =[
            'title' =>'Manajemen Officer',
            'data' =>$data,
        ];
        return view('firesystem/dailySectionActivity/formOfficer', $params);
    }

    public function saveOfficer(Request $request)
    {
        $id = intval($request->input('id',0));

        if($id){
            $data = Officer::find($id);
        }else{
            $data = new Officer();
        }
        $data->shift = $request->shift;
        $data->officer = $request->officer;
        $data->company = '5000';
        $data->plant = '5001';
        $data->date_activity = $request->date_activity;
        $data->status_pic = $request->status_pic;

        try{
            $data->save();
            return "
            <div class='alert alert-success'>Officer Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Officer Failed! Officer not saved!</div>";
        }
    }

    public function deleteOfficer(Request $request){

        $id = intval($request->input('id',0));
        try{
            Officer::find($id)->delete();
            return "
            <div class='alert alert-success'>Officer Remove Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        }catch(\Exception $ex){
            return "<div class='alert alert-danger'>Remove Failed! Officer not removed!</div>";
        }
    }

    public function showActivity(Request $request){
        $idActivity = $request->input('id');

        $dailySectionActivity = DailySectionActivity::where('id', $idActivity)->get();
        $params=[
             'dailySectionActivity'=>$dailySectionActivity,
             'title'=>'Daily Section'
        ];
        return view('firesystem/dailySectionActivity/showActivity', $params);

    }

    public function formActivity(Request $request)
    {
        $id = $request->input('id');
        $date_activity=date('Y-m-d');
        $officerFilter = Officer::where('date_activity', 'LIKE', '%' .$date_activity. '%')->get();
        if($id){
            $data = DailySectionActivity::find($id);
        }else{
            $data = new DailySectionActivity();
        }
        $params =[
            'title' =>'Manajemen Officer',
            'data' =>$data,
            'officerFilter' =>$officerFilter,
        ];
        return view('firesystem/dailySectionActivity/formActivity', $params);
    }

    public function saveActivity(Request $request)
    {
        $id = intval($request->input('id',0));

        if($id){
            $data = DailySectionActivity::find($id);
        }else{
            $data = new DailySectionActivity();
        }

        $data->type_action=$request->type_action;
        $data->date_activity=$request->date_activity;
        $data->timesheet_start=$request->timesheet_start;
        $data->timesheet_end=$request->timesheet_end;
        $data->daily_activity=$request->daily_activity;
        $data->shift1=$request->shift1;
        $data->shift2=$request->shift2;
        $data->shift3=$request->shift3;
        $data->pic=$request->pic;
        $data->note=$request->note;
        $data->list_tools='[{"tools":"Apar","total":1},{"tools":"Tali","total":2}]';
        $data->company='5000';
        $data->plant='5001';
        $data->status='OPEN';

        if(!is_null($data->image)){
            if(is_null($request->file('image')) && $request->file('image')==''){
                $fileImage=$data->image;
            }else{
                $rules=[
                    'image' => 'mimes:jpeg,bmp,png,gif,jpg,pdf',
                ];
                $validator=Validator::make($request->all(),$rules);
                if($validator->fails()){
                    return "<div class='alert alert-danger'>Terjadi kesalahan!Format tidak valid!</div>";
                }else{
                    $fotoImage=$request->file('image');
                    $destinationPath = 'public/uploads/DailySectionReport/'.$request->image_id;
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $fileImage = date('YmdHis') . '_' . $fotoImage->getClientOriginalName();
                    $fotoImage->move($destinationPath, $fileImage);
                }

            }
        }

        if(is_null($data->image)){
            if(is_null($request->file('image')) && $request->file('image')==''){
                $fileImage=$data->image;
            }else{
                $rules=[
                    'image' => 'mimes:jpeg,bmp,png,gif,jpg,pdf',
                ];
                $validator=Validator::make($request->all(),$rules);
                if($validator->fails()){
                    return "<div class='alert alert-danger'>Terjadi kesalahan!Format tidak valid!</div>";
                }else{
                    $fotoImage=$request->file('image');
                    $destinationPath = 'public/uploads/DailySectionReport/'.$request->image_id;
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $fileImage = date('YmdHis') . '_' . $fotoImage->getClientOriginalName();
                    $fotoImage->move($destinationPath, $fileImage);
                }

            }
        }

        $data->image = $fileImage;

        try{
            $data->save();
            return "
            <div class='alert alert-success'>Activity Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Activity Failed! Activity not saved!</div>";
        }
    }

    public function deleteActivity(Request $request){

        $id = intval($request->input('id',0));
        try{
            DailySectionActivity::find($id)->delete();
            return "
            <div class='alert alert-success'>Daily Section Remove Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        }catch(\Exception $ex){
            return "<div class='alert alert-danger'>Remove Failed! Daily Section not removed!</div>";
        }
    }
}
