<?php

namespace App\Http\Controllers\Backend\FireSystem;

use Illuminate\Http\Request;
use App\Models\FireSystem\FireReport\FireReport;
use App\Models\FireSystem\FireReport\FireReportImage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use PDF;
use DB;
use Charts;

class FireReportController extends Controller
{
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $fireReport=FireReport::all();
        $params=[
             'fireReport'=>$fireReport,
             'title'=>'Manajemen Diagnosa Penyakit'
        ];

        return view('firesystem/fireReport/index', $params);
    }

    public function form(Request $request)
    {
        $id = $request->input('id');
        if($id){
            $data = FireReport::find($id);
        }else{
            $data = new FireReport();
        }
        $params =[
            'title' =>'Manajemen Fire Report',
            'data' =>$data,
        ];
        return view('firesystem/fireReport/form', $params);
    }

    public function save(Request $request)
    {
        $id = intval($request->input('id',0));
        if($id){
            $data = FireReport::find($id);
        }else{
             $data = new FireReport();
        }

        $year = date('Y');
        $month = date('m');
        $day = date('d');

        $lastID = FireReport::orderBy('id', 'DESC')->first();
        $last = $lastID->id+1;
        $number = $last.'/SH/'.$month.'/'.$day.'/FR/'.$month.'/'.$year;

        $data->fire_date = $request->fire_date;
        $data->fire_time = $request->fire_time;
        $data->job_walking_timestart = $request->job_walking_timestart;
        $data->job_walking_timeend = $request->job_walking_timeend;
        if($id){
          $data->pic_badge = $request->pic_badge;
          $data->pic_name = $request->pic_name;
        }else{
          $data->pic_badge = '-';
          $data->pic_name = 'Administrator';
        }
        $data->incident_place = $request->incident_place;
        $data->note_place = $request->note_place;
        $data->damage_from_fire = $request->damage_from_fire;
        $data->fire_material_type = $request->fire_material_type;
        $data->number_victims = $request->number_victims;
        $data->description_of_fire = $request->description_of_fire;
        $data->unsafe_condition = $request->unsafe_condition;
        $data->unsafe_action = $request->unsafe_action;
        $data->list_employees = $request->list_employees;
        $data->work_on_fire = $request->work_on_fire;
        $data->fire_extinguishers = '["Apar","Hydrant","PMK"]';
        $data->shift = $request->shift;
        $data->list_tembusan = $request->list_tembusan;
        $data->saran = $request->saran;
        $data->company = '5000';
        $data->plant = '5001';
        if($id){
            $data->fire_number = $request->fire_number;
            $data->status = 'CLOSE';
        }else{
            $data->fire_number = $number;
            $data->status = 'OPEN';
        }
        try{
            $data->save();
            return "
            <div class='alert alert-success'>Fire Report Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Report Failed! Fire Report not saved!</div>";
        }
    }

    public function delete(Request $request){

        $id = intval($request->input('id',0));
        try{
            FireReport::find($id)->delete();
            return "
            <div class='alert alert-success'>Fire Report Remove Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        }catch(\Exception $ex){
            return "<div class='alert alert-danger'>Remove Failed! Fire Report not removed!</div>";
        }
    }

    public function showImage(Request $request){

        $id = intval($request->input('id',0));
        $data = FireReportImage::where('id_fire_report', 'LIKE', '%' .$id. '%')->get();
        $params =[
            'title' =>'Manajemen Fire Report Image',
            'data' =>$data,
        ];
        return view('firesystem/fireReport/showImage', $params);
    }

    public function uploadImage(Request $request)
    {
        $id = $request->input('id');
        $params =[
            'title' =>'Manajemen Fire Report Image',
            'id' =>$id,
        ];
        return view('firesystem/fireReport/uploadImage', $params);
    }

    public function saveImage(Request $request)
    {
        $id = intval($request->input('id',0));

        $data = new FireReportImage();

        if(!is_null($data->image)){
            if(is_null($request->file('image')) && $request->file('image')==''){
                $fileImage=$data->image;
            }else{
                $rules=[
                    'image' => 'mimes:jpeg,bmp,png,gif,jpg,pdf',
                ];
                $validator=Validator::make($request->all(),$rules);
                if($validator->fails()){
                    return "<div class='alert alert-danger'>Terjadi kesalahan!Format tidak valid!</div>";
                }else{
                    $fotoImage=$request->file('image');
                    $destinationPath = 'public/uploads/FireReport/'.$request->image_id;
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $fileImage = date('YmdHis') . '_' . $fotoImage->getClientOriginalName();
                    $fotoImage->move($destinationPath, $fileImage);
                }

            }
        }

        if(is_null($data->image)){
            if(is_null($request->file('image')) && $request->file('image')==''){
                $fileImage=$data->image;
            }else{
                $rules=[
                    'image' => 'mimes:jpeg,bmp,png,gif,jpg,pdf',
                ];
                $validator=Validator::make($request->all(),$rules);
                if($validator->fails()){
                    return "<div class='alert alert-danger'>Terjadi kesalahan!Format tidak valid!</div>";
                }else{
                    $fotoImage=$request->file('image');
                    $destinationPath = 'public/uploads/FireReport/'.$request->image_id;
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $fileImage = date('YmdHis') . '_' . $fotoImage->getClientOriginalName();
                    $fotoImage->move($destinationPath, $fileImage);
                }

            }
        }

        $data->id_fire_report = $id;
        $data->image = $fileImage;
        $data->description = $request->description;
        $data->status = $request->status;

        try{
            $data->save();
            return "
            <div class='alert alert-success'>Fire Report Image Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Upload Failed! Fire Report Image not saved!</div>";
        }
    }

    public function getPDF(Request $request)
    {
        // $id = intval($request->input('id',0));
        // $data = FireReport::find($id);
        // $params =[
        //     'title' =>'Manajemen Fire Report Image',
        //     'data' =>$data,
        // ];
        // $pdf = PDF::loadView('firesystem/fireReport/getPDF', $params);
        // //
        // // // return $pdf->download('firesystem.pdf');
        // return $pdf->stream();
        // $users = FireReport::where(DB::raw("(DATE_FORMAT(fire_date,'%Y'))"),date('Y'))->get();



        // $chart = Charts::database($users, 'bar', 'highcharts')
			  //     ->title("Monthly new Register Users")
			  //     ->elementLabel("Total Users")
			  //     ->dimensions(1000, 500)
			  //     ->responsive(false)
			  //     ->groupByMonth(date('Y'), true);
        //     $pie  =	 Charts::create('pie', 'highcharts')
        //     				    ->title('My nice chart')
        //     				    ->labels(['First', 'Second', 'Third'])
        //     				    ->values([5,10,20])
        //     				    ->dimensions(1000,500)
        //     				    ->responsive(false);
        //
        // return view('firesystem/fireReport/getPDF',compact('chart', 'pie'));



        // return view('firesystem/fireReport/getPDF', $params);
    }
}
