<?php

namespace App\Http\Controllers\Backend\FireSystem;

use Illuminate\Http\Request;
use App\Models\FireSystem\GasCO2\GasCO2;
use App\Models\FireSystem\GasCO2\InspectionGas;
use App\Models\GlobalList\Plant;
use App\Models\GlobalList\PPlant;

use Illuminate\Support\Facades\Input;

class GasCO2Controller extends Controller
{
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $gasCO2 = GasCO2::all();
        $inspectionGas = InspectionGas::all();

        $params=[
             'gasCO2'=>$gasCO2,
             'inspectionGas'=>$inspectionGas,
             'title'=>'Gas CO2'
        ];

        return view('firesystem/reportGasCO2/index', $params);
    }

    public function formGasCO2(Request $request)
    {
        $plant = Plant::all();
        $id = $request->input('id');
        if($id){
            $data = GasCO2::find($id);
        }else{
            $data = new GasCO2();
        }
        $params =[
            'title' =>'Manajemen Officer',
            'data' =>$data,
            'plant' =>$plant,
        ];
        return view('firesystem/reportGasCO2/formGasCO2', $params);
    }

    public function saveGasCO2(Request $request)
    {
        $lastPplant = GasCO2::orderBy('pplant', 'DESC')->first();

        $id = intval($request->input('id',0));
        if($id){
            $data = GasCO2::find($id);
        }else{
            $data = new GasCO2();
        }

        $data->pplant = $lastPplant->pplant+1;
        $data->plant = $request->plant;
        $data->pplantdesc = $request->pplantdesc;
        $data->cemetron = 'Cemetron';
        $data->total_max_cemetron = '6';
        $data->samator = 'Samator';
        $data->total_max_samator = '19';

        try{
            $data->save();
            return "
            <div class='alert alert-success'>Gas CO2 Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Gas CO2 Failed! Gas CO2 not saved!</div>";
        }
    }

    public function deleteGasCO2(Request $request){

        $id = intval($request->input('id',0));
        try{
            GasCO2::find($id)->delete();
            return "
            <div class='alert alert-success'>Gas CO2 Remove Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        }catch(\Exception $ex){
            return "<div class='alert alert-danger'>Remove Failed! Gas CO2 not removed!</div>";
        }

    }

    public function showInspectionGasCO2(Request $request){
        $idInspection = $request->input('id');

        $inspectionGas = InspectionGas::where('id', $idInspection)->get();
        $params=[
             'inspectionGas'=>$inspectionGas,
             'title'=>'Inspection Gas'
        ];
        return view('firesystem/reportGasCO2/showInspectionGasCO2', $params);

    }

    public function formInspectionGasCO2(Request $request)
    {
        $id = $request->input('id');
        $plant = Plant::all();
        $pplant = GasCO2::all();

        if($id){
            $data = InspectionGas::find($id);
        }else{
            $data = new InspectionGas();
        }
        $params =[
            'title' =>'Manajemen Officer',
            'data' =>$data,
            'plant' =>$plant,
            'pplant' =>$pplant,
        ];
        return view('firesystem/reportGasCO2/formInspectionGasCO2', $params);
    }

    public function saveInspectionGasCO2(Request $request)
    {

        $id = intval($request->input('id',0));
        // $date_activity=date('Y-m-d');
        // $time_activity=date('H:i:s');
        if($id){
            $data = InspectionGas::find($id);
        }else{
            $data = new InspectionGas();
        }

        $pplant=explode('|', $request->pplant);
        $data->plant=$pplant[0];
        $data->pplant=$pplant[1];
        $data->pplantdesc=$pplant[2];
        $data->total_max_cemetron=6;
        $data->total_residual_cemetron=$request->total_residual_cemetron;
        $data->total_usage_cemetron=$data->total_max_cemetron-$data->total_residual_cemetron;
        $data->total_max_samator=19;
        $data->total_residual_samator=$request->total_residual_samator;
        $data->total_usage_samator=$data->total_max_samator-$data->total_residual_samator;
        $data->pic_name='Administrator';
        $data->pic_badge='-';
        $data->shift=$request->shift;
        $data->note=$request->note;
        $data->inspection_date=$request->inspection_date;
        $data->inspection_time=$request->inspection_time;

        try{
            $data->save();
            return "
            <div class='alert alert-success'>Inspection Gas CO2 Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            dd($ex);
            return "<div class='alert alert-danger'>Inspection Gas CO2 Failed! Inspection Gas CO2 not saved!</div>";
        }
    }

    public function deleteInspectionGasCO2(Request $request){

        $id = intval($request->input('id',0));
        try{
            InspectionGas::find($id)->delete();
            return "
            <div class='alert alert-success'>Inspection Gas CO2 Remove Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        }catch(\Exception $ex){
            return "<div class='alert alert-danger'>Remove Failed! Inspection Gas CO2 not removed!</div>";
        }

    }
}
