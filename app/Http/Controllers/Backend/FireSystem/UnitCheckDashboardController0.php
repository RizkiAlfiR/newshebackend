<?php

namespace App\Http\Controllers\Backend\FireSystem;

use Illuminate\Http\Request;
use App\Models\FireSystem\UnitCheck\ListVehicle;
use App\Models\FireSystem\UnitCheck\InspectionPickup;
use App\Models\FireSystem\UnitCheck\InspectionPMK;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

class UnitCheckDashboardController extends Controller
{
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $listVehicle = ListVehicle::all();
        $listVehiclePickup = ListVehicle::where('type', 'LIKE', '%' .'PICKUP'. '%')->get();
        $listVehiclePMK = ListVehicle::where('type', 'LIKE', '%' .'PMK'. '%')->get();

        $params=[
             'listVehicle'=>$listVehicle,
             'listVehiclePickup'=>$listVehiclePickup,
             'listVehiclePMK'=>$listVehiclePMK,
             'title'=>'List Vehicle'
        ];

        return view('firesystem/unitCheckDashboard/index', $params);
    }

    public function getDate(Request $request)
    {
        $bulan = $request->bulan;

        // $month = intval($request->input('date',0));
        test();
    }
    public function test()
    {
      dd('test');

    }

    public function masterVehicle()
    {
        $title = date('F Y');
        $month3 = date('Y-m');
        $month2 = date('Y-m', strtotime('-1 month', strtotime($month3)));
        $month1 = date('Y-m', strtotime('-2 month', strtotime($month3)));

        $listVehiclePickup = ListVehicle::where('type', 'LIKE', '%' .'PICKUP'. '%')->select('vehicle_code')->get();
        $listVehiclePMK = ListVehicle::where('type', 'LIKE', '%' .'PMK'. '%')->select('vehicle_code')->get();

        $countPickup=$listVehiclePickup->count();
        $countPMK=$listVehiclePMK->count();

        $pickup=[];
        $inspectionPickup=[];
        for ($i = 0; $i < $countPickup; $i++){
          $pickup[] = [
              'valuePickup' => $listVehiclePickup[$i]->vehicle_code,
          ];
          $inspectionPickup[] = [
              'valueInspectionPickup' => InspectionPickup::where('report_date', 'LIKE', '%' .$month. '%')->
              where('vehicle_code', 'LIKE', '%' .$listVehiclePickup[$i]->vehicle_code. '%')->sum('sum_negative'),
          ];
        }

        $pmk=[];
        $inspectionPMK=[];
        for ($i = 0; $i < $countPMK; $i++){
          $pmk[] = [
              'valuePMK' => $listVehiclePMK[$i]->vehicle_code,
          ];
          $inspectionPMK[] = [
              'valueInspectionPMK' => InspectionPMK::where('report_date', 'LIKE', '%' .$month. '%')->
              where('vehicle_code', 'LIKE', '%' .$listVehiclePMK[$i]->vehicle_code. '%')->sum('sum_negative'),
          ];
        }

        $isPickup = [];
        foreach ($pickup as $item){
            $isPickup[] = $item['valuePickup'];
        }

        $isPMK = [];
        foreach ($pmk as $item){
            $isPMK[] = $item['valuePMK'];
        }

        $isInspectionPickup = [];
        foreach ($inspectionPickup as $item){
            $isInspectionPickup[] = (int)$item['valueInspectionPickup'];
        }

        $isInspectionPMK = [];
        foreach ($inspectionPMK as $item){
            $isInspectionPMK[] = (int)$item['valueInspectionPMK'];
        }

        $params = [
            'isPickup' => $isPickup,
            'isPMK' => $isPMK,
            'isInspectionPickup' => $isInspectionPickup,
            'isInspectionPMK' => $isInspectionPMK,
            'title' => $title,
        ];

        return response()->json($params);
    }
}
