<?php

namespace App\Http\Controllers\Backend\FireSystem;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InspectionAreaController extends Controller
{
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // mengambil data dari table accident report
        $inspectionArea = DB::table('inspection_area')->get();

    	   // mengirim data accident ke view index
        return view('master/inspectionArea', ['inspectionArea' => $inspectionArea]);
    }


}
