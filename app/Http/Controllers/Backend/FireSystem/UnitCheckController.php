<?php

namespace App\Http\Controllers\Backend\FireSystem;

use Illuminate\Http\Request;
use App\Models\FireSystem\UnitCheck\ListVehicle;
use App\Models\FireSystem\UnitCheck\InspectionPickup;
use App\Models\FireSystem\UnitCheck\InspectionPMK;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

class UnitCheckController extends Controller
{
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $listVehicle = ListVehicle::all();
        $listVehiclePickup = ListVehicle::where('type', 'LIKE', '%' .'PICKUP'. '%')->get();
        $listVehiclePMK = ListVehicle::where('type', 'LIKE', '%' .'PMK'. '%')->get();

        $params=[
             'listVehicle'=>$listVehicle,
             'listVehiclePickup'=>$listVehiclePickup,
             'listVehiclePMK'=>$listVehiclePMK,
             'title'=>'List Vehicle'
        ];

        return view('firesystem/unitCheck/index', $params);
    }

    public function formMaster(Request $request)
    {
        $id = $request->input('id');
        if($id){
            $data = ListVehicle::find($id);
        }else{
            $data = new ListVehicle();
        }
        $params =[
            'title' =>'Manajemen Vehicle',
            'data' =>$data,
        ];
        return view('firesystem/unitCheck/formMaster', $params);
    }

    public function saveMaster(Request $request)
    {
        $id = intval($request->input('id',0));
        if($id){
            $data = ListVehicle::find($id);
        }else{
            $data = new ListVehicle();
        }

        $data->type = $request->type;
        $data->vehicle_code = $request->vehicle_code;
        $data->vehicle_merk = $request->vehicle_merk;
        $data->tahun = $request->tahun;
        $data->vehicle_type = $request->vehicle_type;
        $data->nomor_rangka = $request->nomor_rangka;
        $data->company = '5000';
        $data->plant = '5001';
        $data->last_inspection = $request->last_inspection;
        $data->exp_inspection = $request->exp_inspection;
        $data->sum_negative = $request->sum_negative;

        if(!is_null($data->image)){
            if(is_null($request->file('image')) && $request->file('image')==''){
                $fileImage=$data->image;
            }else{
                $rules=[
                    'image' => 'mimes:jpeg,bmp,png,gif,jpg,pdf',
                ];
                $validator=Validator::make($request->all(),$rules);
                if($validator->fails()){
                    return "<div class='alert alert-danger'>Terjadi kesalahan!Format tidak valid!</div>";
                }else{
                    $fotoImage=$request->file('image');
                    $destinationPath = 'public/uploads/MasterVehicle/'.$request->image_id;
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $fileImage = date('YmdHis') . '_' . $fotoImage->getClientOriginalName();
                    $fotoImage->move($destinationPath, $fileImage);
                }

            }
        }

        if(is_null($data->image)){
            if(is_null($request->file('image')) && $request->file('image')==''){
                $fileImage=$data->image;
            }else{
                $rules=[
                    'image' => 'mimes:jpeg,bmp,png,gif,jpg,pdf',
                ];
                $validator=Validator::make($request->all(),$rules);
                if($validator->fails()){
                    return "<div class='alert alert-danger'>Terjadi kesalahan!Format tidak valid!</div>";
                }else{
                    $fotoImage=$request->file('image');
                    $destinationPath = 'public/uploads/MasterVehicle/'.$request->image_id;
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $fileImage = date('YmdHis') . '_' . $fotoImage->getClientOriginalName();
                    $fotoImage->move($destinationPath, $fileImage);
                }

            }
        }

        $data->image = $fileImage;

        try{
            $data->save();
            return "
            <div class='alert alert-success'>Vehicle Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Vehicle Failed! Vehicle not saved!</div>";
        }
    }

    public function deleteMaster(Request $request){

        $id = intval($request->input('id',0));
        try{
            ListVehicle::find($id)->delete();
            return "
            <div class='alert alert-success'>Vehicle Remove Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        }catch(\Exception $ex){
            return "<div class='alert alert-danger'>Remove Failed! Vehicle not removed!</div>";
        }

    }

    public function formInspectionPickup(Request $request)
    {
        $id = $request->input('id');
        if($id){
            $data = ListVehicle::find($id);
        }else{
            $data = new ListVehicle();
        }
        $params =[
            'title' =>'Manajemen Inspection Pickup',
            'data' =>$data,
        ];
        return view('firesystem/unitCheck/formInspectionPickup', $params);
    }

    public function saveInspectionPickup(Request $request)
    {
        $id = intval($request->input('id',0));

        $data = new InspectionPickup();
        $data->vehicle_id=$id;
        $data->vehicle_code=$request->vehicle_code;
        $data->report_date=$request->report_date;
        $data->report_time=$request->report_time;
        $data->shift=$request->shift;
        $data->pemanasan=$request->pemanasan;
        $data->spedometer=$request->spedometer;
        $data->level_bbm=$request->level_bbm;
        $data->oli_mesin=$request->oli_mesin;
        $data->oli_rem=$request->oli_rem;
        $data->oli_power=$request->oli_power;
        $data->air_radiator=$request->air_radiator;
        $data->air_cad_radiator=$request->air_cad_radiator;
        $data->air_wiper=$request->air_wiper;
        $data->lampu_cabin=$request->lampu_cabin;
        $data->lampu_kota=$request->lampu_kota;
        $data->lampu_jauh=$request->lampu_jauh;
        $data->lampu_sign_kiri=$request->lampu_sign_kiri;
        $data->lampu_sign_kanan=$request->lampu_sign_kanan;
        $data->lampu_rem=$request->lampu_rem;
        $data->lampu_atret=$request->lampu_atret;
        $data->lampu_sorot=$request->lampu_sorot;
        $data->panel_dashboard=$request->panel_dashboard;
        $data->ban_depan_kiri=$request->ban_depan_kiri;
        $data->ban_depan_kanan=$request->ban_depan_kanan;
        $data->ban_belakang_kiri_dalam=$request->ban_belakang_kiri_dalam;
        $data->ban_belakang_kiri_luar=$request->ban_belakang_kiri_luar;
        $data->ban_belakang_kanan_dalam=$request->ban_belakang_kanan_dalam;
        $data->ban_belakang_kanan_luar=$request->ban_belakang_kanan_luar;
        $data->kaca_spion_kiri=$request->kaca_spion_kiri;
        $data->kaca_spion_kanan=$request->kaca_spion_kanan;
        $data->pic_badge='-';
        $data->pic_name='Administrator';

        $sum=0;
        if($request->level_bbm == '3/4' || $request->level_bbm == 'F'){
          $sum=$sum+1;
        }
        if($request->oli_mesin == 'H'){
          $sum=$sum+1;
        }
        if($request->oli_rem == 'H'){
          $sum=$sum+1;
        }
        if($request->oli_power == 'H'){
          $sum=$sum+1;
        }
        if($request->air_radiator == '3/4' || $request->air_radiator == 'F'){
          $sum=$sum+1;
        }
        if($request->air_cad_radiator == '3/4' || $request->air_cad_radiator == 'F'){
          $sum=$sum+1;
        }
        if($request->air_wiper == '3/4' || $request->air_wiper == 'F'){
          $sum=$sum+1;
        }
        if($request->ban_depan_kiri < 30 || 40 < $request->ban_depan_kiri){
          $sum=$sum+1;
        }
        if($request->ban_depan_kanan < 30 || 40 < $request->ban_depan_kanan){
          $sum=$sum+1;
        }
        if($request->ban_belakang_kiri_dalam < 30 || 40 < $request->ban_belakang_kiri_dalam){
          $sum=$sum+1;
        }
        if($request->ban_belakang_kiri_luar < 30 || 40 < $request->ban_belakang_kiri_luar){
          $sum=$sum+1;
        }
        if($request->ban_belakang_kanan_dalam < 30 || 40 < $request->ban_belakang_kanan_dalam){
          $sum=$sum+1;
        }
        if($request->ban_belakang_kanan_luar < 30 || 40 < $request->ban_belakang_kanan_luar){
          $sum=$sum+1;
        }
        if($request->lampu_cabin == 'MATI'){
          $sum=$sum+1;
        }
        if($request->lampu_kota == 'MATI'){
          $sum=$sum+1;
        }
        if($request->lampu_jauh == 'MATI'){
          $sum=$sum+1;
        }
        if($request->lampu_sign_kiri == 'MATI'){
          $sum=$sum+1;
        }
        if($request->lampu_sign_kanan == 'MATI'){
          $sum=$sum+1;
        }
        if($request->lampu_rem == 'MATI'){
          $sum=$sum+1;
        }
        if($request->lampu_atret == 'MATI'){
          $sum=$sum+1;
        }
        if($request->lampu_sorot == 'MATI'){
          $sum=$sum+1;
        }
        if($request->panel_dashboard == 'MATI'){
          $sum=$sum+1;
        }
        if($request->kaca_spion_kiri == 'PECAH'){
          $sum=$sum+1;
        }
        if($request->kaca_spion_kanan == 'PECAH'){
          $sum=$sum+1;
        }

        $dataVehicle = ListVehicle::find($id);
        $dataVehicle->type = $dataVehicle->type;
        $dataVehicle->vehicle_code = $dataVehicle->vehicle_code;
        $dataVehicle->vehicle_merk = $dataVehicle->vehicle_merk;
        $dataVehicle->tahun = $dataVehicle->tahun;
        $dataVehicle->vehicle_type = $dataVehicle->vehicle_type;
        $dataVehicle->nomor_rangka = $dataVehicle->nomor_rangka;
        $dataVehicle->company = $dataVehicle->company;
        $dataVehicle->plant = $dataVehicle->plant;
        $dataVehicle->image = $dataVehicle->image;
        $dataVehicle->last_inspection = $request->report_date;
        $exp = date('Y-m-d', strtotime('+7 days', strtotime($request->report_date)));
        $dataVehicle->exp_inspection = $exp;
        $dataVehicle->sum_negative = $sum;
        $data->sum_negative = $sum;

        try{
            $data->save();
            $dataVehicle->save();
            return "
            <div class='alert alert-success'>Inspection Pickup Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Inspection Pickup Failed! Inspection Pickup not saved!</div>";
        }
    }

    public function historyInspectionPickup(Request $request)
    {
        $id = $request->input('id');

        $listVehicle = ListVehicle::find($id);
        $inspectionPickup = InspectionPickup::where('vehicle_id', 'LIKE', '%' .$id. '%')->get();

        $params =[
            'title' => 'Manajemen Inspection Pickup',
            'listVehicle' => $listVehicle,
            'inspectionPickup' => $inspectionPickup,
        ];
        return view('firesystem/unitCheck/historyInspectionPickup', $params);
    }


    public function deleteInspectionPickup(Request $request){

        $id = intval($request->input('id',0));
        try{
            InspectionPickup::find($id)->delete();
            return "
            <div class='alert alert-success'>Inspection Pickup Remove Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        }catch(\Exception $ex){
            return "<div class='alert alert-danger'>Remove Failed! Inspection Pickup not removed!</div>";
        }
    }

    public function formInspectionPMK(Request $request)
    {
        $id = $request->input('id');
        if($id){
            $data = ListVehicle::find($id);
        }else{
            $data = new ListVehicle();
        }
        $params =[
            'title' =>'Manajemen Inspection Pickup',
            'data' =>$data,
        ];
        return view('firesystem/unitCheck/formInspectionPMK', $params);
    }

    public function saveInspectionPMK(Request $request)
    {
        $id = intval($request->input('id',0));

        $data = new InspectionPMK();
        $data->vehicle_id = $id;
        $data->vehicle_code=$request->vehicle_code;
        $data->report_date=$request->report_date;
        $data->report_time=$request->report_time;
        $data->shift=$request->shift;
        $data->pemanasan=$request->pemanasan;
        $data->spedometer=$request->spedometer;
        $data->level_bbm=$request->level_bbm;
        $data->working_pressure=$request->working_pressure;
        $data->hour_meter_mesin=$request->hour_meter_mesin;
        $data->hour_meter_pompa=$request->hour_meter_pompa;
        $data->oli_mesin=$request->oli_mesin;
        $data->oli_rem=$request->oli_rem;
        $data->oli_power=$request->oli_power;
        $data->air_radiator=$request->air_radiator;
        $data->air_cad_radiator=$request->air_cad_radiator;
        $data->air_wiper=$request->air_wiper;
        $data->lampu_cabin=$request->lampu_cabin;
        $data->lampu_kota=$request->lampu_kota;
        $data->lampu_jauh=$request->lampu_jauh;
        $data->lampu_sign_kiri=$request->lampu_sign_kiri;
        $data->lampu_sign_kanan=$request->lampu_sign_kanan;
        $data->lampu_rem=$request->lampu_rem;
        $data->lampu_atret=$request->lampu_atret;
        $data->lampu_sorot=$request->lampu_sorot;
        $data->panel_dashboard=$request->panel_dashboard;
        $data->ban_depan_kiri=$request->ban_depan_kiri;
        $data->ban_depan_kanan=$request->ban_depan_kanan;
        $data->ban_belakang_kiri_dalam=$request->ban_belakang_kiri_dalam;
        $data->ban_belakang_kiri_luar=$request->ban_belakang_kiri_luar;
        $data->ban_belakang_kanan_dalam=$request->ban_belakang_kanan_dalam;
        $data->ban_belakang_kanan_luar=$request->ban_belakang_kanan_luar;
        $data->kaca_spion_kiri=$request->kaca_spion_kiri;
        $data->kaca_spion_kanan=$request->kaca_spion_kanan;
        $data->dongkrak=$request->dongkrak;
        $data->stang_kabin=$request->stang_kabin;
        $data->ganjal_ban=$request->ganjal_ban;
        $data->kunci_roda=$request->kunci_roda;
        $data->hammer=$request->hammer;
        $data->kotak_pppk=$request->kotak_pppk;
        $data->hose_25=$request->hose_25;
        $data->hose_15=$request->hose_15;
        $data->nozzle_15_js=$request->nozzle_15_js;
        $data->nozzle_15_akron=$request->nozzle_15_akron;
        $data->nozzle_15_jet=$request->nozzle_15_jet;
        $data->y_valve=$request->y_valve;
        $data->red_25=$request->red_25;
        $data->nozzle_foam=$request->nozzle_foam;
        $data->kunci_hydrant=$request->kunci_hydrant;
        $data->hose_section=$request->hose_section;
        $data->kunci_hose=$request->kunci_hose;
        $data->apar=$request->apar;
        $data->kapak=$request->kapak;
        $data->linggis=$request->linggis;
        $data->tangga_ganda=$request->tangga_ganda;
        $data->senter=$request->senter;
        $data->gantol=$request->gantol;
        $data->timba=$request->timba;
        $data->karung=$request->karung;
        $data->jacket_pmk=$request->jacket_pmk;
        $data->helm_pmk=$request->helm_pmk;
        $data->sirine=$request->sirine;
        $data->acsesoris=$request->acsesoris;
        $data->pic_badge='-';
        $data->pic_name='Administrator';

        $sum=0;
        if($request->level_bbm == '3/4' || $request->level_bbm == 'F'){
          $sum=$sum+1;
        }
        if($request->working_pressure < 20 || 30 < $request->working_pressure){
          $sum=$sum+1;
        }
        if($request->hour_meter_mesin < 4 || 5 < $request->hour_meter_mesin){
          $sum=$sum+1;
        }
        if($request->hour_meter_pompa < 3 || 4 < $request->hour_meter_pompa){
          $sum=$sum+1;
        }
        if($request->oli_mesin == 'H'){
          $sum=$sum+1;
        }
        if($request->oli_rem == 'H'){
          $sum=$sum+1;
        }
        if($request->oli_power == 'H'){
          $sum=$sum+1;
        }
        if($request->air_radiator == '3/4' || $request->air_radiator == 'F'){
          $sum=$sum+1;
        }
        if($request->air_cad_radiator == '3/4' || $request->air_cad_radiator == 'F'){
          $sum=$sum+1;
        }
        if($request->air_wiper == '3/4' || $request->air_wiper == 'F'){
          $sum=$sum+1;
        }
        if($request->ban_depan_kiri < 30 || 40 < $request->ban_depan_kiri){
          $sum=$sum+1;
        }
        if($request->ban_depan_kanan < 30 || 40 < $request->ban_depan_kanan){
          $sum=$sum+1;
        }
        if($request->ban_belakang_kiri_dalam < 30 || 40 < $request->ban_belakang_kiri_dalam){
          $sum=$sum+1;
        }
        if($request->ban_belakang_kiri_luar < 30 || 40 < $request->ban_belakang_kiri_luar){
          $sum=$sum+1;
        }
        if($request->ban_belakang_kanan_dalam < 30 || 40 < $request->ban_belakang_kanan_dalam){
          $sum=$sum+1;
        }
        if($request->ban_belakang_kanan_luar < 30 || 40 < $request->ban_belakang_kanan_luar){
          $sum=$sum+1;
        }
        if($request->lampu_cabin == 'MATI'){
          $sum=$sum+1;
        }
        if($request->lampu_kota == 'MATI'){
          $sum=$sum+1;
        }
        if($request->lampu_jauh == 'MATI'){
          $sum=$sum+1;
        }
        if($request->lampu_sign_kiri == 'MATI'){
          $sum=$sum+1;
        }
        if($request->lampu_sign_kanan == 'MATI'){
          $sum=$sum+1;
        }
        if($request->lampu_rem == 'MATI'){
          $sum=$sum+1;
        }
        if($request->lampu_atret == 'MATI'){
          $sum=$sum+1;
        }
        if($request->lampu_sorot == 'MATI'){
          $sum=$sum+1;
        }
        if($request->panel_dashboard == 'MATI'){
          $sum=$sum+1;
        }
        if($request->kaca_spion_kiri == 'PECAH'){
          $sum=$sum+1;
        }
        if($request->kaca_spion_kanan == 'PECAH'){
          $sum=$sum+1;
        }
        if($request->dongkrak == 'TIDAK'){
          $sum=$sum+1;
        }
        if($request->stang_kabin == 'TIDAK'){
          $sum=$sum+1;
        }
        if($request->ganjal_ban == 'TIDAK'){
          $sum=$sum+1;
        }
        if($request->kunci_roda == 'TIDAK'){
          $sum=$sum+1;
        }
        if($request->hammer == 'TIDAK'){
          $sum=$sum+1;
        }
        if($request->kotak_pppk == 'TIDAK'){
          $sum=$sum+1;
        }
        if($request->sirine == 'MATI'){
          $sum=$sum+1;
        }
        if($request->acsesoris == 'MATI'){
          $sum=$sum+1;
        }
        if($request->hose_25 < 1){
          $sum=$sum+1;
        }
        if($request->hose_15 < 1){
          $sum=$sum+1;
        }
        if($request->nozzle_15_js < 1){
          $sum=$sum+1;
        }
        if($request->nozzle_15_akron < 1){
          $sum=$sum+1;
        }
        if($request->nozzle_15_jet < 1){
          $sum=$sum+1;
        }
        if($request->y_valve < 1){
          $sum=$sum+1;
        }
        if($request->red_25 < 1){
          $sum=$sum+1;
        }
        if($request->nozzle_foam < 1){
          $sum=$sum+1;
        }
        if($request->kunci_hydrant < 2){
          $sum=$sum+1;
        }
        if($request->hose_section < 1){
          $sum=$sum+1;
        }
        if($request->kunci_hose < 2){
          $sum=$sum+1;
        }
        if($request->apar < 5){
          $sum=$sum+1;
        }
        if($request->kapak < 1){
          $sum=$sum+1;
        }
        if($request->linggis < 1){
          $sum=$sum+1;
        }
        if($request->tangga_ganda < 1){
          $sum=$sum+1;
        }
        if($request->senter < 3){
          $sum=$sum+1;
        }
        if($request->gantol < 3){
          $sum=$sum+1;
        }
        if($request->timba < 5){
          $sum=$sum+1;
        }
        if($request->karung < 5){
          $sum=$sum+1;
        }
        if($request->jacket_pmk < 5){
          $sum=$sum+1;
        }
        if($request->helm_pmk < 5){
          $sum=$sum+1;
        }

        $dataVehicle = ListVehicle::find($id);
        $dataVehicle->type = $dataVehicle->type;
        $dataVehicle->vehicle_code = $dataVehicle->vehicle_code;
        $dataVehicle->vehicle_merk = $dataVehicle->vehicle_merk;
        $dataVehicle->tahun = $dataVehicle->tahun;
        $dataVehicle->vehicle_type = $dataVehicle->vehicle_type;
        $dataVehicle->nomor_rangka = $dataVehicle->nomor_rangka;
        $dataVehicle->company = $dataVehicle->company;
        $dataVehicle->plant = $dataVehicle->plant;
        $dataVehicle->image = $dataVehicle->image;
        $dataVehicle->last_inspection = $request->report_date;
        $exp = date('Y-m-d', strtotime('+7 days', strtotime($request->report_date)));
        $dataVehicle->exp_inspection = $exp;
        $dataVehicle->sum_negative = $sum;
        $data->sum_negative = $sum;

        try{
            $data->save();
            $dataVehicle->save();
            return "
            <div class='alert alert-success'>Inspection PMK Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Inspection PMK Failed! Inspection PMK not saved!</div>";
        }
    }

    public function historyInspectionPMK(Request $request)
    {
        $id = $request->input('id');
        $listVehicle = ListVehicle::find($id);
        $inspectionPMK = InspectionPMK::where('vehicle_id', 'LIKE', '%' .$id. '%')->get();

        $params =[
            'title' => 'Manajemen Inspection Pickup',
            'listVehicle' => $listVehicle,
            'inspectionPMK' => $inspectionPMK,
        ];
        return view('firesystem/unitCheck/historyInspectionPMK', $params);
    }

    public function deleteInspectionPMK(Request $request){

        $id = intval($request->input('id',0));
        try{
            InspectionPMK::find($id)->delete();
            return "
            <div class='alert alert-success'>Inspection PMK Remove Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        }catch(\Exception $ex){
            return "<div class='alert alert-danger'>Remove Failed! Inspection PMK not removed!</div>";
        }
    }
}
