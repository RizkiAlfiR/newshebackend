<?php

namespace App\Http\Controllers\Backend\FireSystem;

use Illuminate\Http\Request;
use App\Models\FireSystem\InspectionAlarm\LokasiAlarm;
use App\Models\FireSystem\InspectionAlarm\ParameterPengecekanAlarm;
use App\Models\FireSystem\InspectionAlarm\InspectionAlarm;
use App\Models\FireSystem\GlobalList\InspectionArea;
use App\Models\GlobalList\Plant;
use App\Models\GlobalList\UnitKerja;

class InspectionAlarmController extends Controller
{
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $lokasiAlarm=LokasiAlarm::all();
        $parameterPengecekanAlarm=ParameterPengecekanAlarm::all();
        $inspectionAlarm=InspectionAlarm::all();
        $params=[
             'lokasiAlarm'=>$lokasiAlarm,
             'inspectionAlarm'=>$inspectionAlarm,
             'parameterPengecekanAlarm'=>$parameterPengecekanAlarm,
             'title'=>'Alarm'
        ];

        return view('firesystem/inspectionAlarm/index', $params);
    }

    public function formMaster(Request $request)
    {
        $id = $request->input('id');
        $inspectionArea = InspectionArea::all();
        $unitKerja = UnitKerja::all();
        $plant = Plant::all();
        if($id){
            $data = LokasiAlarm::find($id);
        }else{
            $data = new LokasiAlarm();
        }
        $params =[
            'title' => 'Manajemen Lokasi Alarm',
            'data' => $data,
            'inspectionArea' => $inspectionArea,
            'unitKerja' => $unitKerja,
            'plant' => $plant,
        ];
        return view('firesystem/inspectionAlarm/formMaster', $params);
    }

    public function saveMaster(Request $request)
    {
        $id = intval($request->input('id',0));

        if($id){
            $data = LokasiAlarm::find($id);
        }else{
            $data = new LokasiAlarm();
        }

        $data->area = $request->area;
        $data->company = $request->company;
        $data->plant = $request->plant;
        $data->unit_kerja = $request->unit_kerja;
        $data->qr_code = $data->area;
        //Image::make('public')->put($data->area.".png", base64_decode(DNS2D::getBarcodePNG($data->area, "QRCODE")));
        //echo '<img src="data:image/png;base64,' . $data->qr_code . '" alt="barcode"   />';
        try{
            $data->save();
            return "
            <div class='alert alert-success'>Location Alarm Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Location Alarm Failed! Location Alarm not saved!</div>";
        }
    }

    public function deleteMaster(Request $request){

        $id = intval($request->input('id',0));
        try{
            LokasiAlarm::find($id)->delete();
            return "
            <div class='alert alert-success'>Location Alarm Remove Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        }catch(\Exception $ex){
            return "<div class='alert alert-danger'>Remove Failed! Location Alarm not removed!</div>";
        }
    }

    public function formParameter(Request $request)
    {
        $type=["WEEK", "MONTH"];
        $id = $request->input('id');
        if($id){
            $data = ParameterPengecekanAlarm::find($id);
        }else{
            $data = new ParameterPengecekanAlarm();
        }
        $params =[
            'title' => 'Manajemen Parameter Alarm',
            'data' => $data,
            'type' => $type,
        ];
        return view('firesystem/inspectionAlarm/formParameter', $params);
    }

    public function saveParameter(Request $request)
    {
        $id = intval($request->input('id',0));

        if($id){
            $data = ParameterPengecekanAlarm::find($id);
        }else{
            $data = new ParameterPengecekanAlarm();
        }

        $data->parameter = $request->parameter;
        $data->type = $request->type;

        try{
            $data->save();
            return "
            <div class='alert alert-success'>Parameter Alarm Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Parameter Alarm Failed! Parameter Alarm not saved!</div>";
        }
    }

    public function deleteParameter(Request $request){

        $id = intval($request->input('id',0));
        try{
            ParameterPengecekanAlarm::find($id)->delete();
            return "
            <div class='alert alert-success'>Inspection Parameter Remove Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        }catch(\Exception $ex){
            return "<div class='alert alert-danger'>Remove Failed! Inspection Parameter not removed!</div>";
        }
    }

    public function formInspectionAlarm(Request $request)
    {
        $type=["WEEK", "MONTH"];
        $lokasiAlarm = LokasiAlarm::all();
        $id = $request->input('id');
        if($id){
            $data = InspectionAlarm::find($id);
        }else{
            $data = new InspectionAlarm();
        }
        $params =[
            'title' => 'Manajemen Parameter Alarm',
            'data' => $data,
            'type' => $type,
            'lokasiAlarm' => $lokasiAlarm,
        ];
        return view('firesystem/inspectionAlarm/formInspectionAlarm', $params);
    }

    public function deleteHistoryInspectionAlarm(Request $request){

        $id = intval($request->input('id',0));
        try{
            InspectionAlarm::find($id)->delete();
            return "
            <div class='alert alert-success'>Inspection Alarm Remove Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        }catch(\Exception $ex){
            return "<div class='alert alert-danger'>Remove Failed! Inspection Alarm not removed!</div>";
        }
    }

    public function historyInspectionAlarm(Request $request)
    {
        $id = $request->input('id');

        $lokasiAlarm = LokasiAlarm::find($id);
        $inspectionAlarm = InspectionAlarm::where('id_alarm', 'LIKE', '%' .$id. '%')->get();

        $params =[
            'title' => 'Manajemen Inspection Alarm',
            'lokasiAlarm' => $lokasiAlarm,
            'inspectionAlarm' => $inspectionAlarm,
        ];
        return view('firesystem/inspectionAlarm/historyInspectionAlarm', $params);
    }


}
