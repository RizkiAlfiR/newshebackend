<?php

namespace App\Http\Controllers\Backend\FireSystem;

use Illuminate\Http\Request;
use App\Models\FireSystem\InspectionApar\LokasiApar;
use App\Models\FireSystem\InspectionApar\InspectionApar;
use App\Models\FireSystem\GlobalList\InspectionArea;
use App\Models\FireSystem\GlobalList\UnitKerja;
use App\Models\GlobalList\Plant;

class InspectionAparController extends Controller
{
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $lokasiApar=LokasiApar::all();
        $inspectionApar=InspectionApar::all();
        $params=[
             'lokasiApar'=>$lokasiApar,
             'inspectionApar'=>$inspectionApar,
             'title'=>'Apar'
        ];

        return view('firesystem/inspectionApar/index', $params);
    }

    public function formMaster(Request $request)
    {
        $id = $request->input('id');
        $inspectionArea = InspectionArea::all();
        $plant = Plant::all();
        $unitKerja = UnitKerja::all();
        if($id){
            $data = LokasiApar::find($id);
        }else{
            $data = new LokasiApar();
        }
        $params =[
            'title' => 'Manajemen Lokasi Apar',
            'data' => $data,
            'inspectionArea' => $inspectionArea,
            'plant' => $plant,
            'unitKerja' => $unitKerja,
        ];
        return view('firesystem/inspectionApar/formMaster', $params);
    }

    public function saveMaster(Request $request)
    {
        $id = intval($request->input('id',0));

        if($id){
            $data = LokasiApar::find($id);
        }else{
            $data = new LokasiApar();
        }
        $data->unit_kerja = $request->unit_kerja;
        $data->area = $request->area;
        $data->company = $request->company;
        $data->plant = $request->plant;
        $data->qr_code = $data->area;
        $data->last_inspection = $data->last_inspection;
        $data->exp_inspection = $data->exp_inspection;
        //Image::make('public')->put($data->area.".png", base64_decode(DNS2D::getBarcodePNG($data->area, "QRCODE")));
        //echo '<img src="data:image/png;base64,' . $data->qr_code . '" alt="barcode"   />';
        try{
            $data->save();
            return "
            <div class='alert alert-success'>Location Apar Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Location Apar Failed! Location Apar not saved!</div>";
        }
    }

    public function deleteMaster(Request $request){

        $id = intval($request->input('id',0));
        try{
            LokasiApar::find($id)->delete();
            return "
            <div class='alert alert-success'>Location Apar Remove Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        }catch(\Exception $ex){
            return "<div class='alert alert-danger'>Remove Failed! Location Apar not removed!</div>";
        }
    }

    public function formInspectionApar(Request $request)
    {
        $id = $request->input('id');
        if($id){
            $data = LokasiApar::find($id);
        }else{
            $data = new LokasiApar();
        }

        $params =[
            'title' => 'Manajemen Lokasi Apar',
            'data' => $data,
        ];
        return view('firesystem/inspectionApar/formInspectionApar', $params);
    }

    public function saveInspectionApar(Request $request)
    {
        $id = intval($request->input('id',0));

        $data = new InspectionApar();
        $data->id_apar = $id;
        $data->inspection_date = $request->inspection_date;
        $data->status_cond = $request->status_cond;
        $data->klem = $request->klem;
        $data->hose = $request->hose;
        $data->seal = $request->seal;
        $data->sapot = $request->sapot;
        $data->press = $request->press;
        $data->weight = $request->weight;
        $data->note = $request->note;
        $data->pic_badge = '-';
        $data->pic_name = 'Administrator';

        $dataLokasi = LokasiApar::find($id);
        $dataLokasi->unit_kerja = $dataLokasi->unit_kerja;
        $dataLokasi->area = $dataLokasi->area;
        $dataLokasi->company = $dataLokasi->company;
        $dataLokasi->plant = $dataLokasi->plant;
        $dataLokasi->qr_code = $dataLokasi->area;
        $dataLokasi->last_inspection = $request->inspection_date;
        $exp = date('Y-m-d', strtotime('+7 days', strtotime($request->inspection_date)));
        $dataLokasi->exp_inspection = $exp;

        try{
            $data->save();
            $dataLokasi->save();
            return "
            <div class='alert alert-success'>Inspection Apar Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Inspection Apar Failed! Inspection Apar not saved!</div>";
        }
    }

    public function historyInspectionApar(Request $request)
    {
        $id = $request->input('id');

        $lokasiApar = LokasiApar::find($id);
        $inspectionApar = InspectionApar::where('id_apar', 'LIKE', '%' .$id. '%')->get();

        $params =[
            'title' => 'Manajemen Inspection Apar',
            'lokasiApar' => $lokasiApar,
            'inspectionApar' => $inspectionApar,
        ];
        return view('firesystem/inspectionApar/historyInspectionApar', $params);
    }

    public function deleteHistory(Request $request){

        $id = intval($request->input('id',0));
        try{
            InspectionApar::find($id)->delete();
            return "
            <div class='alert alert-success'>Inspection Apar Remove Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        }catch(\Exception $ex){
            return "<div class='alert alert-danger'>Remove Failed! Inspection Apar not removed!</div>";
        }
    }

    public function updateInspectionApar(Request $request)
    {
        $id = $request->input('id');
        if($id){
            $data = InspectionApar::find($id);
            $dataLokasi = LokasiApar::find($data->id_apar);
        }else{
            $data = new InspectionApar();
        }
        $params =[
            'title' => 'Manajemen Inspection Apar',
            'data' => $data,
            'dataLokasi' => $dataLokasi,
        ];
        return view('firesystem/inspectionApar/updateInspectionApar', $params);
    }

    public function saveUpdateInspectionApar(Request $request)
    {
        $id = intval($request->input('id',0));

        if($id){
            $data = InspectionApar::find($id);
        }else{
            $data = new InspectionApar();
        }

        $data->id_apar = $request->id_apar;
        $data->inspection_date = $request->inspection_date;
        $data->status_cond = $request->status_cond;
        $data->klem = $request->klem;
        $data->hose = $request->hose;
        $data->seal = $request->seal;
        $data->sapot = $request->sapot;
        $data->press = $request->press;
        $data->weight = $request->weight;
        $data->note = $request->note;
        if($id){
          $data->pic_badge = $request->pic_badge;
          $data->pic_name = $request->pic_name;
        }else{
          $data->pic_badge = '-';
          $data->pic_name = 'Administrator';
        }
        try{
            $data->save();
            return "
            <div class='alert alert-success'>Inspection Apar Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Inspection Apar Failed! Inspection Apar not saved!</div>";
        }
    }
}
