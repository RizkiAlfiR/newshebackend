<?php

namespace App\Http\Controllers\Backend\APD;

use Illuminate\Http\Request;
use App\Models\APD\OrderAPD;
use App\Models\APD\PinjamAPD;
use App\Models\APD\DetailOrder;
use App\Models\APD\DetailPinjam;

use Illuminate\Support\Facades\Input;
use Image;

class ReturnAPDController extends Controller
{
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $returnAPD=PinjamAPD::where('status', 'release')
        ->where('cek_status',1)
        ->get();

        $telatAPD=PinjamAPD::where('status', 'release')
        ->where('cek_status',2)->get();

        $params=[
             'returnAPD'=>$returnAPD,
             'telatAPD'=>$telatAPD,
             'title'=>'Order APD'
        ];

        return view('apd/returnAPD/index', $params);
    }

    public function showPinjam(Request $request){
        $idPinjam = $request->input('id');

        $pinjamAPD = pinjamAPD::where('id', $idPinjam)->get();
        $detailPinjam = DetailPinjam::where('kode_pinjam', $idPinjam)->get();
        $params=[
             'pinjamAPD'=>$pinjamAPD,
             'detailPinjam'=>$detailPinjam,
             'title'=>'Detail Pinjam untuk Return'
        ];
        return view('apd/returnAPD/showPinjam', $params);

    }

    public function return(Request $request)
    {
        $id = intval($request->input('id',0));
        $retur_at=$request->retur_at;
        $status=$request->status;

        // $cekEmployee=User::where('no_badge', $no_badge)->first();

        $data = PinjamAPD::find($id);  
        // dd($data);
        $data->retur_at=date('Y-m-d H:i:s');
        $data->status="return";

        $data->save();

        $dataPinjam = DetailPinjam::where('kode_pinjam', $data->id)->update([
            'retur_at'=>$data->retur_at,
            'status'=>$data->status
        ]);

        try{
            return "
            <div class='alert alert-success'>Return APD Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Return APD is Failed!</div>";
        }
    }
}