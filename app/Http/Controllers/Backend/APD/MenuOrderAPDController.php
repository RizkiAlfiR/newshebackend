<?php

namespace App\Http\Controllers\Backend\APD;

use Illuminate\Http\Request;

class MenuOrderAPDController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('apd\menuOrderAPD');
    }


}
