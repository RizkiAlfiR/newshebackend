<?php

namespace App\Http\Controllers\Backend\APD;

use Illuminate\Http\Request;
use App\Models\GlobalList\Plant;
use App\Models\GlobalList\MasterCategory;
use App\Models\APD\MasterAPD;
use App\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Image;

class StockAPDController extends Controller
{
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $pelindungKepala=MasterAPD::where('group_code', '100')->get();
        $pelindungMata=MasterAPD::where('group_code', '200')->get();
        $pelindungTelinga=MasterAPD::where('group_code', '300')->get();
        $pelindungHidung=MasterAPD::where('group_code', '400')->get();
        $pelindungTangan=MasterAPD::where('group_code', '500')->get();
        $pelindungBadan=MasterAPD::where('group_code', '600')->get();
        $pelindungKaki=MasterAPD::where('group_code', '700')->get();
        $fullBodyHarness=MasterAPD::where('group_code', '800')->get();
        $paketObatP3K=MasterAPD::where('group_code', '900')->get();
        $lainlain=MasterAPD::where('group_code', '910')->get();

        $params=[
             'pelindungKepala'=>$pelindungKepala,
             'pelindungMata'=>$pelindungMata,
             'pelindungTelinga'=>$pelindungTelinga,
             'pelindungHidung'=>$pelindungHidung,
             'pelindungTangan'=>$pelindungTangan,
             'pelindungBadan'=>$pelindungBadan,
             'pelindungKaki'=>$pelindungKaki,
             'fullBodyHarness'=>$fullBodyHarness,
             'paketObatP3K'=>$paketObatP3K,
             'lainlain'=>$lainlain,
             'title'=>'Stock APD'
        ];

        return view('apd/stockAPD/index', $params);
    }

    public function formMaster(Request $request)
    {
        $id = $request->input('id');
        $masterAPD = MasterAPD::all();
        $plant = Plant::all();
        $mastercategory = MasterCategory::all();
        if($id){
            $data = MasterAPD::find($id);
        }else{
            $data = new MasterAPD();
        }
        $params =[
            'title' => 'Master APD',
            'data' => $data,
            'plant' => $plant,
            'mastercategory' => $mastercategory,
        ];
        return view('apd/stockAPD/formMaster', $params);
    }

    public function saveMaster(Request $request)
    {
        $id = intval($request->input('id',0));

        if($id){
            $data = MasterAPD::find($id);
            $data->code_comp = $request->code_comp;
            $data->code_plant = $request->code_plant;
            $data->group_code = $request->group_code;
            $data->nama = $request->nama;
            $data->merk = $request->merk;
            $data->size = $request->size;
            $data->stok = $request->stok;
            $data->kode = $request->kode;
            $data->a_stok = $request->stok;
            $data->create_at=date('Y-m-d H:i:s');
            $data->name_origin = $request->name_origin;
            $data->masa_text = $request->masa_text;
            if(!is_null($data->foto_bef)){
                if(is_null($request->file('foto_bef')) && $request->file('foto_bef')==''){
                    $fileFotoBef=$data->foto_bef;
                }else{
                    $rules=[
                        'foto_bef' => 'mimes:jpeg,bmp,png,gif,jpg,pdf',
                    ];
                    $validator=Validator::make($request->all(),$rules);
                    if($validator->fails()){
                        return "<div class='alert alert-danger'>Terjadi kesalahan!Format tidak valid!</div>";
                    }else{
                        $fotoBef=$request->file('foto_bef');
                        $destinationPath = 'public/uploads/APD/'.$request->foto_bef_id;
                        if (!file_exists($destinationPath)) {
                            File::makeDirectory($destinationPath, $mode = 0777, true, true);
                        }
                        $fileFotoBef = date('YmdHis') . '_' . $fotoBef->getClientOriginalName();
                        $fotoBef->move($destinationPath, $fileFotoBef);
                    }
                }
            }
    
            if(is_null($data->foto_bef)){
                if(is_null($request->file('foto_bef')) && $request->file('foto_bef')==''){
                    $fileFotoBef=$data->foto_bef;
                }else{
                    $rules=[
                        'foto_bef' => 'mimes:jpeg,bmp,png,gif,jpg,pdf',
                    ];
                    $validator=Validator::make($request->all(),$rules);
                    if($validator->fails()){
                        return "<div class='alert alert-danger'>Terjadi kesalahan!Format tidak valid!</div>";
                    }else{
                        $fotoBef=$request->file('foto_bef');
                        $destinationPath = 'public/uploads/APD/'.$request->foto_bef_id;
                        if (!file_exists($destinationPath)) {
                            File::makeDirectory($destinationPath, $mode = 0777, true, true);
                        }
                        $fileFotoBef= date('YmdHis') . '_' . $fotoBef->getClientOriginalName();
                        $fotoBef->move($destinationPath, $fileFotoBef);
                    }
                }
            }

            $data->foto_bef = $fileFotoBef;

            // if ($request->hasFile('foto_bef')) {
            //     $image_tmp = Input::file('foto_bef');
            //     if ($image_tmp->isValid()) {
            //         $extension = $image_tmp->getClientOriginalExtension();
            //         $filename = rand(111, 99999).'.'.$extension;
            //         $image_save = 'public/uploads/APD/' .$filename;

            //         Image::make($image_tmp)->save($image_save);
            //         $data->foto_bef = $filename;
            //     }
            // }
        }else{
            $data = new MasterAPD();
            $data->code_comp = $request->code_comp;
            $data->code_plant = $request->code_plant;
            $data->group_code = $request->group_code;
            $data->nama = $request->nama;
            $data->merk = $request->merk;
            $data->size = $request->size;
            $data->stok = $request->stok;
            $data->kode = $request->group_code."-".$data->id;
            $data->a_stok = $request->stok;
            $data->create_at=date('Y-m-d H:i:s');
            $data->name_origin = $request->name_origin;
            $data->masa_text = $request->masa_text;
            $data->foto_bef = $request->foto_bef;

            if ($request->hasFile('foto_bef')) {
                $image_tmp = Input::file('foto_bef');
                if ($image_tmp->isValid()) {
                    $extension = $image_tmp->getClientOriginalExtension();
                    $filename = rand(111, 99999).'.'.$extension;
                    $image_save = 'public/uploads/APD/' .$filename;

                    Image::make($image_tmp)->save($image_save);
                    $data->foto_bef = $filename;
                }
            }

            $data->save();

            $nilai = substr($data->id, 0);

            $kode_apd = (int) $nilai;

            $auto_kode = $request->group_code."-".str_pad($kode_apd, 6, "0",  STR_PAD_LEFT);
            $data->kode = $auto_kode;
        }

        try{
            $data->save();
            return "
            <div class='alert alert-success'>Input New Master APD Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Input New APD is Failed! Master APD not saved!</div>";
        }
    }

    public function deleteMaster(Request $request){

        $id = intval($request->input('id',0));
        try{
            MasterAPD::find($id)->delete();
            return "
            <div class='alert alert-success'>Master APD Remove Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        }catch(\Exception $ex){
            return "<div class='alert alert-danger'>Remove Failed! Master APD not removed!</div>";
        }
    }

    public function percentage(){
        
        $masterApd=MasterAPD::select('group_name')->groupby('group_name')->get();
        $currentData=[];
        foreach($masterApd as $item){
            $sumStock=MasterAPD::where('group_name',$item->group_name)->sum('stok');
            $currentData[]=[
                'name'=>$item->group_name,
                'y'=>(int)$sumStock
            ];
        }

        $params=[
            'data' => $currentData
        ];

        return response()->json($params);
    }

    public function percentageView(){

        $masterApd=MasterAPD::select('group_name')->groupby('group_name')->get();
        $currentData=[];
        foreach($masterApd as $item){
            $sumStock=MasterAPD::where('group_name',$item->group_name)->sum('stok');
            $currentData[]=[
                'name'=>$item->group_name,
                'y'=>(int)$sumStock
            ];
        }

        $params =[
            'data' => $currentData
        ];

        return view ('apd.stockAPD.percentageAPD', $params);
    }

    // public function percentageTabel(){
    //     $masterApd=MasterAPD::select('group_name')->groupby('group_name')->get();
    //     $currentData=[];
    //     foreach($masterApd as $item){
    //         $sumStock=MasterAPD::where('group_name',$item->group_name)->sum('stok');
    //         $currentData[]=[
    //             'name'=>$item->group_name,
    //             'y'=>(int)$sumStock
    //         ];
    //     }

    //     $params =[
    //         'data' => $currentData
    //     ];

    //     return view('apd/stockAPD/percentageAPD', $params);

    // }
}