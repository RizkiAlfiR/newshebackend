<?php

namespace App\Http\Controllers\Backend\APD;

use Illuminate\Http\Request;
use App\Models\APD\OrderAPD;
use App\Models\APD\PinjamAPD;
use App\Models\APD\DetailOrder;
use App\Models\APD\DetailPinjam;
use Illuminate\Support\Facades\Input;
use Image;

class OrderAPDController extends Controller
{
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $orderPersonal=OrderAPD::where('individu', '1')->get();
        $orderUnit=OrderAPD::where('unit', '1')->get();
        $orderPinjam=PinjamAPD::all();

        $params=[
             'orderPersonal'=>$orderPersonal,
             'orderUnit'=>$orderUnit,
             'orderPinjam'=>$orderPinjam,
             'title'=>'Order APD'
        ];

        return view('apd/orderAPD/index', $params);
    }

    public function formOrderPersonal(Request $request)
    {
        $id = $request->input('id');
        $orderPersonal= OrderAPD::all();
        if($id){
            $data = OrderAPD::find($id);
        }else{
            $data = new OrderAPD();
        }
        $params =[
            'title' => 'Order Baru',
            'data' => $data,
            'orderPersonal' => $orderPersonal,
        ];
        return view('apd/orderAPD/formOrderPersonal', $params);
    }

    public function jumlahOrder(){
        
        $orderPersonal=DetailOrder::select('kode_apd')->groupby('kode_apd')->where('keterangan', 'Permintaan Personal')->get();
        $currentDataPersonal=[];
        foreach($orderPersonal as $item){
            $sumJumlahPersonal=DetailOrder::where('kode_apd',$item->kode_apd)->where('keterangan', 'Permintaan Personal')->sum('jumlah');
            $currentDataPersonal[]=[
                'name'=>$item->kode_apd,
                'y'=>(int)$sumJumlahPersonal
            ];
        }

        $orderUnit=DetailOrder::select('kode_apd')->groupby('kode_apd')->where('keterangan', 'Permintaan Unit Kerja')->get();
        $currentDataUnit=[];
        foreach($orderUnit as $item){
            $sumJumlahUnit=DetailOrder::where('kode_apd',$item->kode_apd)->where('keterangan', 'Permintaan Unit Kerja')->sum('jumlah');
            $currentDataUnit[]=[
                'name'=>$item->kode_apd,
                'y'=>(int)$sumJumlahUnit
            ];
        }

        $orderPinjam=DetailPinjam::select('kode_apd')->groupby('kode_apd')->get();
        $currentDataPinjam=[];
        foreach($orderPinjam as $item){
            $sumJumlahPinjam=DetailPinjam::where('kode_apd',$item->kode_apd)->sum('jumlah');
            $currentDataPinjam[]=[
                'name'=>$item->kode_apd,
                'y'=>(int)$sumJumlahPinjam
            ];
        }

        $params=[
            'personal' => $currentDataPersonal,
            'unit' => $currentDataUnit,
            'pinjam' => $currentDataPinjam
        ];

        return response()->json($params);
    }

    public function jumlahOrderView(){

        $orderPersonal=DetailOrder::select('kode_apd')->groupby('kode_apd')->where('keterangan', 'Permintaan Personal')->get();
        $currentDataPersonal=[];
        foreach($orderPersonal as $item){
            $sumJumlahPersonal=DetailOrder::where('kode_apd',$item->kode_apd)->where('keterangan', 'Permintaan Personal')->sum('jumlah');
            $currentDataPersonal[]=[
                'name'=>$item->kode_apd,
                'y'=>(int)$sumJumlahPersonal
            ];
        }

        $orderUnit=DetailOrder::select('kode_apd')->groupby('kode_apd')->where('keterangan', 'Permintaan Unit Kerja')->get();
        $currentDataUnit=[];
        foreach($orderUnit as $item){
            $sumJumlahUnit=DetailOrder::where('kode_apd',$item->kode_apd)->where('keterangan', 'Permintaan Unit Kerja')->sum('jumlah');
            $currentDataUnit[]=[
                'name'=>$item->kode_apd,
                'y'=>(int)$sumJumlahUnit
            ];
        }

        $orderPinjam=DetailPinjam::select('kode_apd')->groupby('kode_apd')->get();
        $currentDataPinjam=[];
        foreach($orderPinjam as $item){
            $sumJumlahPinjam=DetailPinjam::where('kode_apd',$item->kode_apd)->sum('jumlah');
            $currentDataPinjam[]=[
                'name'=>$item->kode_apd,
                'y'=>(int)$sumJumlahPinjam
            ];
        }

        $params=[
            'personal' => $currentDataPersonal,
            'unit' => $currentDataUnit,
            'pinjam' => $currentDataPinjam
        ];

        return view ('apd.orderAPD.index', $params);
    }

    public function pengaduan(Request $request)
    {  
        $dataRusak=DetailOrder::where('status', 'release')->where('keterangan', 'Permintaan Personal')->whereNotNull('foto')->get();
        $dataHilang=DetailOrder::where('status', 'release')->where('keterangan', 'Permintaan Personal')->whereNotNull('file')->get();

        $params=[
            'dataRusak'=>$dataRusak,
            'dataHilang'=>$dataHilang,
            'title'=>'Order APD'
       ];

       //dd($params);
       return view('apd/pengaduanAPD/index', $params);
    }
}
