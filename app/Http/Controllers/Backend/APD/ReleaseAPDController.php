<?php

namespace App\Http\Controllers\Backend\APD;

use Illuminate\Http\Request;
use App\Models\APD\OrderAPD;
use App\Models\APD\PinjamAPD;
use App\Models\APD\DetailOrder;
use App\Models\APD\DetailPinjam;

use Illuminate\Support\Facades\Input;
use Image;

class ReleaseAPDController extends Controller
{
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $releasePersonal=OrderAPD::whereNotNull('approve2_by')->where('individu', '1')->whereNull('release_at')->get();
        $releaseUnit=OrderAPD::whereNotNull('approve2_by')->where('unit', '1')->whereNull('release_at')->get();
        $releasePinjam=PinjamAPD::whereNotNull('approve2_by')->whereNull('release_at')->get();

        $params=[
             'releasePersonal'=>$releasePersonal,
             'releaseUnit'=>$releaseUnit,
             'releasePinjam'=>$releasePinjam,
             'title'=>'Order APD'
        ];

        return view('apd/releaseAPD/index', $params);
    }

    public function showOrder(Request $request){
        $idOrder = $request->input('id');

        $orderAPD = OrderAPD::where('id', $idOrder)->get();
        $detailOrder = DetailOrder::where('kode_order', $idOrder)->get();
        $params=[
             'orderAPD'=>$orderAPD,
             'detailOrder'=>$detailOrder,
             'title'=>'Detail Order untuk Release'
        ];
        return view('apd/releaseAPD/showOrder', $params);

    }

    public function showPinjam(Request $request){
        $idPinjam = $request->input('id');

        $pinjamAPD = pinjamAPD::where('id', $idPinjam)->get();
        $detailPinjam = DetailPinjam::where('kode_pinjam', $idPinjam)->get();
        $params=[
             'pinjamAPD'=>$pinjamAPD,
             'detailPinjam'=>$detailPinjam,
             'title'=>'Detail Pinjam untuk Release'
        ];
        return view('apd/releaseAPD/showPinjam', $params);

    }

    public function release(Request $request)
    {
        $id = intval($request->input('id',0));
        $release_at=$request->release_at;
        $status=$request->status;

        // $cekEmployee=User::where('no_badge', $no_badge)->first();

        $data = OrderAPD::find($id);  
        // dd($data);
        $data->release_at=date('Y-m-d H:i:s');
        $data->status="release";

        $data->save();

        $dataOrder = DetailOrder::where('kode_order', $data->id)->update([
            'release_at'=>$data->release_at,
            'status'=>$data->status
        ]);


        // $data->save();
        // dd($data);

        // $data->create_by=$cekEmployee->username;
        
        //Image::make('public')->put($data->area.".png", base64_decode(DNS2D::getBarcodePNG($data->area, "QRCODE")));
        //echo '<img src="data:image/png;base64,' . $data->qr_code . '" alt="barcode"   />';
        try{
            return "
            <div class='alert alert-success'>Release APD Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Release APD is Failed!</div>";
        }
    }

    public function releasePinjam(Request $request)
    {
        $id = intval($request->input('id',0));
        $release_at=$request->release_at;
        $status=$request->status;

        // $cekEmployee=User::where('no_badge', $no_badge)->first();

        $data = PinjamAPD::find($id);  
        // dd($data);
        $data->release_at=date('Y-m-d H:i:s');
        $data->status="release";

        $data->save();

        $dataPinjam = DetailPinjam::where('kode_pinjam', $data->id)->update([
            'release_at'=>$data->release_at,
            'status'=>$data->status
        ]);


        // $data->save();
        // dd($data);

        // $data->create_by=$cekEmployee->username;
        
        //Image::make('public')->put($data->area.".png", base64_decode(DNS2D::getBarcodePNG($data->area, "QRCODE")));
        //echo '<img src="data:image/png;base64,' . $data->qr_code . '" alt="barcode"   />';
        try{
            return "
            <div class='alert alert-success'>Release APD Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Release APD is Failed!</div>";
        }
    }
}