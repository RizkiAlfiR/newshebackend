<?php

namespace App\Http\Controllers\Backend\APD;

use Illuminate\Http\Request;
use App\Models\APD\OrderAPD;
use App\Models\APD\PinjamAPD;

class MenuAPDController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $releasePersonal=OrderAPD::whereNotNull('approve2_by')->where('individu', '1')->whereNull('release_at')->count();
        $releaseUnit=OrderAPD::whereNotNull('approve2_by')->where('unit', '1')->whereNull('release_at')->count();
        $releasePinjam=PinjamAPD::whereNotNull('approve2_by')->whereNull('release_at')->count();

        $jumlahRelease = $releasePersonal + $releasePinjam + $releaseUnit;
        $jumlahReturn = PinjamAPD::where('status', 'release')->where('cek_status',1)->count();
        $jumlahReturnTelat = PinjamAPD::where('status', 'release')->where('cek_status',2)->count();

        $params = [
            'jumlahRelease'=>$jumlahRelease,
            'jumlahReturn'=>$jumlahReturn,
            'jumlahReturnTelat'=>$jumlahReturnTelat
        ];

        return view('apd.menuAPD', $params);
    }


}
