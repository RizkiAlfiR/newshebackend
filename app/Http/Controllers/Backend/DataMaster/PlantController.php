<?php

namespace App\Http\Controllers\Backend\DataMaster;

use Illuminate\Http\Request;
use App\Models\GlobalList\Plant;

class PlantController extends Controller
{
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $plant=Plant::all();
        return view('dataMaster/plant/index', ['plant' => $plant]);
    }

    public function delete(Request $request){

        $id = intval($request->input('id',0));
        try{
            Plant::find($id)->delete();
            return "
            <div class='alert alert-success'>Plant Remove Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        }catch(\Exception $ex){
            return "<div class='alert alert-danger'>Remove Failed! Plant not removed!</div>";
        }
    }
}
