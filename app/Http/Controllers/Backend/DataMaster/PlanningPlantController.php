<?php

namespace App\Http\Controllers\Backend\DataMaster;

use Illuminate\Http\Request;
use App\Models\GlobalList\PPlant;
use Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class PlanningPlantController extends Controller
{
    public function readItems(Request $req) {
        $data = PPlant::all ();
        return view ( 'dataMaster.planningplant.index' )->withData ( $data );
    }

    public function addMaster(Request $request)
    {
        $id = $request->input('id');
        if($id) {
            $data = PPlant::find($id);
        } else
        {
            $data = new PPlant();
        }
        $params =[
            'title' => 'Manajemen Planning Plant',
            'data' => $data,
        ];
        return view('dataMaster.planningplant.formMaster', $params);
    }

    public function saveMaster(Request $request)
    {
        $id = intval($request->input('id',0));

        if($id) {
            $data = PPlant::find($id);
        } else
        {
            $data = new PPlant();
            // $checkData = PPlant::where(['pplantdesc' => $request->pplantdesc])->first();
            // if($checkData){
            //     return "<div class='alert alert-danger'>Data sudah tersedia!</div>";
            // }
        }

        $data->plant = $request->plant;
        $data->pplant = $request->pplant;
        $data->pplantdesc = $request->pplantdesc;
        $data->rnum = 0;
        
        try {
            $data->save();
            return "
            <div class='alert alert-success'> Add Planning Plant Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Add Planning Plant Failed! Planning Plant not saved!</div>";
        }
    }

    public function deleteMaster(Request $request)
    {
        $id = intval($request->input('id', 0));
        $data = PPlant::find($id);

        try {
            $data->delete();
            return "
            <div class='alert alert-success'>Planning Plant Remove Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch(\Exception $ex){
            return "<div class='alert alert-danger'>Remove Failed! Planning Plant not removed!</div>";
        }
    }
}
