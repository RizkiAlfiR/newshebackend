<?php

namespace App\Http\Controllers\Backend\DataMaster;

use Illuminate\Http\Request;
use App\Models\GlobalList\News;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;

class NewsController extends Controller
{
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = News::all();
        $params=[
             'data'=>$data,
             'title'=>'News'
        ];

        return view('dataMaster/news/index', $params);
    }

    public function form(Request $request)
    {
        $id = $request->input('id');
        if($id){
            $data = News::find($id);
        }else{
            $data = new News();
        }
        $params =[
            'title' =>'Manajemen News',
            'data' =>$data,
        ];
        return view('dataMaster/news/form', $params);
    }

    public function save(Request $request)
    {
        $id = intval($request->input('id',0));
        if($id){
            $data = News::find($id);
        }else{
            $data = new News();
        }

        $kal="Belajar Dasar PHP";
        $sub = substr($kal, 0, 5);

        $data->head = $request->head;
        $data->content = $request->content;
        $data->head_title = substr($request->head, 0, 30);
        $data->content_title = substr($request->content, 0, 50);

        $data->writer = 'Administrator';
        if($id){
          $data->date=$data->date;
          $data->time=$data->time;
        } else {
          $data->date = date('Y-m-d');
          $data->time = date('H:s:i');
        }

        if(!is_null($data->image)){
            if(is_null($request->file('image')) && $request->file('image')==''){
                $fileImage=$data->image;
            }else{
                $rules=[
                    'image' => 'mimes:jpeg,bmp,png,gif,jpg,pdf',
                ];
                $validator=Validator::make($request->all(),$rules);
                if($validator->fails()){
                    return "<div class='alert alert-danger'>Terjadi kesalahan!Format tidak valid!</div>";
                }else{
                    $fotoImage=$request->file('image');
                    $destinationPath = 'public/uploads/News/'.$request->image_id;
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $fileImage = date('YmdHis') . '_' . $fotoImage->getClientOriginalName();
                    $fotoImage->move($destinationPath, $fileImage);
                }

            }
        }

        if(is_null($data->image)){
            if(is_null($request->file('image')) && $request->file('image')==''){
                $fileImage=$data->image;
            }else{
                $rules=[
                    'image' => 'mimes:jpeg,bmp,png,gif,jpg,pdf',
                ];
                $validator=Validator::make($request->all(),$rules);
                if($validator->fails()){
                    return "<div class='alert alert-danger'>Terjadi kesalahan!Format tidak valid!</div>";
                }else{
                    $fotoImage=$request->file('image');
                    $destinationPath = 'public/uploads/News/'.$request->image_id;
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $fileImage = date('YmdHis') . '_' . $fotoImage->getClientOriginalName();
                    $fotoImage->move($destinationPath, $fileImage);
                }

            }
        }

        $data->image = $fileImage;

        try{
            $data->save();
            return "
            <div class='alert alert-success'>News Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>News Failed!News not saved!</div>";
        }
    }

    public function delete(Request $request){

        $id = intval($request->input('id',0));
        try{
            News::find($id)->delete();
            return "
            <div class='alert alert-success'>News Remove Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        }catch(\Exception $ex){
            return "<div class='alert alert-danger'>Remove Failed! News not removed!</div>";
        }

    }
}
