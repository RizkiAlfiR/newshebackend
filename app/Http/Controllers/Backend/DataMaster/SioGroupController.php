<?php

namespace App\Http\Controllers\Backend\DataMaster;

use Illuminate\Http\Request;
use App\Models\GlobalList\SioGroup;
use Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class SioGroupController extends Controller
{
    public function readItems(Request $req) {
        $data = SioGroup::all();
        return view ( 'dataMaster.siogroup.index' )->withData ( $data );
    }

    public function addMaster(Request $request)
    {
        $id = $request->input('id');
        if($id) {
            $data = SioGroup::find($id);
        } else
        {
            $data = new SioGroup();
        }
        $params =[
            'title' => 'Manajemen Sio Group',
            'data' => $data,
        ];
        return view('dataMaster.siogroup.formMaster', $params);
    }

    public function saveMaster(Request $request)
    {
        $id = intval($request->input('id', 0));

        if($id) {
            $data = SioGroup::find($id);
        } else
        {
            $data = new SioGroup();
            $checkData = SioGroup::where(['NAME' => $request->NAME])->first();
            if($checkData){
                return "<div class='alert alert-danger'>Data sudah tersedia!</div>";
            }
        }

        $data->NICKNAME = $request->NICKNAME;
        $data->NAME = $request->NAME;
        
        try {
            $data->save();
            return "
            <div class='alert alert-success'> Add Sio Group Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Add Sio Group Failed! Sio Group not saved!</div>";
        }
    }

    public function deleteMaster(Request $request)
    {
        $id = intval($request->input('id', 0));
        $data = SioGroup::find($id);

        try {
            $data->delete();
            return "
            <div class='alert alert-success'>Sio Group Remove Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch(\Exception $ex){
            return "<div class='alert alert-danger'>Remove Failed! Sio Group not removed!</div>";
        }
    }

}
