<?php

namespace App\Http\Controllers\Backend\DataMaster;

use Illuminate\Http\Request;
use App\Models\GlobalList\Vendor;
use Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class DataVendorController extends Controller
{
    public function readItems(Request $req) {
        $data = Vendor::all();
        return view ( 'dataMaster.datavendor.index' )->withData ( $data );
    }

    public function addMaster(Request $request)
    {
        $id = $request->input('id');

        if($id) {
            $data = Vendor::find($id);
        } else
        {
            $data = new Vendor();
        }
        $params =[
            'title' => 'Manajemen Vendor',
            'data' => $data,
        ];
        return view('dataMaster.datavendor.formMaster', $params);
    }

    public function saveMaster(Request $request)
    {
        $id = intval($request->input('id', 0));

        if($id) {
            $data = Vendor::find($id);
        } else
        {
            $data = new Vendor();
            $checkData = Vendor::where(['NAME1' => $request->NAME1])->first();
            if($checkData){
                return "<div class='alert alert-danger'>Data sudah tersedia!</div>";
            }
        }

        $data->LIFNR = $request->LIFNR;
        $data->NAME1 = $request->NAME1;
        $data->KATEGORI = $request->KATEGORI;
        $data->ALAMAT = $request->ALAMAT;
        $data->EMAIL = $request->EMAIL;
        $data->PASS = null;
        $data->TYV = $request->TYV;
        $data->FLAG = 1;
        $data->KETERANGAN = $request->KETERANGAN;
        $data->KTOKK = 1100;
        $data->rnum = 200;
        
        try {
            $data->save();
            return "
            <div class='alert alert-success'> Add Vendor Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Add Vendor Failed! Vendor not saved!</div>";
        }
    }

    public function editItem(Request $request)
    {
        // $id = intval($request->input('id', 0));

        // if($id) {
        //     $data = Vendor::find($id);
        // } else
        // {
        //     $data = new Vendor();
        //     $checkData = Vendor::where(['NAME1' => $request->NAME1])->first();
        //     if($checkData){
        //         return "<div class='alert alert-danger'>Data sudah tersedia!</div>";
        //     }
        // }

        $data->LIFNR = $request->LIFNR;
        $data->NAME1 = $request->NAME1;
        $data->KATEGORI = $request->KATEGORI;
        $data->ALAMAT = $request->ALAMAT;
        $data->EMAIL = $request->EMAIL;
        $data->PASS = null;
        $data->TYV = $request->TYV;
        $data->FLAG = 1;
        $data->KETERANGAN = $request->KETERANGAN;
        $data->KTOKK = 1100;
        $data->rnum = 200;
        
        try {
            $data->update();
            return "
            <div class='alert alert-success'> Update Vendor Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Update Vendor Failed! Vendor not saved!</div>";
        }
    }

    public function deleteMaster(Request $request)
    {
        $id = intval($request->input('id', 0));
        $data = Vendor::find($id);

        try {
            $data->delete();
            return "
            <div class='alert alert-success'>Vendor Remove Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch(\Exception $ex){
            return "<div class='alert alert-danger'>Remove Failed! Vendor not removed!</div>";
        }
    }
}
