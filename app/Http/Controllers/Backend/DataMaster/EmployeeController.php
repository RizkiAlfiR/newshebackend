<?php

namespace App\Http\Controllers\Backend\DataMaster;

use Illuminate\Http\Request;
use App\Models\GlobalList\Employee;

class EmployeeController extends Controller
{
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $employee=Employee::all();
        return view('dataMaster/employee/index', ['employee' => $employee]);
    }

    public function delete(Request $request){

        $id = intval($request->input('id',0));
        try{
            Employee::find($id)->delete();
            return "
            <div class='alert alert-success'>Employe Remove Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        }catch(\Exception $ex){
            return "<div class='alert alert-danger'>Remove Failed! Employe not removed!</div>";
        }
    }
}
