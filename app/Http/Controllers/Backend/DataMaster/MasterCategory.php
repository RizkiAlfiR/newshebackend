<?php

namespace App\Http\Controllers\Backend\DataMaster;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MasterCategoryController extends Controller
{
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // mengambil data dari table accident report
        $mastercategory = DB::table('apd_master_category')->get();

    	   // mengirim data accident ke view index
        return view('master/mastercategory', ['mastercategory' => $mastercategory]);
    }

    public function create()
    {
        return view('accidentreportcreate');
    }

    // public function store(Request $request)
    // {
    //     // insert data ke table pegawai
    //     DB::table('accident_reports')->insert([
    //         'badge_vict' => $request->badge_vict,
    //     ]);
    //     // alihkan halaman ke halaman pegawai
    //     return redirect('/accidentreport');
    // }
}
