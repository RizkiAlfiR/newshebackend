<?php

namespace App\Http\Controllers\Backend\DataMaster;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\GlobalList\ReportCategory;
use Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class ReportCategoryController extends Controller
{
    public function readItems(Request $req) {
        $data = ReportCategory::all();
        return view ( 'dataMaster.reportcategory.index' )->withData ( $data );
    }

    public function addMaster(Request $request)
    {
        $id = $request->input('id');
        if($id) {
            $data = ReportCategory::find($id);
        } else
        {
            $data = new ReportCategory();
        }
        $params =[
            'title' => 'Manajemen Report Category',
            'data' => $data,
        ];
        return view('dataMaster.reportcategory.formMaster', $params);
    }

    public function saveMaster(Request $request)
    {
        $id = intval($request->input('id', 0));

        if($id) {
            $data = ReportCategory::find($id);
        } else
        {
            $data = new ReportCategory();
            $checkData = ReportCategory::where(['DESKRIPSI' => $request->DESKRIPSI])->first();
            if($checkData){
                return "<div class='alert alert-danger'>Data sudah tersedia!</div>";
            }
        }

        $data->KODE = $request->KODE;
        $data->DESKRIPSI = $request->DESKRIPSI;
        $data->CREATE_AT = null;
        $data->CREATE_BY = null;
        $data->UPDATE_AT = null;
        $data->UPDATE_BY = null;
        $data->DELETE_AT = null;
        $data->DELETE_BY = null;
        $data->COMPANY = 5000;
        $data->PLANT = 5001;
        $data->KODE_EN = $request->KODE_EN;
        
        try {
            $data->save();
            return "
            <div class='alert alert-success'> Add Report Category Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Add Report Category Failed! Report Category not saved!</div>";
        }
    }

    public function deleteMaster(Request $request)
    {
        $id = intval($request->input('id', 0));
        $data = ReportCategory::find($id);

        try {
            $data->delete();
            return "
            <div class='alert alert-success'>Report Category Remove Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch(\Exception $ex){
            return "<div class='alert alert-danger'>Remove Failed! Report Category not removed!</div>";
        }
    }
}
