<?php

namespace App\Http\Controllers\Backend\DataMaster;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\GlobalList\FuncLocation;
use App\Models\GlobalList\PPlant;
use Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class FuncLocationController extends Controller
{
    public function readItems(Request $req) {
        $data = FuncLocation::all();
        return view ( 'dataMaster.funcloc.index' )->withData ( $data );
    }

    public function addMaster(Request $request)
    {
        $id = $request->input('id');
        $data_pplant = PPlant::all();

        if($id) {
            $data = FuncLocation::find($id);
        } else
        {
            $data = new FuncLocation();
        }
        $params =[
            'title' => 'Manajemen Functional Location',
            'data' => $data,
            'data_pplant' => $data_pplant
        ];
        return view('dataMaster.funcloc.formMaster', $params);
    }

    public function saveMaster(Request $request)
    {
        $id = intval($request->input('id', 0));

        if($id) {
            $data = FuncLocation::find($id);
        } else
        {
            $data = new FuncLocation();
            $checkData = FuncLocation::where(['idequpment' => $request->idequpment])->first();
            if($checkData){
                return "<div class='alert alert-danger'>Data sudah tersedia!</div>";
            }
        }

        $data->idequpment = $request->idequpment;
        $data->equptname = $request->equptname;
        $data->sapequipnum = $request->idequpment;
        $data->equpmentcode = $request->equpmentcode;
        $data->pplant = $request->pplant;
        $data->status = $request->status;
        $data->funcloc = $request->funcloc;
        $data->mplant = $request->mplant;
        $data->rnum = 200;
        
        try {
            $data->save();
            return "
            <div class='alert alert-success'> Add Functional Location Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Add Functional Location Failed! Functional Location not saved!</div>";
        }
    }

    public function deleteMaster(Request $request)
    {
        $id = intval($request->input('id', 0));
        $data = FuncLocation::find($id);

        try {
            $data->delete();
            return "
            <div class='alert alert-success'>Functional Location Remove Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch(\Exception $ex){
            return "<div class='alert alert-danger'>Remove Failed! Functional Location not removed!</div>";
        }
    }
}
