<?php

namespace App\Http\Controllers\API\APD;

use App\Models\APD\OrderAPD;
use App\Models\APD\MasterAPD;
use App\Models\APD\DetailOrder;
use App\Models\APD\Wishlist;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use App\Http\Controllers\Controller;

class ApiOrderAPDController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }

    public function index(Request $request)
    {
        $data=OrderAPD::with('getDetailOrder')->get();

        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Order Item List Found',
            'data' => $data
        ], 200);
    }

    public function personal(Request $request)
    {
        $data=OrderAPD::where('individu', 1)->with('getDetailOrder')->get();

        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Order Item List Found',
            'data' => $data
        ], 200);
    }

    public function unitkerja(Request $request)
    {
        $data=OrderAPD::where('unit', 1)->with('getDetailOrder')->get();

        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Order Item List Found',
            'data' => $data
        ], 200);
    }


    public function getDataId(Request $request)
    {
        $apiName='DETAILWITHID';
        $id_number = $request->id;

        if($id_number){
            $data =OrderAPD::with('getDetailOrder')->where('id', $id_number)->get( );
            return response()->json([
                'success' => true,
                'status' => 200,
                'message' => 'Order Item List Found',
                'data' => $data
            ], 200);

          } else {
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Item list with id ' . $id_number . ' cannot be found',
                'data'=>null
            ], 400);
        }

    }

    public function addOrderPersonal(Request $request)
    {
        $apiName='ORDERPERSONALCONTROLLER';
        $kode_order=$request->kode_order;
        $no_badge=$request->no_badge;
        $kode_uk=$request->kode_uk;
        $uk_text=$request->uk_text;
        $approve1_at=$request->approve1_at;
        $approve1_by=$request->approve1_by;
        $approve2_at=$request->approve2_at;
        $approve2_by=$request->approve2_by;
        $create_at=$request->create_at;
        $create_by=$request->create_by;
        $update_at=$request->update_at;
        $update_by=$request->update_by;
        $code_comp=$request->code_comp;
        $code_plant=$request->code_plant;
        $name=$request->name;
        $closed_at=$request->closed_at;
        $closed_by=$request->closed_by;
        $updated_at=$request->updated_at;
        $created_at=$request->created_at;
        $status=$request->status;
        $reject_at=$request->reject_at;
        $reject_by=$request->reject_by;
        $note_reject=$request->note_reject;
        $keterangan=$request->keterangan;
        $inidvidu=$request->individu;
        $unit=$request->unit;


        $cekEmployee=User::where('no_badge', $no_badge)->first();

        try {
            $data = new OrderAPD();

            $data->kode_order="RID-".$request->id;
            $data->no_badge=$cekEmployee->no_badge;
            $data->kode_uk=$cekEmployee->uk_kode;
            $data->uk_text=$cekEmployee->unit_kerja;
            $data->approve1_at=$approve1_at;
            $data->approve1_by=$approve1_by;
            $data->approve2_at=$approve2_at;
            $data->approve2_by=$approve2_by;
            $data->create_at=date('Y-m-d H:i:s');
            $data->create_by=$cekEmployee->username;
            $data->update_at=$update_at;
            $data->update_by=$update_by;
            $data->code_comp="5000";
            $data->code_plant=$cekEmployee->plant;
            $data->name=$cekEmployee->name;
            $data->closed_at=$closed_at;
            $data->closed_by=$closed_by;
            $data->updated_at=$updated_at;
            $data->created_at=$created_at;
            $data->status=$status;
            $data->reject_at=$reject_at;
            $data->reject_by=$reject_by;
            $data->note_reject=$note_reject;
            $data->keterangan="Permintaan Personal";
            $data->individu="1";
            $data->unit="0";


            $data->save();

            $nilai = substr($data->id, 0);
            // dd($nilai);
            $kode = (int) $nilai;
            // dd($data->id);

            $auto_kode = "RID-" .str_pad($kode, 5, "0",  STR_PAD_LEFT);
            $data->kode_order = $auto_kode;
            $data->save();

            $detail = json_decode($request->detail);
            // dd($detail);
            $currentParams=[];

            foreach($detail as $key => $value){
                $dataMaster = MasterAPD::where('kode', $value->kode_apd)->first();

                $currentParams[] = [
                    'kode_order' => $data->id,
                    'kode_apd' => $value->kode_apd,
                    'jumlah' => $value->jumlah,
                    'files' => $dataMaster->foto_bef,
                    'masa_exp' => $dataMaster->masa_exp,
                    'create_at' => $data->create_at,
                    'create_by' => $data->create_by,
                    'code_comp' => $data->code_comp,
                    'code_plant' => $data->code_plant,
                    'keterangan' => $data->keterangan,
                    'created_at' => date("Y-m-d"),
                    'updated_at' => date("Y-m-d"),
                    'nomor_order' => $data->kode_order,
                    'nama' => $data->name,
                    'unit' =>$data->uk_text,
                    'merk' =>$dataMaster->merk,
                    'nama_apd' => $dataMaster->nama,
                    'status' => $data->status,
                    'no_badge' => $data->no_badge
                ];
            }

            DetailOrder::insert($currentParams);
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];
            return response()->json($params);
        } catch (Exception $e) {

            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
        }
    }

    public function addOrderUnitKerja(Request $request)
    {
        $apiName='ORDERUNITCONTROLLER';
        $kode_order=$request->kode_order;
        $no_badge=$request->no_badge;
        $kode_uk=$request->kode_uk;
        $uk_text=$request->uk_text;
        $approve1_at=$request->approve1_at;
        $approve1_by=$request->approve1_by;
        $approve2_at=$request->approve2_at;
        $approve2_by=$request->approve2_by;
        $create_at=$request->create_at;
        $create_by=$request->create_by;
        $update_at=$request->update_at;
        $update_by=$request->update_by;
        $code_comp=$request->code_comp;
        $code_plant=$request->code_plant;
        $name=$request->name;
        $closed_at=$request->closed_at;
        $closed_by=$request->closed_by;
        $updated_at=$request->updated_at;
        $created_at=$request->created_at;
        $status=$request->status;
        $reject_at=$request->reject_at;
        $reject_by=$request->reject_by;
        $note_reject=$request->note_reject;
        $keterangan=$request->keterangan;
        $individu=$request->individu;
        $unit=$request->unit;


        $cekEmployee=User::where('no_badge', $no_badge)->first();

        try {
            $data = new OrderAPD();

            $data->kode_order="RUK-".$request->id;
            $data->no_badge=$cekEmployee->no_badge;
            $data->kode_uk=$cekEmployee->uk_kode;
            $data->uk_text=$cekEmployee->unit_kerja;
            $data->approve1_at=$approve1_at;
            $data->approve1_by=$approve1_by;
            $data->approve2_at=$approve2_at;
            $data->approve2_by=$approve2_by;
            $data->create_at=date('Y-m-d H:i:s');
            $data->create_by=$cekEmployee->username;
            $data->update_at=$update_at;
            $data->update_by=$update_by;
            $data->code_comp="5000";
            $data->code_plant=$cekEmployee->plant;
            $data->name=$cekEmployee->name;
            $data->closed_at=$closed_at;
            $data->closed_by=$closed_by;
            $data->updated_at=$updated_at;
            $data->created_at=$created_at;
            $data->status=$status;
            $data->reject_at=$reject_at;
            $data->reject_by=$reject_by;
            $data->note_reject=$note_reject;
            $data->keterangan="Permintaan Unit Kerja";
            $data->individu="0";
            $data->unit="1";

            $data->save();
            $nilai = substr($data->id, 0);
            // dd($nilai);
            $kode = (int) $nilai;
            // dd($data->id);
            //tambahkan sebanyak + 1

            $auto_kode = "RUK-" .str_pad($kode, 5, "0",  STR_PAD_LEFT);
            $data->kode_order = $auto_kode;
            $data->save();

            $detail = json_decode($request->detail);
            // dd($detail);
            $currentParams=[];

            foreach($detail as $key => $value){
                $dataMaster = MasterAPD::where('kode', $value->kode_apd)->first();

                $currentParams[] = [
                    'kode_order' => $data->id,
                    'kode_apd' => $value->kode_apd,
                    'jumlah' => $value->jumlah,
                    'files' => $dataMaster->foto_bef,
                    'masa_exp' => $dataMaster->masa_exp,
                    'create_at' => $data->create_at,
                    'create_by' => $data->create_by,
                    'code_comp' => $data->code_comp,
                    'code_plant' => $data->code_plant,
                    'keterangan' => $data->keterangan,
                    'created_at' => date("Y-m-d"),
                    'updated_at' => date("Y-m-d"),
                    'nomor_order' => $data->kode_order,
                    'nama' => $data->name,
                    'unit' =>$data->uk_text,
                    'merk' =>$dataMaster->merk,
                    'nama_apd' => $dataMaster->nama,
                    'status' => $data->status,
                    'no_badge' => $data->no_badge
                ];
            }

            DetailOrder::insert($currentParams);
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];
            return response()->json($params);
        } catch (Exception $e) {

            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
        }
    }

    public function wishlist(Request $request)
    {
        $apiName='ORDERUNITCONTROLLER';
        $no_badge=$request->no_badge;
        $kode_apd=$request->kode_apd;
        $masterAPD = MasterAPD::where('kode', $kode_apd)->first();

        $stok=$masterAPD->a_stok - 1;

        MasterAPD::where('kode', $kode_apd)->update(['a_stok' => $stok]);
        $name_apd=$masterAPD->nama;
        $jumlah=$request->jumlah;
        $status=$request->status;

        $cekEmployee=User::where('no_badge', $no_badge)->first();

        try {
            $data = new Wishlist();
            $data->no_badge=$cekEmployee->no_badge;
            $data->kode_apd=$kode_apd;
            $data->name_apd=$name_apd;
            $data->jumlah=$jumlah;
            $data->status=$status;

            $data->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];
            return response()->json($params);
        } catch (Exception $e) {

            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
        }
    }

    public function showWishlist(Request $request)
    {
      $apiName='MASTERAPD';
      $badge=null;
      $status=null;
      $badge = $request->badge;
      $status = $request->status;
      if($badge) {
          $data = Wishlist::where('no_badge', 'LIKE', '%' .$badge. '%')->where('status', 'LIKE', '%' .$status. '%')->get( );
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];
      } else {
              $data = Wishlist::all();
              $params = [
                  'is_success' => true,
                  'status' => 200,
                  'message' => 'success',
                  'data' => $data
              ];
        }
        return response()->json($params);
    }

    public function deleteWishlist(Request $request){
        $apiName='DELETE_WISHLIST';
        $id=$request->id;

        try {
            Wishlist::find($id)->delete();
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
            ];
            return response()->json($params);

        } catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404, $e->getMessage(),json_encode($sendingParams));
        }
    }

    public function cancelWishlist(Request $request){
        $apiName='CANCEL_WISHLIST';
        $id=$request->id;
        $kode_apd=$request->kode_apd;
        $masterAPD = MasterAPD::where('kode', $kode_apd)->first();

        $stok=$masterAPD->a_stok + 1;

        MasterAPD::where('kode', $kode_apd)->update(['a_stok' => $stok]);

        try {
            Wishlist::find($id)->delete();
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
            ];
            return response()->json($params);

        } catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404, $e->getMessage(),json_encode($sendingParams));
        }
    }

    public function minusMaster(Request $request){
        $apiName='CANCEL_WISHLIST';
        $kode_apd=$request->kode_apd;
        $minus=$request->minus;
        $masterAPD = MasterAPD::where('kode', $kode_apd)->first();

        $stok=$masterAPD->a_stok - $minus;

        try {
            MasterAPD::where('kode', $kode_apd)->update(['a_stok' => $stok]);
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
            ];
            return response()->json($params);

        } catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404, $e->getMessage(),json_encode($sendingParams));
        }

    }

}
