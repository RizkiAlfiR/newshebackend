<?php

namespace App\Http\Controllers\API\APD;

use App\Models\APD\MasterAPD;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use App\Http\Controllers\Controller;

class ApiMasterAPDController extends Controller
{
    // protected $user;
    //
    // public function __construct()
    // {
    //     $this->user = JWTAuth::parseToken()->authenticate();
    // }

    public function index(Request $request)
    {
        $apiName='MASTERAPD';
        $search=null;
        $groupAPD=null;
        $searchApd = $request->search;
        $groupAPD = $request->groupAPD;
        if($searchApd){
            $data = MasterAPD::where('code_plant', 'LIKE', '%' .$searchApd. '%')->get( );
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

        } else if($groupAPD) {
            $data = MasterAPD::where('group_code', 'LIKE', '%' .$groupAPD. '%')->get( );
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];
        } else {
                // $data = MasterAPD::all();
                $data=MasterAPD::select('name_origin')->groupby('name_origin')->get();
                $params = [
                    'is_success' => true,
                    'status' => 200,
                    'message' => 'success',
                    'data' => $data
                ];
          }
          return response()->json($params);

    }

    public function getApdPlant(Request $request)
    {
        $plant=$request->code_plant;
        $data=MasterAPD::where('code_plant', $plant)->get();
        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Order Item List Found',
            'data' => $data
        ], 200);
    }

    public function addMaster(Request $request)
    {
        $apiName='ADDMASTERAPDCONTROLLER';
        $kode=$request->kode;
        $nama=$request->nama;
        $merk=$request->merk;
        $stok=$request->stok;
        $create_at=$request->create_at;
        $create_by=$request->create_by;
        $update_at=$request->update_at;
        $update_by=$request->update_by;
        $code_comp=$request->code_comp;
        $code_plant=$request->code_plant;
        $a_stok=$request->a_stok;
        $size=$request->size;
        $individu=$request->individu;
        $uk=$request->uk;
        $pinjam=$request->pinjam;
        $masa_exp=$request->masa_exp;
        $max_order=$request->max_order;
        $masa_text=$request->masa_text;
        $foto_bef=$request->foto_bef;
        $name_origin=$request->name_origin;
        $order_group=$request->order_group;
        $group_code=$request->group_code;
        $group_name=$request->group_name;
        $order=$request->order;

        $cekEmployee=User::where('no_badge', $create_by)->first();

        $sendingParams = [
            'kode' => $kode,
            'nama' => $nama,
            'merk' => $merk,
            'stok' => $stok,
            'create_at' => $create_at,
            'create_by' => $cekEmployee,
            'update_at' => $update_at,
            'update_by' => $update_by,
            'code_comp' => $code_comp,
            'code_plant' => $code_plant,
            'a_stok' => $a_stok,
            'size' => $size,
            'individu' => $individu,
            'uk' => $uk,
            'pinjam' => $pinjam,
            'masa_exp' => $masa_exp,
            'max_order' => $max_order,
            'masa_text' => $masa_text,
            'foto_bef' => $foto_bef,
            'name_origin' => $name_origin,
            'order_group' => $order_group,
            'group_code' => $group_code,
            'group_name'=> $group_name,
            'order' => $order
        ];

        try {
            $data = new MasterAPD();
            $data->kode=$kode;
            $data->nama=$nama;
            $data->merk=$merk;
            $data->stok=$stok;
            $data->create_at=date('Y-m-d H:i:s');
            $data->create_by=$cekEmployee->username;
            $data->update_at=$update_at;
            $data->update_by=$update_by;
            $data->code_comp=$code_comp;
            $data->code_plant=$code_plant;
            $data->a_stok=$a_stok;
            $data->size=$size;
            $data->individu=$individu;
            $data->uk=$uk;
            $data->pinjam=$pinjam;
            $data->masa_exp=$masa_exp;
            $data->max_order=$max_order;
            $data->masa_text=$masa_text;
            $data->foto_bef=$foto_bef;
            $data->name_origin=$name_origin;
            $data->order_group=$order_group;
            $data->group_code=$group_code;
            $data->group_name=$group_name;
            $data->order=$order;

            $data->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        } catch (Exception $e) {

            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
        }

        return response()->json($params);
    }

    public function updateMaster(Request $request)
    {
        $apiName='UPDATEMASTERAPDCONTROLLER';
        $id=$request->id;
        $kode=$request->kode;
        $nama=$request->nama;
        $merk=$request->merk;
        $stok=$request->stok;
        $update_at=$request->update_at;
        $update_by=$request->update_by;
        $code_comp=$request->code_comp;
        $code_plant=$request->code_plant;
        $a_stok=$request->a_stok;
        $size=$request->size;
        $individu=$request->individu;
        $uk=$request->uk;
        $pinjam=$request->pinjam;
        $masa_exp=$request->masa_exp;
        $max_order=$request->max_order;
        $masa_text=$request->masa_text;
        $filename=null;
        $foto_bef=$request->file('foto_bef');
        if($foto_bef){
            $destinationPath = 'images/';
            $filename = $foto_bef->getClientOriginalName();
            $foto_bef->move($destinationPath, $filename);
        } else {
            $filename=null;
        }
        $name_origin=$request->name_origin;
        $order_group=$request->order_group;
        $group_code=$request->group_code;
        $group_name=$request->group_name;
        $order=$request->order;

        $cekEmployee=User::where('no_badge', $update_by)->first();

        $sendingParams = [
            'id' => $id,
            'kode' => $kode,
            'nama' => $nama,
            'merk' => $merk,
            'stok' => $stok,
            'update_at' => $update_at,
            'update_by' => $cekEmployee,
            'code_comp' => $code_comp,
            'code_plant' => $code_plant,
            'a_stok' => $a_stok,
            'size' => $size,
            'individu' => $individu,
            'uk' => $uk,
            'pinjam' => $pinjam,
            'masa_exp' => $masa_exp,
            'max_order' => $max_order,
            'masa_text' => $masa_text,
            'foto_bef' => $foto_bef,
            'name_origin' => $name_origin,
            'order_group' => $order_group,
            'group_code' => $group_code,
            'group_name'=> $group_name,
            'order' => $order
        ];

        try {
            $data = MasterAPD::find($id);
            $data->kode=$kode;
            $data->nama=$nama;
            $data->merk=$merk;
            $data->stok=$stok;
            $data->update_at=date('Y-m-d H:i:s');
            $data->update_by=$cekEmployee->username;
            $data->code_comp=$code_comp;
            $data->code_plant=$code_plant;
            $data->a_stok=$a_stok;
            $data->size=$size;
            $data->individu=$individu;
            $data->uk=$uk;
            $data->pinjam=$pinjam;
            $data->masa_exp=$masa_exp;
            $data->max_order=$max_order;
            $data->masa_text=$masa_text;
            $data->foto_bef=$filename;
            $data->name_origin=$name_origin;
            $data->order_group=$order_group;
            $data->group_code=$group_code;
            $data->group_name=$group_name;
            $data->order=$order;

            $data->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        } catch (Exception $e) {

            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
        }

        return response()->json($params);
    }

    public function percentage(Request $request){
        
        $masterApd=MasterAPD::select('group_name')->groupby('group_name')->get();
        $currentData=[];
        $totalAll = MasterAPD::sum('stok');
        foreach($masterApd as $item){
            $sumStock=MasterAPD::where('group_name',$item->group_name)->sum('stok');
            $currentData[]=[
                'group_name'=>$item->group_name . ' (' . round((int)$sumStock/(int)$totalAll*100,2) . '%)',
                'stok'=>(int)$sumStock,
                // 'percentage'=>round((int)$sumStock/(int)$totalAll*100,2)
            ];
        }

        $params=[
            'data' => $currentData
        ];

        return response()->json($params);
    }

}
