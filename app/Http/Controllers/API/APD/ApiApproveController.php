<?php

namespace App\Http\Controllers\API\APD;

use App\Models\APD\OrderAPD;
use App\Models\APD\DetailOrder;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use App\Http\Controllers\Controller;

class ApiApproveController extends Controller
{
    protected $user;
 
    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }

    public function index(Request $request)
    {  
        $data=OrderAPD::whereNull('approve1_by')->whereNull('reject_by')->with('getDetailOrder')->get();
        
        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Order Item List Found',
            'data' => $data
        ], 200);
    }

    public function listPersonal(Request $request)
    { 
        $apiName='DETAILWITHID';
        $role_id = $request->role_id;
        
        if($role_id==3){
            $data=OrderAPD::whereNull('approve1_by')->whereNull('reject_by')->where('individu', 1)->with('getDetailOrder')->get();
            return response()->json([
                'success' => true,
                'status' => 200,
                'message' => 'Order Item List Found',
                'data' => $data
            ], 200);
            
          } else if($role_id==2){
            $data=OrderAPD::whereNull('approve2_by')->whereNotNull('approve1_by')->whereNull('reject_by')->where('individu', 1)->with('getDetailOrder')->get();
        
            return response()->json([
                'success' => true,
                'status' => 200,
                'message' => 'Order Item List Found',
                'data' => $data
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Item list cannot be found',
                'data'=>null
            ], 400);
        }
    }

    public function listUnit(Request $request)
    {  
        $apiName='DETAILWITHID';
        $role_id = $request->role_id;
        
        if($role_id==3){
            $data=OrderAPD::whereNull('approve1_by')->whereNull('reject_by')->where('unit', 1)->with('getDetailOrder')->get();
            return response()->json([
                'success' => true,
                'status' => 200,
                'message' => 'Order Item List Found',
                'data' => $data
            ], 200);
            
          } else if($role_id==2){
            $data=OrderAPD::whereNull('approve2_by')->whereNotNull('approve1_by')->whereNull('reject_by')->where('unit', 1)->with('getDetailOrder')->get();
        
            return response()->json([
                'success' => true,
                'status' => 200,
                'message' => 'Order Item List Found',
                'data' => $data
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Item list cannot be found',
                'data'=>null
            ], 400);
        }
    }

    public function show($id)
    {
        $order_apd = $this->user->order_apd()->find($id);
    
        if (!$order_apd) {
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Item list with id ' . $id . ' cannot be found',
                'data'=>null
            ], 400);
        }
    
        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Item list with id ' . $id . ' found',
            'data' => $order_apd
        ], 200);
    }

    public function approveOrder(Request $request)
    {
        
        $apiName='APPROVEORDERCONTROLLER';
        $id=$request->id;
        $update_by=$request->update_by;
        $status=$request->status;

        $cekEmployee=User::where('no_badge', $update_by)->first();

        try {
            $data = OrderAPD::find($id);
            if(is_null($data->approve1_at) && is_null($data->approve1_by)){
                $data->approve1_at=date('Y-m-d H:i:s');
                $data->approve1_by=$cekEmployee->username;
            }else{
                $data->approve2_at=date('Y-m-d H:i:s');
                $data->approve2_by=$cekEmployee->username;
            }
    
            $data->update_at=date('Y-m-d H:i:s');
            $data->update_by=$cekEmployee->username;
            $data->status=$status;

            $data->save();

            $dataOrder = DetailOrder::where('kode_order', $data->id)->update([
                'update_at'=>$data->update_at,
                'update_by'=>$data->update_by
            ]);
    
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        } catch (Exception $e) {

            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
        }
    }

    public function rejectOrder(Request $request)
    {
        
        $apiName='REJECTORDERCONTROLLER';
        $id=$request->id;
        $reject_by=$request->reject_by;
        $status=$request->status;
        $note_reject=$request->note_reject;

        $cekEmployee=User::where('no_badge', $reject_by)->first();

        try {
            $data = OrderAPD::find($id);
            $data->update_at=date('Y-m-d H:i:s');
            $data->update_by=$cekEmployee->username;    
            $data->reject_at=date('Y-m-d H:i:s');
            $data->reject_by=$cekEmployee->username;
            $data->status=$status;
            $data->note_reject=$note_reject;

            $data->save();

            $dataOrder = DetailOrder::where('kode_order', $data->id)->update([
                'update_at'=>$data->update_at,
                'update_by'=>$data->update_by,
                'reject_at'=>$data->reject_at,
                'reject_by'=>$data->reject_by,
                'note_reject'=>$data->note_reject
            ]);
        
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        } catch (Exception $e) {

            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
        }
    }

    public function index2(Request $request)
    {  
        $data=OrderAPD::whereNull('approve2_by')->whereNotNull('approve1_by')->whereNull('reject_by')->with('getDetailOrder')->get();
        
        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Order Item List Found',
            'data' => $data
        ], 200);
    }

    public function listPersonal2(Request $request)
    {  
        $data=OrderAPD::whereNull('approve2_by')->whereNotNull('approve1_by')->whereNull('reject_by')->where('individu', 1)->with('getDetailOrder')->get();
        
        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Order Item List Found',
            'data' => $data
        ], 200);
    }

    public function listUnit2(Request $request)
    {  
        $data=OrderAPD::whereNull('approve2_by')->whereNotNull('approve1_by')->whereNull('reject_by')->where('unit', 1)->with('getDetailOrder')->get();
        
        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Order Item List Found',
            'data' => $data
        ], 200);
    }

}
