<?php

namespace App\Http\Controllers\API\APD;

use App\Models\APD\OrderAPD;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use App\Http\Controllers\Controller;

class ApiApprove2Controller extends Controller
{
    protected $user;
 
    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }

    public function index(Request $request)
    {  
        $data=OrderAPD::whereNull('approve2_by')->with('getDetailOrder')->get();
        
        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Order Item List Found',
            'data' => $data
        ], 200);
    }

    public function show($id)
    {
        $order_apd = $this->user->order_apd()->find($id);
    
        if (!$order_apd) {
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Item list with id ' . $id . ' cannot be found',
                'data'=>null
            ], 400);
        }
    
        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Item list with id ' . $id . ' found',
            'data' => $order_apd
        ], 200);
    }

    public function approve2(Request $request)
    {
        $approveBy=$request->approve_by;
        $kodeOrder=$request->kode_order;
        if(!is_null($kodeOrder)){
            $data=OrderAPD::where('kode_order',$kodeOrder)
            ->whereNull('approve2_by')->first();
            $cekEmployee=User::where('id',$approveBy)->first();
            if($data){
                $data->approve2_at=date('Y-m-d H:i:s');
                $data->approve2_by=$cekEmployee->username;

                $data->save();

                return response()->json([
                    'success' => true,
                    'status' => 200,
                    'message' => 'Order Item List Found',
                    'data' => $data
                ], 200);
            }else{
                // data tidak ada
                return response()->json([
                    'success' => false,
                    'status' => 401,
                    'message' => 'Sorry, Item list with kode order ' . $kodeOrder . ' cannot be found',
                    'data'=>null
                ], 400);
            }
        }else{
            // id tidak ada
            return response()->json([
                'success' => false,
                'status' => 403,
                'message' => 'Sorry, id ' . $id . ' cannot be found',
                'data'=>null
            ], 402);
        }
    }

}
