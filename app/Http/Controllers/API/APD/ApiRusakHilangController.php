<?php

namespace App\Http\Controllers\API\APD;

use App\Models\APD\Release;
use App\Models\APD\MasterAPD;
use App\Models\APD\DetailOrder;
use App\Models\APD\OrderAPD;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use App\Http\Controllers\Controller;

class ApiRusakHilangController extends Controller
{
    protected $user;
 
    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }

    public function releasePersonal(Request $request)
    {  
        $data=DetailOrder::where('status', 'release')->where('keterangan', 'Permintaan Personal')->get();
        
        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Order Item List Found',
            'data' => $data
        ], 200);
    }

    public function getIdPersonal(Request $request)
    {
        $apiName='RELEASEDETAILWITHID';
        $id_number = $request->id;
        
        if($id_number){
            $data=DetailOrder::where('status', 'release')->where('keterangan', 'Permintaan Personal')->where('id', $id_number)->get();
            return response()->json([
                'success' => true,
                'status' => 200,
                'message' => 'Order Item List Found',
                'data' => $data
            ], 200);
            
          } else {
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Item list with id ' . $id_number . ' cannot be found',
                'data'=>null
            ], 400);
        }
    }

    public function addRusak(Request $request)
    {
        $apiName='RUSAKCONTROLLER';
        
        $id=$request->id;
        $filename=null;
        $foto=$request->file('foto');
        if($foto){
            $destinationPath = 'public/uploads/APD/rusak';
            $filename = $foto->getClientOriginalName();
            $foto->move($destinationPath, $filename);
        } else {
            $filename=null;
        }

        // $cekDetail=DetailOrder::where('id', $id_detail)->first();
        // // dd($cekDetail);
        // $cekMaster=MasterAPD::where('kode', $cekDetail->kode_apd)->first();
        // $cekEmployee=User::where('no_badge', $create_by)->first();

    
        try {
            $data = DetailOrder::find($id);
            $data->foto=$filename;

            $data->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        } catch (Exception $e) {

            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
        }

        return response()->json($params);
    }

    public function addHilang(Request $request)
    {
        $apiName='HILANGCONTROLLER';
        
        $id=$request->id;
        $filename=null;
        $file=$request->file;
        if($file){
            $destinationPath = 'public/uploads/APD/hilang';
            $filename = $file->getClientOriginalName();
            $file->move($destinationPath, $filename);
        } else {
            $filename=null;
        }

        // $cekDetail=DetailOrder::where('id', $id_detail)->first();
        // // dd($cekDetail);
        // $cekMaster=MasterAPD::where('kode', $cekDetail->kode_apd)->first();
        // $cekEmployee=User::where('no_badge', $create_by)->first();

    
        try {
            $data = DetailOrder::find($id);
            $data->file=$filename;

            $data->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        } catch (Exception $e) {

            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
        }

        return response()->json($params);
    }

}
