<?php

namespace App\Http\Controllers\API\APD;

use App\Models\APD\DetailOrder;
use App\Models\APD\OrderAPD;
use App\Models\APD\MasterAPD;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use App\Http\Controllers\Controller;

class ApiDetailOrderController extends Controller
{
    protected $user;
 
    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }

    public function index(Request $request)
    {
        $data=DetailOrder::with('getMasterAPD')->get();
        
        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Order Item List Found',
            'data' => $data
        ], 200);
    }

    public function getDetailId(Request $request)
    {   
        $apiName='DETAILWITHID';
        $id = $request->kode_order;
        
        if($id){
            $data = DetailOrder::with('getMasterAPD')->where('kode_order', $id)->get( );
            return response()->json([
                'success' => true,
                'status' => 200,
                'message' => 'Order Item List Found',
                'data' => $data
            ], 200);
            
          } else {
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Item list with id ' . $id . ' cannot be found',
                'data'=>null
            ], 400);
        }
    
    }

    // public function show($id)
    // {
    //     $detail_order = $this->user->detail_order()->find($id);
    
    //     if (!$detail_order) {
    //         return response()->json([
    //             'success' => false,
    //             'status' => 401,
    //             'message' => 'Sorry, Item list with id ' . $id . ' cannot be found',
    //             'data'=>null
    //         ], 400);
    //     }
    
    //     return response()->json([
    //         'success' => true,
    //         'status' => 200,
    //         'message' => 'Item list with id ' . $id . ' found',
    //         'data' => $detail_order
    //     ], 200);
    // }

    public function addDetail($kode_order, $kode_apd, Request $request)
    {
        $cekDataOrder=OrderAPD::where ('id', $kode_order)->first();
        if(!$cekDataOrder){
            // data tidak ada
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Item list with kode order ' . $kodeOrder . ' cannot be found',
                'data'=>null
            ], 400);
        }

        $cekDataMaster=MasterAPD::where('kode', $kode_apd)->first();
        if(!$cekDataMaster){
            // data tidak ada
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Item list with kode order ' . $kodeOrder . ' cannot be found',
                'data'=>null
            ], 400);
        }

        $apiName='DETAILCONTROLLER';
        $kode_apd=$cekDataMaster->kode_apd;
        $kode_order=$request->kode_order;
        $jumlah=$cekDataOrder->jumlah;
        $jumlah_apr=$request->jumlah_apr;
        $create_at=$cekDataOrder->create_at;
        $create_by=$cekDataOrder->create_by;
        $update_at=$cekDataOrder->update_at;
        $update_by=$cekDataOrder->update_by;
        $code_comp=$cekDataOrder->code_comp;
        $code_plant=$cekDataOrder->code_plant;
        $release_at=$request->release_at;
        $release_by=$request->release_by;
        $jumlah_release=$request->jumlah_release;
        // $filename=null;
        $files = $cekDataMaster->foto_bef;
        // if($files){
        //     $destinationPath = 'images/';
        //     $filename = $files->getClientOriginalName();
        //     $files->move($destinationPath, $filename);
        // } else {
        //     $filename=null;
        // }
        
        $reject_at=$request->reject_at;
        $reject_by=$request->reject_by;
        $jumlah_reject=$request->jumlah_reject;
        $note_reject=$request->note_reject;
        $masa_exp=$request->masa_exp;


        $sendingParams = [
            'kode_apd' => $kode_apd,
            'kode_order' => $kode_order,
            'jumlah' => $jumlah,
            'jumlah_apr' => $jumlah_apr,
            'create_at' => $create_at,
            'create_by' => $create_by,
            'update_at' => $update_at,
            'update_by' => $update_by,
            'code_comp' => $code_comp,
            'code_plant' => $code_plant,
            'release_at' => $release_at,
            'release_by' => $release_by,
            'jumlah_release' => $jumlah_release,
            'files' => $files,
            'reject_at' => $reject_by,
            'reject_by' => $reject_by,
            'jumlah_reject' => $jumlah_reject,
            'note_reject' => $note_reject,
            'masa_exp' => $masa_exp 
        ];

        try {
            $data = new DetailOrder();
            $data->kode_apd=$kode_apd;
            $data->kode_order=$kode_order;
            $data->jumlah=$jumlah;
            $data->jumlah_apr=$jumlah_apr;
            $data->create_at=$create_at;
            $data->create_by=$create_by;
            $data->update_at=$update_at;
            $data->update_by=$update_by;
            $data->code_comp=$code_comp;
            $data->code_plant=$code_plant;
            $data->release_at=$release_at;
            $data->release_by=$release_by;
            $data->jumlah_release=$jumlah_release;
            $data->files=$files;
            $data->reject_at=$reject_at;
            $data->reject_by=$reject_by;
            $data->jumlah_reject=$jumlah_reject;
            $data->note_reject=$note_reject;
            $data->masa_exp=$masa_exp;

            $data->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        } catch (Exception $e) {

            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
        }

        return response()->json($params);
    }

    public function updateDetail($kode_order, $kode_apd, Request $request)
    {
        $cekDataOrder=OrderAPD::where ('id', $kode_order)->first();
        if(!$cekDataOrder){
            // data tidak ada
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Item list with kode order ' . $kodeOrder . ' cannot be found',
                'data'=>null
            ], 400);
        }

        $cekDataMaster=MasterAPD::where('kode', $kode_apd)->first();
        if(!$cekDataMaster){
            // data tidak ada
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Item list with kode order ' . $kodeOrder . ' cannot be found',
                'data'=>null
            ], 400);
        }

        $apiName='UPDATECONTROLLER';
        $id=$request->id;
        $jumlah=$cekDataOrder->jumlah;
        $jumlah_apr=$request->jumlah_apr;
        $update_at=$cekDataOrder->update_at;
        $update_by=$cekDataOrder->update_by;
        $release_at=$request->release_at;
        $release_by=$request->release_by;
        $jumlah_release=$request->jumlah_release;
        $files = $cekDataMaster->foto_bef;
        
        $reject_at=$request->reject_at;
        $reject_by=$request->reject_by;
        $jumlah_reject=$request->jumlah_reject;
        $note_reject=$request->note_reject;
        $masa_exp=$request->masa_exp;


        $sendingParams = [
            'id' => $id,
            'jumlah' => $jumlah,
            'jumlah_apr' => $jumlah_apr,
            'update_at' => $update_at,
            'update_by' => $update_by,
            'release_at' => $release_at,
            'release_by' => $release_by,
            'jumlah_release' => $jumlah_release,
            'files' => $files,
            'reject_at' => $reject_by,
            'reject_by' => $reject_by,
            'jumlah_reject' => $jumlah_reject,
            'note_reject' => $note_reject,
            'masa_exp' => $masa_exp 
        ];

        try {
            $data = DetailOrder::find($id);
            $data->jumlah=$jumlah;
            $data->jumlah_apr=$jumlah_apr;
            $data->update_at=$update_at;
            $data->update_by=$update_by;
            $data->release_at=$release_at;
            $data->release_by=$release_by;
            $data->jumlah_release=$jumlah_release;
            $data->files=$files;
            $data->reject_at=$reject_at;
            $data->reject_by=$reject_by;
            $data->jumlah_reject=$jumlah_reject;
            $data->note_reject=$note_reject;
            $data->masa_exp=$masa_exp;

            $data->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        } catch (Exception $e) {

            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
        }

        return response()->json($params);
    }

}
