<?php

namespace App\Http\Controllers\API\APD;

use App\Models\APD\Release;
use App\Models\APD\MasterAPD;
use App\Models\APD\DetailOrder;
use App\Models\APD\DetailPinjam;
use App\Models\APD\OrderAPD;
use App\Models\APD\PinjamAPD;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use App\Http\Controllers\Controller;

class ApiReleaseController extends Controller
{
    protected $user;
 
    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }

    public function listPersonal(Request $request)
    {  
        $data=OrderAPD::whereNotNull('approve2_by')->where('individu', '1')->with('getDetailOrder')->get();
        
        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Order Item List Found',
            'data' => $data
        ], 200);
    }

    public function listUnitKerja(Request $request)
    {  
        $data=OrderAPD::whereNotNull('approve2_by')->where('unit', '1')->with('getDetailOrder')->get();
        
        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Order Item List Found',
            'data' => $data
        ], 200);
    }

    public function listPinjam(Request $request)
    {  
        $data=PinjamAPD::whereNotNull('approve2_by')->with('getDetailPinjam')->get();
        
        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Order Item List Found',
            'data' => $data
        ], 200);
    }

    public function show($id)
    {
        $order_apd = $this->user->order_apd()->find($id);
    
        if (!$order_apd) {
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Item list with id ' . $id . ' cannot be found',
                'data'=>null
            ], 400);
        }
    
        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Item list with id ' . $id . ' found',
            'data' => $order_apd
        ], 200);
    }

    public function addRelease(Request $request)
    {
        
        $apiName='RELEASECONTROLLER';
        $id=$request->id;
        $status=$request->status;

        // $cekEmployee=User::where('no_badge', $reject_by)->first();

        try {
            $data = OrderAPD::find($id);  
            $data->release_at=date('Y-m-d H:i:s');
            $data->status=$status;

            $data->save();

            $dataOrder = DetailOrder::where('kode_order', $data->id)->update([
                'release_at'=>$data->release_at,
                'status'=>$data->status
            ]);
        
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        } catch (Exception $e) {

            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
        }
    }

    public function addReleasePinjam(Request $request)
    {
        
        $apiName='RELEASEPINJAMCONTROLLER';
        $id=$request->id;
        $status=$request->status;

        // $cekEmployee=User::where('no_badge', $reject_by)->first();

        try {
            $data = pinjamAPD::find($id);  
            $data->release_at=date('Y-m-d H:i:s');
            $data->status=$status;

            $data->save();

            $dataOrder = DetailPinjam::where('kode_pinjam', $data->id)->update([
                'release_at'=>$data->release_at,
                'status'=>$data->status
            ]);
        
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        } catch (Exception $e) {

            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
        }
    }

    public function releaseUnit(Request $request)
    {  
        $data=DetailOrder::where('status', 'release')->where('keterangan', 'Permintaan Unit Kerja')->get();
        
        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Order Item List Found',
            'data' => $data
        ], 200);
    }

    public function releasePinjam(Request $request)
    {  
        $data=DetailPinjam::where('status', 'release')->get();
        
        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Order Item List Found',
            'data' => $data
        ], 200);
    }
    
}
