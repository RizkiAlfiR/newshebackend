<?php

namespace App\Http\Controllers\API\APD;

use App\Models\APD\PinjamAPD;
use App\Models\APD\MasterAPD;
use App\Models\APD\DetailPinjam;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use App\Http\Controllers\Controller;

class ApiPinjamAPDController extends Controller
{
    protected $user;
 
    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }

    public function index(Request $request)
    {   
        $data = PinjamAPD::with('getDetailPinjam')->get();

        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Order Item List Found',
            'data' =>$data
        ], 200);
    }

    public function getDataId(Request $request)
    {   
        $apiName='DETAILPINJAMWITHID';
        $id_number = $request->id;
        
        if($id_number){
            $data = PinjamAPD::with('getDetailPinjam')->where('id', $id_number)->get( );
            return response()->json([
                'success' => true,
                'status' => 200,
                'message' => 'Order Item List Found',
                'data' => $data
            ], 200);
            
          } else {
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Item list with id ' . $id_number . ' cannot be found',
                'data'=>null
            ], 400);
        }
    
    }

    public function addpinjam(Request $request)
    {
        $apiName='PINJAMCONTROLLER';
        $kode_pinjam=$request->kode_pinjam;
        $nopeg=$request->nopeg;
        $unit_kerja=$request->unit_kerja;
        $status_retur=$request->status_retur;
        $retur_at=$request->retur_at;
        $approve1_at=$request->approve1_at;
        $approve1_by=$request->approve1_by;
        $approve2_at=$request->approve2_at;
        $approve2_by=$request->approve2_by;
        $create_at=$request->create_at;
        $create_by=$request->create_by;
        $update_at=$request->update_at;
        $update_by=$request->update_by;
        $code_comp=$request->code_comp;
        $code_plant=$request->code_plant;
        $tanggal_pinjam=$request->tanggal_pinjam;
        $uk_text=$request->uk_text;
        $note=$request->note;
        $closed_at=$request->closed_at;
        $closed_by=$request->closed_by;
        $updated_at=$request->updated_at;
        $created_at=$request->created_at;
        $status=$request->status;
        $reject_at=$request->reject_at;
        $reject_by=$request->reject_by;
        $note_reject=$request->note_reject;
        $keterangan=$request->keterangan;
        $name=$request->name;
        $retur_date=$request->retur_date;

        $cekEmployee=User::where('no_badge', $nopeg)->first();


        try {
            $data = new PinjamAPD();
            $data->kode_pinjam="PJM-".$request->id;
            $data->nopeg=$cekEmployee->no_badge;
            $data->unit_kerja=$cekEmployee->uk_kode;
            $data->approve1_at=$approve1_at;
            $data->approve1_by=$approve1_by;
            $data->approve2_at=$approve2_at;
            $data->approve2_by=$approve2_by;
            $data->create_at=date('Y-m-d H:i:s');
            $data->create_by=$cekEmployee->username;
            $data->update_at=$update_at;
            $data->update_by=$update_by;
            $data->code_comp="5000";
            $data->code_plant=$cekEmployee->plant;
            $data->tanggal_pinjam=date('Y-m-d H:i:s');
            $data->uk_text=$cekEmployee->unit_kerja;
            $data->note=$note;
            $data->closed_at=$closed_at;
            $data->closed_by=$closed_by;
            $data->updated_at=$updated_at;
            $data->created_at=$created_at;
            $data->status=$status;
            $data->reject_at=$reject_at;
            $data->reject_by=$reject_by;
            $data->note_reject=$note_reject;
            $data->keterangan="Permintaan Pinjam";
            $data->name=$cekEmployee->name;
            $data->retur_at=$retur_at;
            $data->retur_date=$retur_date;

            $data->save();

            $nilai = substr($data->id, 0);
            // dd($nilai);
            $kode = (int) $nilai;
            // dd($data->id);

            $auto_kode = "PJM-" .str_pad($kode, 5, "0",  STR_PAD_LEFT);
            $data->kode_pinjam = $auto_kode;
            $data->save();

            $detail = json_decode($request->detail);
            // dd($detail); 
            $currentParams=[];

            foreach($detail as $key => $value){

                $dataMaster = MasterAPD::where('kode', $value->kode_apd)->first();

                $currentParams[] = [
                    'kode_pinjam' => $data->id,
                    'kode_apd' => $value->kode_apd,
                    'jumlah' => $value->jumlah,
                    'create_by' => $data->create_by,
                    'update_at' => $data->update_at,
                    'update_by' => $data->update_by,
                    'code_comp' => $data->code_comp,
                    'code_plant' => $data->code_plant,
                    'retur_at' => $data->retur_at,
                    'reject_at' => $data->reject_at,
                    'reject_by' => $data->reject_by,
                    'note_reject' => $data->note_reject,
                    'created_at' => date("Y-m-d"),
                    'updated_at' => date("Y-m-d"),
                    'nomor_pinjam' => $data->kode_pinjam,
                    'nama' => $data->name,
                    'unit' => $data->unit_kerja,
                    'merk' => $dataMaster->merk,
                    'nama_apd' => $dataMaster->nama,
                    'status' => $data->status,
                    'no_badge' => $data->nopeg,
                    'retur_date'=>$data->retur_date,
                ];
            }

            DetailPinjam::insert($currentParams);

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        } catch (Exception $e) {

            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
        }
    }

}
