<?php

namespace App\Http\Controllers\API\APD;

use App\Models\APD\DetailPinjam;
use App\Models\APD\PinjamAPD;
use App\Models\APD\MasterAPD;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use App\Http\Controllers\Controller;

class ApiDetailPinjamController extends Controller
{
    protected $user;
 
    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }

    public function index(Request $request)
    {   
        $data = DetailPinjam::with('getMasterAPD')->get();

        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Order Item List Found',
            'data' => $data
        ], 200);
    }

    public function show($id)
    {
        $detail_pinjam = $this->user->detail_pinjam()->find($id);
    
        if (!$detail_pinjam) {
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Item list with id ' . $id . ' cannot be found',
                'data'=>null
            ], 400);
        }
    
        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Item list with id ' . $id . ' found',
            'data' => $detail_pinjam
        ], 200);
    }

    public function getDetailId(Request $request)
    {   
        $apiName='DETAILPINJAMWITHID';
        $id = $request->kode_pinjam;
        
        if($id){
            $data = DetailPinjam::with('getMasterAPD')->where('kode_pinjam', $id)->get( );
            return response()->json([
                'success' => true,
                'status' => 200,
                'message' => 'Order Item List Found',
                'data' => $data
            ], 200);
            
          } else {
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Item list with id ' . $id . ' cannot be found',
                'data'=>null
            ], 400);
        }
    
    }

    public function addDetailPinjam($kode_pinjam, $kode_apd, Request $request)
    {
        $cekDataPinjam=PinjamAPD::where ('id', $kode_pinjam)->first();
        if(!$cekDataPinjam){
            // data tidak ada
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Item list with kode order ' . $kode_pinjam . ' cannot be found',
                'data'=>null
            ], 400);
        }


        $apiName='DETAILPINJAMCONTROLLER';
        $kode_apd=$cekDataPinjam->kode_apd;
        $kode_pinjam=$request->kode_pinjam;
        $jumlah=$cekDataPinjam->jumlah;
        $jumlah_apr=$request->jumlah_apr;
        $create_at=$cekDataPinjam->create_at;
        $create_by=$cekDataPinjam->create_by;
        $update_at=$cekDataPinjam->update_at;
        $update_by=$cekDataPinjam->update_by;
        $code_comp=$cekDataPinjam->code_comp;
        $code_plant=$cekDataPinjam->code_plant;
        $release_at=$request->release_at;
        $release_by=$request->release_by;
        $jumlah_release=$request->jumlah_release;
        
        
        $retur_at=$request->retur_at;
        $retur_by=$request->retur_by;
        $jumlah_retur=$request->jumlah_retur;
        $reject_at=$request->reject_at;
        $reject_by=$request->reject_by;
        $jumlah_reject=$request->jumlah_reject;
        $note_reject=$request->note_reject;


        $sendingParams = [
            'kode_apd' => $kode_apd,
            'kode_pinjam' => $kode_pinjam,
            'jumlah' => $jumlah,
            'jumlah_apr' => $jumlah_apr,
            'create_at' => $create_at,
            'create_by' => $create_by,
            'update_at' => $update_at,
            'update_by' => $update_by,
            'code_comp' => $code_comp,
            'code_plant' => $code_plant,
            'release_at' => $release_at,
            'release_by' => $release_by,
            'jumlah_release' => $jumlah_release,
            //'files' => $files,
            'retur_at' => $retur_at,
            'retur_by' => $retur_by,
            'jumlah_retur' => $jumlah_retur,
            'reject_at' => $reject_by,
            'reject_by' => $reject_by,
            'jumlah_reject' => $jumlah_reject,
            'note_reject' => $note_reject
        ];

        try {
            $data = new DetailPinjam();
            $data->kode_apd=$kode_apd;
            $data->kode_pinjam=$kode_pinjam;
            $data->jumlah=$jumlah;
            $data->jumlah_apr=$jumlah_apr;
            $data->create_at=$create_at;
            $data->create_by=$create_by;
            $data->update_at=$update_at;
            $data->update_by=$update_by;
            $data->code_comp=$code_comp;
            $data->code_plant=$code_plant;
            $data->release_at=$release_at;
            $data->release_by=$release_by;
            $data->jumlah_release=$jumlah_release;
            //$data->files=$files;
            $data->retur_at=$retur_at;
            $data->retur_by=$retur_by;
            $data->jumlah_retur=$jumlah_retur;
            $data->reject_at=$reject_at;
            $data->reject_by=$reject_by;
            $data->jumlah_reject=$jumlah_reject;
            $data->note_reject=$note_reject;

            $data->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        } catch (Exception $e) {

            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
        }

        return response()->json($params);
    }

}
