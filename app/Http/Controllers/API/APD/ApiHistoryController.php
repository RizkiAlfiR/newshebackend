<?php

namespace App\Http\Controllers\API\APD;

use App\Models\APD\History;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use App\Http\Controllers\Controller;

class ApiHistoryController extends Controller
{
    protected $user;
 
    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }

    public function index(Request $request)
    {   
        // $data=History::with('getOrder')->get();

        $data = History::select('history.id',
                    'history.kode_apd', 'order_apd.kode_order','order_apd.no_badge','order_apd.kode_uk',
                    'order_apd.approve2_at', 'order_apd.approve2_by', 'order_apd.name', 'order_apd.closed_at', 
                    'order_apd.closed_by','detail_order.jumlah','detail_order.jumlah_apr', 'detail_order.create_at', 
                    'detail_order.create_by', 'detail_order.update_at','detail_order.update_by', 
                    'detail_order.code_comp', 'detail_order.code_plant', 'detail_order.release_at',
                    'detail_order.release_by', 'detail_order.jumlah_release', 'detail_order.files', 
                    'detail_order.reject_at','detail_order.note_reject', 'detail_order.masa_exp','master_apd.kode',
                    'master_apd.nama', 'master_apd.merk', 'master_apd.group_code', 'master_apd.group_name')
                    ->join('order_apd', 'order_apd.kode_order', '=', 'kode_order_apd')
                    ->join('detail_order', 'detail_order.kode_order', '=', 'order_apd.id')
                    ->join('master_apd', 'master_apd.kode', '=', 'detail_order.kode_apd')
                    ->get();
        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'History List Found',
            'data' => $data
        ], 200);


    }

    public function show($id)
    {
        $history = $this->user->history()->find($id);
    
        if (!$history) {
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, History list with id ' . $id . ' cannot be found',
                'data'=>null
            ], 400);
        }
    
        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'History list with id ' . $id . ' found',
            'data' => $history
        ], 200);
    }

}
