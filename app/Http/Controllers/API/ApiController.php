<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\RegisterAuthRequest;
use App\User;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    public $loginAfterSignUp = true;

    public function register(Request $request)
    {
        $user = new User();
        $user->role_id = $request->role_id;
        $user->email = $request->email;
        $user->username = $request->username;
        $user->password = bcrypt($request->password);
        $user->name = $request->name;
        $user->company = $request->company;
        $user->plant = $request->plant;
        $user->ad = $request->ad;
        $user->unit_kerja = $request->unit_kerja;
        $user->uk_kode = $request->uk_kode;
        $user->no_badge = $request->no_badge;
        $user->cost_center = $request->cost_center;
        $user->cc_text = $request->cc_text;
        $user->position = $request->position;
        $user->pos_text = $request->pos_text;
        $user->is_k3 = $request->is_k3;
        $token=JWTAuth::fromUser($user);
        $user->save();

        // if ($this->loginAfterSignUp) {
        //     return $this->login($request);
        // }

        // return response()->json([
        //     'success' => true,
        //     'message' => 'Successfully Registered!'
        // ], 200);

        $token=JWTAuth::fromUser($user);
        return response()->json([
            'success' => true,
            'data' => $user,
            'token'=> $token
        ], 200);
    }

    public function login(Request $request)
    {
        $input = $request->only('email', 'password');
        $jwt_token = null;

        if (!$jwt_token = JWTAuth::attempt($input)) {
            return response()->json([
                'success' => false,
                'message' => 'Invalid Credential User',
                'response_code' => 401,
            ], 401);
        }

        return response()->json([
            'success' => true,
            'message' => 'Successfully Login',
            'response_code' => 200,
            'token' => $jwt_token,
        ]);
    }

    public function logout(Request $request)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);

        try {
            JWTAuth::invalidate($request->token);

            return response()->json([
                'success' => true,
                'message' => 'Successfully Logout'
            ]);
        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, the user cannot be logged out'
            ], 500);
        }
    }

    public function getAuthUser(Request $request)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);

        // $user = JWTAuth::authenticate($request->token);
        try {
            $user = JWTAuth::authenticate($request->token);

            return response()->json([
                'success' => true,
                'response_code' => 200,
                'message' => 'Successfully Get Auth',
                'user' => $user
            ]);
        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, check your session'
            ], 500);
        }
    }

    public function validasi(Request $request)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);

        $user = JWTAuth::authenticate($request->token);

        return response()->json(['user' => $user]);
    }
}
