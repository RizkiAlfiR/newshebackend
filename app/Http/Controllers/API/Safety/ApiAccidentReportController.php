<?php

namespace App\Http\Controllers\API\Safety;

use App\Models\Safety\AccidentReport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use App\Http\Controllers\Controller;

class ApiAccidentReportController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }

    public function index()
    {
        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Accident List found',
            'data' => AccidentReport::all()
        ], 200);
    }

    public function search(Request $request)
    {   
        $search = null;
        $search = $request->search;
        if($search){
            $data = AccidentReport::where('name_vict', 'LIKE', '%' .$search. '%')->get( );
            $params = [
                'success' => true,
                'status' => 200,
                'message' => 'Accident Report list found',
                'data' => $data
            ];

        } else {
            $data = AccidentReport::all();
            $params = [
                'success' => true,
                'status' => 200,
                'message' => 'Accident Report list found',
                'data' => $data
            ];
        }
        return response()->json($params);
    }

    public function show($id)
    {
        $accident_report = $this->user->accident_reports()->find($id);
        $accident_report = AccidentReport::all()->find($id);

        if (!$accident_report) {
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Accident with id ' . $id . ' cannot be found'
            ], 400);
        }

        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Accident with id ' . $id . ' found',
            'data' => $accident_report
        ], 200);
    }

    public function store(Request $request)
    {
        $accident_report = new AccidentReport();
        $accident_report->date_acd = $request->date_acd;
        $accident_report->time_acd = $request->time_acd;
        $accident_report->stat_vict = $request->stat_vict;
        $accident_report->state_vict_text = $request->state_vict_text;
        $accident_report->badge_vict = $request->badge_vict;
        $accident_report->name_vict = $request->name_vict;
        $accident_report->uk_vict = $request->uk_vict;
        $accident_report->uk_vict_text = $request->uk_vict_text;
        $accident_report->company_code = $request->company_code;
        $accident_report->company_text = $request->company_text;
        $accident_report->ven_code = $request->ven_code;
        $accident_report->ven_text = $request->ven_text;
        $accident_report->date_in = $request->date_in;
        $accident_report->position = $request->position;
        $accident_report->position_text = $request->position_text;
        $accident_report->age = $request->age;
        $accident_report->no_jamsos = $request->no_jamsos;
        $accident_report->no_bpjs = $request->no_bpjs;
        $accident_report->shift = $request->shift;
        $accident_report->shift_text = $request->shift_text;
        $accident_report->region = $request->region;
        $accident_report->work_task = $request->work_task;
        $accident_report->spv_badge = $request->spv_badge;
        $accident_report->spv_name = $request->spv_name;
        $accident_report->spv_position = $request->spv_position;
        $accident_report->spv_position_text = $request->spv_position_text;
        $accident_report->location = $request->location;
        $accident_report->location_text = $request->location_text;
        $accident_report->acd_cause = $request->acd_cause;
        $accident_report->s_badge = $request->s_badge;
        $accident_report->s_name = $request->s_name;
        $accident_report->s_position = $request->s_position;
        $accident_report->s_pos_text = $request->s_pos_text;
        $accident_report->inj_note = $request->inj_note;
        $accident_report->inj_text = $request->inj_text;
        $accident_report->first_aid = $request->first_aid;
        $accident_report->next_aid = $request->next_aid;
        $accident_report->safety_req = $request->safety_req;
        $accident_report->acd_rpt_text = $request->acd_rpt_text;
        $accident_report->unsafe_act = $request->unsafe_act;
        $accident_report->unsafe_cond = $request->unsafe_cond;
        $accident_report->note = $request->note;
        $accident_report->inv_badge = $request->inv_badge;
        $accident_report->inv_name = $request->inv_name;
        $accident_report->status_report = $request->status_report;

        $filenamebefore = null;
        $pict_before = $request->file('pict_before');
        if($pict_before)
        {
            $destinationPath = 'public/uploads/accident/';
            $filenamebefore = $pict_before->getClientOriginalName();
            $pict_before->move($destinationPath, $filenamebefore);
            $accident_report->pict_before = $filenamebefore;
        } else
        {
            $filenamebefore = null;
        }

        $filenameafter = null;
        $pict_after = $request->file('pict_after');
        if($pict_after)
        {
            $destinationPath = 'public/uploads/accident/';
            $filenameafter = $pict_after->getClientOriginalName();
            $pict_after->move($destinationPath, $filenameafter);
            $accident_report->pict_after = $filenameafter;
        } else
        {
            $filenameafter = null;
        }

        $accident_report->follow_up = $request->follow_up;

        if ($this->user->accident_reports()->save($accident_report))
            return response()->json([
                'success' => true,
                'status' => 200,
                'message' => 'Succesfully added new Accident',
                'data' => $accident_report
            ]);
        else
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Accident could not be added'
            ], 500);
    }

    public function update(Request $request)
    {
        $id = $request->id;
        // $accident_report = $this->user->accident_reports()->find($id);
        $accident_report = AccidentReport::find($id);

        if (!$accident_report) {
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Accident with id ' . $id . ' cannot be found'
            ], 400);
        }

        $params = $request->all();
        $params['pict_before'] = $request->file('pict_before');

        $filenamepictbefore = null;
        $params['pict_before'] = $request->file('pict_before');
        if($params['pict_before'])
        {
            $destinationPath = 'public/uploads/accident/';
            $filenamepictbefore = $params['pict_before']->getClientOriginalName();
            $params['pict_before']->move($destinationPath, $filenamepictbefore);
            $params['pict_before'] = $filenamepictbefore;
        } else {
            $check = AccidentReport::find($id);
            $params['pict_before'] = $check->pict_before;
        }

        $params['pict_after'] = $request->file('pict_after');

        // $filenamepictafter = null;
        $params['pict_after'] = $request->file('pict_after');
        if($params['pict_after'])
        {
            $destinationPath = 'public/uploads/accident/';
            $filenamepictafter = $params['pict_after']->getClientOriginalName();
            $params['pict_after']->move($destinationPath, $filenamepictafter);
            $params['pict_after'] = $filenamepictafter;
        } else {
            $check = AccidentReport::find($id);
            $params['pict_after'] = $check->pict_after;
        }

        $updated = $accident_report->update($params);

        if ($updated) {
            return response()->json([
                'success' => true,
                'status' => 200
            ]);
        } else {
            return response()->json([
                'success' => false,
                'status' => 501,
                'message' => 'Sorry, Accident could not be updated'
            ], 500);
        }
    }

    public function destroy($id)
    {
        $accident_report = $this->user->accident_reports()->find($id);

        if (!$accident_report) {
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Accident with id ' . $id . ' cannot be found'
            ], 400);
        }

        if ($accident_report->delete()) {
            return response()->json([
                'success' => true,
                'message' => 'Successfully deleted Accident',
                'status' => 200
            ]);
        } else {
            return response()->json([
                'success' => false,
                'status' => 501,
                'message' => 'Accident could not be deleted'
            ], 500);
        }
    }
}
