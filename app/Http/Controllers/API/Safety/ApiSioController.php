<?php

namespace App\Http\Controllers\API\Safety;

use App\Models\Safety\Sio;
use App\Models\GlobalList\SioGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use App\Http\Controllers\Controller;

class ApiSioController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }

    public function index()
    {   
        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Sio Certification list found',
            'data' => Sio::all()
        ], 200);
    }

    public function search(Request $request)
    {   
        $search = null;
        $search = $request->search;
        if($search){
            $data = Sio::where('nama', 'LIKE', '%' .$search. '%')->get( );
            $params = [
                'success' => true,
                'status' => 200,
                'message' => 'Sio Certification list found',
                'data' => $data
            ];

        } else {
            $data = Sio::all();
            $params = [
                'success' => true,
                'status' => 200,
                'message' => 'Sio Certification list found',
                'data' => $data
            ];
        }
        return response()->json($params);
    }

    public function show($id)
    {
        // $sio = $this->user->sios()->find($id);
        $sio = Sio::all()->find($id);
    
        if (!$sio) {
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Sio with id ' . $id . ' cannot be found'
            ], 400);
        }
    
        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Sio with id ' . $id . ' found',
            'data' => $sio
        ], 200);
    }

    public function store(Request $request)
    {
        $sio = new Sio();
        $sio->badge = $request->badge;
        $sio->unit_kerja = $request->unit_kerja;
        $sio->no_lisensi = $request->no_lisensi;
        $sio->kelas = $request->kelas;
        $sio->masa_berlaku = $request->masa_berlaku;

        $filenamefoto = null;
        $foto = $request->file('foto');
        if($foto)
        {
            $destinationPath = 'public/uploads/sio/';
            $filenamefoto = $foto->getClientOriginalName();
            $foto->move($destinationPath, $filenamefoto);
            $sio->foto = $filenamefoto;
        } else
        {
            $filenamefoto = null;
        }

        $sio->user_name = $request->user_name;
        $sio->unit_kerja_txt = $request->unit_kerja_txt;
        $sio->grup = $request->grup;
        $sio->company = $request->company;
        $sio->plant = $request->plant;
        $sio->nama = $request->nama;
        $sio->time_certification = $request->time_certification;
        $sio->masa_awal = $request->masa_awal;
        $sio->start_date = $request->start_date;
        $sio->end_date = $request->end_date;
    
        if ($this->user->sios()->save($sio))
            return response()->json([
                'success' => true,
                'status' => 200,
                'message' => 'Succesfully added new Sio Certification',
                'data' => $sio
            ]);
        else
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Sio Certification could not be added'
            ], 400);
    }

    public function update(Request $request)
    {
        $id = $request->id;
        // $sio = $this->user->sios()->find($id);
        $sio = Sio::find($id);
    
        if (!$sio) {
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Sio with id ' . $id . ' cannot be found'
            ], 400);
        }

        $params = $request->all();
        $params['foto'] = $request->file('foto');

        // $filenamefoto = null;
        $params['foto'] = $request->file('foto');
        if($params['foto'])
        {
            $destinationPath = 'public/uploads/sio/';
            $filenamefoto = $params['foto']->getClientOriginalName();
            $params['foto']->move($destinationPath, $filenamefoto);
            $params['foto'] = $filenamefoto;
        } else {
            $check = Sio::find($id);
            $params['foto'] = $check->foto;
        }

        $updated = $sio->update($params);
    
        if ($updated) {
            return response()->json([
                'success' => true,
                'status' => 200
            ]);
        } else {
            return response()->json([
                'success' => false,
                'status' => 501,
                'message' => 'Sorry, Sio could not be updated'
            ], 500);
        }
    }

    public function destroy($id)
    {
        $sio = $this->user->sios()->find($id);
    
        if (!$sio) {
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Sio with id ' . $id . ' cannot be found'
            ], 400);
        }
    
        if ($sio->delete()) {
            return response()->json([
                'success' => true,
                'message' => 'Successfully deleted Sio',
                'status' => 200
            ]);
        } else {
            return response()->json([
                'success' => false,
                'status' => 501,
                'message' => 'Sio could not be deleted'
            ], 500);
        }
    }

    public function indexgroup()
    {   
        // return $this->user
        //     ->accident_reports()
        //     ->get(['id', 'user_id', 'date_acd', 'time_acd', 'stat_vict', 'state_vict_text', 'badge_vict', 'name_vict', 'uk_vict', 'uk_vict_text',
        //     'company_code', 'company_text', 'ven_code', 'ven_text', 'date_in', 'position', 'position_text', 'age', 'no_jamsos',
        //     'no_bpjs', 'shift', 'shift_text', 'region', 'work_task', 'spv_badge', 'spv_name', 'spv_position', 'spv_position_text',
        //     'location', 'location_text', 'acd_cause', 's_badge', 's_name', 's_position', 's_pos_text', 'inj_note', 'inj_text',
        //     'first_aid', 'next_aid', 'safety_req', 'acd_rpt_text', 'unsafe_act', 'unsafe_cond', 'note', 'inv_badge', 'inv_name',
        //     'status_report', 'pict_before', 'pict_after','follow_up'])
        //     ->toArray();
        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Sio Group list found',
            'data' => SioGroup::all()
        ], 200);
    }
}
