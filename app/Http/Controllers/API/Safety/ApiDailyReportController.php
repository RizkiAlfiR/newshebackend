<?php

namespace App\Http\Controllers\API\Safety;

use App\Models\Safety\DailyReport;
use App\Models\Safety\UnsafeDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use App\Http\Controllers\Controller;

class ApiDailyReportController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }

    public function index()
    {   
        $data = DailyReport::with('unsafe_detail_reports')->get();

        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Daily Report list found',
            'data' => $data , 
        ], 200);
    }

    public function search(Request $request)
    {   
        $search = null;
        $search = $request->search;
        if($search){
            $data = DailyReport::with('unsafe_detail_reports')->where('no_dokumen', 'LIKE', '%' .$search. '%')->get();
            $params = [
                'success' => true,
                'status' => 200,
                'message' => 'Daily Report list found',
                'data' => $data
            ];

        } else {
            $data = DailyReport::with('unsafe_detail_reports')->get();
            $params = [
                'success' => true,
                'status' => 200,
                'message' => 'Daily Report list found',
                'data' => $data
            ];
        }
        return response()->json($params);
    }

    public function show($id)
    {
        $dailyreport = DailyReport::with('unsafe_detail_reports')->get()->find($id);
        // $dailyreport = DailyReport::with('unsafe_detail_reports')->where(['id' => $id])->get();

    
        if (!$dailyreport) {
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Header Daily Report with id ' . $id . ' cannot be found'
            ], 400);
        }
    
        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Header Daily Report with id ' . $id . ' found',
            'data' => $dailyreport
        ], 200);
    }

    public function store(Request $request)
    {
        $reporttemuan = new UnsafeDetail();
        $params = [
            'shift' => $request->shift,
            'no_dokumen' => "R/538/001/S" . $request->shift . "/",
            'status' => "OPEN",
            'user_id' => $this->user->id,
            'tanggal' => date("Y-m-d"),
            'company' => "5000",
            'plant' => "5002",
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ];
        $id = $this->user->daily_reports()->insertGetId($params);
        $dailyreport = DailyReport::find($id);

        $reporttemuan->no_dokumen = "R/538/002/";
        $reporttemuan->id_report = $id;
        $reporttemuan->temuan = $request->temuan;
        $reporttemuan->lokasi_kode = $request->lokasi_kode;
        $reporttemuan->lokasi_text = $request->lokasi_text;
        $reporttemuan->penyebab = $request->penyebab;
        $reporttemuan->tanggal_real = $request->tanggal_real;
        $filenamebef = null;
        $foto_bef = $request->file('foto_bef');
        if($foto_bef)
        {
            $destinationPath = 'public/uploads/unsafe/';
            $filenamebef = $foto_bef->getClientOriginalName();
            $foto_bef->move($destinationPath, $filenamebef);
            $reporttemuan->foto_bef = $filenamebef;
        } else
        {
            $filenamebef = null;
        }

        $filenameaft = null;
        $foto_aft = $request->file('foto_aft');
        if($foto_aft)
        {
            $destinationPath = 'public/uploads/unsafe/';
            $filenameaft = $foto_aft->getClientOriginalName();
            $foto_aft->move($destinationPath, $filenameaft);
            $reporttemuan->foto_aft = $filenameaft;
        } else
        {
            $filenameaft = null;
        }

        $reporttemuan->status = "OPEN";
        $reporttemuan->rekomendasi = $request->rekomendasi;
        $reporttemuan->user_name = $request->user_name;
        $reporttemuan->jenis_temuan = $request->jenis_temuan;
        $reporttemuan->company = "5000";
        $reporttemuan->plant = "5002";
        $reporttemuan->tindak_lanjut = $request->tindak_lanjut;
        $reporttemuan->note = $request->note;
        $reporttemuan->potensi_bahaya = $request->potensi_bahaya;
        $reporttemuan->priority = $request->priority;
        $reporttemuan->uk_kode = $request->uk_kode;
        $reporttemuan->uk_text = $request->uk_text;

        $this->user->unsafe_details()->save($reporttemuan);
        $dailyreport->no_dokumen = "R/538/001/S" . $request->shift . "/" . $dailyreport->id;
        $dailyreport->save();
        $reporttemuan->no_dokumen = "R/538/002/" . $reporttemuan->id;
    
        if ($this->user->unsafe_details()->save($reporttemuan))
            return response()->json([
                'success' => true,
                'status' => 200,
                'message' => 'Succesfully added new Unsafe Detail Daily Report',
                'data' => $reporttemuan
            ]);
        else
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Header Daily Report could not be added'
            ], 400);
    }

    public function update(Request $request)
    {
        $id = $request->id;
        // $reporttemuan = $this->user->unsafe_details()->find($id);
        $reporttemuan = UnsafeDetail::find($id);
    
        if (!$reporttemuan) {
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Header Daily Report with id ' . $id . ' cannot be found'
            ], 400);
        }

        $params = $request->all();
        $params['foto_bef'] = $request->file('foto_bef');

        // $filenamebef = null;
        $params['foto_bef'] = $request->file('foto_bef');
        if($params['foto_bef'])
        {
            $destinationPath = 'public/uploads/unsafe/';
            $filenamebef = $params['foto_bef']->getClientOriginalName();
            $params['foto_bef']->move($destinationPath, $filenamebef);
            $params['foto_bef'] = $filenamebef;
        } else
        {
            $filenamebef = null;
        }

        $params['foto_aft'] = $request->file('foto_aft');

        // $filenameaft = null;
        $params['foto_aft'] = $request->file('foto_aft');
        if($params['foto_aft'])
        {
            $destinationPath = 'public/uploads/unsafe/';
            $filenameaft = $params['foto_aft']->getClientOriginalName();
            $params['foto_aft']->move($destinationPath, $filenameaft);
            $params['foto_aft'] = $filenameaft;
        } else
        {
            $filenameaft = null;
        }
    
        $updated = $reporttemuan->update($params);
    
        if ($updated) {
            return response()->json([
                'success' => true,
                'status' => 200
            ]);
        } else {
            return response()->json([
                'success' => false,
                'status' => 501,
                'message' => 'Sorry, Header Daily Report could not be updated'
            ], 500);
        }
    }

    public function destroy($id)
    {
        $reporttemuan = $this->user->unsafe_details()->find($id);
    
        if (!$reporttemuan) {
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Header Daily Report with id ' . $id . ' cannot be found'
            ], 400);
        }
    
        if ($reporttemuan->delete()) {
            return response()->json([
                'success' => true,
                'message' => 'Successfully deleted Header Daily Report',
                'status' => 200
            ]);
        } else {
            return response()->json([
                'success' => false,
                'status' => 501,
                'message' => 'Header Daily Report could not be deleted'
            ], 500);
        }
    }
}
