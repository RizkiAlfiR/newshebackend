<?php

namespace App\Http\Controllers\API\Safety;

use Illuminate\Http\Request;
use App\Models\Safety\DailyReportK3Detail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use App\Http\Controllers\Controller;

class ApiRecapitulationController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }

    public function index()
    {   
        $data = DailyReportK3Detail::where(['status' => 'CLOSE'])->get();

        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Recapitulation list found',
            'data' => $data , 
        ], 200);
    }

    public function search(Request $request)
    {   
        $search = null;
        $search = $request->search;
        if($search){
            $data = DailyReportK3Detail::where(
                ['status' => 'CLOSE'],
                ['unit_name', 'LIKE', '%' .$search. '%']
            )->get();
            $params = [
                'success' => true,
                'status' => 200,
                'message' => 'Recapitulation list found',
                'data' => $data
            ];

        } else {
            $data = DailyReportK3Detail::where(
                ['status' => 'CLOSE'],
                ['unit_name', 'LIKE', '%' .$search. '%']
            )->get();
            $params = [
                'success' => true,
                'status' => 200,
                'message' => 'Recapitulation list found',
                'data' => $data
            ];
        }
        return response()->json($params);
    }

    public function show($id)
    {
        $data = DailyReportK3Detail::where(['status' => 'CLOSE'])->get()->find($id);
        // $data = DailyReportK3::with('unsafe_report_details_k3')->where(['id_unsafe' => $id])->get();
    
        if (!$data) {
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Recapitulation with id ' . $id . ' cannot be found'
            ], 400);
        }
    
        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Recapitulation with id ' . $id . ' found',
            'data' => $data
        ], 200);
    }

    public function chart()
    {
        $monthlyTransaction = [];
        for ($i = 0; $i <= 11; $i++){
            $monthIsOpen  = DailyReportK3Detail::whereMonth('created_at', $this->generateMonth($i))
                ->where('status', 'OPEN')
                // ->where('user_id',$userId)
                ->count();
            $monthIsClose  = DailyReportK3Detail::whereMonth('created_at', $this->generateMonth($i))
                ->where('status', 'CLOSE')
                // ->where('user_id',$userId)
                ->count();

            $monthName = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
            $monthlyTransaction[] = [
                'month' => $monthName[$i],
                'valueIsOpen' => $monthIsOpen,
                'valueIsClose'=> $monthIsClose
            ];
        }
        $isOpen = [];
        $isClose = [];
        foreach ($monthlyTransaction as $item){
            $isOpen[] = $item['valueIsOpen'];
            $isClose[] = $item['valueIsClose'];
        }

        // $params = [
        //     'isOpen' => $isOpen,
        //     'isClose' => $isClose,
        //     'data' => $monthlyTransaction
        // ];

        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Recapitulation chart found',
            'data' => $monthlyTransaction, 
        ], 200);
    }

    private function generateMonth($idx)
    {
        if(strlen($idx) < 2)
        {
            $idx = '0'.$idx;
        }
        return $idx;
    }
}
