<?php

namespace App\Http\Controllers\API\Safety;

use App\Models\Safety\DailyReportK3;
use App\Models\Safety\DailyReportK3Detail;
use App\Models\Safety\ListFileUpload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use App\Http\Controllers\Controller;

class ApiDailyReportK3Controller extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }

    public function indexheader()
    {   
        $data = DailyReportK3::with('unsafe_report_details_k3.list_file_upload')->get();

        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Daily Report K3 list found',
            'data' => $data , 
        ], 200);
    }

    public function searchheader(Request $request)
    {   
        $search = null;
        $search = $request->search;
        if($search){
            $data = DailyReportK3::with('unsafe_report_details_k3')->where('area_txt', 'LIKE', '%' .$search. '%')->get();
            $params = [
                'success' => true,
                'status' => 200,
                'message' => 'Daily Report K3 list found',
                'data' => $data
            ];

        } else {
            $data = DailyReportK3::with('unsafe_report_details_k3')->get();
            $params = [
                'success' => true,
                'status' => 200,
                'message' => 'Daily Report K3 list found',
                'data' => $data
            ];
        }
        return response()->json($params);
    }

    public function showheader($id)
    {
        $data = DailyReportK3::with('unsafe_report_details_k3')->get()->find($id);
        // $data = DailyReportK3::with('unsafe_report_details_k3')->where(['id_unsafe' => $id])->get();
    
        if (!$data) {
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Header Daily Report with id ' . $id . ' cannot be found'
            ], 400);
        }
    
        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Header Daily Report with id ' . $id . ' found',
            'data' => $data
        ], 200);
    }

    public function storeheader(Request $request)
    {
        $header = new DailyReportK3();
        $header->report_date = $request->report_date;
        $header->shift = $request->shift;
        $header->area_code = $request->area_code;
        $header->area_txt = $request->area_txt;
        $header->sub_area_code = $request->sub_area_code;
        $header->sub_area_txt = $request->sub_area_txt;
        $header->plant_code = $request->plant_code;
        $header->plant_txt = $request->plant_txt;
        $header->inspector_code = $request->inspector_code;
        $header->inspector_name = $request->inspector_name;
        $header->is_smig = $request->is_smig;
        $header->accident = $request->accident;
        $header->incident = $request->incident;
        $header->nearmiss = $request->nearmiss;
    
        if ($this->user->daily_reports_k3()->save($header))
            return response()->json([
                'success' => true,
                'status' => 200,
                'message' => 'Succesfully added new Header Unsafe K3',
                'data' => $header
            ]);
        else
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Header Unsafe K3 could not be added'
            ], 400);
    }

    public function storedetail(Request $request)
    {
        $detail = new DailyReportK3Detail;
        $detail->id_unsafe_report = $request->id_unsafe_report;
        $detail->unit_code = $request->unit_code;
        $detail->unit_name = $request->unit_name;
        $detail->activity_description = $request->activity_description;
        $detail->unsafe_type = $request->unsafe_type;
        $detail->potential_hazard = $request->potential_hazard;
        $detail->follow_up = $request->follow_up;
        $detail->is_smig = $request->is_smig;
        $detail->o_clock_incident = $request->o_clock_incident;
        $detail->id_category = $request->id_category;
        $detail->deskripsi = $request->deskripsi;
        $detail->kode_en = $request->kode_en;
        $detail->status = $request->status;
        $detail->tindak_lanjut = $request->tindak_lanjut;
        $detail->recomendation = $request->recomendation;
        $detail->trouble_maker = $request->trouble_maker;
        $detail->close_date = $request->close_date;
        $detail->close_by = $request->close_by;
        $detail->mail_list = $request->mail_list;
        $detail->comitment_date = $request->comitment_date;
        $detail->verification_date = $request->verification_date;

        $filenamefotoBef = null;
        $foto_bef = $request->file('foto_bef');
        if($foto_bef)
        {
            $destinationPath = 'public/uploads/unsafe/';
            $filenamefotoBef = $foto_bef->getClientOriginalName();
            $foto_bef->move($destinationPath, $filenamefotoBef);
            $detail->foto_bef = $filenamefotoBef;
        } else
        {
            $filenamefotoBef = null;
        }

        $filenamefotoAft = null;
        $foto_aft = $request->file('foto_aft');
        if($foto_aft)
        {
            $destinationPath = 'public/uploads/unsafe/';
            $filenamefotoAft = $foto_aft->getClientOriginalName();
            $foto_aft->move($destinationPath, $filenamefotoAft);
            $detail->foto_aft = $filenamefotoAft;
        } else
        {
            $filenamefotoAft = null;
        }
    
        if ($this->user->unsafe_details_k3()->save($detail))
            return response()->json([
                'success' => true,
                'status' => 200,
                'message' => 'Succesfully added new Detail Unsafe K3',
                'data' => $detail
            ]);
        else
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Detail Unsafe K3 could not be added'
            ], 400);
    }

    public function updateheader(Request $request)
    {
        $id = $request->id;
        // $header = $this->user->daily_reports_k3()->find($id);
        $header = DailyReportK3::find($id);
    
        if (!$header) {
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Header Daily Report with id ' . $id . ' cannot be found'
            ], 400);
        }

        $params = $request->all();
    
        $updated = $header->update($params);
    
        if ($updated) {
            return response()->json([
                'success' => true,
                'status' => 200
            ]);
        } else {
            return response()->json([
                'success' => false,
                'status' => 501,
                'message' => 'Sorry, Header Daily Report could not be updated'
            ], 500);
        }
    }

    public function updateDetail(Request $request)
    {
        $id = $request->id;
        // $detail = $this->user->unsafe_k3_detail()->find($id);
        $detail = DailyReportK3Detail::find($id);
    
        if (!$detail) {
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Detail Daily Report with id ' . $id . ' cannot be found'
            ], 400);
        }

        $params = $request->all();

        $foto_bef = $request->file('foto_bef');
        if($foto_bef)
        {
            $destinationPath = 'public/uploads/unsafe/';
            $filenamefotoBef = $foto_bef->getClientOriginalName();
            $foto_bef->move($destinationPath, $filenamefotoBef);
            $detail->foto_bef = $filenamefotoBef;
        } else {
            $check = UnsafeDetail::find($id);
            $params['foto_bef'] = $check->foto_bef;
        }

        $foto_aft = $request->file('foto_aft');
        if($foto_aft)
        {
            $destinationPath = 'public/uploads/unsafe/';
            $filenamefotoAft = $foto_aft->getClientOriginalName();
            $foto_aft->move($destinationPath, $filenamefotoAft);
            $detail->foto_aft = $filenamefotoAft;
        } else {
            $check = UnsafeDetail::find($id);
            $params['foto_aft'] = $check->foto_aft;
        }
    
        $updated = $detail->update($params);
    
        if ($updated) {
            return response()->json([
                'success' => true,
                'status' => 200
            ]);
        } else {
            return response()->json([
                'success' => false,
                'status' => 501,
                'message' => 'Sorry, Detail Daily Report could not be updated'
            ], 500);
        }
    }

    public function destroyheader($id)
    {
        $header = $this->user->daily_reports_k3()->find($id);
    
        if (!$header) {
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Header Daily Report with id ' . $id . ' cannot be found'
            ], 400);
        }
    
        if ($header->delete()) {
            return response()->json([
                'success' => true,
                'message' => 'Successfully deleted Header Daily Report',
                'status' => 200
            ]);
        } else {
            return response()->json([
                'success' => false,
                'status' => 501,
                'message' => 'Header Daily Report could not be deleted'
            ], 500);
        }
    }

    public function destroydetail($id)
    {
        $detail = $this->user->unsafe_details_k3()->find($id);
    
        if (!$detail) {
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Detail Daily Report with id ' . $id . ' cannot be found'
            ], 400);
        }
    
        if ($detail->delete()) {
            return response()->json([
                'success' => true,
                'message' => 'Successfully deleted Detail Daily Report',
                'status' => 200
            ]);
        } else {
            return response()->json([
                'success' => false,
                'status' => 501,
                'message' => 'Detail Daily Report could not be deleted'
            ], 500);
        }
    }
}
