<?php

namespace App\Http\Controllers\API\Safety;

use App\Models\Safety\Tools;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use App\Http\Controllers\Controller;

class ApiToolsController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }

    public function index()
    {   
        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Tools Certification list found',
            'data' => Tools::all()
        ], 200);
    }

    public function search(Request $request)
    {   
        $search = null;
        $search = $request->search;
        if($search){
            $data = Tools::where('equipment', 'LIKE', '%' .$search. '%')->get( );
            $params = [
                'success' => true,
                'status' => 200,
                'message' => 'Tools Certification list found',
                'data' => $data
            ];

        } else {
            $data = Tools::all();
            $params = [
                'success' => true,
                'status' => 200,
                'message' => 'Tools Certification list found',
                'data' => $data
            ];
        }
        return response()->json($params);
    }

    public function show($id)
    {
        // $tool = $this->user->tools()->find($id);
        $tool = Tools::all()->find($id);
    
        if (!$tool) {
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Tools with id ' . $id . ' cannot be found'
            ], 400);
        }
    
        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Tools with id ' . $id . ' found',
            'data' => $tool
        ], 200);
    }

    public function store(Request $request)
    {
        $tool = new Tools();
        $tool->no_buku = $request->no_buku;
        $tool->equipment = $request->equipment;
        $tool->kode = $request->kode;
        $tool->data_teknis = $request->data_teknis;
        $tool->nomor_pengesahan = $request->nomor_pengesahan;
        $tool->lokasi = $request->lokasi;
        $tool->uji_ulang = $request->uji_ulang;
        $tool->note = $request->note;
        $tool->user_name = $request->user_name;
        $tool->uk_kode = $request->uk_kode;
        $tool->uk_text = $request->uk_text;
        $tool->company = $request->company;
        $tool->plant = $request->plant;

        $filenamebuku = null;
        $file_buku = $request->file('file_buku');
        if($file_buku)
        {
            $destinationPath = 'public/uploads/tools/';
            $filenamebuku = $file_buku->getClientOriginalName();
            $file_buku->move($destinationPath, $filenamebuku);
            $tool->file_buku = $filenamebuku;
        } else
        {
            $filenamebuku = null;
        }

        $filenameaktual = null;
        $file_aktual = $request->file('file_aktual');
        if($file_aktual)
        {
            $destinationPath = 'public/uploads/tools/';
            $filenameaktual = $file_aktual->getClientOriginalName();
            $file_aktual->move($destinationPath, $filenameaktual);
            $tool->file_aktual = $filenameaktual;
        } else
        {
            $filenameaktual = null;
        }
        
        $tool->uji_awal = $request->uji_awal;
        $tool->time_certification = $request->time_certification;
        $tool->start_date = $request->start_date;
        $tool->end_date = $request->end_date;
    
        if ($this->user->tools()->save($tool))
            return response()->json([
                'success' => true,
                'status' => 200,
                'message' => 'Succesfully added new Tools Certification',
                'data' => $tool
            ]);
        else
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Tools Certification could not be added'
            ], 400);
    }

    public function update(Request $request)
    {
        $id = $request->id;
        // $tool = $this->user->tools()->find($id);
        $tool = Tools::find($id);
    
        if (!$tool) {
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Tools with id ' . $id . ' cannot be found'
            ], 400);
        }

        $params = $request->all();
        $params['file_buku'] = $request->file('file_buku');

        // $filenamebuku = null;
        $params['file_buku'] = $request->file('file_buku');
        if($params['file_buku'])
        {
            $destinationPath = 'public/uploads/tools/';
            $filenamebuku = $params['file_buku']->getClientOriginalName();
            $params['file_buku']->move($destinationPath, $filenamebuku);
            $params['file_buku'] = $filenamebuku;
        } else {
            $check = Tools::find($id);
            $params['file_buku'] = $check->file_buku;
        }

        $params['file_aktual'] = $request->file('file_aktual');

        // $filenameaktual = null;
        $params['file_aktual'] = $request->file('file_aktual');
        if($params['file_aktual'])
        {
            $destinationPath = 'public/uploads/tools/';
            $filenameaktual = $params['file_aktual']->getClientOriginalName();
            $params['file_aktual']->move($destinationPath, $filenameaktual);
            $params['file_aktual'] = $filenameaktual;
        } else {
            $check = Tools::find($id);
            $params['file_aktual'] = $check->file_aktual;
        }
    
        $updated = $tool->update($params);
    
        if ($updated) {
            return response()->json([
                'success' => true,
                'status' => 200
            ]);
        } else {
            return response()->json([
                'success' => false,
                'status' => 501,
                'message' => 'Sorry, Tools could not be updated'
            ], 500);
        }
    }

    public function destroy($id)
    {
        $tool = $this->user->tools()->find($id);
    
        if (!$tool) {
            return response()->json([
                'success' => false,
                'status' => 401,
                'message' => 'Sorry, Tools with id ' . $id . ' cannot be found'
            ], 400);
        }
    
        if ($tool->delete()) {
            return response()->json([
                'success' => true,
                'message' => 'Successfully deleted Tool',
                'status' => 200
            ]);
        } else {
            return response()->json([
                'success' => false,
                'status' => 501,
                'message' => 'Tools could not be deleted'
            ], 500);
        }
    }
}