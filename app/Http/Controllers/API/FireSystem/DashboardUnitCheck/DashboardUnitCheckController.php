<?php

namespace App\Http\Controllers\Api\FireSystem\DashboardUnitCheck;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FireSystem\UnitCheck\InspectionPMK;
use App\Models\FireSystem\UnitCheck\InspectionPickup;
use DB;

class DashboardUnitCheckController extends Controller
{
    public function getListPMK (Request $request){
      $data = InspectionPMK::select(array(DB:Raw('report_date')))->get();
      $params = [
          'is_success' => true,
          'status' => 200,
          'message' => 'success',
          'data' => $data
      ];
      return response()->json($params);
    }

    public function getLisFFtPMK (Request $request){
      $data = InspectionPMK::all();
      $params = [
          'is_success' => true,
          'status' => 200,
          'message' => 'success',
          'data' => $data
      ];
      return response()->json($params);
    }

    public function getInspectionPMK(Request $request){

        $apiName='INSPECTION_PMK';
        $vehicle_code=null;
        $vehicle_code=$request->vehicle_code;

        if($vehicle_code){
          $data = InspectionPMK::where('vehicle_code', 'LIKE', '%' .$vehicle_code. '%')->get();
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];

        } else {
              $data = InspectionPMK::all();
              $params = [
                  'is_success' => true,
                  'status' => 200,
                  'message' => 'success',
                  'data' => $data
              ];
        }
        return response()->json($params);
    }
}
