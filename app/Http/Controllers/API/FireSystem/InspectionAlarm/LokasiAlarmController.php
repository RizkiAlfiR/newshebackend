<?php

namespace App\Http\Controllers\Api\FireSystem\InspectionAlarm;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FireSystem\InspectionAlarm\LokasiAlarm;

class LokasiAlarmController extends Controller
{
    public function getLokasiAlarm(Request $request){

        $apiName='LOKASI_ALARM';
        $id_alarm=null;
        $id_alarm = $request->id_alarm;

        if($id_alarm){
          $data = LokasiAlarm::find($id_alarm);
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];

        } else {
          $data = LokasiAlarm::all();
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];
        }
        return response()->json($params);
    }
}
