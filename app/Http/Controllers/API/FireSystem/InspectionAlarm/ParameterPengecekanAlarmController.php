<?php

namespace App\Http\Controllers\Api\FireSystem\InspectionAlarm;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FireSystem\InspectionAlarm\ParameterPengecekanAlarm;

class ParameterPengecekanAlarmController extends Controller
{
    public function getParameterPengecekanAlarm(Request $request){

        $apiName='PENGECEKAN_ALARM';
        $type=null;
        $type = $request->type;
        if($type){
          $data = ParameterPengecekanAlarm::where('type', 'LIKE', '%' .$type. '%')->limit(10)->get( );
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];

        } else {
              $data = ParameterPengecekanAlarm::all();
              $params = [
                  'is_success' => true,
                  'status' => 200,
                  'message' => 'success',
                  'data' => $data
              ];
        }
        return response()->json($params);
    }
}
