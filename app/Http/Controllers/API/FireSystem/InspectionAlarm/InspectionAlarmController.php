<?php

namespace App\Http\Controllers\Api\FireSystem\InspectionAlarm;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FireSystem\InspectionAlarm\InspectionAlarm;

class InspectionAlarmController extends Controller
{
    public function addInspectionAlarm (Request $request){

        $apiName='INSPECTION_ALARM';
        $id_alarm=$request->id_alarm;
        $location_name=$request->location_name;
        $year=$request->year;
        $month=$request->month;
        $inspection_date=$request->inspection_date;
        $type_periode=$request->type_periode;
        $detail=$request->detail;

        $opt_filename=null;
        $opt_file = $request->file('opt_file');
        if($opt_file){
            $destinationPath = 'images/';
            $opt_filename = $opt_file->getClientOriginalName();
            $opt_file->move($destinationPath, $opt_filename);
        } else {
            $opt_filename=null;
        }

        $sendingParams = [
          'id_alarm' => $id_alarm,
          'location_name' => $location_name,
          'year' => $year,
          'month' => $month,
          'inspection_date' => $inspection_date,
          'type_periode' => $type_periode,
          'detail' => $detail,
          'opt_file' => $opt_file,
        ];


        try{
            $data = new InspectionAlarm();

            $data->id_alarm=$id_alarm;
            $data->location_name=$location_name;
            $data->year=$year;
            $data->month=$month;
            $data->inspection_date=$inspection_date;
            $data->type_periode=$type_periode;
            $data->detail=$detail;
            $data->opt_file=$opt_filename;

            $data->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        }catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));

        }

        return response()->json($params);
    }

    public function getInspectionAlarm(Request $request){

        $apiName='INSPECTION_ALARM';
        $inspection_date=null;
        $start_date=null;
        $end_date=null;
        $inspection_date = $request->inspection_date;
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if($inspection_date){
          $data = InspectionAlarm::where('inspection_date', 'LIKE', '%' .$inspection_date. '%')->limit(10)->get( );
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];

        } elseif ($start_date) {
          $data = InspectionAlarm::select("*")->whereBetween('inspection_date', [$start_date, $end_date])->get();
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];
        }else {
              $data = InspectionAlarm::all();
              $params = [
                  'is_success' => true,
                  'status' => 200,
                  'message' => 'success',
                  'data' => $data
              ];
        }
        return response()->json($params);
    }

    public function detailInspectionAlarm(Request $request){

        $id = $request->id;

        $data = InspectionAlarm::where('id', 'LIKE', '%' .$id. '%')->limit(10)->get( );
        $params = [
            'is_success' => true,
            'status' => 200,
            'message' => 'success',
            'data' => $data
        ];

        return response()->json($params);
    }

    public function updateInspectionAlarm (Request $request){

        $apiName='UPDATE_INSPECTION_ALARM';
        $id=$request->id;
        $id_alarm=$request->id_alarm;
        $location_name=$request->location_name;
        $year=$request->year;
        $month=$request->month;
        $inspection_date=$request->inspection_date;
        $type_periode=$request->type_periode;
        $detail=$request->detail;

        $opt_filename=null;
        $opt_file = $request->file('opt_file');
        if($opt_file){
            $destinationPath = 'images/';
            $opt_filename = $opt_file->getClientOriginalName();
            $opt_file->move($destinationPath, $opt_filename);
        } else {
            $opt_filename=null;
        }

        $sendingParams = [
          'id' => $id,
          'id_alarm' => $id_alarm,
          'location_name' => $location_name,
          'year' => $year,
          'month' => $month,
          'inspection_date' => $inspection_date,
          'type_periode' => $type_periode,
          'detail' => $detail,
          'opt_file' => $opt_file,
        ];


        try{
            $data = InspectionAlarm::find($id);
            $data->id_alarm=$id_alarm;
            $data->location_name=$location_name;
            $data->year=$year;
            $data->month=$month;
            $data->inspection_date=$inspection_date;
            $data->type_periode=$type_periode;
            $data->detail=$detail;
            $data->opt_file=$opt_filename;

            $data->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        }catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));

        }

        return response()->json($params);
    }

    public  function  deleteInspectionAlarm(Request $request){
        $apiName='DELETE_INSPECTION_ALARM';
        $id=$request->id;
        $sendingParams = [
            'id' => $id,
        ];

        try {
            InspectionAlarm::find($id)->delete();
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
            ];
            return response()->json($params);

        } catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404, $e->getMessage(),json_encode($sendingParams));
        }
    }
}
