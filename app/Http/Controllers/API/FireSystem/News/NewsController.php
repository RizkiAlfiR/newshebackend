<?php

namespace App\Http\Controllers\Api\FireSystem\News;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FireSystem\DailySectionActivity\DailySectionActivity;
use App\Models\FireSystem\FireReport\FireReport;
use App\Models\FireSystem\Lotto\Lotto;
use App\Models\FireSystem\DailySectionActivity\Officer;

class NewsController extends Controller
{
    public function getNewsOfficer(Request $request){
        $apiName='NEWS_OFFICER';
        $date_activity=date('y-m-d');

        $data = Officer::where('date_activity', 'LIKE', '%' .$date_activity. '%')->get();
        $params = [
            'is_success' => true,
            'status' => 200,
            'message' => 'success',
            'data' => $data
        ];
        return response()->json($params);
    }


    public function getNewsDaily(Request $request){
        $apiName='NEWS_DAILY';

        $data = DailySectionActivity::orderBy('id', 'DESC')->limit(10)->get();
        $params = [
            'is_success' => true,
            'status' => 200,
            'message' => 'success',
            'data' => $data
        ];
        return response()->json($params);
    }

    public function getNewsFire(Request $request){
        $apiName='NEWS_FIRE';

        $data = FireReport::with('image')->orderBy('id', 'DESC')->limit(10)->get();
        $params = [
            'is_success' => true,
            'status' => 200,
            'message' => 'success',
            'data' => $data
        ];
        return response()->json($params);
    }

    public function getNewsLotto(Request $request){
        $apiName='NEWS_LOTTO';

        $data = Lotto::orderBy('id', 'DESC')->limit(10)->get();
        $params = [
            'is_success' => true,
            'status' => 200,
            'message' => 'success',
            'data' => $data
        ];
        return response()->json($params);
    }


}
