<?php

namespace App\Http\Controllers\Api\FireSystem\FireReport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FireSystem\FireReport\FireReportImage;

class FireReportImageController extends Controller
{
    public function addFireReportImage (Request $request){

        $apiName='ADD_FIRE_REPORT';
        $id_fire_report=$request->id_fire_report;
        $description=$request->description;
        $status=$request->status;
        $image_filename=null;
        $image = $request->file('image');
        if($image){
            $destinationPath = 'public/uploads/FireReport/';
            $image_filename = $image->getClientOriginalName();
            $image->move($destinationPath, $image_filename);
        } else {
            $image_filename=null;
        }

        $sendingParams = [
          'id_fire_report' => $id_fire_report,
          'description' => $description,
          'status' => $status,
          'image' => $image,
        ];


        try{
            $data = new FireReportImage();
            $data->id_fire_report=$id_fire_report;
            $data->description=$description;
            $data->status=$status;
            $data->image=$image_filename;

            $data->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        }catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));

        }

        return response()->json($params);
    }

    public function getFireReportImage(Request $request){

        $apiName='GET_FIRE_REPORT_IMAGE';
        $id_fire_report=null;
        $id_fire_report=$request->id_fire_report;

        if($id_fire_report){
          $data = FireReportImage::where('id_fire_report', 'LIKE', '%' .$id_fire_report. '%')->get( );
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];
        } else {
          $data = FireReportImage::all();
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];
        }
        return response()->json($params);
    }

    public function updateFireReportImage (Request $request){

        $apiName='UPDATE_FIRE_REPORT';
        $id=$request->id;
        $id_fire_report=$request->id_fire_report;
        $description=$request->description;
        $status=$request->status;

        $image_filename=null;
        $image = $request->file('image');
        if($image){
            $destinationPath = 'public/uploads/FireReport/';
            $image_filename = $image->getClientOriginalName();
            $image->move($destinationPath, $image_filename);
        } else {
            $image_filename=null;
        }

        $sendingParams = [
          'id_fire_report' => $id_fire_report,
          'description' => $description,
          'status' => $status,
          'image' => $image,
        ];

        try{
            $data = FireReportImage::find($id);
            $data->id_fire_report=$id_fire_report;
            $data->description=$description;
            $data->status=$status;
            $data->image=$image_filename;

            $data->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);
            $data->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        }catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));

        }

        return response()->json($params);
    }

    public  function  deleteFireReportImage(Request $request){
        $apiName='DELETE_FIRE_REPORT_IMAGE';
        $id=$request->id;
        $sendingParams = [
            'id' => $id,
        ];

        try {
            FireReportImage::find($id)->delete();
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
            ];
            return response()->json($params);

        } catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404, $e->getMessage(),json_encode($sendingParams));
        }
    }
}
