<?php

namespace App\Http\Controllers\Api\FireSystem\FireReport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FireSystem\FireReport\FireReport;

class FireReportController extends Controller
{
    public function addFireReport (Request $request){

        $year = date('Y');
        $month = date('m');
        $day = date('d');

        $lastID = FireReport::orderBy('id', 'DESC')->first();
        $last = $lastID->id+1;
        $number = $last.'/SH/'.$month.'/'.$day.'/FR/'.$month.'/'.$year;

        $apiName='ADD_FIRE_REPORT';
        $fire_date=$request->fire_date;
        $fire_time=$request->fire_time;
        $job_walking_timestart=$request->job_walking_timestart;
        $job_walking_timeend=$request->job_walking_timeend;
        $pic_badge=$request->pic_badge;
        $pic_name=$request->pic_name;
        $incident_place=$request->incident_place;
        $note_place=$request->note_place;
        $fire_happened_on=$request->fire_happened_on;
        $damage_from_fire=$request->damage_from_fire;
        $fire_material_type=$request->fire_material_type;
        $number_victims=$request->number_victims;
        $description_of_fire=$request->description_of_fire;
        $unsafe_condition=$request->unsafe_condition;
        $unsafe_action=$request->unsafe_action;
        $list_employees=$request->list_employees;
        $work_on_fire=$request->work_on_fire;
        $fire_extinguishers=$request->fire_extinguishers;
        $shift=$request->shift;
        $list_tembusan=$request->list_tembusan;
        $saran=$request->saran;
        $company=$request->company;
        $plant=$request->plant;
        $fire_number=$number;
        $status=$request->status;
        $followup_at=$request->followup_at;
        $followup_note=$request->followup_note;


        $sendingParams = [
          'fire_date' => $fire_date,
          'fire_time' => $fire_time,
          'job_walking_timestart' => $job_walking_timestart,
          'job_walking_timeend' => $job_walking_timeend,
          'pic_badge' => $pic_badge,
          'pic_name' => $pic_name,
          'incident_place' => $incident_place,
          'note_place' => $note_place,
          'fire_happened_on' => $fire_happened_on,
          'damage_from_fire' => $damage_from_fire,
          'fire_material_type' => $fire_material_type,
          'number_victims' => $number_victims,
          'description_of_fire' => $description_of_fire,
          'unsafe_condition' => $unsafe_condition,
          'unsafe_action' => $unsafe_action,
          'list_employees' => $list_employees,
          'work_on_fire' => $work_on_fire,
          'fire_extinguishers' => $fire_extinguishers,
          'shift' => $shift,
          'list_tembusan' => $list_tembusan,
          'saran' => $saran,
          'company' => $company,
          'plant' => $plant,
          'fire_number' => $fire_number,
          'status' => $status,
          'followup_at' => $followup_at,
          'followup_note' => $followup_note,
        ];


        try{
            $data = new FireReport();
            $data->fire_date=$fire_date;
            $data->fire_time=$fire_time;
            $data->job_walking_timestart=$job_walking_timestart;
            $data->job_walking_timeend=$job_walking_timeend;
            $data->pic_badge=$pic_badge;
            $data->pic_name=$pic_name;
            $data->incident_place=$incident_place;
            $data->note_place=$note_place;
            $data->fire_happened_on=$fire_happened_on;
            $data->damage_from_fire=$damage_from_fire;
            $data->fire_material_type=$fire_material_type;
            $data->number_victims=$number_victims;
            $data->description_of_fire=$description_of_fire;
            $data->unsafe_condition=$unsafe_condition;
            $data->unsafe_action=$unsafe_action;
            $data->list_employees=$list_employees;
            $data->work_on_fire=$work_on_fire;
            $data->fire_extinguishers=$fire_extinguishers;
            $data->shift=$shift;
            $data->list_tembusan=$list_tembusan;
            $data->saran=$saran;
            $data->company=$company;
            $data->plant=$plant;
            $data->fire_number=$fire_number;
            $data->status=$status;
            $data->followup_at=$followup_at;
            $data->followup_note=$followup_note;
            $data->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        }catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
        }
        return response()->json($params);
    }

    public function getFireReport(Request $request){

        $apiName='FIRE_REPORT';
        $fire_date=null;
        $start_date=null;
        $end_date=null;
        $fire_number=null;
        $fire_date = $request->fire_date;
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $fire_number = $request->fire_number;

        if($fire_date){
          $data = FireReport::where('fire_date', 'LIKE', '%' .$fire_date. '%')->get( );
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];

        } else if ($start_date) {
          $data = FireReport::select("*")->whereBetween('fire_date', [$start_date, $end_date])->get();
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];
        } else if ($fire_number){
          $data = FireReport::where('fire_number', 'LIKE', '%' .$fire_number. '%')->limit(10)->get( );
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];
        } else {
          $data = FireReport::orderBy('id', 'DESC')->get();
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];
        }
        return response()->json($params);
    }

    public function detailFireReport(Request $request)
    {
        $apiName='DETAIL_FIRE_REPORT';
        $id=$request->id;

        $sendingParams = [
            'id' => $id,
        ];
        $data=FireReport::with('image')->find($id);

        return response()->json([
            'is_success' => true,
            'status' => 200,
            'message' => 'success',
            'data' => $data
        ], 200);
    }

    public function updateFireReport (Request $request){

        $apiName='UPDATE_FIRE_REPORT';
        $id=$request->id;
        $fire_date=$request->fire_date;
        $fire_time=$request->fire_time;
        $job_walking_timestart=$request->job_walking_timestart;
        $job_walking_timeend=$request->job_walking_timeend;
        $pic_badge=$request->pic_badge;
        $pic_name=$request->pic_name;
        $incident_place=$request->incident_place;
        $note_place=$request->note_place;
        $fire_happened_on=$request->fire_happened_on;
        $damage_from_fire=$request->damage_from_fire;
        $fire_material_type=$request->fire_material_type;
        $number_victims=$request->number_victims;
        $description_of_fire=$request->description_of_fire;
        $unsafe_condition=$request->unsafe_condition;
        $unsafe_action=$request->unsafe_action;
        $list_employees=$request->list_employees;
        $work_on_fire=$request->work_on_fire;
        $fire_extinguishers=$request->fire_extinguishers;
        $shift=$request->shift;
        $list_tembusan=$request->list_tembusan;
        $saran=$request->saran;
        $company=$request->company;
        $plant=$request->plant;
        $fire_number=$request->fire_number;
        $status=$request->status;
        $followup_at=$request->followup_at;
        $followup_note=$request->followup_note;

        $sendingParams = [
          'id' => $id,
          'fire_date' => $fire_date,
          'fire_time' => $fire_time,
          'job_walking_timestart' => $job_walking_timestart,
          'job_walking_timeend' => $job_walking_timeend,
          'pic_badge' => $pic_badge,
          'pic_name' => $pic_name,
          'incident_place' => $incident_place,
          'note_place' => $note_place,
          'fire_happened_on' => $fire_happened_on,
          'damage_from_fire' => $damage_from_fire,
          'fire_material_type' => $fire_material_type,
          'number_victims' => $number_victims,
          'description_of_fire' => $description_of_fire,
          'unsafe_condition' => $unsafe_condition,
          'unsafe_action' => $unsafe_action,
          'list_employees' => $list_employees,
          'work_on_fire' => $work_on_fire,
          'fire_extinguishers' => $fire_extinguishers,
          'shift' => $shift,
          'list_tembusan' => $list_tembusan,
          'saran' => $saran,
          'company' => $company,
          'plant' => $plant,
          'fire_number' => $fire_number,
          'status' => $status,
          'followup_at' => $followup_at,
          'followup_note' => $followup_note,
        ];

        try{
            $data = FireReport::find($id);
            $data->fire_date=$fire_date;
            $data->fire_time=$fire_time;
            $data->job_walking_timestart=$job_walking_timestart;
            $data->job_walking_timeend=$job_walking_timeend;
            $data->pic_badge=$pic_badge;
            $data->pic_name=$pic_name;
            $data->incident_place=$incident_place;
            $data->note_place=$note_place;
            $data->fire_happened_on=$fire_happened_on;
            $data->damage_from_fire=$damage_from_fire;
            $data->fire_material_type=$fire_material_type;
            $data->number_victims=$number_victims;
            $data->description_of_fire=$description_of_fire;
            $data->unsafe_condition=$unsafe_condition;
            $data->unsafe_action=$unsafe_action;
            $data->list_employees=$list_employees;
            $data->work_on_fire=$work_on_fire;
            $data->fire_extinguishers=$fire_extinguishers;
            $data->shift=$shift;
            $data->list_tembusan=$list_tembusan;
            $data->saran=$saran;
            $data->company=$company;
            $data->plant=$plant;
            $data->fire_number=$fire_number;
            $data->status=$status;
            $data->followup_at=$followup_at;
            $data->followup_note=$followup_note;
            $data->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        }catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));

        }

        return response()->json($params);
    }

    public  function  deleteFireReport(Request $request){
        $apiName='DELETE_FIRE_REPORT';
        $id=$request->id;
        $sendingParams = [
            'id' => $id,
        ];

        try {
            FireReport::find($id)->delete();
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
            ];
            return response()->json($params);

        } catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404, $e->getMessage(),json_encode($sendingParams));
        }
    }
}
