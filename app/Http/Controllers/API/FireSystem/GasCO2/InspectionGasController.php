<?php

namespace App\Http\Controllers\Api\FireSystem\GasCO2;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FireSystem\GasCO2\InspectionGas;

class InspectionGasController extends Controller
{
    public function addInspectionGas (Request $request){
        $apiName='INSPECTION_GAS';
        $plant=$request->plant;
        $pplant=$request->pplant;
        $pplantdesc=$request->pplantdesc;
        $total_max_cemetron=$request->total_max_cemetron;
        $total_residual_cemetron=$request->total_residual_cemetron;
        $total_usage_cemetron=$total_max_cemetron-$total_residual_cemetron;
        $total_max_samator=$request->total_max_samator;
        $total_residual_samator=$request->total_residual_samator;
        $total_usage_samator=$total_max_samator-$total_residual_samator;
        $inspection_date=date('Y-m-d');
        $inspection_time=date('H:i:s');
        $pic_name=$request->pic_name;
        $pic_badge=$request->pic_badge;
        $shift=$request->shift;
        $note=$request->note;

        $sendingParams = [
          'plant' => $plant,
          'pplant' => $pplant,
          'pplantdesc' => $pplantdesc,
          'total_max_cemetron' => $total_max_cemetron,
          'total_residual_cemetron' => $total_residual_cemetron,
          'total_usage_cemetron' => $total_usage_cemetron,
          'total_max_samator' => $total_max_samator,
          'total_residual_samator' => $total_residual_samator,
          'total_usage_samator' => $total_usage_samator,
          'inspection_date' => $inspection_date,
          'inspection_time' => $inspection_time,
          'pic_name' => $pic_name,
          'pic_badge' => $pic_badge,
          'shift' => $shift,
          'note' => $note,
        ];

        try{
            $data = new InspectionGas();
            $data->plant=$plant;
            $data->pplant=$pplant;
            $data->pplantdesc=$pplantdesc;
            $data->total_max_cemetron=$total_max_cemetron;
            $data->total_residual_cemetron=$total_residual_cemetron;
            $data->total_usage_cemetron=$total_usage_cemetron;
            $data->total_max_samator=$total_max_samator;
            $data->total_residual_samator=$total_residual_samator;
            $data->total_usage_samator=$total_usage_samator;
            $data->inspection_date=$inspection_date;
            $data->inspection_time=$inspection_time;
            $data->pic_name=$pic_name;
            $data->pic_badge=$pic_badge;
            $data->shift=$shift;
            $data->note=$note;
            $data->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        }catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));

        }
        return response()->json($params);
    }

    public function updateInspectionGas (Request $request){

        $apiName='UPDATE_INSPECTION_GAS';
        $id=$request->id;
        $plant=$request->plant;
        $pplant=$request->pplant;
        $pplantdesc=$request->pplantdesc;
        $total_max_cemetron=$request->total_max_cemetron;
        $total_residual_cemetron=$request->total_residual_cemetron;
        $total_usage_cemetron=$total_max_cemetron-$total_residual_cemetron;
        $total_max_samator=$request->total_max_samator;
        $total_residual_samator=$request->total_residual_samator;
        $total_usage_samator=$total_max_samator-$total_residual_samator;
        $inspection_date=$request->inspection_date;
        $inspection_time=$request->inspection_time;
        $pic_name=$request->pic_name;
        $pic_badge=$request->pic_badge;
        $shift=$request->shift;
        $note=$request->note;

        $sendingParams = [
            'id' => $id,
            'plant' => $plant,
            'pplant' => $pplant,
            'pplantdesc' => $pplantdesc,
            'total_max_cemetron' => $total_max_cemetron,
            'total_residual_cemetron' => $total_residual_cemetron,
            'total_usage_cemetron' => $total_usage_cemetron,
            'total_max_samator' => $total_max_samator,
            'total_residual_samator' => $total_residual_samator,
            'total_usage_samator' => $total_usage_samator,
            'inspection_date' => $inspection_date,
            'inspection_time' => $inspection_time,
            'pic_name' => $pic_name,
            'pic_badge' => $pic_badge,
            'shift' => $shift,
            'note' => $note,
        ];

        try{
            $data = InspectionGas::find($id);
            $data->plant=$plant;
            $data->pplant=$pplant;
            $data->pplantdesc=$pplantdesc;
            $data->total_max_cemetron=$total_max_cemetron;
            $data->total_residual_cemetron=$total_residual_cemetron;
            $data->total_usage_cemetron=$total_usage_cemetron;
            $data->total_max_samator=$total_max_samator;
            $data->total_residual_samator=$total_residual_samator;
            $data->total_usage_samator=$total_usage_samator;
            $data->inspection_date=$inspection_date;
            $data->inspection_time=$inspection_time;
            $data->pic_name=$pic_name;
            $data->pic_badge=$pic_badge;
            $data->shift=$shift;
            $data->note=$note;
            $data->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        }catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404, $e->getMessage(),json_encode($sendingParams));

        }
        return response()->json($params);
    }

    public function getInspectionGas(Request $request){

        $apiName='INSPECTION_GAS';
        $inspection_date=null;
        $pplantdesc=null;
        $inspection_date = $request->inspection_date;
        $pplantdesc = $request->pplantdesc;

        if($inspection_date){
          $data = InspectionGas::where('inspection_date', 'LIKE', '%' .$inspection_date. '%')->get( );
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];

        } elseif ($pplantdesc) {
          $data = InspectionGas::where('pplantdesc', 'LIKE', '%' .$pplantdesc. '%')->get( );
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];
        }else {
              $data = InspectionGas::orderBy('id', 'DESC')->get();
              $params = [
                  'is_success' => true,
                  'status' => 200,
                  'message' => 'success',
                  'data' => $data
              ];
        }
        return response()->json($params);
    }

    public function detailInspectionGas(Request $request){
        $apiName='DETAIL_INSPECTION_GAS';
        $id=$request->id;

        $sendingParams = [
            'id' => $id,
        ];

        $data = InspectionGas::find($id);
        $params = [
            'is_success' => true,
            'status' => 200,
            'message' => 'success',
            'data' => $data
        ];

        return response()->json($params);
    }

    public  function  deleteInspectionGas(Request $request){
        $apiName='DELETE_INSPECTION_GAS';
        $id=$request->id;
        $sendingParams = [
            'id' => $id,
        ];

        try {
            InspectionGas::find($id)->delete();
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
            ];
            return response()->json($params);

        } catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404, $e->getMessage(),json_encode($sendingParams));
        }
    }
}
