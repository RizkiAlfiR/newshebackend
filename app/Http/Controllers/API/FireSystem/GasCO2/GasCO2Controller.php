<?php

namespace App\Http\Controllers\Api\FireSystem\GasCO2;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FireSystem\GasCO2\GasCO2;

class GasCO2Controller extends Controller
{
    public function getGasCO2(Request $request){

    $apiName='GAS_CO2';
    $searchEmp=null;
    $searchEmp = $request->searchEmp;
    if($searchEmp){
      $data = GasCO2::where('plant', 'LIKE', '%' .$searchEmp. '%')->get();
      $params = [
          'is_success' => true,
          'status' => 200,
          'message' => 'success',
          'data' => $data
      ];

    } else {
          $data = GasCO2::all();
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];
    }
    return response()->json($params);
  }
}
