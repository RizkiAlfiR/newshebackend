<?php

namespace App\Http\Controllers\Api\FireSystem\UnitCheck;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FireSystem\UnitCheck\InspectionPickup;
use App\Models\FireSystem\UnitCheck\ListVehicle;

class InspectionPickupController extends Controller
{
    public function addInspectionPickup (Request $request){

        $apiName='INSPECTION_PICKUP';
        $vehicle_id=$request->vehicle_id;
        $vehicle_code=$request->vehicle_code;
        $report_date=date('Y-m-d');
        $report_time=date('H:i:s');
        $shift=$request->shift;
        $pemanasan=$request->pemanasan;
        $spedometer=$request->spedometer;
        $level_bbm=$request->level_bbm;
        $oli_mesin=$request->oli_mesin;
        $oli_rem=$request->oli_rem;
        $oli_power=$request->oli_power;
        $air_radiator=$request->air_radiator;
        $air_cad_radiator=$request->air_cad_radiator;
        $air_wiper=$request->air_wiper;
        $lampu_cabin=$request->lampu_cabin;
        $lampu_kota=$request->lampu_kota;
        $lampu_jauh=$request->lampu_jauh;
        $lampu_sign_kiri=$request->lampu_sign_kiri;
        $lampu_sign_kanan=$request->lampu_sign_kanan;
        $lampu_rem=$request->lampu_rem;
        $lampu_atret=$request->lampu_atret;
        $lampu_sorot=$request->lampu_sorot;
        $panel_dashboard=$request->panel_dashboard;
        $ban_depan_kiri=$request->ban_depan_kiri;
        $ban_depan_kanan=$request->ban_depan_kanan;
        $ban_belakang_kiri_dalam=$request->ban_belakang_kiri_dalam;
        $ban_belakang_kiri_luar=$request->ban_belakang_kiri_luar;
        $ban_belakang_kanan_dalam=$request->ban_belakang_kanan_dalam;
        $ban_belakang_kanan_luar=$request->ban_belakang_kanan_luar;
        $kaca_spion_kiri=$request->kaca_spion_kiri;
        $kaca_spion_kanan=$request->kaca_spion_kanan;
        $pic_badge=$request->pic_badge;
        $pic_name=$request->pic_name;

        $sendingParams = [
          'vehicle_id' => $vehicle_id,
          'vehicle_code' => $vehicle_code,
          'report_date' => $report_date,
          'report_time' => $report_time,
          'shift' => $shift,
          'pemanasan' => $pemanasan,
          'spedometer' => $spedometer,
          'level_bbm' => $level_bbm,
          'oli_mesin' => $oli_mesin,
          'oli_rem' => $oli_rem,
          'oli_power' => $oli_power,
          'air_radiator' => $air_radiator,
          'air_cad_radiator' => $air_cad_radiator,
          'air_wiper' => $air_wiper,
          'lampu_cabin' => $lampu_cabin,
          'lampu_kota' => $lampu_kota,
          'lampu_jauh' => $lampu_jauh,
          'lampu_sign_kiri' => $lampu_sign_kiri,
          'lampu_sign_kanan' => $lampu_sign_kanan,
          'lampu_rem' => $lampu_rem,
          'lampu_atret' => $lampu_atret,
          'lampu_sorot' => $lampu_sorot,
          'panel_dashboard' => $panel_dashboard,
          'ban_depan_kiri' => $ban_depan_kiri,
          'ban_depan_kanan' => $ban_depan_kanan,
          'ban_belakang_kiri_dalam' => $ban_belakang_kiri_dalam,
          'ban_belakang_kiri_luar' => $ban_belakang_kiri_luar,
          'ban_belakang_kanan_dalam' => $ban_belakang_kanan_dalam,
          'ban_belakang_kanan_luar' => $ban_belakang_kanan_luar,
          'kaca_spion_kiri' => $kaca_spion_kiri,
          'kaca_spion_kanan' => $kaca_spion_kanan,
          'pic_badge' => $pic_badge,
          'pic_name' => $pic_name,
        ];

        try{
            $data = new InspectionPickup();
            $data->vehicle_id=$vehicle_id;
            $data->vehicle_code=$vehicle_code;
            $data->report_date=$report_date;
            $data->report_time=$report_time;
            $data->shift=$shift;
            $data->pemanasan=$pemanasan;
            $data->spedometer=$spedometer;
            $data->level_bbm=$level_bbm;
            $data->oli_mesin=$oli_mesin;
            $data->oli_rem=$oli_rem;
            $data->oli_power=$oli_power;
            $data->air_radiator=$air_radiator;
            $data->air_cad_radiator=$air_cad_radiator;
            $data->air_wiper=$air_wiper;
            $data->lampu_cabin=$lampu_cabin;
            $data->lampu_kota=$lampu_kota;
            $data->lampu_jauh=$lampu_jauh;
            $data->lampu_sign_kiri=$lampu_sign_kiri;
            $data->lampu_sign_kanan=$lampu_sign_kanan;
            $data->lampu_rem=$lampu_rem;
            $data->lampu_atret=$lampu_atret;
            $data->lampu_sorot=$lampu_sorot;
            $data->panel_dashboard=$panel_dashboard;
            $data->ban_depan_kiri=$ban_depan_kiri;
            $data->ban_depan_kanan=$ban_depan_kanan;
            $data->ban_belakang_kiri_dalam=$ban_belakang_kiri_dalam;
            $data->ban_belakang_kiri_luar=$ban_belakang_kiri_luar;
            $data->ban_belakang_kanan_dalam=$ban_belakang_kanan_dalam;
            $data->ban_belakang_kanan_luar=$ban_belakang_kanan_luar;
            $data->kaca_spion_kiri=$kaca_spion_kiri;
            $data->kaca_spion_kanan=$kaca_spion_kanan;
            $data->pic_badge=$pic_badge;
            $data->pic_name=$pic_name;

            $dataVehicle = ListVehicle::find($vehicle_id);
            $dataVehicle->type=$dataVehicle->type;
            $dataVehicle->vehicle_code=$dataVehicle->vehicle_code;
            $dataVehicle->vehicle_merk=$dataVehicle->vehicle_merk;
            $dataVehicle->tahun=$dataVehicle->tahun;
            $dataVehicle->vehicle_type=$dataVehicle->vehicle_type;
            $dataVehicle->nomor_rangka=$dataVehicle->nomor_rangka;
            $dataVehicle->company=$dataVehicle->company;
            $dataVehicle->plant=$dataVehicle->plant;
            $dataVehicle->image=$dataVehicle->image;
            $dataVehicle->last_inspection=date('Y-m-d');
            $dataVehicle->exp_inspection=date('Y-m-d', strtotime('+1 days', strtotime($dataVehicle->last_inspections)));
            $dataVehicle->sum_negative='30';

            $data->save();
            $dataVehicle->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        }catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));

        }

        return response()->json($params);
    }

    public function getInspectionPickup(Request $request){

        $apiName='INSPECTION_PICKUP';
        $vehicle_code=null;
        $vehicle_code=$request->vehicle_code;

        if($vehicle_code){
          $data = InspectionPickup::where('vehicle_code', 'LIKE', '%' .$vehicle_code. '%')->get();
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];

        } else {
              $data = InspectionPickup::all();
              $params = [
                  'is_success' => true,
                  'status' => 200,
                  'message' => 'success',
                  'data' => $data
              ];
        }
        return response()->json($params);
    }

    public function detailInspectionPickup(Request $request){

        $id = $request->id;

        $data = InspectionPickup::where('id', 'LIKE', '%' .$id. '%')->get();
        $params = [
            'is_success' => true,
            'status' => 200,
            'message' => 'success',
            'data' => $data
        ];

        return response()->json($params);
    }

    public function updateInspectionPickup (Request $request){

        $apiName='UPDATE_INSPECTION_PICKUP';
        $id=$request->id;
        $vehicle_code=$request->vehicle_code;
        $report_date=$request->report_date;
        $report_time=$request->report_time;
        $shift=$request->shift;
        $pemanasan=$request->pemanasan;
        $spedometer=$request->spedometer;
        $level_bbm=$request->level_bbm;
        $oli_mesin=$request->oli_mesin;
        $oli_rem=$request->oli_rem;
        $oli_power=$request->oli_power;
        $air_radiator=$request->air_radiator;
        $air_cad_radiator=$request->air_cad_radiator;
        $air_wiper=$request->air_wiper;
        $lampu_cabin=$request->lampu_cabin;
        $lampu_kota=$request->lampu_kota;
        $lampu_jauh=$request->lampu_jauh;
        $lampu_sign_kiri=$request->lampu_sign_kiri;
        $lampu_sign_kanan=$request->lampu_sign_kanan;
        $lampu_rem=$request->lampu_rem;
        $lampu_atret=$request->lampu_atret;
        $lampu_sorot=$request->lampu_sorot;
        $panel_dashboard=$request->panel_dashboard;
        $ban_depan_kiri=$request->ban_depan_kiri;
        $ban_depan_kanan=$request->ban_depan_kanan;
        $ban_belakang_kiri_dalam=$request->ban_belakang_kiri_dalam;
        $ban_belakang_kiri_luar=$request->ban_belakang_kiri_luar;
        $ban_belakang_kanan_dalam=$request->ban_belakang_kanan_dalam;
        $ban_belakang_kanan_luar=$request->ban_belakang_kanan_luar;
        $kaca_spion_kiri=$request->kaca_spion_kiri;
        $kaca_spion_kanan=$request->kaca_spion_kanan;
        $pic_badge=$request->pic_badge;
        $pic_name=$request->pic_name;

        $sendingParams = [
          'id' => $id,
          'vehicle_code' => $vehicle_code,
          'report_date' => $report_date,
          'report_time' => $report_time,
          'shift' => $shift,
          'pemanasan' => $pemanasan,
          'spedometer' => $spedometer,
          'level_bbm' => $level_bbm,
          'oli_mesin' => $oli_mesin,
          'oli_rem' => $oli_rem,
          'oli_power' => $oli_power,
          'air_radiator' => $air_radiator,
          'air_cad_radiator' => $air_cad_radiator,
          'air_wiper' => $air_wiper,
          'lampu_cabin' => $lampu_cabin,
          'lampu_kota' => $lampu_kota,
          'lampu_jauh' => $lampu_jauh,
          'lampu_sign_kiri' => $lampu_sign_kiri,
          'lampu_sign_kanan' => $lampu_sign_kanan,
          'lampu_rem' => $lampu_rem,
          'lampu_atret' => $lampu_atret,
          'lampu_sorot' => $lampu_sorot,
          'panel_dashboard' => $panel_dashboard,
          'ban_depan_kiri' => $ban_depan_kiri,
          'ban_depan_kanan' => $ban_depan_kanan,
          'ban_belakang_kiri_dalam' => $ban_belakang_kiri_dalam,
          'ban_belakang_kiri_luar' => $ban_belakang_kiri_luar,
          'ban_belakang_kanan_dalam' => $ban_belakang_kanan_dalam,
          'ban_belakang_kanan_luar' => $ban_belakang_kanan_luar,
          'kaca_spion_kiri' => $kaca_spion_kiri,
          'kaca_spion_kanan' => $kaca_spion_kanan,
          'pic_badge' => $pic_badge,
          'pic_name' => $pic_name,
        ];


        try{
            $data = InspectionPickup::find($id);
            $data->vehicle_code=$vehicle_code;
            $data->report_date=$report_date;
            $data->report_time=$report_time;
            $data->shift=$shift;
            $data->pemanasan=$pemanasan;
            $data->spedometer=$spedometer;
            $data->level_bbm=$level_bbm;
            $data->oli_mesin=$oli_mesin;
            $data->oli_rem=$oli_rem;
            $data->oli_power=$oli_power;
            $data->air_radiator=$air_radiator;
            $data->air_cad_radiator=$air_cad_radiator;
            $data->air_wiper=$air_wiper;
            $data->lampu_cabin=$lampu_cabin;
            $data->lampu_kota=$lampu_kota;
            $data->lampu_jauh=$lampu_jauh;
            $data->lampu_sign_kiri=$lampu_sign_kiri;
            $data->lampu_sign_kanan=$lampu_sign_kanan;
            $data->lampu_rem=$lampu_rem;
            $data->lampu_atret=$lampu_atret;
            $data->lampu_sorot=$lampu_sorot;
            $data->panel_dashboard=$panel_dashboard;
            $data->ban_depan_kiri=$ban_depan_kiri;
            $data->ban_depan_kanan=$ban_depan_kanan;
            $data->ban_belakang_kiri_dalam=$ban_belakang_kiri_dalam;
            $data->ban_belakang_kiri_luar=$ban_belakang_kiri_luar;
            $data->ban_belakang_kanan_dalam=$ban_belakang_kanan_dalam;
            $data->ban_belakang_kanan_luar=$ban_belakang_kanan_luar;
            $data->kaca_spion_kiri=$kaca_spion_kiri;
            $data->kaca_spion_kanan=$kaca_spion_kanan;
            $data->pic_badge=$pic_badge;
            $data->pic_name=$pic_name;

            $data->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        }catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
        }
        return response()->json($params);
    }

    public  function  deleteInspectionPickup(Request $request){
        $apiName='DELETE_INSPECTION_PICKUP';
        $id=$request->id;
        $sendingParams = [
            'id' => $id,
        ];

        try {
            InspectionPickup::find($id)->delete();
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
            ];
            return response()->json($params);

        } catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404, $e->getMessage(),json_encode($sendingParams));
        }
    }
}
