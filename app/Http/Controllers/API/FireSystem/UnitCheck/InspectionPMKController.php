<?php

namespace App\Http\Controllers\Api\FireSystem\UnitCheck;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FireSystem\UnitCheck\InspectionPMK;
use App\Models\FireSystem\UnitCheck\ListVehicle;

class InspectionPMKController extends Controller
{
    public function addInspectionPMK (Request $request){
        $apiName='INSPECTION_PMK';
        $vehicle_id=$request->vehicle_id;

        $report_date=date('Y-m-d');
        $report_time=date('H:i:s');
        $shift=$request->shift;
        $pemanasan=$request->pemanasan;
        $spedometer=$request->spedometer;
        $level_bbm=$request->level_bbm;
        $working_pressure=$request->working_pressure;
        $hour_meter_mesin=$request->hour_meter_mesin;
        $hour_meter_pompa=$request->hour_meter_pompa;
        $oli_mesin=$request->oli_mesin;
        $oli_rem=$request->oli_rem;
        $oli_power=$request->oli_power;
        $air_radiator=$request->air_radiator;
        $air_cad_radiator=$request->air_cad_radiator;
        $air_wiper=$request->air_wiper;
        $lampu_cabin=$request->lampu_cabin;
        $lampu_kota=$request->lampu_kota;
        $lampu_jauh=$request->lampu_jauh;
        $lampu_sign_kiri=$request->lampu_sign_kiri;
        $lampu_sign_kanan=$request->lampu_sign_kanan;
        $lampu_rem=$request->lampu_rem;
        $lampu_atret=$request->lampu_atret;
        $lampu_sorot=$request->lampu_sorot;
        $panel_dashboard=$request->panel_dashboard;
        $ban_depan_kiri=$request->ban_depan_kiri;
        $ban_depan_kanan=$request->ban_depan_kanan;
        $ban_belakang_kiri_dalam=$request->ban_belakang_kiri_dalam;
        $ban_belakang_kiri_luar=$request->ban_belakang_kiri_luar;
        $ban_belakang_kanan_dalam=$request->ban_belakang_kanan_dalam;
        $ban_belakang_kanan_luar=$request->ban_belakang_kanan_luar;
        $kaca_spion_kiri=$request->kaca_spion_kiri;
        $kaca_spion_kanan=$request->kaca_spion_kanan;

        $dongkrak=$request->dongkrak;
        $stang_kabin=$request->stang_kabin;
        $ganjal_ban=$request->ganjal_ban;
        $kunci_roda=$request->kunci_roda;
        $hammer=$request->hammer;
        $kotak_pppk=$request->kotak_pppk;
        $hose_25=$request->hose_25;
        $hose_15=$request->hose_15;
        $nozzle_15_js=$request->nozzle_15_js;
        $nozzle_15_akron=$request->nozzle_15_akron;
        $nozzle_15_jet=$request->nozzle_15_jet;
        $y_valve=$request->y_valve;
        $red_25=$request->red_25;
        $nozzle_foam=$request->nozzle_foam;
        $kunci_hydrant=$request->kunci_hydrant;
        $hose_section=$request->hose_section;
        $kunci_hose=$request->kunci_hose;
        $apar=$request->apar;
        $kapak=$request->kapak;
        $linggis=$request->linggis;
        $tangga_ganda=$request->tangga_ganda;
        $senter=$request->senter;
        $gantol=$request->gantol;
        $timba=$request->timba;
        $karung=$request->karung;
        $jacket_pmk=$request->jacket_pmk;
        $helm_pmk=$request->helm_pmk;
        $sirine=$request->sirine;
        $acsesoris=$request->acsesoris;
        $pic_badge=$request->pic_badge;
        $pic_name=$request->pic_name;
        $note=$request->note;

        $sum=0;
        if($level_bbm == '3/4' || $level_bbm == 'F'){
          $sum=$sum+1;
        }
        if($working_pressure < 20 || 30 < $working_pressure){
          $sum=$sum+1;
        }
        if($hour_meter_mesin < 4 || 5 < $hour_meter_mesin){
          $sum=$sum+1;
        }
        if($hour_meter_pompa < 3 || 4 < $hour_meter_pompa){
          $sum=$sum+1;
        }
        if($oli_mesin == 'H'){
          $sum=$sum+1;
        }
        if($oli_rem == 'H'){
          $sum=$sum+1;
        }
        if($oli_power == 'H'){
          $sum=$sum+1;
        }
        if($air_radiator == '3/4' || $air_radiator == 'F'){
          $sum=$sum+1;
        }
        if($air_cad_radiator == '3/4' || $air_cad_radiator == 'F'){
          $sum=$sum+1;
        }
        if($air_wiper == '3/4' || $air_wiper == 'F'){
          $sum=$sum+1;
        }
        if($ban_depan_kiri < 30 || 40 < $ban_depan_kiri){
          $sum=$sum+1;
        }
        if($ban_depan_kanan < 30 || 40 < $ban_depan_kanan){
          $sum=$sum+1;
        }
        if($ban_belakang_kiri_dalam < 30 || 40 < $ban_belakang_kiri_dalam){
          $sum=$sum+1;
        }
        if($ban_belakang_kiri_luar < 30 || 40 < $ban_belakang_kiri_luar){
          $sum=$sum+1;
        }
        if($ban_belakang_kanan_dalam < 30 || 40 < $ban_belakang_kanan_dalam){
          $sum=$sum+1;
        }
        if($ban_belakang_kanan_luar < 30 || 40 < $ban_belakang_kanan_luar){
          $sum=$sum+1;
        }
        if($lampu_cabin == 'MATI'){
          $sum=$sum+1;
        }
        if($lampu_kota == 'MATI'){
          $sum=$sum+1;
        }
        if($lampu_jauh == 'MATI'){
          $sum=$sum+1;
        }
        if($lampu_sign_kiri == 'MATI'){
          $sum=$sum+1;
        }
        if($lampu_sign_kanan == 'MATI'){
          $sum=$sum+1;
        }
        if($lampu_rem == 'MATI'){
          $sum=$sum+1;
        }
        if($lampu_atret == 'MATI'){
          $sum=$sum+1;
        }
        if($lampu_sorot == 'MATI'){
          $sum=$sum+1;
        }
        if($panel_dashboard == 'MATI'){
          $sum=$sum+1;
        }
        if($kaca_spion_kiri == 'PECAH'){
          $sum=$sum+1;
        }
        if($kaca_spion_kanan == 'PECAH'){
          $sum=$sum+1;
        }
        if($dongkrak == 'TIDAK'){
          $sum=$sum+1;
        }
        if($stang_kabin == 'TIDAK'){
          $sum=$sum+1;
        }
        if($ganjal_ban == 'TIDAK'){
          $sum=$sum+1;
        }
        if($kunci_roda == 'TIDAK'){
          $sum=$sum+1;
        }
        if($hammer == 'TIDAK'){
          $sum=$sum+1;
        }
        if($kotak_pppk == 'TIDAK'){
          $sum=$sum+1;
        }
        if($sirine == 'MATI'){
          $sum=$sum+1;
        }
        if($acsesoris == 'MATI'){
          $sum=$sum+1;
        }
        if($hose_25 < 1){
          $sum=$sum+1;
        }
        if($hose_15 < 1){
          $sum=$sum+1;
        }
        if($nozzle_15_js < 1){
          $sum=$sum+1;
        }
        if($nozzle_15_akron < 1){
          $sum=$sum+1;
        }
        if($nozzle_15_jet < 1){
          $sum=$sum+1;
        }
        if($y_valve < 1){
          $sum=$sum+1;
        }
        if($red_25 < 1){
          $sum=$sum+1;
        }
        if($nozzle_foam < 1){
          $sum=$sum+1;
        }
        if($kunci_hydrant < 2){
          $sum=$sum+1;
        }
        if($hose_section < 1){
          $sum=$sum+1;
        }
        if($kunci_hose < 2){
          $sum=$sum+1;
        }
        if($apar < 5){
          $sum=$sum+1;
        }
        if($kapak < 1){
          $sum=$sum+1;
        }
        if($linggis < 1){
          $sum=$sum+1;
        }
        if($tangga_ganda < 1){
          $sum=$sum+1;
        }
        if($senter < 3){
          $sum=$sum+1;
        }
        if($gantol < 3){
          $sum=$sum+1;
        }
        if($timba < 5){
          $sum=$sum+1;
        }
        if($karung < 5){
          $sum=$sum+1;
        }
        if($jacket_pmk < 5){
          $sum=$sum+1;
        }
        if($helm_pmk < 5){
          $sum=$sum+1;
        }

        $data = ListVehicle::find($vehicle_id);
        $type=$data->type;
        $vehicle_code=$data->vehicle_code;
        $vehicle_merk=$data->vehicle_merk;
        $tahun=$data->tahun;
        $vehicle_type=$data->vehicle_type;
        $nomor_rangka=$data->nomor_rangka;
        $company=$data->company;
        $plant=$data->plant;
        $image=$data->image;
        $last_inspection=date('Y-m-d');
        $exp_inspection=date('Y-m-d', strtotime('+7 days', strtotime($last_inspection)));
        $sum_negative=$sum;

        $sendingParams = [
          'vehicle_id' => $vehicle_id,
          'report_date' => $report_date,
          'report_time' => $report_time,
          'shift' => $shift,
          'pemanasan' => $pemanasan,
          'spedometer' => $spedometer,
          'level_bbm' => $level_bbm,
          'working_pressure' => $working_pressure,
          'hour_meter_mesin' => $hour_meter_mesin,
          'hour_meter_pompa' => $hour_meter_pompa,
          'oli_mesin' => $oli_mesin,
          'oli_rem' => $oli_rem,
          'oli_power' => $oli_power,
          'air_radiator' => $air_radiator,
          'air_cad_radiator' => $air_cad_radiator,
          'air_wiper' => $air_wiper,
          'lampu_cabin' => $lampu_cabin,
          'lampu_kota' => $lampu_kota,
          'lampu_jauh' => $lampu_jauh,
          'lampu_sign_kiri' => $lampu_sign_kiri,
          'lampu_sign_kanan' => $lampu_sign_kanan,
          'lampu_rem' => $lampu_rem,
          'lampu_atret' => $lampu_atret,
          'lampu_sorot' => $lampu_sorot,
          'panel_dashboard' => $panel_dashboard,
          'ban_depan_kiri' => $ban_depan_kiri,
          'ban_depan_kanan' => $ban_depan_kanan,
          'ban_belakang_kiri_dalam' => $ban_belakang_kiri_dalam,
          'ban_belakang_kiri_luar' => $ban_belakang_kiri_luar,
          'ban_belakang_kanan_dalam' => $ban_belakang_kanan_dalam,
          'ban_belakang_kanan_luar' => $ban_belakang_kanan_luar,
          'kaca_spion_kiri' => $kaca_spion_kiri,
          'kaca_spion_kanan' => $kaca_spion_kanan,
          'dongkrak' => $dongkrak,
          'stang_kabin' => $stang_kabin,
          'ganjal_ban' => $ganjal_ban,
          'kunci_roda' => $kunci_roda,
          'hammer' => $hammer,
          'kotak_pppk' => $kotak_pppk,
          'hose_25' => $hose_25,
          'hose_15' => $hose_15,
          'nozzle_15_js' => $nozzle_15_js,
          'nozzle_15_akron' => $nozzle_15_akron,
          'nozzle_15_jet' => $nozzle_15_jet,
          'y_valve' => $y_valve,
          'red_25' => $red_25,
          'nozzle_foam' => $nozzle_foam,
          'kunci_hydrant' => $kunci_hydrant,
          'hose_section' => $hose_section,
          'kunci_hose' => $kunci_hose,
          'apar' => $apar,
          'kapak' => $kapak,
          'linggis' => $linggis,
          'tangga_ganda' => $tangga_ganda,
          'senter' => $senter,
          'gantol' => $gantol,
          'timba' => $timba,
          'karung' => $karung,
          'jacket_pmk' => $jacket_pmk,
          'helm_pmk' => $helm_pmk,
          'sirine' => $sirine,
          'acsesoris' => $acsesoris,
          'pic_badge' => $pic_badge,
          'pic_name' => $pic_name,
          'note' => $note,

          'type' => $type,
          'vehicle_code' => $vehicle_code,
          'vehicle_merk' => $vehicle_merk,
          'tahun' => $tahun,
          'vehicle_type' => $vehicle_type,
          'nomor_rangka' => $nomor_rangka,
          'company' => $company,
          'plant' => $plant,
          'image' => $image,
          'last_inspection' => $last_inspection,
          'exp_inspection' => $exp_inspection,
          'sum_negative' => $sum_negative,
        ];

        try{
            $data = new InspectionPMK();
            $data->vehicle_id=$vehicle_id;
            $data->vehicle_code=$vehicle_code;
            $data->report_date=$report_date;
            $data->report_time=$report_time;
            $data->shift=$shift;
            $data->pemanasan=$pemanasan;
            $data->spedometer=$spedometer;
            $data->level_bbm=$level_bbm;
            $data->working_pressure=$working_pressure;
            $data->hour_meter_mesin=$hour_meter_mesin;
            $data->hour_meter_pompa=$hour_meter_pompa;
            $data->oli_mesin=$oli_mesin;
            $data->oli_rem=$oli_rem;
            $data->oli_power=$oli_power;
            $data->air_radiator=$air_radiator;
            $data->air_cad_radiator=$air_cad_radiator;
            $data->air_wiper=$air_wiper;
            $data->lampu_cabin=$lampu_cabin;
            $data->lampu_kota=$lampu_kota;
            $data->lampu_jauh=$lampu_jauh;
            $data->lampu_sign_kiri=$lampu_sign_kiri;
            $data->lampu_sign_kanan=$lampu_sign_kanan;
            $data->lampu_rem=$lampu_rem;
            $data->lampu_atret=$lampu_atret;
            $data->lampu_sorot=$lampu_sorot;
            $data->panel_dashboard=$panel_dashboard;
            $data->ban_depan_kiri=$ban_depan_kiri;
            $data->ban_depan_kanan=$ban_depan_kanan;
            $data->ban_belakang_kiri_dalam=$ban_belakang_kiri_dalam;
            $data->ban_belakang_kiri_luar=$ban_belakang_kiri_luar;
            $data->ban_belakang_kanan_dalam=$ban_belakang_kanan_dalam;
            $data->ban_belakang_kanan_luar=$ban_belakang_kanan_luar;
            $data->kaca_spion_kiri=$kaca_spion_kiri;
            $data->kaca_spion_kanan=$kaca_spion_kanan;
            $data->dongkrak=$dongkrak;
            $data->stang_kabin=$stang_kabin;
            $data->ganjal_ban=$ganjal_ban;
            $data->kunci_roda=$kunci_roda;
            $data->hammer=$hammer;
            $data->kotak_pppk=$kotak_pppk;
            $data->hose_25=$hose_25;
            $data->hose_15=$hose_15;
            $data->nozzle_15_js=$nozzle_15_js;
            $data->nozzle_15_akron=$nozzle_15_akron;
            $data->nozzle_15_jet=$nozzle_15_jet;
            $data->y_valve=$y_valve;
            $data->red_25=$red_25;
            $data->nozzle_foam=$nozzle_foam;
            $data->kunci_hydrant=$kunci_hydrant;
            $data->hose_section=$hose_section;
            $data->kunci_hose=$kunci_hose;
            $data->apar=$apar;
            $data->kapak=$kapak;
            $data->linggis=$linggis;
            $data->tangga_ganda=$tangga_ganda;
            $data->senter=$senter;
            $data->gantol=$gantol;
            $data->timba=$timba;
            $data->karung=$karung;
            $data->jacket_pmk=$jacket_pmk;
            $data->helm_pmk=$helm_pmk;
            $data->sirine=$sirine;
            $data->acsesoris=$acsesoris;
            $data->pic_badge=$pic_badge;
            $data->pic_name=$pic_name;
            $data->note=$note;
            $data->sum_negative=$sum_negative;

            $dataVehicle = ListVehicle::find($vehicle_id);
            $dataVehicle->type=$type;
            $dataVehicle->vehicle_code=$vehicle_code;
            $dataVehicle->vehicle_merk=$vehicle_merk;
            $dataVehicle->tahun=$tahun;
            $dataVehicle->vehicle_type=$vehicle_type;
            $dataVehicle->nomor_rangka=$nomor_rangka;
            $dataVehicle->company=$company;
            $dataVehicle->plant=$plant;
            $dataVehicle->image=$image;
            $dataVehicle->last_inspection=$last_inspection;
            $dataVehicle->exp_inspection=$exp_inspection;
            $dataVehicle->sum_negative=$sum_negative;

            $data->save();
            $dataVehicle->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        }catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));

        }

        return response()->json($params);
    }

    public function getInspectionPMK(Request $request){

        $apiName='INSPECTION_PMK';
        $vehicle_code=null;
        $vehicle_code=$request->vehicle_code;

        if($vehicle_code){
          $data = InspectionPMK::where('vehicle_code', 'LIKE', '%' .$vehicle_code. '%')->get();
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];

        } else {
              $data = InspectionPMK::all();
              $params = [
                  'is_success' => true,
                  'status' => 200,
                  'message' => 'success',
                  'data' => $data
              ];
        }
        return response()->json($params);
    }

    public function detailInspectionPMK(Request $request){

        $vehicle_id = $request->vehicle_id;

        $data = InspectionPMK::where('vehicle_id', 'LIKE', '%' .$vehicle_id. '%')->orderBy('report_date', 'DESC')->first();
        $params = [
            'is_success' => true,
            'status' => 200,
            'message' => 'success',
            'data' => $data
        ];

        return response()->json($params);
    }

    public function updateInspectionPMK (Request $request){

        $apiName='UPDATE_INSPECTION_PMK';
        $id=$request->id;
        $vehicle_id=$request->vehicle_id;
        $vehicle_code=$request->vehicle_code;
        $report_date=$request->report_date;
        $report_time=$request->report_time;
        $shift=$request->shift;
        $pemanasan=$request->pemanasan;
        $spedometer=$request->spedometer;
        $level_bbm=$request->level_bbm;
        $working_pressure=$request->working_pressure;
        $hour_meter_mesin=$request->hour_meter_mesin;
        $hour_meter_pompa=$request->hour_meter_pompa;
        $oli_mesin=$request->oli_mesin;
        $oli_rem=$request->oli_rem;
        $oli_power=$request->oli_power;
        $air_radiator=$request->air_radiator;
        $air_cad_radiator=$request->air_cad_radiator;
        $air_wiper=$request->air_wiper;
        $lampu_cabin=$request->lampu_cabin;
        $lampu_kota=$request->lampu_kota;
        $lampu_jauh=$request->lampu_jauh;
        $lampu_sign_kiri=$request->lampu_sign_kiri;
        $lampu_sign_kanan=$request->lampu_sign_kanan;
        $lampu_rem=$request->lampu_rem;
        $lampu_atret=$request->lampu_atret;
        $lampu_sorot=$request->lampu_sorot;
        $panel_dashboard=$request->panel_dashboard;
        $ban_depan_kiri=$request->ban_depan_kiri;
        $ban_depan_kanan=$request->ban_depan_kanan;
        $ban_belakang_kiri_dalam=$request->ban_belakang_kiri_dalam;
        $ban_belakang_kiri_luar=$request->ban_belakang_kiri_luar;
        $ban_belakang_kanan_dalam=$request->ban_belakang_kanan_dalam;
        $ban_belakang_kanan_luar=$request->ban_belakang_kanan_luar;
        $kaca_spion_kiri=$request->kaca_spion_kiri;
        $kaca_spion_kanan=$request->kaca_spion_kanan;
        $dongkrak=$request->dongkrak;
        $stang_kabin=$request->stang_kabin;
        $ganjal_ban=$request->ganjal_ban;
        $kunci_roda=$request->kunci_roda;
        $hammer=$request->hammer;
        $kotak_pppk=$request->kotak_pppk;
        $hose_25=$request->hose_25;
        $hose_15=$request->hose_15;
        $nozzle_15_js=$request->nozzle_15_js;
        $nozzle_15_akron=$request->nozzle_15_akron;
        $nozzle_15_jet=$request->nozzle_15_jet;
        $y_valve=$request->y_valve;
        $red_25=$request->red_25;
        $nozzle_foam=$request->nozzle_foam;
        $kunci_hydrant=$request->kunci_hydrant;
        $hose_section=$request->hose_section;
        $kunci_hose=$request->kunci_hose;
        $apar=$request->apar;
        $kapak=$request->kapak;
        $linggis=$request->linggis;
        $tangga_ganda=$request->tangga_ganda;
        $senter=$request->senter;
        $gantol=$request->gantol;
        $timba=$request->timba;
        $karung=$request->karung;
        $jacket_pmk=$request->jacket_pmk;
        $helm_pmk=$request->helm_pmk;
        $sirine=$request->sirine;
        $acsesoris=$request->acsesoris;
        $pic_badge=$request->pic_badge;
        $pic_name=$request->pic_name;
        $note=$request->note;

        $sum=0;
        if($level_bbm == '3/4' || $level_bbm == 'F'){
          $sum=$sum+1;
        }
        if($working_pressure < 20 || 30 < $working_pressure){
          $sum=$sum+1;
        }
        if($hour_meter_mesin < 4 || 5 < $hour_meter_mesin){
          $sum=$sum+1;
        }
        if($hour_meter_pompa < 3 || 4 < $hour_meter_pompa){
          $sum=$sum+1;
        }
        if($oli_mesin == 'H'){
          $sum=$sum+1;
        }
        if($oli_rem == 'H'){
          $sum=$sum+1;
        }
        if($oli_power == 'H'){
          $sum=$sum+1;
        }
        if($air_radiator == '3/4' || $air_radiator == 'F'){
          $sum=$sum+1;
        }
        if($air_cad_radiator == '3/4' || $air_cad_radiator == 'F'){
          $sum=$sum+1;
        }
        if($air_wiper == '3/4' || $air_wiper == 'F'){
          $sum=$sum+1;
        }
        if($ban_depan_kiri < 30 || 40 < $ban_depan_kiri){
          $sum=$sum+1;
        }
        if($ban_depan_kanan < 30 || 40 < $ban_depan_kanan){
          $sum=$sum+1;
        }
        if($ban_belakang_kiri_dalam < 30 || 40 < $ban_belakang_kiri_dalam){
          $sum=$sum+1;
        }
        if($ban_belakang_kiri_luar < 30 || 40 < $ban_belakang_kiri_luar){
          $sum=$sum+1;
        }
        if($ban_belakang_kanan_dalam < 30 || 40 < $ban_belakang_kanan_dalam){
          $sum=$sum+1;
        }
        if($ban_belakang_kanan_luar < 30 || 40 < $ban_belakang_kanan_luar){
          $sum=$sum+1;
        }
        if($lampu_cabin == 'MATI'){
          $sum=$sum+1;
        }
        if($lampu_kota == 'MATI'){
          $sum=$sum+1;
        }
        if($lampu_jauh == 'MATI'){
          $sum=$sum+1;
        }
        if($lampu_sign_kiri == 'MATI'){
          $sum=$sum+1;
        }
        if($lampu_sign_kanan == 'MATI'){
          $sum=$sum+1;
        }
        if($lampu_rem == 'MATI'){
          $sum=$sum+1;
        }
        if($lampu_atret == 'MATI'){
          $sum=$sum+1;
        }
        if($lampu_sorot == 'MATI'){
          $sum=$sum+1;
        }
        if($panel_dashboard == 'MATI'){
          $sum=$sum+1;
        }
        if($kaca_spion_kiri == 'PECAH'){
          $sum=$sum+1;
        }
        if($kaca_spion_kanan == 'PECAH'){
          $sum=$sum+1;
        }

        if($dongkrak == 'TIDAK'){
          $sum=$sum+1;
        }
        if($stang_kabin == 'TIDAK'){
          $sum=$sum+1;
        }
        if($ganjal_ban == 'TIDAK'){
          $sum=$sum+1;
        }
        if($kunci_roda == 'TIDAK'){
          $sum=$sum+1;
        }
        if($hammer == 'TIDAK'){
          $sum=$sum+1;
        }
        if($kotak_pppk == 'TIDAK'){
          $sum=$sum+1;
        }
        if($sirine == 'MATI'){
          $sum=$sum+1;
        }
        if($acsesoris == 'MATI'){
          $sum=$sum+1;
        }
        if($hose_25 < 1){
          $sum=$sum+1;
        }
        if($hose_15 < 1){
          $sum=$sum+1;
        }
        if($nozzle_15_js < 1){
          $sum=$sum+1;
        }
        if($nozzle_15_akron < 1){
          $sum=$sum+1;
        }
        if($nozzle_15_jet < 1){
          $sum=$sum+1;
        }
        if($y_valve < 1){
          $sum=$sum+1;
        }
        if($red_25 < 1){
          $sum=$sum+1;
        }
        if($nozzle_foam < 1){
          $sum=$sum+1;
        }
        if($kunci_hydrant < 2){
          $sum=$sum+1;
        }
        if($hose_section < 1){
          $sum=$sum+1;
        }
        if($kunci_hose < 2){
          $sum=$sum+1;
        }
        if($apar < 5){
          $sum=$sum+1;
        }
        if($kapak < 1){
          $sum=$sum+1;
        }
        if($linggis < 1){
          $sum=$sum+1;
        }
        if($tangga_ganda < 1){
          $sum=$sum+1;
        }
        if($senter < 3){
          $sum=$sum+1;
        }
        if($gantol < 3){
          $sum=$sum+1;
        }
        if($timba < 5){
          $sum=$sum+1;
        }
        if($karung < 5){
          $sum=$sum+1;
        }
        if($jacket_pmk < 5){
          $sum=$sum+1;
        }
        if($helm_pmk < 5){
          $sum=$sum+1;
        }

        $data = ListVehicle::find($vehicle_id);
        $type=$data->type;
        $vehicle_code=$data->vehicle_code;
        $vehicle_merk=$data->vehicle_merk;
        $tahun=$data->tahun;
        $vehicle_type=$data->vehicle_type;
        $nomor_rangka=$data->nomor_rangka;
        $company=$data->company;
        $plant=$data->plant;
        $image=$data->image;
        $last_inspection=$report_date;
        $exp_inspection=date('Y-m-d', strtotime('+7 days', strtotime($last_inspection)));
        $sum_negative=$sum;

        $sendingParams = [
          'id' => $id,
          'vehicle_id' => $vehicle_id,
          'vehicle_code' => $vehicle_code,
          'report_date' => $report_date,
          'report_time' => $report_time,
          'shift' => $shift,
          'pemanasan' => $pemanasan,
          'spedometer' => $spedometer,
          'level_bbm' => $level_bbm,
          'working_pressure' => $working_pressure,
          'hour_meter_mesin' => $hour_meter_mesin,
          'hour_meter_pompa' => $hour_meter_pompa,
          'oli_mesin' => $oli_mesin,
          'oli_rem' => $oli_rem,
          'oli_power' => $oli_power,
          'air_radiator' => $air_radiator,
          'air_cad_radiator' => $air_cad_radiator,
          'air_wiper' => $air_wiper,
          'lampu_cabin' => $lampu_cabin,
          'lampu_kota' => $lampu_kota,
          'lampu_jauh' => $lampu_jauh,
          'lampu_sign_kiri' => $lampu_sign_kiri,
          'lampu_sign_kanan' => $lampu_sign_kanan,
          'lampu_rem' => $lampu_rem,
          'lampu_atret' => $lampu_atret,
          'lampu_sorot' => $lampu_sorot,
          'panel_dashboard' => $panel_dashboard,
          'ban_depan_kiri' => $ban_depan_kiri,
          'ban_depan_kanan' => $ban_depan_kanan,
          'ban_belakang_kiri_dalam' => $ban_belakang_kiri_dalam,
          'ban_belakang_kiri_luar' => $ban_belakang_kiri_luar,
          'ban_belakang_kanan_dalam' => $ban_belakang_kanan_dalam,
          'ban_belakang_kanan_luar' => $ban_belakang_kanan_luar,
          'kaca_spion_kiri' => $kaca_spion_kiri,
          'kaca_spion_kanan' => $kaca_spion_kanan,
          'dongkrak' => $dongkrak,
          'stang_kabin' => $stang_kabin,
          'ganjal_ban' => $ganjal_ban,
          'kunci_roda' => $kunci_roda,
          'hammer' => $hammer,
          'kotak_pppk' => $kotak_pppk,
          'hose_25' => $hose_25,
          'hose_15' => $hose_15,
          'nozzle_15_js' => $nozzle_15_js,
          'nozzle_15_akron' => $nozzle_15_akron,
          'nozzle_15_jet' => $nozzle_15_jet,
          'y_valve' => $y_valve,
          'red_25' => $red_25,
          'nozzle_foam' => $nozzle_foam,
          'kunci_hydrant' => $kunci_hydrant,
          'hose_section' => $hose_section,
          'kunci_hose' => $kunci_hose,
          'apar' => $apar,
          'kapak' => $kapak,
          'linggis' => $linggis,
          'tangga_ganda' => $tangga_ganda,
          'senter' => $senter,
          'gantol' => $gantol,
          'timba' => $timba,
          'karung' => $karung,
          'jacket_pmk' => $jacket_pmk,
          'helm_pmk' => $helm_pmk,
          'sirine' => $sirine,
          'acsesoris' => $acsesoris,
          'pic_badge' => $pic_badge,
          'pic_name' => $pic_name,
          'note' => $note,

          'type' => $type,
          'vehicle_code' => $vehicle_code,
          'vehicle_merk' => $vehicle_merk,
          'tahun' => $tahun,
          'vehicle_type' => $vehicle_type,
          'nomor_rangka' => $nomor_rangka,
          'company' => $company,
          'plant' => $plant,
          'image' => $image,
          'last_inspection' => $last_inspection,
          'exp_inspection' => $exp_inspection,
          'sum_negative' => $sum_negative,
        ];


        try{
            $data = InspectionPMK::find($id);
            $data->vehicle_id=$vehicle_id;
            $data->vehicle_code=$vehicle_code;
            $data->report_date=$report_date;
            $data->report_time=$report_time;
            $data->shift=$shift;
            $data->pemanasan=$pemanasan;
            $data->spedometer=$spedometer;
            $data->level_bbm=$level_bbm;
            $data->working_pressure=$working_pressure;
            $data->hour_meter_mesin=$hour_meter_mesin;
            $data->hour_meter_pompa=$hour_meter_pompa;
            $data->oli_mesin=$oli_mesin;
            $data->oli_rem=$oli_rem;
            $data->oli_power=$oli_power;
            $data->air_radiator=$air_radiator;
            $data->air_cad_radiator=$air_cad_radiator;
            $data->air_wiper=$air_wiper;
            $data->lampu_cabin=$lampu_cabin;
            $data->lampu_kota=$lampu_kota;
            $data->lampu_jauh=$lampu_jauh;
            $data->lampu_sign_kiri=$lampu_sign_kiri;
            $data->lampu_sign_kanan=$lampu_sign_kanan;
            $data->lampu_rem=$lampu_rem;
            $data->lampu_atret=$lampu_atret;
            $data->lampu_sorot=$lampu_sorot;
            $data->panel_dashboard=$panel_dashboard;
            $data->ban_depan_kiri=$ban_depan_kiri;
            $data->ban_depan_kanan=$ban_depan_kanan;
            $data->ban_belakang_kiri_dalam=$ban_belakang_kiri_dalam;
            $data->ban_belakang_kiri_luar=$ban_belakang_kiri_luar;
            $data->ban_belakang_kanan_dalam=$ban_belakang_kanan_dalam;
            $data->ban_belakang_kanan_luar=$ban_belakang_kanan_luar;
            $data->kaca_spion_kiri=$kaca_spion_kiri;
            $data->kaca_spion_kanan=$kaca_spion_kanan;
            $data->dongkrak=$dongkrak;
            $data->stang_kabin=$stang_kabin;
            $data->ganjal_ban=$ganjal_ban;
            $data->kunci_roda=$kunci_roda;
            $data->hammer=$hammer;
            $data->kotak_pppk=$kotak_pppk;
            $data->hose_25=$hose_25;
            $data->hose_15=$hose_15;
            $data->nozzle_15_js=$nozzle_15_js;
            $data->nozzle_15_akron=$nozzle_15_akron;
            $data->nozzle_15_jet=$nozzle_15_jet;
            $data->y_valve=$y_valve;
            $data->red_25=$red_25;
            $data->nozzle_foam=$nozzle_foam;
            $data->kunci_hydrant=$kunci_hydrant;
            $data->hose_section=$hose_section;
            $data->kunci_hose=$kunci_hose;
            $data->apar=$apar;
            $data->kapak=$kapak;
            $data->linggis=$linggis;
            $data->tangga_ganda=$tangga_ganda;
            $data->senter=$senter;
            $data->gantol=$gantol;
            $data->timba=$timba;
            $data->karung=$karung;
            $data->jacket_pmk=$jacket_pmk;
            $data->helm_pmk=$helm_pmk;
            $data->sirine=$sirine;
            $data->acsesoris=$acsesoris;
            $data->pic_badge=$pic_badge;
            $data->pic_name=$pic_name;
            $data->note=$note;
            $data->sum_negative=$sum_negative;

            $dataVehicle = ListVehicle::find($vehicle_id);
            $dataVehicle->type=$type;
            $dataVehicle->vehicle_code=$vehicle_code;
            $dataVehicle->vehicle_merk=$vehicle_merk;
            $dataVehicle->tahun=$tahun;
            $dataVehicle->vehicle_type=$vehicle_type;
            $dataVehicle->nomor_rangka=$nomor_rangka;
            $dataVehicle->company=$company;
            $dataVehicle->plant=$plant;
            $dataVehicle->image=$image;
            $dataVehicle->last_inspection=$last_inspection;
            $dataVehicle->exp_inspection=$exp_inspection;
            $dataVehicle->sum_negative=$sum_negative;

            $data->save();
            $dataVehicle->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        }catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
        }
        return response()->json($params);
    }

    public  function  deleteInspectionPMK(Request $request){
        $apiName='DELETE_INSPECTION_PMK';
        $id=$request->id;
        $sendingParams = [
            'id' => $id,
        ];

        try {
            InspectionPMK::find($id)->delete();
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
            ];
            return response()->json($params);

        } catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404, $e->getMessage(),json_encode($sendingParams));
        }
    }
}
