<?php

namespace App\Http\Controllers\Api\FireSystem\UnitCheck;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FireSystem\UnitCheck\ListVehicle;

class ListVehicleController extends Controller
{
    public function getListVehicle(Request $request){

        $apiName='LIST_VEHICLE';
        $type=null;
        $id_vehicle=null;
        $type = $request->type;
        $id_vehicle = $request->id_vehicle;

        if($id_vehicle){
          $data = ListVehicle::find($id_vehicle);
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];
        } else if($type){
          $data = ListVehicle::where('type', 'LIKE', '%' .$type. '%')->get();
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];

        } else {
          $data = ListVehicle::all();
          // $data = ListVehicle::select('vehicle_code', 'sum_negative')->get();
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];
        }

        return response()->json($params);
    }
}
