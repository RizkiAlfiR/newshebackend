<?php

namespace App\Http\Controllers\Api\FireSystem\DailySectionActivity;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FireSystem\DailySectionActivity\Officer;

class OfficerController extends Controller
{
    public function getOfficer(Request $request){
        $apiName='OFFICER';
        $date_activity=null;
        $date_activity = $request->date_activity;

        if($date_activity){
          $data = Officer::where('date_activity', 'LIKE', '%' .$date_activity. '%')->get( );
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];
        } else {
          $data = Officer::all();
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];
        }
        return response()->json($params);
    }
}
