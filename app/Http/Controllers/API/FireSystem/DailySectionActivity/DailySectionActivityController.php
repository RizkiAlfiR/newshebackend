<?php

namespace App\Http\Controllers\Api\FireSystem\DailySectionActivity;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FireSystem\DailySectionActivity\DailySectionActivity;

class DailySectionActivityController extends Controller
{
    public function addDailySectionActivity (Request $request){

        $apiName='ADD_DAILY_SECTION_ACTIVITY';
        $type_action=$request->type_action;
        $date_activity=$request->date_activity;
        $timesheet_start=$request->timesheet_start;
        $timesheet_end=$request->timesheet_end;
        $daily_activity=$request->daily_activity;
        $filename=null;
        $image = $request->file('image');
        if($image){
            $destinationPath = 'public/uploads/DailySectionReport/';
            $filename = $image->getClientOriginalName();
            $image->move($destinationPath, $filename);
        } else {
            $filename=null;
        }
        $shift1=$request->shift1;
        $shift2=$request->shift2;
        $shift3=$request->shift3;
        $pic=$request->pic;
        $note=$request->note;
        $list_tools=$request->list_tools;
        $company=$request->company;
        $plant=$request->plant;
        $status=$request->status;
        $sendingParams = [
            'type_action' => $type_action,
            'date_activity' => $date_activity,
            'timesheet_start' => $timesheet_start,
            'timesheet_end' => $timesheet_end,
            'daily_activity' => $daily_activity,
            'image' => $image,
            'shift1' => $shift1,
            'shift2' => $shift2,
            'shift3' => $shift3,
            'pic' => $pic,
            'note' => $note,
            'list_tools' => $list_tools,
            'company' => $company,
            'plant' => $plant,
            'status' => $status,
        ];

        try{
            $data = new DailySectionActivity();
            $data->type_action=$type_action;
            $data->date_activity=$date_activity;
            $data->timesheet_start=$timesheet_start;
            $data->timesheet_end=$timesheet_end;
            $data->daily_activity=$daily_activity;
            $data->image=$filename;
            $data->shift1=$shift1;
            $data->shift2=$shift2;
            $data->shift3=$shift3;
            $data->pic=$pic;
            $data->note=$note;
            $data->list_tools=$list_tools;
            $data->company=$company;
            $data->plant=$plant;
            $data->status=$status;
            $data->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        }catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404, $e->getMessage(),json_encode($sendingParams));

        }

        return response()->json($params);
    }

    public function getDailySectionActivity(Request $request){
        $apiName='DAILY_SECTION_ACTIVITY';
        $date_activity=null;
        $start_date=null;
        $end_date=null;
        $type_action=null;
        $status=null;
        $date_activity = $request->date_activity;
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $type_action = $request->type_action;
        $status = $request->status;

        if($date_activity){
          $data = DailySectionActivity::where('date_activity', 'LIKE', '%' .$date_activity. '%')->get();
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];
        } else if ($start_date) {
          $data = DailySectionActivity::select("*")->whereBetween('date_activity', [$start_date, $end_date])->get();
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];
        } else if ($type_action) {
          $data = DailySectionActivity::where('type_action', 'LIKE', '%' .$type_action. '%')->get( );
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];
        } else if ($status) {
          $data = DailySectionActivity::where('status', 'LIKE', '%' .$status. '%')->get( );
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];
        } else {
          $data = DailySectionActivity::orderBy('id', 'DESC')->get();
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];
        }
        return response()->json($params);
    }

    public function detailDailySectionActivity(Request $request){
        $apiName='DETAIL_DAILY_SECTION_ACTIVITY';
        $id=$request->id;

        $sendingParams = [
            'id' => $id,
        ];

        $data = DailySectionActivity::find($id);
        $params = [
            'is_success' => true,
            'status' => 200,
            'message' => 'success',
            'data' => $data
        ];

        return response()->json($params);
    }

    public function updateDailySectionActivity (Request $request){

        $apiName='UPDATE_DAILY_SECTION_ACTIVITY';
        $id=$request->id;
        $type_action=$request->type_action;
        $date_activity=$request->date_activity;
        $timesheet_start=$request->timesheet_start;
        $timesheet_end=$request->timesheet_end;
        $daily_activity=$request->daily_activity;

        $filename=null;
        $image = $request->file('image');
        if($image){
            $destinationPath = 'public/uploads/DailySectionReport/';
            $filename = $image->getClientOriginalName();
            $image->move($destinationPath, $filename);
        } else {
            $check = DailySectionActivity::find($id);
            $filename = $check->image;
        }
        $shift1=$request->shift1;
        $shift2=$request->shift2;
        $shift3=$request->shift3;
        $pic=$request->pic;
        $note=$request->note;
        $list_tools=$request->list_tools;
        $company=$request->company;
        $plant=$request->plant;
        $status=$request->status;

        $sendingParams = [
            'id' => $id,
            'type_action' => $type_action,
            'date_activity' => $date_activity,
            'timesheet_start' => $timesheet_start,
            'timesheet_end' => $timesheet_end,
            'daily_activity' => $daily_activity,
            'image' => $filename,
            'shift1' => $shift1,
            'shift2' => $shift2,
            'shift3' => $shift3,
            'pic' => $pic,
            'note' => $note,
            'list_tools' => $list_tools,
            'company' => $company,
            'plant' => $plant,
            'status' => $status,
        ];

        try{
            $data = DailySectionActivity::find($id);
            $data->type_action=$type_action;
            $data->date_activity=$date_activity;
            $data->timesheet_start=$timesheet_start;
            $data->timesheet_end=$timesheet_end;
            $data->daily_activity=$daily_activity;
            $data->image=$filename;
            $data->shift1=$shift1;
            $data->shift2=$shift2;
            $data->shift3=$shift3;
            $data->pic=$pic;
            $data->note=$note;
            $data->list_tools=$list_tools;
            $data->company=$company;
            $data->plant=$plant;
            $data->status=$status;
            $data->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        }catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404, $e->getMessage(),json_encode($sendingParams));

        }
        return response()->json($params);
    }

    public  function  deleteDailySectionActivity(Request $request){

        $apiName='DELETE_DAILY_SECTION_ACTIVITY';
        $id=$request->id;
        $sendingParams = [
            'id' => $id,
        ];

        try {
            DailySectionActivity::find($id)->delete();
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
            ];
            return response()->json($params);

        } catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404, $e->getMessage(),json_encode($sendingParams));
        }
    }
}
