<?php

namespace App\Http\Controllers\Api\FireSystem\Lotto;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FireSystem\Lotto\DetailLotto;

class DetailLottoController extends Controller
{
    public function getDetailLotto(Request $request){

      // $textdt="05 june 2019";
      // $dt= strtotime( $textdt);
      // $currdt=$dt;
      // $nextmonth=strtotime($textdt."+1 month");
      // $i=0;
      // do
      // {
      //     $weekday= date("w",$currdt);
      //     $nextday=7-$weekday;
      //     $endday=abs($weekday-6);
      //     $startarr[$i]=$currdt;
      //     $endarr[$i]=strtotime(date("Y-m-d",$currdt)."+$endday day");
      //     $currdt=strtotime(date("Y-m-d",$endarr[$i])."+1 day");
      //     echo "Week ".($i+1). "-> start date = ". date("Y-m-d",$startarr[$i])." end date = ". date("Y-m-d",$endarr[$i])."<br>";
      //      $i++;
      //
      // }while($endarr[$i-1]<$nextmonth);

  //     $month = 5;
  // $year = 2019;
  //
  // $week = date("W", strtotime($year . "-" . $month ."-01")); // weeknumber of first day of month
  //
  // Echo date("d/m/Y", strtotime($year . "-" . $month ."-01")) ." - "; // first day of month
  // $unix = strtotime($year."W".$week ."+1 week");
  // While(date("m", $unix) == $month){ // keep looping/output of while it's correct month
  //
  //    Echo date("d/m/Y", $unix-86400) . "\n"; // Sunday of previous week
  //    Echo date("d/m/Y", $unix) ." - "; // this week's monday
  //    $unix = $unix + (86400*7);
  // }
  // Echo date("d/m/Y", strtotime("last day of ".$year . "-" . $month));
        $apiName='LIST_DETAIL_LOTTO';
        $id_lotto=null;
        $id_lotto = $request->id_lotto;

        if($id_lotto){
          $data = DetailLotto::where('id_lotto', 'LIKE', '%' .$id_lotto. '%')->get( );
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];
        } else {
          $data = DetailLotto::all();
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];
        }
        return response()->json($params);
    }

    public function addDetailLotto (Request $request){

        $apiName='CREATE_DETAIL_LOTTO';
        $id_lotto=$request->id_lotto;
        $type=$request->type;
        $unit=$request->unit;
        $pic_badge=$request->pic_badge;
        $pic_name=$request->pic_name;
        $note=$request->note;
        $time=$request->time;

        $sendingParams = [
          'id_lotto' => $id_lotto,
          'type' => $type,
          'unit' => $unit,
          'pic_badge' => $pic_badge,
          'pic_name' => $pic_name,
          'note' => $note,
          'time' => $time,
        ];

        try{
            $data = new DetailLotto();
            $data->id_lotto=$id_lotto;
            $data->type=$type;
            $data->unit=$unit;
            $data->pic_badge=$pic_badge;
            $data->pic_name=$pic_name;
            $data->note=$note;
            $data->time=$time;

            $data->save();
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        }catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
        }
        return response()->json($params);
    }

    public function updateDetailLotto (Request $request){

        $apiName='UPDATE_DETAIL_LOTTO';
        $id=$request->id;
        $id_lotto=$request->id_lotto;
        $type=$request->type;
        $unit=$request->unit;
        $pic_badge=$request->pic_badge;
        $pic_name=$request->pic_name;
        $note=$request->note;
        $time=$request->time;

        $sendingParams = [
          'id' => $id,
          'id_lotto' => $id_lotto,
          'type' => $type,
          'unit' => $unit,
          'pic_badge' => $pic_badge,
          'pic_name' => $pic_name,
          'note' => $note,
          'time' => $time,
        ];

        try{
            $data = DetailLotto::find($id);
            $data->id_lotto=$id_lotto;
            $data->type=$type;
            $data->unit=$unit;
            $data->pic_badge=$pic_badge;
            $data->pic_name=$pic_name;
            $data->note=$note;
            $data->time=$time;

            $data->save();
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        }catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
        }
        return response()->json($params);
    }

    public function deleteDetailLotto(Request $request){
        $apiName='DELETE_DETAIL_LOTTO';
        $id=$request->id;
        $sendingParams = [
            'id' => $id,
        ];

        try {
            DetailLotto::find($id)->delete();
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
            ];
            return response()->json($params);

        } catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404, $e->getMessage(),json_encode($sendingParams));
        }
    }
}
