<?php

namespace App\Http\Controllers\Api\FireSystem\Lotto;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FireSystem\Lotto\Lotto;

class LottoController extends Controller
{
    public function getLotto(Request $request){

        $apiName='LIST_LOTTO';
        $date=null;
        $id_lotto=null;
        $start_date=null;
        $end_date=null;
        $status=null;
        $date = $request->date;
        $id_lotto = $request->id_lotto;
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $status = $request->status;

        if($date){
          $data = Lotto::where('date', 'LIKE', '%' .$date. '%')->get( );
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];

        } else if($id_lotto){
          $data = Lotto::find($id_lotto);
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];

        } else if ($start_date) {
          $data = Lotto::select("*")->whereBetween('date', [$start_date, $end_date])->get();
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];
        } else if ($status) {
          $data = Lotto::where('status', 'LIKE', '%' .$status. '%')->get( );
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];
        } else {
          $data = Lotto::all();
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];
        }
        return response()->json($params);
    }

    public function getDetailLotto(Request $request){

      $apiName='DETAIL_LOTTO';
      $id_lotto=null;
      $id_lotto = $request->id_lotto;

      $data = Lotto::find($id_lotto);
      $params = [
          'is_success' => true,
          'status' => 200,
          'message' => 'success',
          'data' => $data
      ];
      return response()->json($params);
    }

    public function addLotto (Request $request){

        $apiName='CREATE_LOTTO';
        $equipment_number=$request->equipment_number;
        $location=$request->location;
        $no_er=$request->no_er;
        $activity_description=$request->activity_description;
        $date=$request->date;
        $time=$request->time;
        $note=$request->note;
        $safety_key=$request->safety_key;
        $safety_tools=$request->safety_tools;
        $status=$request->status;

        $sendingParams = [
          'equipment_number' => $equipment_number,
          'location' => $location,
          'no_er' => $no_er,
          'activity_description' => $activity_description,
          'date' => $date,
          'time' => $time,
          'note' => $note,
          'safety_key' => $safety_key,
          'safety_tools' => $safety_tools,
          'status' => $status,
        ];

        try{
            $data = new Lotto();
            $data->equipment_number=$equipment_number;
            $data->location=$location;
            $data->no_er=$no_er;
            $data->activity_description=$activity_description;
            $data->date=$date;
            $data->time=$time;
            $data->note=$note;
            $data->safety_key=$safety_key;
            $data->safety_tools=$safety_tools;
            $data->status=$status;

            $data->save();
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        }catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
        }
        return response()->json($params);
    }

    public function updateLotto (Request $request){

        $apiName='UPDATE_LOTTO';
        $id=$request->id;
        $equipment_number=$request->equipment_number;
        $location=$request->location;
        $no_er=$request->no_er;
        $activity_description=$request->activity_description;
        $date=$request->date;
        $time=$request->time;
        $note=$request->note;
        $safety_key=$request->safety_key;
        $safety_tools=$request->safety_tools;
        $status=$request->status;

        $data = Lotto::find($id);
        $k3_drawin_badge=$data->k3_drawin_badge;
        $k3_drawin_name=$data->k3_drawin_name;
        $k3_drawin_ket=$data->k3_drawin_ket;
        $k3_drawout_badge=$data->k3_drawout_badge;
        $k3_drawout_name=$data->k3_drawout_name;
        $k3_drawout_ket=$data->k3_drawout_ket;

        $pmll_drawin_badge=$data->pmll_drawin_badge;
        $pmll_drawin_name=$data->pmll_drawin_name;
        $pmll_drawin_ket=$data->pmll_drawin_ket;
        $pmll_drawout_badge=$data->pmll_drawout_badge;
        $pmll_drawout_name=$data->pmll_drawout_name;
        $pmll_drawout_ket=$data->pmll_drawout_ket;

        $operator_drawin_badge=$data->operator_drawin_badge;
        $operator_drawin_name=$data->operator_drawin_name;
        $operator_drawin_ket=$data->operator_drawin_ket;
        $operator_drawout_badge=$data->operator_drawout_badge;
        $operator_drawout_name=$data->operator_drawout_name;
        $operator_drawout_ket=$data->operator_drawout_ket;

        $pmlt_drawin_badge=$data->pmlt_drawin_badge;
        $pmlt_drawin_name=$data->pmlt_drawin_name;
        $pmlt_drawin_ket=$data->pmlt_drawin_ket;
        $pmlt_drawout_badge=$data->pmlt_drawout_badge;
        $pmlt_drawout_name=$data->pmlt_drawout_name;
        $pmlt_drawout_ket=$data->pmlt_drawout_ket;

        $sendingParams = [
          'id' => $id,
          'equipment_number' => $equipment_number,
          'location' => $location,
          'no_er' => $no_er,
          'activity_description' => $activity_description,
          'date' => $date,
          'time' => $time,
          'note' => $note,
          'safety_key' => $safety_key,
          'safety_tools' => $safety_tools,
          'status' => $status,

          'k3_drawin_badge' => $k3_drawin_badge,
          'k3_drawin_name' => $k3_drawin_name,
          'k3_drawin_ket' => $k3_drawin_ket,
          'k3_drawout_badge' => $k3_drawout_badge,
          'k3_drawout_name' => $k3_drawout_name,
          'k3_drawout_ket' => $k3_drawout_ket,

          'pmll_drawin_badge' => $pmll_drawin_badge,
          'pmll_drawin_name' => $pmll_drawin_name,
          'pmll_drawin_ket' => $pmll_drawin_ket,
          'pmll_drawout_badge' => $pmll_drawout_badge,
          'pmll_drawout_name' => $pmll_drawout_name,
          'pmll_drawout_ket' => $pmll_drawout_ket,

          'operator_drawin_badge' => $operator_drawin_badge,
          'operator_drawin_name' => $operator_drawin_name,
          'operator_drawin_ket' => $operator_drawin_ket,
          'operator_drawout_badge' => $operator_drawout_badge,
          'operator_drawout_name' => $operator_drawout_name,
          'operator_drawout_ket' => $operator_drawout_ket,

          'pmlt_drawin_badge' => $pmlt_drawin_badge,
          'pmlt_drawin_name' => $pmlt_drawin_name,
          'pmlt_drawin_ket' => $pmlt_drawin_ket,
          'pmlt_drawout_badge' => $pmlt_drawout_badge,
          'pmlt_drawout_name' => $pmlt_drawout_name,
          'pmlt_drawout_ket' => $pmlt_drawout_ket,
        ];

        try{
            $data = Lotto::find($id);
            $data->equipment_number=$equipment_number;
            $data->location=$location;
            $data->no_er=$no_er;
            $data->activity_description=$activity_description;
            $data->date=$date;
            $data->time=$time;
            $data->note=$note;
            $data->safety_key=$safety_key;
            $data->safety_tools=$safety_tools;
            $data->status=$status;

            $data->k3_drawin_badge=$k3_drawin_badge;
            $data->k3_drawin_name=$k3_drawin_name;
            $data->k3_drawin_ket=$k3_drawin_ket;
            $data->k3_drawout_badge=$k3_drawout_badge;
            $data->k3_drawout_name=$k3_drawout_name;
            $data->k3_drawout_ket=$k3_drawout_ket;

            $data->pmll_drawin_badge=$pmll_drawin_badge;
            $data->pmll_drawin_name=$pmll_drawin_name;
            $data->pmll_drawin_ket=$pmll_drawin_ket;
            $data->pmll_drawout_badge=$pmll_drawout_badge;
            $data->pmll_drawout_name=$pmll_drawout_name;
            $data->pmll_drawout_ket=$pmll_drawout_ket;

            $data->operator_drawin_badge=$operator_drawin_badge;
            $data->operator_drawin_name=$operator_drawin_name;
            $data->operator_drawin_ket=$operator_drawin_ket;
            $data->operator_drawout_badge=$operator_drawout_badge;
            $data->operator_drawout_name=$operator_drawout_name;
            $data->operator_drawout_ket=$operator_drawout_ket;

            $data->pmlt_drawin_badge=$pmlt_drawin_badge;
            $data->pmlt_drawin_name=$pmlt_drawin_name;
            $data->pmlt_drawin_ket=$pmlt_drawin_ket;
            $data->pmlt_drawout_badge=$pmlt_drawout_badge;
            $data->pmlt_drawout_name=$pmlt_drawout_name;
            $data->pmlt_drawout_ket=$pmlt_drawout_ket;

            $data->save();
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        }catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
        }
        return response()->json($params);
    }

    public function updateDrawINLotto (Request $request){

        $apiName='UPDATE_DRAWIN_LOTTO';
        $id=$request->id;
        $data = Lotto::find($id);

        $equipment_number=$data->equipment_number;
        $location=$data->location;
        $no_er=$data->no_er;
        $activity_description=$data->activity_description;
        $date=$data->date;
        $time=$data->time;
        $note=$data->note;
        $safety_key=$data->safety_key;
        $safety_tools=$data->safety_tools;
        $status=$data->status;

        $k3_drawin_badge=$request->k3_drawin_badge;
        $k3_drawin_name=$request->k3_drawin_name;
        $k3_drawin_ket=$request->k3_drawin_ket;
        $k3_drawout_badge=$data->k3_drawout_badge;
        $k3_drawout_name=$data->k3_drawout_name;
        $k3_drawout_ket=$data->k3_drawout_ket;

        $pmll_drawin_badge=$request->pmll_drawin_badge;
        $pmll_drawin_name=$request->pmll_drawin_name;
        $pmll_drawin_ket=$request->pmll_drawin_ket;
        $pmll_drawout_badge=$data->pmll_drawout_badge;
        $pmll_drawout_name=$data->pmll_drawout_name;
        $pmll_drawout_ket=$data->pmll_drawout_ket;

        $operator_drawin_badge=$request->operator_drawin_badge;
        $operator_drawin_name=$request->operator_drawin_name;
        $operator_drawin_ket=$request->operator_drawin_ket;
        $operator_drawout_badge=$data->operator_drawout_badge;
        $operator_drawout_name=$data->operator_drawout_name;
        $operator_drawout_ket=$data->operator_drawout_ket;

        $pmlt_drawin_badge=$request->pmlt_drawin_badge;
        $pmlt_drawin_name=$request->pmlt_drawin_name;
        $pmlt_drawin_ket=$request->pmlt_drawin_ket;
        $pmlt_drawout_badge=$data->pmlt_drawout_badge;
        $pmlt_drawout_name=$data->pmlt_drawout_name;
        $pmlt_drawout_ket=$data->pmlt_drawout_ket;

        $sendingParams = [
          'id' => $id,
          'equipment_number' => $equipment_number,
          'location' => $location,
          'no_er' => $no_er,
          'activity_description' => $activity_description,
          'date' => $date,
          'time' => $time,
          'note' => $note,
          'safety_key' => $safety_key,
          'safety_tools' => $safety_tools,
          'status' => $status,

          'k3_drawin_badge' => $k3_drawin_badge,
          'k3_drawin_name' => $k3_drawin_name,
          'k3_drawin_ket' => $k3_drawin_ket,
          'k3_drawout_badge' => $k3_drawout_badge,
          'k3_drawout_name' => $k3_drawout_name,
          'k3_drawout_ket' => $k3_drawout_ket,

          'pmll_drawin_badge' => $pmll_drawin_badge,
          'pmll_drawin_name' => $pmll_drawin_name,
          'pmll_drawin_ket' => $pmll_drawin_ket,
          'pmll_drawout_badge' => $pmll_drawout_badge,
          'pmll_drawout_name' => $pmll_drawout_name,
          'pmll_drawout_ket' => $pmll_drawout_ket,

          'operator_drawin_badge' => $operator_drawin_badge,
          'operator_drawin_name' => $operator_drawin_name,
          'operator_drawin_ket' => $operator_drawin_ket,
          'operator_drawout_badge' => $operator_drawout_badge,
          'operator_drawout_name' => $operator_drawout_name,
          'operator_drawout_ket' => $operator_drawout_ket,

          'pmlt_drawin_badge' => $pmlt_drawin_badge,
          'pmlt_drawin_name' => $pmlt_drawin_name,
          'pmlt_drawin_ket' => $pmlt_drawin_ket,
          'pmlt_drawout_badge' => $pmlt_drawout_badge,
          'pmlt_drawout_name' => $pmlt_drawout_name,
          'pmlt_drawout_ket' => $pmlt_drawout_ket,
        ];

        try{
            $data = Lotto::find($id);
            $data->equipment_number=$equipment_number;
            $data->location=$location;
            $data->no_er=$no_er;
            $data->activity_description=$activity_description;
            $data->date=$date;
            $data->time=$time;
            $data->note=$note;
            $data->safety_key=$safety_key;
            $data->safety_tools=$safety_tools;
            $data->status=$status;

            $data->k3_drawin_badge=$k3_drawin_badge;
            $data->k3_drawin_name=$k3_drawin_name;
            $data->k3_drawin_ket=$k3_drawin_ket;
            $data->k3_drawout_badge=$k3_drawout_badge;
            $data->k3_drawout_name=$k3_drawout_name;
            $data->k3_drawout_ket=$k3_drawout_ket;

            $data->pmll_drawin_badge=$pmll_drawin_badge;
            $data->pmll_drawin_name=$pmll_drawin_name;
            $data->pmll_drawin_ket=$pmll_drawin_ket;
            $data->pmll_drawout_badge=$pmll_drawout_badge;
            $data->pmll_drawout_name=$pmll_drawout_name;
            $data->pmll_drawout_ket=$pmll_drawout_ket;

            $data->operator_drawin_badge=$operator_drawin_badge;
            $data->operator_drawin_name=$operator_drawin_name;
            $data->operator_drawin_ket=$operator_drawin_ket;
            $data->operator_drawout_badge=$operator_drawout_badge;
            $data->operator_drawout_name=$operator_drawout_name;
            $data->operator_drawout_ket=$operator_drawout_ket;

            $data->pmlt_drawin_badge=$pmlt_drawin_badge;
            $data->pmlt_drawin_name=$pmlt_drawin_name;
            $data->pmlt_drawin_ket=$pmlt_drawin_ket;
            $data->pmlt_drawout_badge=$pmlt_drawout_badge;
            $data->pmlt_drawout_name=$pmlt_drawout_name;
            $data->pmlt_drawout_ket=$pmlt_drawout_ket;

            $data->save();
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        }catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
        }
        return response()->json($params);
    }

    public function updateDrawOUTLotto (Request $request){

        $apiName='UPDATE_DRAWOUT_LOTTO';
        $id=$request->id;
        $data = Lotto::find($id);

        $equipment_number=$data->equipment_number;
        $location=$data->location;
        $no_er=$data->no_er;
        $activity_description=$data->activity_description;
        $date=$data->date;
        $time=$data->time;
        $note=$data->note;
        $safety_key=$data->safety_key;
        $safety_tools=$data->safety_tools;
        $status=$data->status;

        $k3_drawin_badge=$data->k3_drawin_badge;
        $k3_drawin_name=$data->k3_drawin_name;
        $k3_drawin_ket=$data->k3_drawin_ket;
        $k3_drawout_badge=$request->k3_drawout_badge;
        $k3_drawout_name=$request->k3_drawout_name;
        $k3_drawout_ket=$request->k3_drawout_ket;

        $pmll_drawin_badge=$data->pmll_drawin_badge;
        $pmll_drawin_name=$data->pmll_drawin_name;
        $pmll_drawin_ket=$data->pmll_drawin_ket;
        $pmll_drawout_badge=$request->pmll_drawout_badge;
        $pmll_drawout_name=$request->pmll_drawout_name;
        $pmll_drawout_ket=$request->pmll_drawout_ket;

        $operator_drawin_badge=$data->operator_drawin_badge;
        $operator_drawin_name=$data->operator_drawin_name;
        $operator_drawin_ket=$data->operator_drawin_ket;
        $operator_drawout_badge=$request->operator_drawout_badge;
        $operator_drawout_name=$request->operator_drawout_name;
        $operator_drawout_ket=$request->operator_drawout_ket;

        $pmlt_drawin_badge=$data->pmlt_drawin_badge;
        $pmlt_drawin_name=$data->pmlt_drawin_name;
        $pmlt_drawin_ket=$data->pmlt_drawin_ket;
        $pmlt_drawout_badge=$request->pmlt_drawout_badge;
        $pmlt_drawout_name=$request->pmlt_drawout_name;
        $pmlt_drawout_ket=$request->pmlt_drawout_ket;

        $sendingParams = [
          'id' => $id,
          'equipment_number' => $equipment_number,
          'location' => $location,
          'no_er' => $no_er,
          'activity_description' => $activity_description,
          'date' => $date,
          'time' => $time,
          'note' => $note,
          'safety_key' => $safety_key,
          'safety_tools' => $safety_tools,
          'status' => $status,

          'k3_drawin_badge' => $k3_drawin_badge,
          'k3_drawin_name' => $k3_drawin_name,
          'k3_drawin_ket' => $k3_drawin_ket,
          'k3_drawout_badge' => $k3_drawout_badge,
          'k3_drawout_name' => $k3_drawout_name,
          'k3_drawout_ket' => $k3_drawout_ket,

          'pmll_drawin_badge' => $pmll_drawin_badge,
          'pmll_drawin_name' => $pmll_drawin_name,
          'pmll_drawin_ket' => $pmll_drawin_ket,
          'pmll_drawout_badge' => $pmll_drawout_badge,
          'pmll_drawout_name' => $pmll_drawout_name,
          'pmll_drawout_ket' => $pmll_drawout_ket,

          'operator_drawin_badge' => $operator_drawin_badge,
          'operator_drawin_name' => $operator_drawin_name,
          'operator_drawin_ket' => $operator_drawin_ket,
          'operator_drawout_badge' => $operator_drawout_badge,
          'operator_drawout_name' => $operator_drawout_name,
          'operator_drawout_ket' => $operator_drawout_ket,

          'pmlt_drawin_badge' => $pmlt_drawin_badge,
          'pmlt_drawin_name' => $pmlt_drawin_name,
          'pmlt_drawin_ket' => $pmlt_drawin_ket,
          'pmlt_drawout_badge' => $pmlt_drawout_badge,
          'pmlt_drawout_name' => $pmlt_drawout_name,
          'pmlt_drawout_ket' => $pmlt_drawout_ket,
        ];

        try{
            $data = Lotto::find($id);
            $data->equipment_number=$equipment_number;
            $data->location=$location;
            $data->no_er=$no_er;
            $data->activity_description=$activity_description;
            $data->date=$date;
            $data->time=$time;
            $data->note=$note;
            $data->safety_key=$safety_key;
            $data->safety_tools=$safety_tools;
            $data->status=$status;

            $data->k3_drawin_badge=$k3_drawin_badge;
            $data->k3_drawin_name=$k3_drawin_name;
            $data->k3_drawin_ket=$k3_drawin_ket;
            $data->k3_drawout_badge=$k3_drawout_badge;
            $data->k3_drawout_name=$k3_drawout_name;
            $data->k3_drawout_ket=$k3_drawout_ket;

            $data->pmll_drawin_badge=$pmll_drawin_badge;
            $data->pmll_drawin_name=$pmll_drawin_name;
            $data->pmll_drawin_ket=$pmll_drawin_ket;
            $data->pmll_drawout_badge=$pmll_drawout_badge;
            $data->pmll_drawout_name=$pmll_drawout_name;
            $data->pmll_drawout_ket=$pmll_drawout_ket;

            $data->operator_drawin_badge=$operator_drawin_badge;
            $data->operator_drawin_name=$operator_drawin_name;
            $data->operator_drawin_ket=$operator_drawin_ket;
            $data->operator_drawout_badge=$operator_drawout_badge;
            $data->operator_drawout_name=$operator_drawout_name;
            $data->operator_drawout_ket=$operator_drawout_ket;

            $data->pmlt_drawin_badge=$pmlt_drawin_badge;
            $data->pmlt_drawin_name=$pmlt_drawin_name;
            $data->pmlt_drawin_ket=$pmlt_drawin_ket;
            $data->pmlt_drawout_badge=$pmlt_drawout_badge;
            $data->pmlt_drawout_name=$pmlt_drawout_name;
            $data->pmlt_drawout_ket=$pmlt_drawout_ket;

            $data->save();
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        }catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
        }
        return response()->json($params);
    }

    public function deleteLotto(Request $request){
        $apiName='DELETE_LOTTO';
        $id=$request->id;
        $sendingParams = [
            'id' => $id,
        ];

        try {
            Lotto::find($id)->delete();
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
            ];
            return response()->json($params);

        } catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404, $e->getMessage(),json_encode($sendingParams));
        }
    }
}
