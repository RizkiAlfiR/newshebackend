<?php

namespace App\Http\Controllers\Api\FireSystem\InspectionHydrant;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FireSystem\InspectionHydrant\LokasiHydrant;

class LokasiHydrantController extends Controller
{
    public function getLokasiHydrant(Request $request){

        $apiName='LOKASI_HYDRANT';
        $id_hydrant=null;
        $id_hydrant = $request->id_hydrant;

        if($id_hydrant){
          $data = LokasiHydrant::find($id_hydrant);
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];

        } else {
          $data = LokasiHydrant::all();
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];
        }
        return response()->json($params);
    }
}
