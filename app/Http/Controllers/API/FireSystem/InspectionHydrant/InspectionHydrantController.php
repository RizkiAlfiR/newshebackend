<?php

namespace App\Http\Controllers\Api\FireSystem\InspectionHydrant;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FireSystem\InspectionHydrant\InspectionHydrant;
use App\Models\FireSystem\InspectionHydrant\LokasiHydrant;

class InspectionHydrantController extends Controller
{
    public function addInspectionHydrant (Request $request){

        $apiName='INSPECTION_HYDRANT';
        $id_hydrant=$request->id_hydrant;
        $inspection_date=date('Y-m-d');
        $pilar_body=$request->pilar_body;
        $pilar_note=$request->pilar_note;
        $valve_kiri=$request->valve_kiri;
        $valve_atas=$request->valve_atas;
        $valve_kanan=$request->valve_kanan;
        $valve_note=$request->valve_note;
        $copling_kiri=$request->copling_kiri;
        $copling_kanan=$request->copling_kanan;
        $copling_note=$request->copling_note;
        $t_copling_kiri=$request->t_copling_kiri;
        $t_copling_kanan=$request->t_copling_kanan;
        $t_copling_note=$request->t_copling_note;
        $box_casing=$request->box_casing;
        $box_hose=$request->box_hose;
        $box_nozle=$request->box_nozle;
        $box_kunci=$request->box_kunci;
        $box_note=$request->box_note;
        $press=$request->press;
        $temuan=$request->temuan;
        $pic_badge=$request->pic_badge;
        $pic_name=$request->pic_name;

        $foto_temuan_name=null;
        $foto_temuan = $request->file('foto_temuan');
        if($foto_temuan){
            $destinationPath = 'public/uploads/InspectionHydrant/';
            $foto_temuan_name = $foto_temuan->getClientOriginalName();
            $foto_temuan->move($destinationPath, $foto_temuan_name);
        } else {
            $foto_temuan_name=null;
        }

        $area=$request->area;
        $unit_kerja=$request->unit_kerja;
        $company=$request->company;
        $plant=$request->plant;
        $qr_code=$request->qr_code;
        $last_inspection = $inspection_date;
        $exp_inspection = date('Y-m-d', strtotime('+7 days', strtotime($last_inspection)));

        $sendingParams = [
          'id_hydrant' => $id_hydrant,
          'inspection_date' => $inspection_date,
          'pilar_body' => $pilar_body,
          'pilar_note' => $pilar_note,
          'valve_kiri' => $valve_kiri,
          'valve_atas' => $valve_atas,
          'valve_kanan' => $valve_kanan,
          'valve_note' => $valve_note,
          'copling_kiri' => $copling_kiri,
          'copling_kanan' => $copling_kanan,
          'copling_note' => $copling_note,
          't_copling_kiri' => $t_copling_kiri,
          't_copling_kanan' => $t_copling_kanan,
          't_copling_note' => $t_copling_note,
          'box_casing' => $box_casing,
          'box_hose' => $box_hose,
          'box_nozle' => $box_nozle,
          'box_kunci' => $box_kunci,
          'box_note' => $box_note,
          'press' => $press,
          'temuan' => $temuan,
          'pic_badge' => $pic_badge,
          'pic_name' => $pic_name,
          'foto_temuan' => $foto_temuan,
          'area' => $area,
          'unit_kerja' => $unit_kerja,
          'company' => $company,
          'plant' => $plant,
          'qr_code' => $qr_code,
          'last_inspection' => $last_inspection,
          'exp_inspection' => $exp_inspection,
        ];

        try{
            $data = new InspectionHydrant();
            $data->id_hydrant=$id_hydrant;
            $data->inspection_date=$inspection_date;
            $data->pilar_body=$pilar_body;
            $data->pilar_note=$pilar_note;
            $data->valve_kiri=$valve_kiri;
            $data->valve_atas=$valve_atas;
            $data->valve_kanan=$valve_kanan;
            $data->valve_note=$valve_note;
            $data->copling_kiri=$copling_kiri;
            $data->copling_kanan=$copling_kanan;
            $data->copling_note=$copling_note;
            $data->t_copling_kiri=$t_copling_kiri;
            $data->t_copling_kanan=$t_copling_kanan;
            $data->t_copling_note=$t_copling_note;
            $data->box_casing=$box_casing;
            $data->box_hose=$box_hose;
            $data->box_nozle=$box_nozle;
            $data->box_kunci=$box_kunci;
            $data->box_note=$box_note;
            $data->press=$press;
            $data->temuan=$temuan;
            $data->pic_badge=$pic_badge;
            $data->pic_name=$pic_name;
            $data->foto_temuan=$foto_temuan_name;

            $dataLokasi = LokasiHydrant::find($id_hydrant);
            $dataLokasi->unit_kerja=$unit_kerja;
            $dataLokasi->area=$area;
            $dataLokasi->company=$company;
            $dataLokasi->plant=$plant;
            $dataLokasi->qr_code=$area;
            $dataLokasi->last_inspection=$last_inspection;
            $dataLokasi->exp_inspection=$exp_inspection;

            $data->save();
            $dataLokasi->save();
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        }catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));

        }

        return response()->json($params);
    }

    public function getInspectionHydrant(Request $request){

        $apiName='INSPECTION_HYDRANT';
        $inspection_date=null;
        $start_date=null;
        $end_date=null;
        $inspection_date = $request->inspection_date;
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if($inspection_date){
          $data = InspectionHydrant::where('inspection_date', 'LIKE', '%' .$inspection_date. '%')->limit(10)->get( );
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];

        } elseif ($start_date) {
          $data = InspectionHydrant::select("*")->whereBetween('inspection_date', [$start_date, $end_date])->get();
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];
        }else {
              $data = InspectionHydrant::all();
              $params = [
                  'is_success' => true,
                  'status' => 200,
                  'message' => 'success',
                  'data' => $data
              ];
        }
        return response()->json($params);
    }

    public function detailInspectionHydrant(Request $request){

        $id_hydrant = $request->id_hydrant;

        $data = InspectionHydrant::where('id_hydrant', 'LIKE', '%' .$id_hydrant. '%')->orderBy('inspection_date', 'DESC')->first();

        $params = [
            'is_success' => true,
            'status' => 200,
            'message' => 'success',
            'data' => $data
        ];

        return response()->json($params);
    }

    public function updateInspectionHydrant (Request $request){

        $apiName='UPDATE_INSPECTION_HYDRANT';
        $id=$request->id;
        $id_hydrant=$request->id_hydrant;
        $inspection_date=$request->inspection_date;
        $pilar_body=$request->pilar_body;
        $pilar_note=$request->pilar_note;
        $valve_kiri=$request->valve_kiri;
        $valve_atas=$request->valve_atas;
        $valve_kanan=$request->valve_kanan;
        $valve_note=$request->valve_note;
        $copling_kiri=$request->copling_kiri;
        $copling_kanan=$request->copling_kanan;
        $copling_note=$request->copling_note;
        $t_copling_kiri=$request->t_copling_kiri;
        $t_copling_kanan=$request->t_copling_kanan;
        $t_copling_note=$request->t_copling_note;
        $box_casing=$request->box_casing;
        $box_hose=$request->box_hose;
        $box_nozle=$request->box_nozle;
        $box_kunci=$request->box_kunci;
        $box_note=$request->box_note;
        $press=$request->press;
        $temuan=$request->temuan;
        $pic_badge=$request->pic_badge;
        $pic_name=$request->pic_name;

        $foto_temuan_name=null;
        $foto_temuan = $request->file('foto_temuan');
        if($foto_temuan){
            $destinationPath = 'public/uploads/InspectionHydrant/';
            $foto_temuan_name = $foto_temuan->getClientOriginalName();
            $foto_temuan->move($destinationPath, $foto_temuan_name);
        } else {
            $check = InspectionHydrant::find($id);
            $foto_temuan_name = $check->foto_temuan;
        }

        $sendingParams = [
          'id' => $id,
          'id_hydrant' => $id_hydrant,
          'inspection_date' => $inspection_date,
          'pilar_body' => $pilar_body,
          'pilar_note' => $pilar_note,
          'valve_kiri' => $valve_kiri,
          'valve_atas' => $valve_atas,
          'valve_kanan' => $valve_kanan,
          'valve_note' => $valve_note,
          'copling_kiri' => $copling_kiri,
          'copling_kanan' => $copling_kanan,
          'copling_note' => $copling_note,
          't_copling_kiri' => $t_copling_kiri,
          't_copling_kanan' => $t_copling_kanan,
          't_copling_note' => $t_copling_note,
          'box_casing' => $box_casing,
          'box_hose' => $box_hose,
          'box_nozle' => $box_nozle,
          'box_kunci' => $box_kunci,
          'box_note' => $box_note,
          'press' => $press,
          'temuan' => $temuan,
          'pic_badge' => $pic_badge,
          'pic_name' => $pic_name,
          'foto_temuan' => $foto_temuan_name,
        ];


        try{
            $data = InspectionHydrant::find($id);
            $data->id_hydrant=$id_hydrant;
            $data->inspection_date=$inspection_date;
            $data->pilar_body=$pilar_body;
            $data->pilar_note=$pilar_note;
            $data->valve_kiri=$valve_kiri;
            $data->valve_atas=$valve_atas;
            $data->valve_kanan=$valve_kanan;
            $data->valve_note=$valve_note;
            $data->copling_kiri=$copling_kiri;
            $data->copling_kanan=$copling_kanan;
            $data->copling_note=$copling_note;
            $data->t_copling_kiri=$t_copling_kiri;
            $data->t_copling_kanan=$t_copling_kanan;
            $data->t_copling_note=$t_copling_note;
            $data->box_casing=$box_casing;
            $data->box_hose=$box_hose;
            $data->box_nozle=$box_nozle;
            $data->box_kunci=$box_kunci;
            $data->box_note=$box_note;
            $data->press=$press;
            $data->temuan=$temuan;
            $data->pic_badge=$pic_badge;
            $data->pic_name=$pic_name;
            $data->foto_temuan=$foto_temuan_name;

            $data->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        }catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));
        }
        return response()->json($params);
    }

    public  function  deleteInspectionHydrant(Request $request){
        $apiName='DELETE_INSPECTION_HYDRANT';
        $id=$request->id;
        $sendingParams = [
            'id' => $id,
        ];

        try {
            InspectionHydrant::find($id)->delete();
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
            ];
            return response()->json($params);

        } catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404, $e->getMessage(),json_encode($sendingParams));
        }
    }
}
