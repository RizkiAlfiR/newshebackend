<?php

namespace App\Http\Controllers\Api\FireSystem\InspectionApar;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FireSystem\InspectionApar\InspectionApar;
use App\Models\FireSystem\InspectionApar\LokasiApar;

class InspectionAparController extends Controller
{
    public function addInspectionApar (Request $request){

        $apiName='INSPECTION_APAR';
        $id_apar=$request->id_apar;
        $klem=$request->klem;
        $press=$request->press;
        $hose=$request->hose;
        $seal=$request->seal;
        $sapot=$request->sapot;
        $weight=$request->weight;
        $status_cond=$request->status_cond;
        $inspection_date=date('Y-m-d');
        $note=$request->note;
        $pic_badge=$request->pic_badge;
        $pic_name=$request->pic_name;
        $area=$request->area;
        $unit_kerja=$request->unit_kerja;
        $company=$request->company;
        $plant=$request->plant;
        $qr_code=$request->qr_code;
        $last_inspection = $inspection_date;

        $exp_inspection = date('Y-m-d', strtotime('+7 days', strtotime($last_inspection)));

        $sendingParams = [
          'id_apar' => $id_apar,
          'klem' => $klem,
          'press' => $press,
          'hose' => $hose,
          'seal' => $seal,
          'sapot' => $sapot,
          'weight' => $weight,
          'status_cond' => $status_cond,
          'inspection_date' => $inspection_date,
          'note' => $note,
          'pic_badge' => $pic_badge,
          'pic_name' => $pic_name,
          'area' => $area,
          'unit_kerja' => $unit_kerja,
          'company' => $company,
          'plant' => $plant,
          'qr_code' => $qr_code,
          'last_inspection' => $last_inspection,
          'exp_inspection' => $exp_inspection,
        ];


        try{
            $data = new InspectionApar();
            $data->id_apar=$id_apar;
            $data->klem=$klem;
            $data->press=$press;
            $data->hose=$hose;
            $data->seal=$seal;
            $data->sapot=$sapot;
            $data->weight=$weight;
            $data->status_cond=$status_cond;
            $data->inspection_date=$inspection_date;
            $data->note=$note;
            $data->pic_badge=$pic_badge;
            $data->pic_name=$pic_name;

            $dataLokasi = LokasiApar::find($id_apar);
            $dataLokasi->unit_kerja=$unit_kerja;
            $dataLokasi->area=$area;
            $dataLokasi->company=$company;
            $dataLokasi->plant=$plant;
            $dataLokasi->qr_code=$area;
            $dataLokasi->last_inspection=$last_inspection;
            $dataLokasi->exp_inspection=$exp_inspection;

            $data->save();
            $dataLokasi->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        }catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));

        }

        return response()->json($params);
    }

    public function getInspectionApar(Request $request){

        $apiName='INSPECTION_APAR';
        $inspection_date=null;
        $start_date=null;
        $end_date=null;
        $inspection_date = $request->inspection_date;
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if($inspection_date){
          $data = InspectionApar::where('inspection_date', 'LIKE', '%' .$inspection_date. '%')->limit(10)->get( );
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];

        } elseif ($start_date) {
          $data = InspectionApar::select("*")->whereBetween('inspection_date', [$start_date, $end_date])->get();
          $params = [
              'is_success' => true,
              'status' => 200,
              'message' => 'success',
              'data' => $data
          ];
        }else {
              $data = InspectionApar::all();
              $params = [
                  'is_success' => true,
                  'status' => 200,
                  'message' => 'success',
                  'data' => $data
              ];
        }
        return response()->json($params);
    }

    public function detailInspectionApar(Request $request){

        $id_apar = $request->id_apar;

        $data = InspectionApar::where('id_apar', 'LIKE', '%' .$id_apar. '%')->orderBy('inspection_date', 'DESC')->first();
        $params = [
            'is_success' => true,
            'status' => 200,
            'message' => 'success',
            'data' => $data
        ];

        return response()->json($params);
    }

    public function updateInspectionApar (Request $request){

        $apiName='UPDATE_INSPECTION_APAR';
        $id=$request->id;
        $id_apar=$request->id_apar;
        $klem=$request->klem;
        $press=$request->press;
        $hose=$request->hose;
        $seal=$request->seal;
        $sapot=$request->sapot;
        $weight=$request->weight;
        $status_cond=$request->status_cond;
        $inspection_date=$request->inspection_date;
        $note=$request->note;

        $sendingParams = [
          'id' => $id,
          'id_apar' => $id_apar,
          'klem' => $klem,
          'press' => $press,
          'hose' => $hose,
          'seal' => $seal,
          'sapot' => $sapot,
          'weight' => $weight,
          'status_cond' => $status_cond,
          'inspection_date' => $inspection_date,
          'note' => $note,
        ];


        try{
            $data = InspectionApar::find($id);
            $data->id_apar=$id_apar;
            $data->klem=$klem;
            $data->press=$press;
            $data->hose=$hose;
            $data->seal=$seal;
            $data->sapot=$sapot;
            $data->weight=$weight;
            $data->status_cond=$status_cond;
            $data->inspection_date=$inspection_date;
            $data->note=$note;

            $data->save();

            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];

            return response()->json($params);

        }catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404,'Failed to save realisasi!',json_encode($sendingParams));

        }

        return response()->json($params);
    }

    public  function  deleteInspectionApar(Request $request){
        $apiName='DELETE_INSPECTION_APAR';
        $id=$request->id;
        $sendingParams = [
            'id' => $id,
        ];

        try {
            InspectionApar::find($id)->delete();
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
            ];
            return response()->json($params);

        } catch (Exception $e){
            return $this->messageSystem->returnApiMessage($apiName,404, $e->getMessage(),json_encode($sendingParams));
        }
    }
}
