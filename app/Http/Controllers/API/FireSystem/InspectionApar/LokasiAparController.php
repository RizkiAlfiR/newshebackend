<?php

namespace App\Http\Controllers\Api\FireSystem\InspectionApar;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FireSystem\InspectionApar\LokasiApar;

class LokasiAparController extends Controller
{
  public function getLokasiApar(Request $request){

      $apiName='LOKASI_APAR';
      $id_apar=null;
      $id_apar = $request->id_apar;

      if($id_apar){
        $data = LokasiApar::find($id_apar);
        $params = [
            'is_success' => true,
            'status' => 200,
            'message' => 'success',
            'data' => $data
        ];

      } else {
        $data = LokasiApar::all();
        $params = [
            'is_success' => true,
            'status' => 200,
            'message' => 'success',
            'data' => $data
        ];
      }
      return response()->json($params);
  }
}
