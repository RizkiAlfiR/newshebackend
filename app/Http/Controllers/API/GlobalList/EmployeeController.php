<?php

namespace App\Http\Controllers\API\GlobalList;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\GlobalList\Employee;

class EmployeeController extends Controller
{
    public function getEmployee(Request $request){
      $apiName='EMPLOYEE';
      $searchEmp=null;
      $searchEmp = $request->searchEmp;
      if($searchEmp){
        $data = Employee::where('mk_nama', 'LIKE', '%' .$searchEmp. '%')->limit(10)->get( );
        $params = [
            'is_success' => true,
            'status' => 200,
            'message' => 'success',
            'data' => $data
        ];

      } else {
            $data = Employee::all();
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];
      }
      return response()->json($params);
    }
}
