<?php

namespace App\Http\Controllers\API\GlobalList;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\GlobalList\Plant;

class PlantController extends Controller
{
    public function getPlant(Request $request){
      $apiName='PLANT';
      $search=null;
      $searchEmp = $request->searchEmp;
      if($searchEmp){
        $data = Plant::where('plant', 'LIKE', '%' .$searchEmp. '%')->limit(10)->get( );
        $params = [
            'is_success' => true,
            'status' => 200,
            'message' => 'success',
            'data' => $data
        ];

      } else {
            $data = Plant::all();
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];
      }
      return response()->json($params);
    }
}
