<?php

namespace App\Http\Controllers\API\GlobalList;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\GlobalList\InspectionArea;

class InspectionAreaController extends Controller
{
    public function getInspectionArea(){

        $data = InspectionArea::all();
        $params = [
            'is_success' => true,
            'status' => 200,
            'message' => 'success',
            'data' => $data
        ];

        return response()->json($params);
    }

    public function searchInspectionArea(Request $request)
    {
        $search = $request->get('keyword');
        $report = InspectionArea::where('area_name','LIKE', '%' .$search. '%')->get();
        // $pegawai = Pegawai::limit(3)->get();

        if(!$report)
        {
            return response()->json([
                "success" => false,
                "status" => 401,
                "message" => "Data tidak ditemukan"
            ]);
        } else
        {
            return response()->json([
                "success" => true,
                "status" => 200,
                "message" => "Data ditemukan",
                "data" => $report
            ]);
        }
    }
}
