<?php

namespace App\Http\Controllers\API\GlobalList;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\GlobalList\News;

class NewsController extends Controller
{
    public function getNews(Request $request){
      $apiName='NEWS';
      $id=null;
      $id = $request->id;
      if($id){
        $data = News::find($id);
        $params = [
            'is_success' => true,
            'status' => 200,
            'message' => 'success',
            'data' => $data
        ];
      } else {
            $data = News::orderBy('id', 'DESC')->limit(10)->get();
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];
      }
      return response()->json($params);
    }
}
