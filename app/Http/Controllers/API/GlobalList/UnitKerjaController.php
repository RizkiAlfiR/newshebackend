<?php

namespace App\Http\Controllers\API\GlobalList;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\GlobalList\UnitKerja;

class UnitKerjaController extends Controller
{
    public function getUnitKerja(Request $request){
      $apiName='UNIT_KERJA';
      $search=null;
      $searchEmp = $request->searchEmp;
      if($searchEmp){
        $data = UnitKerja::where('muk_nama', 'LIKE', '%' .$searchEmp. '%')->limit(10)->get( );
        $params = [
            'is_success' => true,
            'status' => 200,
            'message' => 'success',
            'data' => $data
        ];

      } else {
            $data = UnitKerja::all();
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];
      }
      return response()->json($params);
    }
}
