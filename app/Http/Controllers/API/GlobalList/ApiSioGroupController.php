<?php

namespace App\Http\Controllers\API\GlobalList;

use Illuminate\Http\Request;
use App\Models\GlobalList\SioGroup;
use App\Http\Controllers\Controller;

class ApiSioGroupController extends Controller
{
    public function search(Request $request)
    {
        $search = $request->get('keyword');
        $siogroup = SioGroup::where('NAME','LIKE', '%' .$search. '%')->limit(10)->get();
        // $pegawai = Pegawai::limit(3)->get();

        if(!$siogroup)
        {
            return response()->json([
                "success" => false,
                "status" => 401,
                "message" => "Data tidak ditemukan"
            ]);
        } else
        {
            return response()->json([
                "success" => true,
                "status" => 200,
                "message" => "Data ditemukan",
                "data" => $siogroup
            ]);
        }
    }
}
