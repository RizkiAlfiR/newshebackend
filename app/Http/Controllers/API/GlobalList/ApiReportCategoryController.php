<?php

namespace App\Http\Controllers\API\GlobalList;

use Illuminate\Http\Request;
use App\Models\GlobalList\ReportCategory;
use App\Http\Controllers\Controller;

class ApiReportCategoryController extends Controller
{
    public function index()
    {   
        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Master All Report Category list found',
            'data' => ReportCategory::all()
        ], 200);
    }

    public function search(Request $request)
    {
        $search = $request->get('keyword');
        $report = ReportCategory::where('DESKRIPSI','LIKE', '%' .$search. '%')->limit(100)->get();
        // $pegawai = Pegawai::limit(3)->get();

        if(!$report)
        {
            return response()->json([
                "success" => false,
                "status" => 401,
                "message" => "Data tidak ditemukan"
            ]);
        } else
        {
            return response()->json([
                "success" => true,
                "status" => 200,
                "message" => "Data ditemukan",
                "data" => $report
            ]);
        }
    }
}
