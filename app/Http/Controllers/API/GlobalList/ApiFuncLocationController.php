<?php

namespace App\Http\Controllers\API\GlobalList;

use Illuminate\Http\Request;
use App\Adapter\Curl;
use App\Models\GlobalList\FuncLocation;
use App\Http\Controllers\Controller;

class ApiFuncLocationController extends Controller
{
    public function getData() {
        $param = [
            "token" => "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjp7ImlkIjoiMzYyIiwicm9sZV9pZCI6IjEiLCJlbWFpbCI6IklORFJBLk5PRklBTkRJQFNFTUVOSU5ET05FU0lBLkNPTSIsInVzZXJuYW1lIjoiSU5EUkEuTk9GSUFOREkiLCJwYXNzd29yZCI6IiRQJEI5NVhcL2dTdmVEXC9tYmlTS2wwVHQ0bEZGSm5TT2pKXC8iLCJuYW1lIjoiSU5EUkEgTk9GSUFOREkiLCJyZXNldF9jb2RlIjpudWxsLCJyZXNldF90aW1lIjpudWxsLCJyZW1lbWJlcl9jb2RlIjoiJFAkQjRFZXJQXC9jTS5vN3VERXJGa1lDSnJYWGV3WWpXSTAiLCJsYXN0X2xvZ2luIjpudWxsLCJkYXRlX2NyZWF0ZWQiOiI2XC8xNFwvMjAxNyAxNTo0MyIsImFjdGl2ZSI6IjEiLCJjb21wYW55IjoiMjAwMCIsInBsYW50IjoiNTAwMiIsImFkIjoiMCIsInVuaXRfa2VyamEiOiJCdXJlYXUgb2YgR3JvdXAgRGVtYW5kIE1nbXQgJiBCdXMuIFByb2NzIiwidWtfa29kZSI6IjUwMDM5MDgzIiwibm9fYmFkZ2UiOiIwMDAwMzM5NiIsImNvc3RfY2VudGVyIjoiMjAwMjAyMzAwMCIsImNjX3RleHQiOiJCSVJPIERFTUFORCBNQU5BR0VNRU5UICYgUFJPU0VTIEJJU05JUyBHIiwicG9zaXRpb24iOiIzMCIsInBvc190ZXh0IjoiTWFuYWdlciIsInVwZGF0ZV9hdCI6IjExXC8yMFwvMjAxOCA4OjExIiwidXBkYXRlX2J5IjoiU1lBSFJJQUwuUkFNQURBTkkiLCJpc19rMyI6IjEiLCJkZXZpY2VfaWQiOiIwYzdhMDBlYjE1MTUyZGY0MzZkYmY4ZGIwOTEzYzFjZDM0MzQ5Zjc3ZjIzMTdiNGVhZWYwMGI3MTE0NjVkY2E4In0sImlhdCI6IjE1NTY1Mzg5OTYiLCJleHAiOjE1NTY1NTY5OTZ9.Wt8RyfFSqC2naMPDpIsiGBh4oMegygSUHMeQzuaEdXM",
            "search" => "",
            "limit" => 500,
            "plant" => ""
        ];
        $data = Curl::post("http://10.15.5.150/dev/she/api/v_mobile/list_master_data/search/funcloc", $param, '');
        // dd($data);
        $current_data = [];

        foreach($data as $item) {
            // dd($item->mk_nopeg);
            $current_data [] = [
                'idequpment' => $item->IDEQUPMENT, 
                'equptname' => $item->EQUPTNAME,
                'equptimage' => $item->EQUPTIMAGE,
                'sapequipnum' => $item->SAPEQUIPNUM,
                'equpmentcode' => $item->EQUPMENTCODE,
                'pplant' => $item->PPLANT,
                'status' => $item->STATUS,
                'funcloc' => $item->FUNCLOC,
                'mplant' => $item->MPLANT,
                'rnum' => $item->RNUM
            ];
        }

        FuncLocation::insert($current_data);
    }

    public function index()
    {   
        return response()->json([
            'success' => true,
            'status' => 200,
            'message' => 'Master All Functional Location list found',
            'data' => FuncLocation::all()
        ], 200);
    }

    public function search(Request $request)
    {
        $search = $request->get('keyword');
        $funcloc = FuncLocation::where('equpmentcode','LIKE', '%' .$search. '%')->limit(10)->get();
        // $pegawai = Pegawai::limit(3)->get();

        if(!$funcloc)
        {
            return response()->json([
                "success" => false,
                "status" => 401,
                "message" => "Data tidak ditemukan"
            ]);
        } else
        {
            return response()->json([
                "success" => true,
                "status" => 200,
                "message" => "Data ditemukan",
                "data" => $funcloc
            ]);
        }
    }
}
