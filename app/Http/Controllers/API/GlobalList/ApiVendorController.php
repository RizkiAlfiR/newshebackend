<?php

namespace App\Http\Controllers\API\GlobalList;

use Illuminate\Http\Request;
use App\Adapter\Curl;
use App\Models\GlobalList\Vendor;
use App\Http\Controllers\Controller;

class ApiVendorController extends Controller
{
    public function getData() {
        $param = [
            "token" => "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjp7ImlkIjoiMzYyIiwicm9sZV9pZCI6IjEiLCJlbWFpbCI6IklORFJBLk5PRklBTkRJQFNFTUVOSU5ET05FU0lBLkNPTSIsInVzZXJuYW1lIjoiSU5EUkEuTk9GSUFOREkiLCJwYXNzd29yZCI6IiRQJEI5NVhcL2dTdmVEXC9tYmlTS2wwVHQ0bEZGSm5TT2pKXC8iLCJuYW1lIjoiSU5EUkEgTk9GSUFOREkiLCJyZXNldF9jb2RlIjpudWxsLCJyZXNldF90aW1lIjpudWxsLCJyZW1lbWJlcl9jb2RlIjoiJFAkQjRFZXJQXC9jTS5vN3VERXJGa1lDSnJYWGV3WWpXSTAiLCJsYXN0X2xvZ2luIjpudWxsLCJkYXRlX2NyZWF0ZWQiOiI2XC8xNFwvMjAxNyAxNTo0MyIsImFjdGl2ZSI6IjEiLCJjb21wYW55IjoiMjAwMCIsInBsYW50IjoiNTAwMiIsImFkIjoiMCIsInVuaXRfa2VyamEiOiJCdXJlYXUgb2YgR3JvdXAgRGVtYW5kIE1nbXQgJiBCdXMuIFByb2NzIiwidWtfa29kZSI6IjUwMDM5MDgzIiwibm9fYmFkZ2UiOiIwMDAwMzM5NiIsImNvc3RfY2VudGVyIjoiMjAwMjAyMzAwMCIsImNjX3RleHQiOiJCSVJPIERFTUFORCBNQU5BR0VNRU5UICYgUFJPU0VTIEJJU05JUyBHIiwicG9zaXRpb24iOiIzMCIsInBvc190ZXh0IjoiTWFuYWdlciIsInVwZGF0ZV9hdCI6IjExXC8yMFwvMjAxOCA4OjExIiwidXBkYXRlX2J5IjoiU1lBSFJJQUwuUkFNQURBTkkiLCJpc19rMyI6IjEiLCJkZXZpY2VfaWQiOiIwYzdhMDBlYjE1MTUyZGY0MzZkYmY4ZGIwOTEzYzFjZDM0MzQ5Zjc3ZjIzMTdiNGVhZWYwMGI3MTE0NjVkY2E4In0sImlhdCI6IjE1NTg1ODM2ODQiLCJleHAiOjE1NTg2MDE2ODR9.auuERaHtgb4MY_aR6ZTqlxC4sgdwlwY2LHy0h2f240E",
            "search" => "",
            "limit" => 255,
            "plant" => ""
        ];
        $data = Curl::post("http://10.15.5.150/dev/she/api/v_mobile/list_master_data/search/vendor", $param, '');
        // dd($data);
        $current_data = [];

        foreach($data as $item) {
            // dd($item->mk_nopeg);
            $current_data [] = [
                'LIFNR' => $item->LIFNR,
                'NAME1' => $item->NAME1,
                'KATEGORI' => $item->KATEGORI,
                'ALAMAT' => $item->ALAMAT,
                'EMAIL' => $item->EMAIL,
                'PASS' => $item->PASS,
                'TYV' => $item->TYV,
                'FLAG' => $item->FLAG,
                'ID' => $item->ID,
                'KETERANGAN' => $item->KETERANGAN,
                'KTOKK' => $item->KTOKK,
                'RNUM' => $item->RNUM,
            ];
        }

        Vendor::insert($current_data);
    }

    public function search(Request $request)
    {
        $search = $request->get('keyword');
        $vendor = Vendor::where('NAME1','LIKE', '%' .$search. '%')->limit(100)->get();
        // $pegawai = Pegawai::limit(3)->get();

        if(!$vendor)
        {
            return response()->json([
                "success" => false,
                "status" => 401,
                "message" => "Data tidak ditemukan"
            ]);
        } else
        {
            return response()->json([
                "success" => true,
                "status" => 200,
                "message" => "Data ditemukan",
                "data" => $vendor
            ]);
        }
    }
}
