<?php

namespace App\Http\Controllers\API\GlobalList;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\GlobalList\PPlant;

class PPlantController extends Controller
{
    public function getPPlant(Request $request){
      $apiName='PPLANT';
      $search=null;
      $searchEmp = $request->searchEmp;
      if($searchEmp){
        $data = PPlant::where('pplantdesc', 'LIKE', '%' .$searchEmp. '%')->limit(10)->get( );
        $params = [
            'is_success' => true,
            'status' => 200,
            'message' => 'success',
            'data' => $data
        ];

      } else {
            $data = PPlant::all();
            $params = [
                'is_success' => true,
                'status' => 200,
                'message' => 'success',
                'data' => $data
            ];
      }
      return response()->json($params);
    }
}
