<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use App\Models\Safety\AccidentReport;
use App\Models\GlobalList\Employee;
use App\Models\GlobalList\FuncLocation;

class AccidentReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $jumlahAccident = AccidentReport::count();
        $jumlahAccidentOpen = AccidentReport::where(['status_report' => 'OPEN'])->count();
        $jumlahAccidentClose = AccidentReport::where(['status_report' => 'CLOSE'])->count();

        $params = [
            'jumlahAccident' => $jumlahAccident,
            'jumlahAccidentOpen' => $jumlahAccidentOpen,
            'jumlahAccidentClose' => $jumlahAccidentClose
        ];

        // mengambil data dari table accident report
        $accident_reports = DB::table('accident_reports')->get();

    	// mengirim data accident ke view index
        return view('safety.accidentreport.index', $params, ['accident_reports' => $accident_reports]);
    }

    public  function show($id) {
        $data = AccidentReport::where('id', $id)->get();
        $data_karyawan = Employee::all();
        $data_uk = UnitKerja::all();
        $params=[
            'data'=> $data,
            'title'=>'Manajemen Transaksi',
            'data_karyawan' => $data_karyawan,
            'data_uk' => $data_uk
        ];
        // dd($params);
        return view('safety.accidentreport.detail', $params);
    }

    public function create()
    {
        return view('safety.accidentreport.form');
    }

    public function store(Request $request)
    {
        // insert data ke table pegawai
        DB::table('accident_reports')->insert([
            'badge_vict' => $request->badge_vict,
        ]);
        // alihkan halaman ke halaman pegawai
        return redirect('/accidentreport');
    }

    public function addFollowUp(Request $request)
    {
        $id = $request->input('id');

        if($id) {
            $data = AccidentReport::find($id);
            //dd($data);
        } else
        {
            $data = new AccidentReport();
        }
        $params =[
            'title' => 'Manajemen Accident Report',
            'data' => $data,
        ];
        return view('safety.accidentreport.formFollowUp', $params);
    }

    public function saveFollowUp(Request $request)
    {
        $id = intval($request->input('id', 0));

        if($id) {
            $data = AccidentReport::find($id);
        } else
        {
            $data = new AccidentReport();
        }

        $data->status_report = $request->status_report;
        $data->follow_up = $request->follow_up;

        if(!is_null($data->pict_after)){
            if(is_null($request->file('pict_after')) && $request->file('pict_after')==''){
                $fileImageAfter = $data->pict_after;
            }else{
                $rules = [
                    'pict_after' => 'mimes:jpeg,bmp,png,gif,jpg,pdf',
                ];
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails())
                {
                    return "<div class='alert alert-danger'>Terjadi kesalahan!Format tidak valid!</div>";
                } else
                {
                    $fotoImageAfter = $request->file('pict_after');
                    $destinationPath = 'public/uploads/accident/'.$request->image_after_id;
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $fileImageAfter = date('YmdHis') . '_' . $fotoImageAfter->getClientOriginalName();
                    $fotoImageAfter->move($destinationPath, $fileImageAfter);
                }
            }
        }

        if(is_null($data->pict_after)){
            if(is_null($request->file('pict_after')) && $request->file('pict_after')==''){
                $fileImageAfter = $data->pict_after;
            }else{
                $rules = [
                    'pict_after' => 'mimes:jpeg,bmp,png,gif,jpg,pdf',
                ];
                $validator=Validator::make($request->all(), $rules);
                if($validator->fails())
                {
                    return "<div class='alert alert-danger'>Terjadi kesalahan!Format tidak valid!</div>";
                } else
                {
                    $fotoImageAfter = $request->file('pict_after');
                    $destinationPath = 'public/uploads/accident/'.$request->image_after_id;
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $fileImageAfter = date('YmdHis') . '_' . $fotoImageAfter->getClientOriginalName();
                    $fotoImageAfter->move($destinationPath, $fileImageAfter);
                }
            }
        }
        $data->pict_after = $fileImageAfter;
        
        try {
            $data->update();
            return "
            <div class='alert alert-success'> Add Follow Up Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Add Follow Up Failed! Follow Up not saved!</div>";
        }
    }
    
    public function addMaster(Request $request)
    {
        $id = $request->input('id');
        $data_peg = Employee::all();
        $data_funloc = FuncLocation::all();

        if($id) {
            $data = AccidentReport::find($id);
            //dd($data);
        } else
        {
            $data = new AccidentReport();
        }
        $params =[
            'title' => 'Manajemen Accident Report',
            'data_peg' => $data_peg,
            'data_funloc' => $data_funloc,
            'data' => $data,
        ];
        return view('safety.accidentreport.formMaster', $params);
    }

    public function saveMaster(Request $request)
    {
        $id = intval($request->input('id', 0));

        if($id) {
            $data = AccidentReport::find($id);
        } else
        {
            $data = new AccidentReport();
            $checkData = AccidentReport::where(['acd_cause' => $request->acd_cause])->first();
            if($checkData){
                return "<div class='alert alert-danger'>Data Buku Lisensi sudah tersedia!</div>";
            }
        }

        $data->date_acd = $request->date_acd;
        $data->time_acd = $request->time_acd;
        $data->shift = $request->shift;
        $data->shift_text = "Shift " . $request->shift;
        $data->stat_vict = $request->stat_vict;
        if($request->stat_vict == 0) {
            $data->state_vict_text = "Non-Karyawan";
            $data->name_vict = $request->name_vict;
            $data->no_bpjs = $request->no_bpjs;
        } else {
            $data->state_vict_text = "Karyawan";
            $temppeg = explode('|', $request->arrpeg);
            $data->badge_vict = $temppeg[1];
            $data->name_vict = $temppeg[0];
            $data->date_in = $temppeg[2];
            $data->position = $temppeg[3];
            $data->position_text = $temppeg[4];
            $data->region = $temppeg[5];
            $data->uk_vict = $temppeg[6];
            $data->uk_vict_text = $temppeg[7];
            $templahir = explode('-', $temppeg[8]);
            $tahunlahir = $templahir[0];
            $data->age = date('Y', strtotime('-' .$tahunlahir. 'Years'));
        }
        $tempspv = explode('|', $request->arrspv);
        $data->spv_badge = $tempspv[1];
        $data->spv_name = $tempspv[0];
        $data->spv_position = $tempspv[2];
        $data->spv_position_text = $tempspv[3];
        $data->work_task = $request->work_task;
        $data->acd_cause = $request->acd_cause;
        $temploc = explode('|', $request->arrloc);
        $data->location = $temploc[1];
        $data->location_text = $temploc[0];
        $tempsaksi = explode('|', $request->arrsaksi);
        $data->s_badge = $tempsaksi[1];
        $data->s_name = $tempsaksi[0];
        $data->s_position = $tempsaksi[2];
        $data->s_pos_text = $tempsaksi[3];
        $data->company_code = 2000;
        $data->company_text = "PT. Semen Indonesia, Tbk.";
        $data->user_id = Auth::id();
        $data->inj_text = $request->inj_text;
        $data->inj_note = $request->inj_note;
        $data->first_aid = $request->first_aid;
        $data->next_aid = $request->next_aid;
        $data->safety_req = $request->safety_req;
        $data->acd_rpt_text = $request->acd_rpt_text;
        $data->unsafe_act = $request->unsafe_act;
        $data->unsafe_cond = $request->unsafe_cond;
        $data->note = $request->note;
        $temppic = explode('|', $request->arrpic);
        $data->spv_badge = $temppic[1];
        $data->spv_name = $temppic[0];
        $data->status_report = $request->status_report;

        if(!is_null($data->pict_before)){
            if(is_null($request->file('pict_before')) && $request->file('pict_before')==''){
                $fileImagePictBef = $data->pict_before;
            }else{
                $rules = [
                    'pict_before' => 'mimes:jpeg,bmp,png,gif,jpg,pdf',
                ];
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails())
                {
                    return "<div class='alert alert-danger'>Terjadi kesalahan!Format tidak valid!</div>";
                } else
                {
                    $fotoImagePictBef = $request->file('pict_before');
                    $destinationPath = 'public/uploads/accident/'.$request->image_pictbef_id;
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $fileImagePictBef = date('YmdHis') . '_' . $fotoImagePictBef->getClientOriginalName();
                    $fotoImagePictBef->move($destinationPath, $fileImagePictBef);
                }
            }
        }

        if(is_null($data->pict_before)){
            if(is_null($request->file('pict_before')) && $request->file('pict_before')==''){
                $fileImagePictBef = $data->pict_before;
            }else{
                $rules = [
                    'pict_before' => 'mimes:jpeg,bmp,png,gif,jpg,pdf',
                ];
                $validator=Validator::make($request->all(), $rules);
                if($validator->fails())
                {
                    return "<div class='alert alert-danger'>Terjadi kesalahan!Format tidak valid!</div>";
                } else
                {
                    $fotoImagePictBef = $request->file('pict_before');
                    $destinationPath = 'public/uploads/accident/'.$request->image_foto_id;
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $fileImagePictBef = date('YmdHis') . '_' . $fotoImagePictBef->getClientOriginalName();
                    $fotoImagePictBef->move($destinationPath, $fileImagePictBef);
                }
            }
        }
        $data->pict_before = $fileImagePictBef;
            
        try {
            $data->save();
            return "
            <div class='alert alert-success'> Add Accident Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            dd($ex);
            return "<div class='alert alert-danger'>Add Accident Failed! Tools Certification not saved!</div>";
        }
    }

    public function showImage(Request $request){
        $id = intval($request->input('id',0));

        if($id) {
            // $data = Sio::where('foto', 'LIKE', '%' .$id. '%')->get();
            $data = AccidentReport::find($id);
            // dd($data);
        } else
        {
            $data = new AccidentReport();
        }
        $params =[
            'title' => 'Manajemen Accident Report',
            'data' => $data,
        ];
        return view('safety.accidentreport.showImage', $params);
    }

    public function deleteMaster(Request $request){

        $id = intval($request->input('id', 0));
        $data = AccidentReport::find($id);

        try {
            $data->delete();
            return "
            <div class='alert alert-success'>Accident Report Remove Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch(\Exception $ex){
            return "<div class='alert alert-danger'>Remove Failed! Accident Report not removed!</div>";
        }
    }

    public function chart()
    {
        $monthlyTransaction = [];
        for ($i = 1; $i <= 12; $i++){
            $monthIsOpen  = AccidentReport::whereMonth('date_acd', $this->generateMonth($i))
                ->where('status_report', 'OPEN')
                // ->where('user_id',$userId)
                ->count();
            $monthIsClose  = AccidentReport::whereMonth('date_acd', $this->generateMonth($i))
                ->where('status_report', 'CLOSE')
                // ->where('user_id',$userId)
                ->count();

            $monthlyTransaction[] = [
                'valueIsOpen' =>$monthIsOpen,
                'valueIsClose'=>$monthIsClose
            ];
        }
        $isOpen = [];
        $isClose = [];
        foreach ($monthlyTransaction as $item){
            $isOpen[] = $item['valueIsOpen'];
            $isClose[] = $item['valueIsClose'];
        }

        $params = [
            'isOpen' => $isOpen,
            'isClose' => $isClose,
            'data' => $monthlyTransaction
        ];

        return response()->json($params);
    }

    public function chart2()
    {
        $monthlyAccident = [];
        for ($i = 1; $i <= 12; $i++){
            $monthIsAccident  = AccidentReport::whereMonth('date_acd', $this->generateMonth($i))
                // ->where('status_report', 'OPEN')
                // ->where('user_id',$userId)
                ->count();

            $monthlyAccident[] = [
                'valueIsAccident' =>$monthIsAccident,
            ];
        }
        $isAccident = [];
        foreach ($monthlyAccident as $item){
            $isAccident[] = $item['valueIsAccident'];
        }

        $params = [
            'isAccident' => $isAccident,
            'data' => $monthlyAccident
        ];

        return response()->json($params);
    }

    private function generateMonth($idx)
    {
        if(strlen($idx) < 2)
        {
            $idx = '0'.$idx;
        }
        return $idx;
    }

    // public function chartPerMonth()
    // {
    //     $monthlyAccident = [];
    //     for ($i = 1; $i <= 12; $i++){
    //         $monthIsAccident  = AccidentReport::whereMonth('date_acd', $this->generateMonth($i))
    //             // ->where('status', 'OPEN')
    //             // ->where('user_id',$userId)
    //             ->count();

    //         $monthlyAccident[] = [
    //             'valueIsAccident' =>$monthIsAccident,
    //         ];
    //     }
    //     $isAccident = [];
    //     foreach ($monthlyAccident as $item){
    //         $isAccident[] = $item['valueIsAccident'];
    //     }

    //     $params = [
    //         'isAccident' => $isAccident,
    //         'data' => $monthlyAccident
    //     ];

    //     return response()->json($params);
    // }
}