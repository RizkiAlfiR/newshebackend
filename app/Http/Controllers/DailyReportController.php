<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Auth;
use App\Models\Safety\DailyReport;
use App\Models\Safety\UnsafeDetail;
use App\Models\GlobalList\FuncLocation;
use App\Models\GlobalList\UnitKerja;

class DailyReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $jumlahReport = UnsafeDetail::count();
        $jumlahReportOpen = UnsafeDetail::where(['status' => 'OPEN'])->count();
        $jumlahReportClose = UnsafeDetail::where(['status' => 'CLOSE'])->count();
        $jumlahDetail = DailyReport::with('unsafe_detail_reports')->count();
        // $HeaderWithDetail = UnsafeDetail::with('daily_reports')->count('id_report', $request->id_report);
        // $Detail = UnsafeDetail::where(['report_id']);
        // $HeaderWithDetail = [DailyReport::where(['id'])->$Detail->count();

        // dd($HeaderWithDetail);
        $params = [
            'jumlahReport' => $jumlahReport,
            'jumlahReportOpen' => $jumlahReportOpen,
            'jumlahReportClose' => $jumlahReportClose,
            'jumlahDetail' => $jumlahDetail
        ];

        // mengambil data dari table accident report
        $daily = DB::table('daily_reports')->get();

    	// mengirim data accident ke view index
        return view('safety.dailyreport.index', $params, ['daily' => $daily]);
    }

    public  function show($id) {
        $data = UnsafeDetail::where('id_report', $id)->get();
        $params=[
            'data'=> $data,
            'title'=>'Manajemen Transaksi',
        ];
        // dd($params);
        return view('safety.dailyreport.headerDetail', $params);
    }

    public function showImage(Request $request){
        $id = intval($request->input('id',0));

        if($id) {
            $data = UnsafeDetail::find($id);
            // $data = UnsafeDetail::where('id_report', $id)->get();
            // dd($data);
        } else
        {
            $data = new UnsafeDetail();
        }
        $params =[
            'title' => 'Manajemen Unsafe Certification',
            'data' => $data,
        ];
        return view('safety.dailyreport.showImage', $params);
    }

    public function addFollowUp(Request $request)
    {
        $id = $request->input('id');

        if($id) {
            $data = UnsafeDetail::find($id);
        } else
        {
            $data = new UnsafeDetail();
        }
        $params =[
            'title' => 'Manajemen Daily Report',
            'data' => $data,
        ];
        return view('safety.dailyreport.formFollowUp', $params);
    }

    public function saveFollowUp(Request $request)
    {
        $id = intval($request->input('id', 0));

        if($id) {
            $data = UnsafeDetail::find($id);
        } else
        {
            $data = new UnsafeDetail();
        }

        $data->tanggal_real = $request->tanggal_real;
        $data->status = $request->status;
        $data->tindak_lanjut = $request->tindak_lanjut;

        if(!is_null($data->foto_aft)){
            if(is_null($request->file('foto_aft')) && $request->file('foto_aft')==''){
                $fileImageAft = $data->foto_aft;
            }else{
                $rules = [
                    'foto_aft' => 'mimes:jpeg,bmp,png,gif,jpg,pdf',
                ];
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails())
                {
                    return "<div class='alert alert-danger'>Terjadi kesalahan!Format tidak valid!</div>";
                } else
                {
                    $fotoImageAft = $request->file('foto_aft');
                    $destinationPath = 'public/uploads/unsafe/'.$request->image_aft_id;
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $fileImageAft = date('YmdHis') . '_' . $fotoImageAft->getClientOriginalName();
                    $fotoImageAft->move($destinationPath, $fileImageAft);
                }
            }
        }

        if(is_null($data->foto_aft)){
            if(is_null($request->file('foto_aft')) && $request->file('foto_aft')==''){
                $fileImageAft = $data->foto_aft;
            }else{
                $rules = [
                    'foto_aft' => 'mimes:jpeg,bmp,png,gif,jpg,pdf',
                ];
                $validator=Validator::make($request->all(), $rules);
                if($validator->fails())
                {
                    return "<div class='alert alert-danger'>Terjadi kesalahan!Format tidak valid!</div>";
                } else
                {
                    $fotoImageAft = $request->file('foto_aft');
                    $destinationPath = 'public/uploads/unsafe/'.$request->image_aft_id;
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $fileImageAft = date('YmdHis') . '_' . $fotoImageAft->getClientOriginalName();
                    $fotoImageAft->move($destinationPath, $fileImageAft);
                }
            }
        }
        $data->foto_aft = $fileImageAft;
        
        try {
            $data->update();
            return "
            <div class='alert alert-success'> Add Follow Up Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Add Follow Up Failed! Follow Up not saved!</div>";
        }
    }

    public function addMaster(Request $request)
    {
        $id = $request->input('id');
        $data_funloc = FuncLocation::all();
        $data_uk = UnitKerja::all();

        if($id) {
            $data = UnsafeDetail::find($id);
        } else
        {
            $data = new UnsafeDetail();
        }
        $params =[
            'title' => 'Manajemen Daily Report',
            'data_funloc' => $data_funloc,
            'data_uk' => $data_uk,
            'data' => $data,
        ];
        return view('safety.dailyreport.formMaster', $params);
    }

    public function saveMaster(Request $request)
    {
        $id = intval($request->input('id', 0));

        if($id) {
            $data = UnsafeDetail::find($id);
        } else
        {
            $data = new UnsafeDetail();
            $checkData = UnsafeDetail::where(['no_dokumen' => $request->no_dokumen])->first();
            if($checkData){
                return "<div class='alert alert-danger'>Data Area sudah tersedia!</div>";
            }
        }

        $data->no_dokumen = "R/538/002/" . $id;
        $data->temuan = $request->temuan;
        $temploc = explode('|', $request->arrloc);
        $data->lokasi_kode = $temploc[1];
        $data->lokasi_text = $temploc[0];
        $data->penyebab = $request->penyebab;
        $data->tanggal_real = $request->tanggal_real;

        if(!is_null($data->foto_bef)){
            if(is_null($request->file('foto_bef')) && $request->file('foto_bef')==''){
                $fileImageFotoBef = $data->foto_bef;
            }else{
                $rules = [
                    'foto_bef' => 'mimes:jpeg,bmp,png,gif,jpg,pdf',
                ];
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails())
                {
                    return "<div class='alert alert-danger'>Terjadi kesalahan!Format tidak valid!</div>";
                } else
                {
                    $fotoImageFotoBef = $request->file('foto_bef');
                    $destinationPath = 'public/uploads/unsafe/'.$request->image_fotobef_id;
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $fileImageFotoBef = date('YmdHis') . '_' . $fotoImageFotoBef->getClientOriginalName();
                    $fotoImageFotoBef->move($destinationPath, $fileImageFotoBef);
                }
            }
        }

        if(is_null($data->foto_bef)){
            if(is_null($request->file('foto_bef')) && $request->file('foto_bef')==''){
                $fileImageFotoBef = $data->foto_bef;
            }else{
                $rules = [
                    'foto_bef' => 'mimes:jpeg,bmp,png,gif,jpg,pdf',
                ];
                $validator=Validator::make($request->all(), $rules);
                if($validator->fails())
                {
                    return "<div class='alert alert-danger'>Terjadi kesalahan!Format tidak valid!</div>";
                } else
                {
                    $fotoImageFotoBef = $request->file('foto_bef');
                    $destinationPath = 'public/uploads/unsafe/'.$request->image_fotobef_id;
                    if (!file_exists($destinationPath)) {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $fileImageFotoBef = date('YmdHis') . '_' . $fotoImageFotoBef->getClientOriginalName();
                    $fotoImageFotoBef->move($destinationPath, $fileImageFotoBef);
                }
            }
        }
        $data->foto_bef = $fileImageFotoBef;
        $data->status = $request->status;
        $data->rekomendasi = $request->rekomendasi;
        $data->user_id = Auth::id();
        $data->user_name = "User With ID " . Auth::id();
        $data->jenis_temuan = $request->jenis_temuan;
        $data->company = 2000;
        $data->plant = 5002;
        $data->tindak_lanjut = $request->tindak_lanjut;
        $data->note = $request->note;
        $data->potensi_bahaya = $request->potensi_bahaya;
        $data->priority = $request->priority;
        $tempuk = explode('|', $request->arruk);
        $data->uk_kode = $tempuk[1];
        $data->uk_text = $tempuk[0];
        
        try {
            $data->save();
            return "
            <div class='alert alert-success'> Add Header Daily Report K3 Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Add Daily Report Failed! Daily Report not saved!</div>";
        }
    }

    public function deleteMaster(Request $request){

        $id = intval($request->input('id', 0));
        $data = UnsafeDetail::find($id);

        try {
            $data->delete();
            return "
            <div class='alert alert-success'> Daily Report Remove Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch(\Exception $ex){
            return "<div class='alert alert-danger'>Remove Failed! Daily Report not removed!</div>";
        }
    }
}
