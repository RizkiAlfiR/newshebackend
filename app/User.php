<?php

namespace App;

use App\Models\Safety\AccidentReport;
use App\Models\Safety\DailyReport;
use App\Models\Safety\UnsafeDetail;
use App\Models\Safety\Sio;
use App\Models\Safety\Tools;
use App\Models\Safety\DailyReportK3;
use App\Models\Safety\DailyReportK3Detail;
use App\Models\Safety\ListFileUpload;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id', 'email', 'username', 'password', 'name', 'company', 'plant', 'ad',
        'unit_kerja', 'uk_kode', 'no_badge', 'cost_center', 'cc_text', 'position',
        'pos_text', 'is_k3'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
 
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function accident_reports()
    {
        return $this->hasMany(AccidentReport::class);
    }

    public function tools()
    {
        return $this->hasMany(Tools::class);
    }

    public function sios()
    {
        return $this->hasMany(Sio::class);
    }

    public function daily_reports()
    {
        return $this->hasMany(DailyReport::class);
    }

    public function unsafe_details()
    {
        return $this->hasMany(UnsafeDetail::class);
    }

    public function daily_reports_k3()
    {
        return $this->hasMany(DailyReportK3::class);
    }

    public function unsafe_details_k3()
    {
        return $this->hasMany(DailyReportK3Detail::class);
    }

    public function unsafe_k3_detail()
    {
        return $this->hasMany(ListFileUpload::class);
    }

    public function pplant()
    {
        return $this->hasMany(PlanningPlant::class);
    }
}
