<?php

namespace App\Models\FireSystem\FireReport;

use Illuminate\Database\Eloquent\Model;

class FireReportImage extends Model
{
    protected $table = 'fire_report_image';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
