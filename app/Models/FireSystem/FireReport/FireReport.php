<?php

namespace App\Models\FireSystem\FireReport;

use Illuminate\Database\Eloquent\Model;

class FireReport extends Model
{
    protected $table = 'fire_report';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function image()
    {
        return $this->hasMany('App\Models\FireSystem\FireReport\FireReportImage', 'id_fire_report', 'id');
    }
}
