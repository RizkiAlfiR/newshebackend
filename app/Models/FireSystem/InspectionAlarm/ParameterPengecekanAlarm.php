<?php

namespace App\Models\FireSystem\InspectionAlarm;

use Illuminate\Database\Eloquent\Model;

class ParameterPengecekanAlarm extends Model
{
    protected $table = 'parameter_pengecekan_alarm';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
