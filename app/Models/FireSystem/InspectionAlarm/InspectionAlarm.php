<?php

namespace App\Models\FireSystem\InspectionAlarm;


use Illuminate\Database\Eloquent\Model;

class InspectionAlarm extends Model
{
    protected $table = 'inspection_alarm';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
