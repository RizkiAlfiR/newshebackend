<?php

namespace App\Models\FireSystem\InspectionAlarm;


use Illuminate\Database\Eloquent\Model;

class LokasiAlarm extends Model
{
    protected $table = 'lokasi_alarm';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
