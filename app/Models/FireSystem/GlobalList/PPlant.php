<?php

namespace App\Models\FireSystem\GlobalList;


use Illuminate\Database\Eloquent\Model;

class PPlant extends Model
{
    protected $table = 'pplant';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
