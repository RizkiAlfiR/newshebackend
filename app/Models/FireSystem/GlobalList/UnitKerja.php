<?php

namespace App\Models\FireSystem\GlobalList;


use Illuminate\Database\Eloquent\Model;

class UnitKerja extends Model
{
    protected $table = 'she_master_unit_kerja';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
