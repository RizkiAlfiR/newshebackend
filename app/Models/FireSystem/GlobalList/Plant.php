<?php

namespace App\Models\FireSystem\GlobalList;


use Illuminate\Database\Eloquent\Model;

class Plant extends Model
{
    protected $table = 'plant';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
