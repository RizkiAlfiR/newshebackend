<?php

namespace App\Models\FireSystem\GlobalList;


use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'she_master_pegawai';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
