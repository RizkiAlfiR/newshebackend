<?php

namespace App\Models\FireSystem\InspectionHydrant;


use Illuminate\Database\Eloquent\Model;

class LokasiHydrant extends Model
{
    protected $table = 'lokasi_hydrant';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function getDetail()
    {
        return $this->hasMany('App\Models\FireSystem\InspectionHydrant\InspectionHydrant', 'id_hydrant', 'id');
    }
}
