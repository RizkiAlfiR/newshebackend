<?php

namespace App\Models\FireSystem\InspectionHydrant;


use Illuminate\Database\Eloquent\Model;

class InspectionHydrant extends Model
{
    protected $table = 'inspection_hydrant';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
