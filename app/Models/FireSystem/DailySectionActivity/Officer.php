<?php

namespace App\Models\FireSystem\DailySectionActivity;

use Illuminate\Database\Eloquent\Model;

class Officer extends Model
{
    protected $table = 'petugas';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
