<?php

namespace App\Models\FireSystem\DailySectionActivity;

use Illuminate\Database\Eloquent\Model;

class DailySectionActivity extends Model
{
    protected $table = 'daily_section_activity';
    protected $primaryKey = 'id';
    public $timestamps = false;

    // public function getTools()
    // {
    //     return $this->hasMany('App\Models\FireSystem\DailySectionActivity\Tools', 'id_daily_activity', 'id');
    // }
}
