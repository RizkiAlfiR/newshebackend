<?php

namespace App\Models\FireSystem\UnitCheck;


use Illuminate\Database\Eloquent\Model;

class ListVehicle extends Model
{
    protected $table = 'list_vehicle';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
