<?php

namespace App\Models\FireSystem\UnitCheck;


use Illuminate\Database\Eloquent\Model;

class InspectionPickup extends Model
{
    protected $table = 'inspection_pickup';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
