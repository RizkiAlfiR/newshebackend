<?php

namespace App\Models\FireSystem\UnitCheck;


use Illuminate\Database\Eloquent\Model;

class InspectionPMK extends Model
{
    protected $table = 'inspection_pmk';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
