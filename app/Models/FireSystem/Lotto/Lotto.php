<?php

namespace App\Models\FireSystem\Lotto;


use Illuminate\Database\Eloquent\Model;

class Lotto extends Model
{
    protected $table = 'lotto';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
