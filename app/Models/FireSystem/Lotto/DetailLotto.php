<?php

namespace App\Models\FireSystem\Lotto;


use Illuminate\Database\Eloquent\Model;

class DetailLotto extends Model
{
    protected $table = 'detail_lotto';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
