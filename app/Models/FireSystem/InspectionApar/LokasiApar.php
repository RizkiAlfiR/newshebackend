<?php

namespace App\Models\FireSystem\InspectionApar;


use Illuminate\Database\Eloquent\Model;

class LokasiApar extends Model
{
    protected $table = 'lokasi_apar';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
