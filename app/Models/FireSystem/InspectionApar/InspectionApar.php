<?php

namespace App\Models\FireSystem\InspectionApar;

use Illuminate\Database\Eloquent\Model;

class InspectionApar extends Model
{
    protected $table = 'inspection_apar';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
