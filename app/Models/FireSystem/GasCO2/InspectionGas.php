<?php

namespace App\Models\FireSystem\GasCO2;


use Illuminate\Database\Eloquent\Model;

class InspectionGas extends Model
{
    protected $table = 'inspection_gas';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
