<?php

namespace App\Models\FireSystem\GasCO2;

use Illuminate\Database\Eloquent\Model;

class GasCO2 extends Model
{
    protected $table = 'list_gas';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
