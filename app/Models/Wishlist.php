<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
    protected $table = 'wishlist';
    protected $primaryKey = 'id';
    public $timestamps = true;

    // public function getRelationDiagnosa()
    // {
    //     return $this->hasMany('App\Models\RelationDiagnosaKlinis', 'diagnosa_penyakit_id', 'id');
    // }

}