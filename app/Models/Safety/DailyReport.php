<?php

namespace App\Models\Safety;

use Illuminate\Database\Eloquent\Model;

class DailyReport extends Model
{
    protected $fillable = [
        'shift', 'no_dokumen', 'status', 'user_name', 'tanggal',
        'company', 'plant'
    ];

    public function unsafe_detail_reports()
    {
        // return $this->hasMany(UnsafeDetail::class);
        return $this->hasMany('App\Models\Safety\UnsafeDetail', 'id_report', 'id');
    }
}
