<?php

namespace App\Models\Safety;

use Illuminate\Database\Eloquent\Model;

class Sio extends Model
{
    protected $fillable = [
        'badge', 'unit_kerja', 'no_lisensi', 'kelas', 'masa_berlaku', 'foto', 'user_name',
        'unit_kerja_txt', 'grup', 'company', 'plant', 'nama', 'time_certification', 'masa_awal',
        'start_date', 'end_date'
    ];
}
