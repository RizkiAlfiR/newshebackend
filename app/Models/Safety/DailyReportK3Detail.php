<?php

namespace App\Models\Safety;

use Illuminate\Database\Eloquent\Model;

class DailyReportK3Detail extends Model
{
    protected $fillable = [
        'unit_code', 'unit_name', 'activity_description', 'unsafe_type', 'potential_hazard',
        'follow_up', 'is_smig', 'o_clock_incident', 'id_category', 'deskripsi', 'kode_en', 'status', 'tindak_lanjut',
        'recomendation', 'trouble_maker', 'close_date', 'close_by', 'mail_list', 'comitment_date', 'foto_bef', 'foto_aft',
        'verificattion_date'
    ];

    public function daily_reports_k3()
    {
        return $this->hasOne(DailyReportK3::class);
    }
}
