<?php

namespace App\Models\Safety;

use Illuminate\Database\Eloquent\Model;

class UnsafeDetail extends Model
{
    protected $fillable = [
        'no_dokumen', 'temuan', 'lokasi_kode', 'lokasi_text', 'penyebab', 'tanggal_real',
        'foto_bef', 'foto_aft', 'status', 'rekomendasi', 'user_name', 'jenis_temuan', 'company',
        'plant', 'tindak_lanjut', 'note', 'potensi_bahaya', 'priority', 'uk_kode', 'uk_text'
    ];

    public function daily_reports()
    {
        return $this->hasOne(DailyReport::class);
    }
}
