<?php

namespace App\Models\Safety;

use Illuminate\Database\Eloquent\Model;

class Tools extends Model
{
    protected $fillable = [
        'no_buku', 'equipment', 'kode', 'data_teknis', 'nomor_pengesahan', 'lokasi', 'uji_ulang', 'note', 'user_name',
        'uk_kode', 'uk_text', 'company', 'plant', 'file_buku', 'file_aktual', 'uji_awal', 'time_certification',
        'start_date', 'end_date'
    ];
}
