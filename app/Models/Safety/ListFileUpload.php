<?php

namespace App\Models\Safety;

use Illuminate\Database\Eloquent\Model;

class ListFileUpload extends Model
{
    protected $fillable = [
        'type_file_upload', 'uploaded_path', 'file_description', 'inspection_date'
    ];
}
