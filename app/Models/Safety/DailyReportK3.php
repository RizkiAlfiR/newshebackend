<?php

namespace App\Models\Safety;

use Illuminate\Database\Eloquent\Model;

class DailyReportK3 extends Model
{
    protected $fillable = [
        'date', 'shift', 'area_code', 'area_txt', 'sub_area_code', 'sub_area_txt',
        'plant_code', 'plant_txt', 'inspector_code', 'inspector_name', 'is_smig',
        'accident', 'incident', 'nearmiss', 'close_date', 'close_by'
    ];

    public function unsafe_report_details_k3()
    {
        return $this->hasMany('App\Models\Safety\DailyReportK3Detail', 'id_unsafe_report', 'id');
    }
}
