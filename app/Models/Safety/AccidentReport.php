<?php

namespace App\Models\Safety;

use Illuminate\Database\Eloquent\Model;

class AccidentReport extends Model
{
    protected $fillable = [
        'date_acd', 'time_acd', 'stat_vict', 'state_vict_text', 'badge_vict', 'name_vict', 'uk_vict', 'uk_vict_text',
        'company_code', 'company_text', 'ven_code', 'ven_text', 'date_in', 'position', 'position_text', 'age', 'no_jamsos',
        'no_bpjs', 'shift', 'shift_text', 'region', 'work_task', 'spv_badge', 'spv_name', 'spv_position', 'spv_position_text',
        'location', 'location_text', 'acd_cause', 's_badge', 's_name', 's_position', 's_pos_text', 'inj_note', 'inj_text',
        'first_aid', 'next_aid', 'safety_req', 'acd_rpt_text', 'unsafe_act', 'unsafe_cond', 'note', 'inv_badge', 'inv_name',
        'status_report', 'pict_before', 'pict_after','follow_up'
    ];
}
