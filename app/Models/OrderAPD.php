<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class OrderAPD extends Model
{
    protected $table = 'order_apd';
    protected $primaryKey = 'id';
    public $timestamps = true;

    public function getDetailOrder()
    {
        return $this->hasMany('App\Models\DetailOrder', 'kode_order', 'id');
    }

}