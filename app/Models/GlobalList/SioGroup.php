<?php

namespace App\Models\GlobalList;

use Illuminate\Database\Eloquent\Model;

class SioGroup extends Model
{
    protected $table = 'she_safety_sio_group';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'NICKNAME', 'NAME'
    ];
}
