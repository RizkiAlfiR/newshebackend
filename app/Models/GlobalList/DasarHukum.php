<?php

namespace App\Models\GlobalList;

use Illuminate\Database\Eloquent\Model;

class DasarHukum extends Model
{
    protected $table = 'she_safety_dasar_hukum';
    public $timestamps = false;

    protected $fillable = [
        'id', 'ayat', 'pasal', 'deskripsi', 'create_at', 'create_by', 'update_at', 'update_by',
        'delete_at', 'delete_by', 'note', 'company', 'plant'
    ];
}
