<?php

namespace App\Models\GlobalList;


use Illuminate\Database\Eloquent\Model;

class PPlant extends Model
{
    protected $table = 'pplant';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
