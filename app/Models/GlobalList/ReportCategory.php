<?php

namespace App\Models\GlobalList;

use Illuminate\Database\Eloquent\Model;

class ReportCategory extends Model
{
    protected $table = 'she_safety_report_category';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'KODE', 'DESKRIPSI', 'CREATE_AT', 'CREATE_BY', 'UPDATE_AT', 'UPDATE_BY',
        'DELETE_AT', 'DELETE_BY', 'COMPANY', 'PLANT', 'KODE_EN'
    ];
}
