<?php

namespace App\Models\GlobalList;


use Illuminate\Database\Eloquent\Model;

class UnitKerja extends Model
{
    protected $table = 'she_master_unit_kerja';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
