<?php

namespace App\Models\GlobalList;


use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'she_master_pegawai';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
