<?php

namespace App\Models\GlobalList;


use Illuminate\Database\Eloquent\Model;

class Plant extends Model
{
    protected $table = 'plant';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
