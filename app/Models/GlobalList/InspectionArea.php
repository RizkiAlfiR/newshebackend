<?php

namespace App\Models\GlobalList;


use Illuminate\Database\Eloquent\Model;

class InspectionArea extends Model
{
    protected $table = 'inspection_area';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
