<?php

namespace App\Models\GlobalList;


use Illuminate\Database\Eloquent\Model;

class MasterCategory extends Model
{
    protected $table = 'apd_master_category';
    protected $primaryKey = 'id';
    public $timestamps = true;
}
