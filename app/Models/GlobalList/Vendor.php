<?php

namespace App\Models\GlobalList;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    protected $table = 'she_master_vendor';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'LIFNR', 'NAME1', 'KATEGORI', 'ALAMAT', 'EMAIL', 'PASS', 'TYV', 'FLAG',
        'KETERANGAN', 'KTOKK', 'RNUM'
    ];
}
