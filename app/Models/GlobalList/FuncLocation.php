<?php

namespace App\Models\GlobalList;

use Illuminate\Database\Eloquent\Model;

class FuncLocation extends Model
{
    protected $table = 'she_master_funcloc';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'idequpment', 'equptname', 'equptimage', 'sapequipnum', 'equpmentcode', 'pplant', 'status', 'funcloc', 'mplant',
        'rnum'
    ];
}
