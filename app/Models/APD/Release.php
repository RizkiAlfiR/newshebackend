<?php

namespace App\Models\APD;


use Illuminate\Database\Eloquent\Model;

class Release extends Model
{
    protected $table = 'release';
    protected $primaryKey = 'id';
    public $timestamps = true;

    // public function getDetailOrder()
    // {
    //     return $this->hasMany('App\Models\APD\DetailOrder', 'kode_order', 'id');
    // }

}