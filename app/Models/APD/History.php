<?php

namespace App\Models\APD;


use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $table = 'history';
    protected $primaryKey = 'id';
    public $timestamps = true;

    public function getOrder()
    {
        return $this->hasMany('App\Models\APD\OrderAPD', 'kode_order', 'kode_order');
    }

    // public $rules=[
    //     'accident_name' =>'required',
        
    // ];

}