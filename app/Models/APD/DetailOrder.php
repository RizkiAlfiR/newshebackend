<?php

namespace App\Models\APD;


use Illuminate\Database\Eloquent\Model;

class DetailOrder extends Model
{
    protected $table = 'detail_order';
    protected $primaryKey = 'id';
    public $timestamps = true;

    public function getMasterAPD()
    {
        return $this->hasMany('App\Models\APD\MasterAPD', 'kode', 'kode_apd');
    }

}