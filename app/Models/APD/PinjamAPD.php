<?php

namespace App\Models\APD;


use Illuminate\Database\Eloquent\Model;

class PinjamAPD extends Model
{
    protected $table = 'pinjam_apd';
    protected $primaryKey = 'id';
    public $timestamps = true;

    public function getDetailPinjam()
    {
        return $this->hasMany('App\Models\APD\DetailPinjam', 'kode_pinjam', 'id');
    }

}