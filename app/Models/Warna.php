<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Warna extends Model
{
    protected $table = 'warna';
    protected $primaryKey = 'id';
    public $timestamps = true;

    // public function getRelationDiagnosa()
    // {
    //     return $this->hasMany('App\Models\DetailOrder', 'diagnosa_penyakit_id', 'id');
    // }

}