<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class MasterAPD extends Model
{
    protected $table = 'master_apd';
    protected $primaryKey = 'id';
    public $timestamps = true;

    // public function getRelationDiagnosa()
    // {
    //     return $this->hasMany('App\Models\RelationDiagnosaKlinis', 'diagnosa_penyakit_id', 'id');
    // }

}