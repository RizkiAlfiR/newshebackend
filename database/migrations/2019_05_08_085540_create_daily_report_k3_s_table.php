<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyReportK3STable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_report_k3_s', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('report_date');
            $table->integer('shift');
            $table->string('area_code')->nullable();
            $table->string('area_txt')->nullable();
            $table->string('sub_area_code')->nullable();
            $table->string('sub_area_txt')->nullable();
            $table->string('plant_code')->nullable();
            $table->string('plant_txt')->nullable();
            $table->string('inspector_code')->nullable();
            $table->string('inspector_name')->nullable();
            $table->bigInteger('user_id')->unsigned();
            $table->integer('is_smig');
            $table->integer('accident')->nullable();
            $table->integer('incident')->nullable();
            $table->integer('nearmiss')->nullable();
            $table->date('close_date')->nullable();
            $table->string('close_by')->nullable();
            $table->timestamps();

            $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daily_report_k3_s');
    }
}
