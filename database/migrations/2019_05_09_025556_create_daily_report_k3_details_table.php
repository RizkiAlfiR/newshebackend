<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyReportK3DetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_report_k3_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_unsafe_report')->unsigned();
            $table->string('unit_code');
            $table->string('unit_name');
            $table->string('activity_description');
            $table->string('unsafe_type')->nullable();
            $table->string('potential_hazard')->nullable();
            $table->string('follow_up')->nullable();
            $table->bigInteger('user_id')->unsigned();
            $table->integer('is_smig')->default(1);
            $table->string('o_clock_incident')->nullable();
            $table->integer('id_category')->nullable();
            $table->string('deskripsi')->nullable();
            $table->string('kode_en')->nullable();
            $table->string('status')->nullable()->default('NULL');
            $table->string('tindak_lanjut')->nullable();
            $table->string('recomendation')->nullable();
            $table->string('trouble_maker')->nullable();
            $table->date('close_date')->nullable();
            $table->string('close_by')->nullable();
            $table->string('mail_list')->nullable();
            $table->date('comitment_date')->nullable();
            $table->string('foto_bef')->nullable();
            $table->string('foto_aft')->nullable();
            $table->string('verification_date')->nullable();
            $table->timestamps();

            $table->foreign('id_unsafe_report')
            ->references('id')
            ->on('daily_report_k3_s')
            ->onDelete('cascade');

            $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daily_report_k3_details');
    }
}
