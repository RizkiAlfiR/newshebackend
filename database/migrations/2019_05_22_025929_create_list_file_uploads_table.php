<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListFileUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('list_file_uploads', function (Blueprint $table) {
            $table->bigIncrements('id_upload_unsafe');
            $table->bigInteger('id_unsafe_report_detail')->unsigned();
            $table->string('type_file_upload')->nullable();
            $table->string('uploaded_path')->nullable();
            $table->string('file_description')->nullable();
            $table->bigInteger('user_id')->unsigned();
            $table->date('inspection_date')->nullable();
            $table->timestamps();

            $table->foreign('id_unsafe_report_detail')
            ->references('id')
            ->on('daily_report_k3_details')
            ->onDelete('cascade');

            $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('list_file_uploads');
    }
}
