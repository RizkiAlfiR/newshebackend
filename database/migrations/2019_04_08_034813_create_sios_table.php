<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('badge');
            $table->string('unit_kerja')->nullable();
            $table->string('no_lisensi');
            $table->string('kelas')->nullable();
            $table->date('masa_berlaku');
            $table->string('foto')->nullable();
            $table->bigInteger('user_id')->unsigned();
            $table->string('user_name')->nullable();
            $table->string('unit_kerja_txt')->nullable();
            $table->string('grup')->nullable();
            $table->string('company')->nullable();
            $table->string('plant')->nullable();
            $table->string('nama')->nullable();
            $table->integer('time_certification');
            $table->date('masa_awal');
            $table->date('start_date');
            $table->date('end_date');
            $table->timestamps();

            $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sios');
    }
}
