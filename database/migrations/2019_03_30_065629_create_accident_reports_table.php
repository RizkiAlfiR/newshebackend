<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccidentReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accident_reports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->date('date_acd');
            $table->string('time_acd');
            $table->integer('stat_vict');
            $table->string('state_vict_text');
            $table->string('badge_vict');
            $table->string('name_vict');
            $table->string('uk_vict');
            $table->string('uk_vict_text');
            $table->string('company_code');
            $table->string('company_text');
            $table->string('ven_code');
            $table->string('ven_text');
            $table->date('date_in');
            $table->string('position');
            $table->string('position_text');
            $table->integer('age');
            $table->string('no_jamsos');
            $table->string('no_bpjs');
            $table->integer('shift');
            $table->string('shift_text');
            $table->string('region');
            $table->string('work_task');
            $table->string('spv_badge');
            $table->string('spv_name');
            $table->string('spv_position');
            $table->string('spv_position_text');
            $table->string('location');
            $table->string('location_text');
            $table->string('acd_cause');
            $table->string('s_badge');
            $table->string('s_name');
            $table->string('s_position');
            $table->string('s_pos_text');
            $table->string('inj_note');
            $table->string('inj_text');
            $table->string('first_aid');
            $table->string('next_aid');
            $table->string('safety_req');
            $table->string('acd_rpt_text');
            $table->string('unsafe_act');
            $table->string('unsafe_cond');
            $table->string('note');
            $table->string('inv_badge');
            $table->string('inv_name');
            $table->string('status_report');
            $table->string('pict_before')->nullable();
            $table->string('pict_after')->nullable();
            $table->string('follow_up')->nullable();
            $table->timestamps();

            $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accident_reports');
    }
}
