<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateToolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tools', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no_buku');
            $table->string('equipment')->nullable();
            $table->string('kode')->nullable();
            $table->string('data_teknis')->nullable();
            $table->string('nomor_pengesahan');
            $table->string('lokasi')->nullable();
            $table->date('uji_ulang')->nullable();
            $table->bigInteger('user_id')->unsigned();
            $table->string('user_name')->nullable();
            $table->string('note')->nullable();
            $table->string('uk_kode')->nullable();
            $table->string('uk_text')->nullable();
            $table->string('company')->nullable();
            $table->string('plant')->nullable();
            $table->string('file_buku')->nullable();
            $table->string('file_aktual')->nullable();
            $table->date('uji_awal')->nullable();
            $table->integer('time_certification')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->timestamps();

            $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tools');
    }
}
