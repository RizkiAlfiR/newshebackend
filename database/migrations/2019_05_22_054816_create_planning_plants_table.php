<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanningPlantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planning_plants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('plant')->nullable();
            $table->string('pplant')->nullable();
            $table->string('pplantdesc')->nullable();
            $table->string('rnum')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('planning_plants');
    }
}
