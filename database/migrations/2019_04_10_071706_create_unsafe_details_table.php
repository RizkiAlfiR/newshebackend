<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnsafeDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unsafe_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no_dokumen')->nullable();
            $table->bigInteger('id_report')->unsigned();
            $table->string('temuan')->nullable();
            $table->string('lokasi_kode')->nullable();
            $table->string('lokasi_text')->nullable();
            $table->string('penyebab')->nullable();
            $table->date('tanggal_real')->nullable();
            $table->string('foto_bef')->nullable();
            $table->string('foto_aft')->nullable();
            $table->string('status')->nullable();
            $table->string('rekomendasi')->nullable();
            $table->bigInteger('user_id')->unsigned();
            $table->string('user_name')->nullable();
            $table->string('jenis_temuan')->nullable();
            $table->string('company')->nullable();
            $table->string('plant')->nullable();
            $table->string('tindak_lanjut')->nullable();
            $table->string('note')->nullable();
            $table->string('potensi_bahaya')->nullable();
            $table->integer('priority')->nullable();
            $table->timestamps();

            $table->foreign('id_report')
            ->references('id')
            ->on('daily_reports')
            ->onDelete('cascade');

            $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unsafe_details');
    }
}
