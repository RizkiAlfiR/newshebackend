<?php

use Illuminate\Http\Request;

Route::post('login', 'API\ApiController@login');
Route::post('register', 'API\ApiController@register');


//---GLOBAL LIST---//
Route::post('getEmployee', 'API\GlobalList\EmployeeController@getEmployee');
Route::post('/getUnitKerja','API\GlobalList\UnitKerjaController@getUnitKerja');
Route::post('/getPlant','API\GlobalList\PlantController@getPlant');
Route::post('/getPPlant','API\GlobalList\PPlantController@getPPlant');
Route::post('/getInspectionArea','API\GlobalList\InspectionAreaController@getInspectionArea');
Route::post('/getNews','API\GlobalList\NewsController@getNews');

//--PERBAIKAN ROUTE ALFI--//
Route::get('master/funcloc/search', 'API\GlobalList\ApiFuncLocationController@search');
Route::get('master/reportcategory/search', 'API\GlobalList\ApiReportCategoryController@search');
Route::get('master/vendor/search', 'API\GlobalList\ApiVendorController@search');
Route::get('master/dasarhukum', 'API\GlobalList\ApiRecapitulationController@dasarhukum');
Route::get('master/pegawai/search', 'API\GlobalList\EmployeeController@getEmployee');
Route::get('master/unitkerja/search', 'API\GlobalList\UnitKerjaController@getUnitKerja');
Route::get('master/pplant/search', 'API\GlobalList\PPlantController@getPPlant');
Route::get('master/inspection_area/search', 'API\GlobalList\InspectionAreaController@searchInspectionArea');
Route::get('master/siogroup/search', 'API\GlobalList\ApiSioGroupController@search');


//---FIRE SYSTEM---//

//NEWS FIRE REPORT//
Route::post('/getNewsOfficer','API\FireSystem\News\NewsController@getNewsOfficer');
Route::post('/getNewsDaily','API\FireSystem\News\NewsController@getNewsDaily');
Route::post('/getNewsFire','API\FireSystem\News\NewsController@getNewsFire');
Route::post('/getNewsLotto','API\FireSystem\News\NewsController@getNewsLotto');

//INPECTION//
Route::post('/getLokasiAlarm','API\FireSystem\InspectionAlarm\LokasiAlarmController@getLokasiAlarm');
Route::post('/getInspectionAlarm','API\FireSystem\InspectionAlarm\InspectionAlarmController@getInspectionAlarm');
Route::post('/addInspectionAlarm','API\FireSystem\InspectionAlarm\InspectionAlarmController@addInspectionAlarm');
Route::post('/updateInspectionAlarm','API\FireSystem\InspectionAlarm\InspectionAlarmController@updateInspectionAlarm');
Route::post('/detailInspectionAlarm','API\FireSystem\InspectionAlarm\InspectionAlarmController@detailInspectionAlarm');
Route::post('/deleteInspectionAlarm','API\FireSystem\InspectionAlarm\InspectionAlarmController@deleteInspectionAlarm');
Route::post('/getParameterPengecekanAlarm','API\FireSystem\InspectionAlarm\ParameterPengecekanAlarmController@getParameterPengecekanAlarm');

Route::post('/getLokasiApar','API\FireSystem\InspectionApar\LokasiAparController@getLokasiApar');
Route::post('/getInspectionApar','API\FireSystem\InspectionApar\InspectionAparController@getInspectionApar');
Route::post('/addInspectionApar','API\FireSystem\InspectionApar\InspectionAparController@addInspectionApar');
Route::post('/updateInspectionApar','API\FireSystem\InspectionApar\InspectionAparController@updateInspectionApar');
Route::post('/detailInspectionApar','API\FireSystem\InspectionApar\InspectionAparController@detailInspectionApar');
Route::post('/deleteInspectionApar','API\FireSystem\InspectionApar\InspectionAparController@deleteInspectionApar');

Route::post('/getLokasiHydrant','API\FireSystem\InspectionHydrant\LokasiHydrantController@getLokasiHydrant');
Route::post('/getInspectionHydrant','API\FireSystem\InspectionHydrant\InspectionHydrantController@getInspectionHydrant');
Route::post('/addInspectionHydrant','API\FireSystem\InspectionHydrant\InspectionHydrantController@addInspectionHydrant');
Route::post('/updateInspectionHydrant','API\FireSystem\InspectionHydrant\InspectionHydrantController@updateInspectionHydrant');
Route::post('/detailInspectionHydrant','API\FireSystem\InspectionHydrant\InspectionHydrantController@detailInspectionHydrant');
Route::post('/deleteInspectionHydrant','API\FireSystem\InspectionHydrant\InspectionHydrantController@deleteInspectionHydrant');


//DAILY SECTION ACTIVITY//
Route::post('/getOfficer','Api\FireSystem\DailySectionActivity\OfficerController@getOfficer');

Route::post('/getDailySectionActivity','Api\FireSystem\DailySectionActivity\DailySectionActivityController@getDailySectionActivity');
Route::post('/addDailySectionActivity','Api\FireSystem\DailySectionActivity\DailySectionActivityController@addDailySectionActivity');
Route::post('/updateDailySectionActivity','Api\FireSystem\DailySectionActivity\DailySectionActivityController@updateDailySectionActivity');
Route::post('/detailDailySectionActivity','Api\FireSystem\DailySectionActivity\DailySectionActivityController@detailDailySectionActivity');
Route::post('/deleteDailySectionActivity','Api\FireSystem\DailySectionActivity\DailySectionActivityController@deleteDailySectionActivity');

//GAS CO2//
Route::post('/getGasCO2','Api\FireSystem\GasCO2\GasCO2Controller@getGasCO2');
Route::post('/getInspectionGas','Api\FireSystem\GasCO2\InspectionGasController@getInspectionGas');
Route::post('/addInspectionGas','Api\FireSystem\GasCO2\InspectionGasController@addInspectionGas');
Route::post('/updateInspectionGas','Api\FireSystem\GasCO2\InspectionGasController@updateInspectionGas');
Route::post('/detailInspectionGas','Api\FireSystem\GasCO2\InspectionGasController@detailInspectionGas');
Route::post('/deleteInspectionGas','Api\FireSystem\GasCO2\InspectionGasController@deleteInspectionGas');

//FIRE REPORT//
Route::post('/addFireReport','API\FireSystem\FireReport\FireReportController@addFireReport');
Route::post('/getFireReport','API\FireSystem\FireReport\FireReportController@getFireReport');
Route::post('/updateFireReport','API\FireSystem\FireReport\FireReportController@updateFireReport');
Route::post('/detailFireReport','API\FireSystem\FireReport\FireReportController@detailFireReport');
Route::post('/deleteFireReport','API\FireSystem\FireReport\FireReportController@deleteFireReport');

//FIRE REPORT IMAGE//
Route::post('/getFireReportImage','API\FireSystem\FireReport\FireReportImageController@getFireReportImage');
Route::post('/addFireReportImage','API\FireSystem\FireReport\FireReportImageController@addFireReportImage');
Route::post('/updateFireReportImage','API\FireSystem\FireReport\FireReportImageController@updateFireReportImage');
Route::post('/deleteFireReportImage','API\FireSystem\FireReport\FireReportImageController@deleteFireReportImage');

//UNIT CHECK//
Route::post('/getListVehicle','API\FireSystem\UnitCheck\ListVehicleController@getListVehicle');

Route::post('/getInspectionPickup','API\FireSystem\UnitCheck\InspectionPickupController@getInspectionPickup');
Route::post('/detailInspectionPickup','API\FireSystem\UnitCheck\InspectionPickupController@detailInspectionPickup');
Route::post('/addInspectionPickup','API\FireSystem\UnitCheck\InspectionPickupController@addInspectionPickup');
Route::post('/updateInspectionPickup','API\FireSystem\UnitCheck\InspectionPickupController@updateInspectionPickup');
Route::post('/deleteInspectionPickup','API\FireSystem\UnitCheck\InspectionPickupController@deleteInspectionPickup');

Route::post('/getInspectionPMK','API\FireSystem\UnitCheck\InspectionPMKController@getInspectionPMK');
Route::post('/detailInspectionPMK','API\FireSystem\UnitCheck\InspectionPMKController@detailInspectionPMK');
Route::post('/addInspectionPMK','API\FireSystem\UnitCheck\InspectionPMKController@addInspectionPMK');
Route::post('/updateInspectionPMK','API\FireSystem\UnitCheck\InspectionPMKController@updateInspectionPMK');
Route::post('/deleteInspectionPMK','API\FireSystem\UnitCheck\InspectionPMKController@deleteInspectionPMK');

//DASHBOARD UNIT CHECK//
Route::post('/getListPMK','API\FireSystem\DashboardUnitCheck\DashboardUnitCheckController@getListPMK');
Route::post('/getListPickup','API\FireSystem\DashboardUnitCheck\DashboardUnitCheckController@getListPickup');

//LOTTO//
Route::post('/getLotto','API\FireSystem\Lotto\LottoController@getLotto');
Route::post('/getDetailLotto','API\FireSystem\Lotto\LottoController@getDetailLotto');
Route::post('/addLotto','API\FireSystem\Lotto\LottoController@addLotto');
Route::post('/updateLotto','API\FireSystem\Lotto\LottoController@updateLotto');
Route::post('/deleteLotto','API\FireSystem\Lotto\LottoController@deleteLotto');

Route::post('/updateDrawINLotto','API\FireSystem\Lotto\LottoController@updateDrawINLotto');
Route::post('/updateDrawOUTLotto','API\FireSystem\Lotto\LottoController@updateDrawOUTLotto');

// ALFI //
Route::post('masterapd', 'API\APD\ApiMasterAPDController@index');
Route::group(['middleware' => 'auth.jwt'], function () {

    Route::post('validasi', 'API\ApiController@validasi');
    Route::get('logout', 'API\ApiController@logout');

    Route::get('user', 'API\ApiController@getAuthUser');

    Route::get('accidentreports', 'API\Safety\ApiAccidentReportController@index');
    Route::post('accidentreports/search', 'API\Safety\ApiAccidentReportController@search');
    Route::get('accidentreports/{id}', 'API\Safety\ApiAccidentReportController@show');
    Route::post('accidentreports', 'API\Safety\ApiAccidentReportController@store');
    Route::post('accidentreports/update', 'API\Safety\ApiAccidentReportController@update');
    Route::delete('accidentreports/{id}', 'API\Safety\ApiAccidentReportController@destroy');

    Route::get('recapitulation', 'API\Safety\ApiRecapitulationController@index');
    Route::post('recapitulation/search', 'API\Safety\ApiRecapitulationController@search');
    Route::get('recapitulation/{id}', 'API\Safety\ApiRecapitulationController@show');
    Route::get('recapitulationchart', 'API\Safety\ApiRecapitulationController@chart');

    Route::post('tools', 'API\Safety\ApiToolsController@index');
    Route::post('tools/search', 'API\Safety\ApiToolsController@search');
    Route::get('tools/{id}', 'API\Safety\ApiToolsController@show');
    Route::post('tools', 'API\Safety\ApiToolsController@store');
    Route::post('tools/update', 'API\Safety\ApiToolsController@update');
    Route::delete('tools/{id}', 'API\Safety\ApiToolsController@destroy');

    Route::get('sio', 'API\Safety\ApiSioController@index');
    Route::post('sio/search', 'API\Safety\ApiSioController@search');
    Route::get('sio/{id}', 'API\Safety\ApiSioController@show');
    Route::post('sio', 'API\Safety\ApiSioController@store');
    Route::post('sio/update', 'API\Safety\ApiSioController@update');
    Route::delete('sio/{id}', 'API\Safety\ApiSioController@destroy');

    Route::get('unsafedetail', 'API\Safety\ApiDailyReportController@index');
    Route::post('unsafedetail/search', 'API\Safety\ApiDailyReportController@search');
    Route::get('unsafedetail/{id}', 'API\Safety\ApiDailyReportController@show');
    Route::post('unsafedetail', 'API\Safety\ApiDailyReportController@store');
    Route::post('unsafedetail/update', 'API\Safety\ApiDailyReportController@update');
    Route::delete('unsafedetail/{id}', 'API\Safety\ApiDailyReportController@destroy');

    Route::get('unsafek3header', 'API\Safety\ApiDailyReportK3Controller@indexheader');
    Route::post('unsafek3header/search', 'API\Safety\ApiDailyReportK3Controller@searchheader');
    Route::get('unsafek3header/{id}', 'API\Safety\ApiDailyReportK3Controller@showheader');
    Route::post('unsafek3header', 'API\Safety\ApiDailyReportK3Controller@storeheader');
    Route::post('unsafek3header/update', 'API\Safety\ApiDailyReportK3Controller@updateheader');
    Route::delete('unsafek3header/{id}', 'API\Safety\ApiDailyReportK3Controller@destroyheader');
    // Route::get('unsafek3detail', 'API\Safety\ApiDailyReportController@indexdetail');
    // Route::get('unsafek3detail/{id}', 'API\Safety\ApiDailyReportController@showdetail');
    Route::post('unsafek3detail', 'API\Safety\ApiDailyReportK3Controller@storedetail');
    Route::post('unsafek3detail/update', 'API\Safety\ApiDailyReportK3Controller@updatedetail');
    Route::delete('unsafek3detail/{id}', 'API\Safety\ApiDailyReportK3Controller@destroydetail');



    // RIZA //
    Route::post('pegawai', 'API\GlobalList\EmployeeController@getEmployee');
    Route::post('plant', 'API\GlobalList\PlantController@getPlant');
    Route::post('unitkerja', 'API\GlobalList\UnitKerjaController@getUnitKerja');

    Route::post('masterapd/add', 'API\APD\ApiMasterAPDController@addMaster');
    Route::post('masterapd/update', 'API\APD\ApiMasterAPDController@updateMaster');
    Route::post('apdplant/{code_plant}', 'API\APD\ApiMasterAPDController@getApdPlant');

    Route::post('masterapd/minusMaster', 'API\APD\ApiOrderAPDController@minusMaster');

    Route::post('orderapd', 'API\APD\ApiOrderAPDController@index');
    Route::post('orderapd/wishlist/create', 'API\APD\ApiOrderAPDController@wishlist');
    Route::post('orderapd/wishlist/get', 'API\APD\ApiOrderAPDController@showWishlist');
    Route::post('orderapd/wishlist/delete', 'API\APD\ApiOrderAPDController@deleteWishlist');
    Route::post('orderapd/wishlist/cancel', 'API\APD\ApiOrderAPDController@cancelWishlist');

    Route::post('orderapd/personal', 'API\APD\ApiOrderAPDController@personal');
    Route::post('orderapd/unitkerja', 'API\APD\ApiOrderAPDController@unitkerja');
    Route::post('orderapd/getid', 'API\APD\ApiOrderAPDController@getDataId');
    Route::post('orderapd/create/personal', 'API\APD\ApiOrderAPDController@addOrderPersonal');
    Route::post('orderapd/create/unitkerja', 'API\APD\ApiOrderAPDController@addOrderUnitKerja');
    Route::post('orderapd/approve', 'API\APD\ApiApproveController@approveOrder');
    Route::post('orderapd/reject', 'API\APD\ApiApproveController@rejectorder');

    Route::post('detailorder', 'API\APD\ApiDetailOrderController@index');
    Route::post('detailorder_create/{kode_order}/{kode_apd}', 'API\APD\ApiDetailOrderController@addDetail');
    Route::post('detailorder_update', 'API\APD\ApiDetailOrderController@updateDetail');
    Route::post('detailorder/getid', 'API\APD\ApiDetailOrderController@getDetailId');

    Route::post('pinjamapd', 'API\APD\ApiPinjamAPDController@index');
    Route::post('pinjamapd/getid', 'API\APD\ApiPinjamAPDController@getDataId');
    Route::post('orderapd/create/pinjam', 'API\APD\ApiPinjamAPDController@addpinjam');
    Route::post('pinjamapd/approve', 'API\APD\ApiApprovePinjamController@approvePinjam');
    Route::post('pinjamapd/reject', 'API\APD\ApiApprovePinjamController@rejectPinjam');

    Route::Post('detailpinjam', 'API\APD\ApiDetailPinjamController@index');
    Route::Post('detailpinjam/getid', 'API\APD\ApiDetailPinjamController@getDetailId');
    Route::post('detailpinjam_create/{kode_pinjam}/{kode_apd}', 'API\APD\ApiDetailPinjamController@addDetailPinjam');

    Route::Post('history', 'API\APD\ApiHistoryController@index');

    Route::Post('approvelist', 'API\APD\ApiApproveController@index');
    Route::Post('approvelist/personal', 'API\APD\ApiApproveController@listPersonal');
    Route::Post('approvelist/unitkerja', 'API\APD\ApiApproveController@listUnit');
    Route::Post('approvelist/pinjam', 'API\APD\ApiApprovePinjamController@index');
    // Route::Post('approve', 'API\APD\ApiApproveController@approve');
    Route::Post('approvelist_second', 'API\APD\ApiApproveController@index2');
    Route::Post('approvelist_second/personal', 'API\APD\ApiApproveController@listPersonal2');
    Route::Post('approvelist_second/unitkerja', 'API\APD\ApiApproveController@listUnit2');
    Route::Post('approvelist_second/pinjam', 'API\APD\ApiApprovePinjamController@index2');
    Route::Post('approve_second', 'API\APD\ApiApprove2Controller@approve2');

    Route::Post('release/personal', 'API\APD\ApiReleaseController@listPersonal');
    Route::Post('release/unitkerja', 'API\APD\ApiReleaseController@listUnitKerja');
    Route::Post('release/pinjam', 'API\APD\ApiReleaseController@listPinjam');
    Route::Post('release/create', 'API\APD\ApiReleaseController@addRelease');
    Route::Post('release/create/pinjam', 'API\APD\ApiReleaseController@addReleasePinjam');
    Route::Post('releaselist/personal', 'API\APD\ApiRusakHilangController@releasePersonal');
    Route::Post('releaselist/personal/getid', 'API\APD\ApiRusakHilangController@getIdPersonal');
    Route::Post('releaselist/unit', 'API\APD\ApiReleaseController@releaseUnit');
    Route::Post('releaselist/pinjam', 'API\APD\ApiReleaseController@releasePinjam');
    Route::Post('hilang', 'API\APD\ApiRusakHilangController@addHilang');
    Route::Post('rusak', 'API\APD\ApiRusakHilangController@addRusak');

    Route::post('stock/percentage', 'API\APD\ApiMasterAPDController@percentage');
});
