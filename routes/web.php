<?php

use Milon\Barcode\DNS2D as DNS2D;
use Milon\Barcode\DNS1D as DNS1D;

Auth::routes();
Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

// SAFETY //
Route::get('/menuSafety', 'MenuSafetyController@index')->name('menuSafety');
// Route::get('/menuSafety', 'Backend\Safety\MenuSafetySystemController@index')->name('menuSafety');

Route::group(['prefix' => 'dailyreport'], function () {
    Route::get('/', 'DailyReportController@index');
    Route::get('/detail/{id}', 'DailyReportController@show');
    Route::post ( '/addMaster', 'DailyReportController@addMaster' );
    Route::post ( '/saveMaster', 'DailyReportController@saveMaster' );
    Route::post ( '/addFollowUp', 'DailyReportController@addFollowUp' );
    Route::post ( '/saveFollowUp', 'DailyReportController@saveFollowUp' );
    Route::post('/deleteMaster', 'DailyReportController@deleteMaster');
    Route::post('/showImage', 'DailyReportController@showImage');
});

Route::group(['prefix' => 'accidentreport'], function () {
    Route::get('/', 'AccidentReportController@index');
    Route::get('/detail/{id}', 'AccidentReportController@show');
    Route::get('/create', 'AccidentReportController@create');
    Route::post('/store', 'AccidentReportController@store');
    Route::post ( '/addMaster', 'AccidentReportController@addMaster' );
    Route::post ( '/saveMaster', 'AccidentReportController@saveMaster' );
    Route::post ( '/addFollowUp', 'AccidentReportController@addFollowUp' );
    Route::post ( '/saveFollowUp', 'AccidentReportController@saveFollowUp' );
    Route::post('/deleteMaster', 'AccidentReportController@deleteMaster');
    Route::post('/showImage', 'AccidentReportController@showImage');
    Route::get('/chart', 'AccidentReportController@chart');
    Route::get('/chart2', 'AccidentReportController@chart2');
});

Route::group(['prefix' => 'recapitulation'], function () {
    Route::get('/', 'RecapitulationController@index');
    Route::get('/chart', 'RecapitulationController@chart');
    Route::get('/persentage', 'RecapitulationController@persentage');
    Route::get('/persentagePlant', 'RecapitulationController@persentagePlant');
    Route::post('/show', 'RecapitulationController@show');
    Route::get('/pdfview', 'RecapitulationController@pdfview');
});

Route::group(['prefix' => 'tools'], function () {
    Route::get('/', 'ToolsController@index');
    Route::get('/detail/{id}', 'ToolsController@show');
    Route::get('/create', 'ToolsController@create');
    Route::post('/store', 'ToolsController@store');
    Route::post ( '/addMaster', 'ToolsController@addMaster' );
    Route::post ( '/saveMaster', 'ToolsController@saveMaster' );
    Route::post('/deleteMaster', 'ToolsController@deleteMaster');
    Route::post('/showImage', 'ToolsController@showImage');
});

Route::group(['prefix' => 'sio'], function () {
    Route::get('/', 'SioController@index');
    Route::get('/detail/{id}', 'SioController@show');
    Route::get('/group', 'SioController@indexgroup');
    // Route::get('/create', 'SioController@create');
    // Route::post('/store', 'SioController@store');
    Route::post ( '/addMaster', 'SioController@addMaster' );
    Route::post ( '/saveMaster', 'SioController@saveMaster' );
    Route::post('/deleteMaster', 'SioController@deleteMaster');
    Route::post('/showImage', 'SioController@showImage');
});

Route::group(['prefix' => 'dailyreportk3'], function () {
    Route::get('/', 'DailyK3Controller@index');
    Route::get('/detail/{id}', 'DailyK3Controller@show');
    Route::post ( '/addMaster', 'DailyK3Controller@addMaster' );
    Route::post ( '/saveMaster', 'DailyK3Controller@saveMaster' );
    Route::post('/deleteMaster', 'DailyK3Controller@deleteMaster');
    Route::post ( '/addFollowUp', 'DailyK3Controller@addFollowUp' );
    Route::post ( '/saveFollowUp', 'DailyK3Controller@saveFollowUp' );
    Route::post('/showImage', 'DailyK3Controller@showImage');
    Route::post ( '/addMasterDetail', 'DailyK3Controller@addMasterDetail' );
    Route::post ( '/saveMasterDetail', 'DailyK3Controller@saveMasterDetail' );
    Route::post('/deleteMasterDetail', 'DailyK3Controller@deleteMasterDetail');
    // Route::post('/save', 'AccidentReportController@save');
    // Route::post('/delete', 'AccidentReportController@delete');
});


Route::group(['prefix' => 'users'], function () {
    Route::get('/', 'UserController@index');
    Route::post('/add', 'UserController@form');
    Route::post('/save', 'UserController@save');
    Route::post('/delete', 'UserController@delete');
});

Route::group(['prefix' => 'pplant'], function () {
    Route::get ( '/', 'Backend\DataMaster\PlanningPlantController@readItems' );
    Route::post ( '/addMaster', 'Backend\DataMaster\PlanningPlantController@addMaster' );
    Route::post ( '/saveMaster', 'Backend\DataMaster\PlanningPlantController@saveMaster' );
    Route::post('/editItem', 'Backend\DataMaster\PlanningPlantController@editItem');
    Route::post('/deleteMaster', 'Backend\DataMaster\PlanningPlantController@deleteMaster');
});

Route::group(['prefix' => 'funclocation'], function () {
    Route::get('/', 'Backend\DataMaster\FuncLocationController@readItems');
    Route::post ( '/addMaster', 'Backend\DataMaster\FuncLocationController@addMaster' );
    Route::post ( '/saveMaster', 'Backend\DataMaster\FuncLocationController@saveMaster' );
    Route::post('/editItem', 'Backend\DataMaster\FuncLocationController@editItem');
    Route::post('/deleteMaster', 'Backend\DataMaster\FuncLocationController@deleteMaster');
});

// Route::group(['prefix' => 'inspecarea'], function () {
//     Route::get('/', 'InspectionAreaController@readItems');
//     Route::post ( '/addMaster', 'InspectionAreaController@addMaster' );
//     Route::post ( '/saveMaster', 'InspectionAreaController@saveMaster' );
//     Route::post('/editItem', 'InspectionAreaController@editItem');
//     Route::post('/deleteMaster', 'InspectionAreaController@deleteMaster');
// });

Route::group(['prefix' => 'pegawai'], function () {
    Route::get('/', 'PegawaiController@index');
    Route::get('/create', 'PegawaiController@create');
    Route::post('/store', 'PegawaiController@store');
    // Route::post('/save', 'AccidentReportController@save');
    // Route::post('/delete', 'AccidentReportController@delete');
});

// Route::group(['prefix' => 'unitkerja'], function () {
//     Route::get('/', 'UnitKerjaController@readItems');
//     Route::post ( '/addMaster', 'UnitKerjaController@addMaster' );
//     Route::post ( '/saveMaster', 'UnitKerjaController@saveMaster' );
//     Route::post('/editItem', 'UnitKerjaController@editItem');
//     Route::post('/deleteMaster', 'UnitKerjaController@deleteMaster');
// });

Route::group(['prefix' => 'datavendor'], function () {
    Route::get('/', 'Backend\DataMaster\DataVendorController@readItems');
    Route::post ( '/addMaster', 'Backend\DataMaster\DataVendorController@addMaster' );
    Route::post ( '/saveMaster', 'Backend\DataMaster\DataVendorController@saveMaster' );
    Route::post('/editItem', 'Backend\DataMaster\DataVendorController@editItem');
    Route::post('/deleteMaster', 'Backend\DataMaster\DataVendorController@deleteMaster');
});

Route::group(['prefix' => 'reportcategory'], function () {
    Route::get('/', 'Backend\DataMaster\ReportCategoryController@readItems');
    Route::post ( '/addMaster', 'Backend\DataMaster\ReportCategoryController@addMaster' );
    Route::post ( '/saveMaster', 'Backend\DataMaster\ReportCategoryController@saveMaster' );
    Route::post('/editItem', 'Backend\DataMaster\ReportCategoryController@editItem');
    Route::post('/deleteMaster', 'Backend\DataMaster\ReportCategoryController@deleteMaster');
});

Route::group(['prefix' => 'siogroup'], function () {
    Route::get ( '/', 'Backend\DataMaster\SioGroupController@readItems' );
    Route::post ( '/addMaster', 'Backend\DataMaster\SioGroupController@addMaster' );
    Route::post ( '/saveMaster', 'Backend\DataMaster\SioGroupController@saveMaster' );
    Route::post('/editItem', 'Backend\DataMaster\SioGroupController@editItem');
    Route::post('/deleteMaster', 'Backend\DataMaster\SioGroupController@deleteMaster');
});


// MASTER DATA //
Route::group(['prefix' => 'employee'], function () {
    Route::get('/', 'Backend\DataMaster\EmployeeController@index');
    Route::post('/add', 'Backend\DataMaster\EmployeeController@form');
    Route::post('/save', 'Backend\DataMaster\EmployeeController@save');
    Route::get('/show', 'Backend\DataMaster\EmployeeController@show');
    Route::post('/delete', 'Backend\DataMaster\EmployeeController@delete');
});

Route::group(['prefix' => 'plant'], function () {
    Route::get('/', 'Backend\DataMaster\PlantController@index');
    Route::post('/add', 'Backend\DataMaster\PlantController@form');
    Route::post('/save', 'Backend\DataMaster\PlantController@save');
    Route::post('/delete', 'Backend\DataMaster\PlantController@delete');
});

Route::group(['prefix' => 'unitKerja'], function () {
    Route::get('/', 'Backend\DataMaster\UnitKerjaController@index');
    Route::post('/add', 'Backend\DataMaster\UnitKerjaController@form');
    Route::post('/save', 'Backend\DataMaster\UnitKerjaController@save');
    Route::post('/delete', 'Backend\DataMaster\UnitKerjaController@delete');
});

Route::group(['prefix' => 'inspectionArea'], function () {
    Route::get('/', 'Backend\DataMaster\InspectionAreaController@index');
    Route::post('/add', 'Backend\DataMaster\InspectionAreaController@form');
    Route::post('/save', 'Backend\DataMaster\InspectionAreaController@save');
    Route::post('/delete', 'Backend\DataMaster\InspectionAreaController@delete');
});

Route::group(['prefix' => 'news'], function () {
    Route::get('/', 'Backend\DataMaster\NewsController@index');
    Route::post('/add', 'Backend\DataMaster\NewsController@form');
    Route::post('/save', 'Backend\DataMaster\NewsController@save');
    Route::post('/delete', 'Backend\DataMaster\NewsController@delete');
});
// END MASTER DATA //



// FIRE SYSTEM //
  // -- MENU -- //
Route::get('/menuFireSystem', 'Backend\FireSystem\MenuFireSystemController@index')->name('menuFireSystem');
Route::get('/menuInspection', 'Backend\FireSystem\MenuInspectionController@index')->name('menuInspection');
Route::get('/menuDailySection', 'Backend\FireSystem\MenuDailySectionController@index')->name('menuDailySection');

  // -- DAILY REPORT SECTION -- //
Route::group(['prefix' => 'dailySectionActivity'], function () {
    Route::get('/', 'Backend\FireSystem\DailySectionActivityController@index');
    Route::post('/addOfficer', 'Backend\FireSystem\DailySectionActivityController@formOfficer');
    Route::post('/saveOfficer', 'Backend\FireSystem\DailySectionActivityController@saveOfficer');
    Route::post('/deleteOfficer', 'Backend\FireSystem\DailySectionActivityController@deleteOfficer');

    Route::post('/addActivity', 'Backend\FireSystem\DailySectionActivityController@formActivity');
    Route::post('/saveActivity', 'Backend\FireSystem\DailySectionActivityController@saveActivity');
    Route::post('/showActivity', 'Backend\FireSystem\DailySectionActivityController@showActivity');
    Route::post('/deleteActivity', 'Backend\FireSystem\DailySectionActivityController@deleteActivity');
});

  // -- INSPECTION REPORT GASCO2 -- //
Route::group(['prefix' => 'reportGasCO2'], function () {
    Route::get('/', 'Backend\FireSystem\GasCO2Controller@index');
    Route::post('/addGasCO2', 'Backend\FireSystem\GasCO2Controller@formGasCO2');
    Route::post('/saveGasCO2', 'Backend\FireSystem\GasCO2Controller@saveGasCO2');
    Route::post('/deleteGasCO2', 'Backend\FireSystem\GasCO2Controller@deleteGasCO2');

    Route::post('/addInspectionGasCO2', 'Backend\FireSystem\GasCO2Controller@formInspectionGasCO2');
    Route::post('/saveInspectionGasCO2', 'Backend\FireSystem\GasCO2Controller@saveInspectionGasCO2');
    Route::post('/showInspectionGasCO2', 'Backend\FireSystem\GasCO2Controller@showInspectionGasCO2');
    Route::post('/deleteInspectionGasCO2', 'Backend\FireSystem\GasCO2Controller@deleteInspectionGasCO2');

});

  // -- INSPECTION REPORT ALARM -- //
Route::group(['prefix' => 'inspectionAlarm'], function () {
    Route::get('/', 'Backend\FireSystem\InspectionAlarmController@index');
    Route::post('/addMaster', 'Backend\FireSystem\InspectionAlarmController@formMaster');
    Route::post('/saveMaster', 'Backend\FireSystem\InspectionAlarmController@saveMaster');
    Route::post('/deleteMaster', 'Backend\FireSystem\InspectionAlarmController@deleteMaster');

    Route::post('/addParameter', 'Backend\FireSystem\InspectionAlarmController@formParameter');
    Route::post('/saveParameter', 'Backend\FireSystem\InspectionAlarmController@saveParameter');
    Route::post('/deleteParameter', 'Backend\FireSystem\InspectionAlarmController@deleteParameter');

    Route::post('/addInspectionAlarm', 'Backend\FireSystem\InspectionAlarmController@formInspectionAlarm');
    Route::post('/saveInspectionAlarm', 'Backend\FireSystem\InspectionAlarmController@saveInspectionAlarm');
    Route::post('/showInspectionAlarm', 'Backend\FireSystem\InspectionAlarmController@showInspectionAlarm');
    Route::post('/deleteInspectionAlarm', 'Backend\FireSystem\InspectionAlarmController@deleteInspectionAlarm');
});

  // -- INSPECTION REPORT APAR-- //
Route::group(['prefix' => 'inspectionApar'], function () {
    Route::get('/', 'Backend\FireSystem\InspectionAparController@index');
    Route::post('/addMaster', 'Backend\FireSystem\InspectionAparController@formMaster');
    Route::post('/saveMaster', 'Backend\FireSystem\InspectionAparController@saveMaster');
    Route::post('/deleteMaster', 'Backend\FireSystem\InspectionAparController@deleteMaster');
    Route::get('/historyInspectionApar', 'Backend\FireSystem\InspectionAparController@historyInspectionApar');
    Route::post('/addInspectionApar', 'Backend\FireSystem\InspectionAparController@formInspectionApar');
    Route::post('/saveInspectionApar', 'Backend\FireSystem\InspectionAparController@saveInspectionApar');
    Route::post('/deleteHistory', 'Backend\FireSystem\InspectionAparController@deleteHistory');
    Route::post('/updateInspectionApar', 'Backend\FireSystem\InspectionAparController@updateInspectionApar');
    Route::post('/saveUpdateInspectionApar', 'Backend\FireSystem\InspectionAparController@saveUpdateInspectionApar');
});

  // -- INSPECTION REPORT HYDRANT-- //
Route::group(['prefix' => 'inspectionHydrant'], function () {
    Route::get('/', 'Backend\FireSystem\InspectionHydrantController@index');
    Route::post('/addMaster', 'Backend\FireSystem\InspectionHydrantController@formMaster');
    Route::post('/saveMaster', 'Backend\FireSystem\InspectionHydrantController@saveMaster');
    Route::post('/deleteMaster', 'Backend\FireSystem\InspectionHydrantController@deleteMaster');
    Route::get('/historyInspectionHydrant', 'Backend\FireSystem\InspectionHydrantController@historyInspectionHydrant');
    Route::get('/addInspectionHydrant', 'Backend\FireSystem\InspectionHydrantController@formInspectionHydrant');
    Route::post('/saveInspectionHydrant', 'Backend\FireSystem\InspectionHydrantController@saveInspectionHydrant');
    Route::post('/deleteHistory', 'Backend\FireSystem\InspectionHydrantController@deleteHistory');
    Route::get('/updateInspectionHydrant', 'Backend\FireSystem\InspectionHydrantController@updateInspectionHydrant');
    Route::post('/saveUpdateInspectionHydrant', 'Backend\FireSystem\InspectionHydrantController@saveUpdateInspectionHydrant');

});

  // -- FIRE REPORT -- //
Route::group(['prefix' => 'fireReport'], function () {
    Route::get('/', 'Backend\FireSystem\FireReportController@index');
    Route::post('/add', 'Backend\FireSystem\FireReportController@form');
    Route::post('/save', 'Backend\FireSystem\FireReportController@save');
    Route::post('/delete', 'Backend\FireSystem\FireReportController@delete');
    Route::post('/showImage', 'Backend\FireSystem\FireReportController@showImage');
    Route::post('/uploadImage', 'Backend\FireSystem\FireReportController@uploadImage');
    Route::post('/saveImage', 'Backend\FireSystem\FireReportController@saveImage');
    Route::get('/getPDF', 'Backend\FireSystem\FireReportController@getPDF');
});

// -- UNIT CHECK DASHBOARD -- //
Route::group(['prefix' => 'unitCheckDashboard'], function () {
  Route::get('/', 'Backend\FireSystem\UnitCheckDashboardController@index');
  Route::post('/add', 'Backend\FireSystem\UnitCheckDashboardController@form');
  Route::post('/save', 'Backend\FireSystem\UnitCheckDashboardController@save');

  Route::post('/getDate', 'Backend\FireSystem\UnitCheckDashboardController@getDate');

  Route::get('/masterVehicle', 'Backend\FireSystem\UnitCheckDashboardController@masterVehicle');
  Route::get('/performaVehicle', 'Backend\FireSystem\UnitCheckDashboardController@performaVehicle');
});


  // -- UNIT CHECK -- //
Route::group(['prefix' => 'unitCheck'], function () {
    Route::get('/', 'Backend\FireSystem\UnitCheckController@index');
    Route::post('/addMaster', 'Backend\FireSystem\UnitCheckController@formMaster');
    Route::post('/saveMaster', 'Backend\FireSystem\UnitCheckController@saveMaster');
    Route::post('/deleteMaster', 'Backend\FireSystem\UnitCheckController@deleteMaster');

    Route::post('/addInspectionPickup', 'Backend\FireSystem\UnitCheckController@formInspectionPickup');
    Route::post('/saveInspectionPickup', 'Backend\FireSystem\UnitCheckController@saveInspectionPickup');
    Route::post('/deleteInspectionPickup', 'Backend\FireSystem\UnitCheckController@deleteInspectionPickup');
    Route::get('/historyInspectionPickup', 'Backend\FireSystem\UnitCheckController@historyInspectionPickup');

    Route::post('/addInspectionPMK', 'Backend\FireSystem\UnitCheckController@formInspectionPMK');
    Route::post('/saveInspectionPMK', 'Backend\FireSystem\UnitCheckController@saveInspectionPMK');
    Route::post('/deleteInspectionPMK', 'Backend\FireSystem\UnitCheckController@deleteInspectionPMK');
    Route::get('/historyInspectionPMK', 'Backend\FireSystem\UnitCheckController@historyInspectionPMK');
});

// -- LOTTO -- //
Route::group(['prefix' => 'lotto'], function () {
  Route::get('/', 'Backend\FireSystem\LottoController@index');
  Route::post('/add', 'Backend\FireSystem\LottoController@form');
  Route::post('/save', 'Backend\FireSystem\LottoController@save');
  Route::post('/delete', 'Backend\FireSystem\LottoController@delete');
  Route::post('/draw', 'Backend\FireSystem\LottoController@draw');
  Route::post('/saveDraw', 'Backend\FireSystem\LottoController@saveDraw');
  Route::post('/employe', 'Backend\FireSystem\LottoController@employe');
});

// END FIRE SYSTEM //

// APD //
// -- MENU -- //
Route::get('/menuAPD', 'Backend\APD\MenuAPDController@index')->name('menuAPD');
Route::get('/menuOrderAPD', 'Backend\APD\MenuOrderAPDController@index')->name('menuOrderAPD');

// -- ORDER APD PEERSONAL -- //
Route::group(['prefix' => 'orderAPD'], function () {
                Route::get('/', 'Backend\APD\OrderAPDController@index');
                Route::post('/addOrderPersonal', 'Backend\APD\OrderAPDController@formOrderPersonal');
                Route::get('/jumlahOrder', 'Backend\APD\OrderAPDController@jumlahOrder');
                Route::get('/jumlahOrderView', 'Backend\APD\OrderAPDController@jumlahOrderView');
                // Route::post('/saveOfficer', 'Backend\FireSystem\DailySectionActivityController@saveOfficer');
                // Route::post('/deleteOfficer', 'Backend\FireSystem\DailySectionActivityController@deleteOfficer');
                
                // Route::post('/addActivity', 'Backend\FireSystem\DailySectionActivityController@formActivity');
                // Route::post('/saveActivity', 'Backend\FireSystem\DailySectionActivityController@saveActivity');
                // Route::post('/showActivity', 'Backend\FireSystem\DailySectionActivityController@showActivity');
                // Route::post('/deleteActivity', 'Backend\FireSystem\DailySectionActivityController@deleteActivity');
                });

// -- STOCK APD -- //
Route::group(['prefix' => 'stockAPD'], function () {
                Route::get('/', 'Backend\APD\StockAPDController@index');
                Route::post('/addMaster', 'Backend\APD\StockAPDController@formMaster');
                Route::post('/saveMaster', 'Backend\APD\StockAPDController@saveMaster');
                Route::post('/editMaster', 'Backend\APD\StockAPDController@formMaster');
                Route::post('/deleteMaster', 'Backend\APD\StockAPDController@deleteMaster');
                Route::get('/percentage', 'Backend\APD\StockAPDController@percentage');
                Route::get('/percentageView', 'Backend\APD\StockAPDController@percentageView');
                Route::get('/percentageTabel', 'Backend\APD\StockAPDController@percentageTabel');
                });

// -- RELEASE APD -- //
Route::group(['prefix' => 'releaseAPD'], function () {
                Route::get('/', 'Backend\APD\ReleaseAPDController@index');
                Route::post('/showOrder', 'Backend\APD\ReleaseAPDController@showOrder');
                Route::post('/showPinjam', 'Backend\APD\ReleaseAPDController@showPinjam');
                Route::post('/release', 'Backend\APD\ReleaseAPDController@release');
                Route::post('/releasePinjam', 'Backend\APD\ReleaseAPDController@releasePinjam');
                });

// -- RETURN APD -- //
Route::group(['prefix' => 'returnAPD'], function () {
                Route::get('/', 'Backend\APD\ReturnAPDController@index');
                Route::post('/showPinjam', 'Backend\APD\ReturnAPDController@showPinjam');
                Route::post('/return', 'Backend\APD\ReturnAPDController@return');
                });

// -- PENGADUAN APD -- //
Route::group(['prefix' => 'pengaduanAPD'], function () {
    Route::get('/', 'Backend\APD\OrderAPDController@pengaduan');
    Route::post('/showPinjam', 'Backend\APD\ReturnAPDController@showPinjam');
    Route::post('/return', 'Backend\APD\ReturnAPDController@return');
    });
